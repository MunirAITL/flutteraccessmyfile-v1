Store Pass
a98mca9ry%%ah@ma*!!#!oO0o!f**0-O

Alias
AccessmYfile

smhafiz@aitl.net
amf_v1.0_230322.apk
=================================================================================
create keystore file
================================================================================================================
keytool -genkey -v -keystore amf.keystore -alias AccessmYfile -keyalg RSA -keysize 2048 -validity 10000

create jks file
=================================================================================================================

keytool -genkey -v -keystore amf.jks -storetype JKS -keyalg RSA -keysize 2048 -validity 10000 -alias AccessmYfile

The JKS keystore uses a proprietary format. It is recommended to migrate to PKCS12 which is an industry standard format using:
=================================================================================================================
keytool -importkeystore -srckeystore amf.jks -destkeystore shohokari.jks -deststoretype pkcs12

generate SSH1 Key
=================================================================================================================
keytool -keystore amf.jks -list -v

SHA1: 56:60:99:69:0D:E7:C3:8D:79:A5:49:90:FA:96:AC:1D:48:A5:67:41

======================================
FACEBOOK AUTH KEY:
keytool -exportcert -alias Mortgage_Magic -keystore mm.keystore | openssl sha1 -binary | openssl base64

full path:
F:\jdk\bin\keytool -exportcert -alias Mortgage_Magic -keystore E:\flutter_app\aitl\ht\herotaskerflutterappver3\HeroTaskerApp\android\doc\mm.keystore | openssl sha1 -binary | openssl base64

z9VPDwuPPKlmYIn8AC/c74qagxs=

========================================
For getting keystore file info
keytool -v -list -keystore mm.keystore
keystore file jks file password = ?
