import UIKit
import Flutter
import GoogleMaps
import Firebase

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
 
  var audioMgr = AudioMgr()
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    
    FirebaseApp.configure()
    GMSServices.provideAPIKey("AIzaSyAr8ofzAsQgU85voEnr5NiUDWFSzMKF8iU")
    
    GeneratedPluginRegistrant.register(with: self)
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
    }
    
    //  https://flutter.dev/docs/development/platform-integration/platform-channels
    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
    let CHANNEL = FlutterMethodChannel(name: "flutter.native/shohokari",
                                                  binaryMessenger: controller.binaryMessenger)
        CHANNEL.setMethodCallHandler({
          (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
          // Note: this method is invoked on the UI thread.
          switch call.method {
            case "audioActivity":
                // do some work, then:
                // 4. call completion handler
                if let args = call.arguments as? Dictionary<String, Any>,
                    let method = args["method"] as? String {
                    if method == "start" {
                        self.audioMgr.start(call: call, result: result)
                    }
                    else if method == "stop" {
                        self.audioMgr.stop()
                    }
                    
                  } else {
                    result(FlutterError.init(code: "bad args", message: nil, details: nil))
                  }
              default:
                break
          }
            
        })   
      
   
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
