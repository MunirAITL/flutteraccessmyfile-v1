import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/view_model/helper/auth/LoginHelper.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

import '../../../data/model/auth/LoginAPIModel.dart';
import '../../../data/network/NetworkMgr.dart';

class LoginAPIMgr with Mixin {
  static final LoginAPIMgr _shared = LoginAPIMgr._internal();

  factory LoginAPIMgr() {
    return _shared;
  }

  LoginAPIMgr._internal();

  wsLoginAPI({
    BuildContext context,
    String email,
    String pwd,
    Function(LoginAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<LoginAPIModel, Null>(
        context: context,
        url: APIAuthCfg.LOGIN_URL,
        param: LoginHelper().getParam(email: email, pwd: pwd),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
