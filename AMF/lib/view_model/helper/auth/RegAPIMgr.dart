import 'dart:convert';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:json_string/json_string.dart';

import '../../../data/model/auth/RegAPIModel.dart';
import '../../../data/network/NetworkMgr.dart';
import 'RegHelper.dart';

class RegAPIMgr with Mixin {
  static final RegAPIMgr _shared = RegAPIMgr._internal();

  factory RegAPIMgr() {
    return _shared;
  }

  RegAPIMgr._internal();

  wsRegAPI({
    BuildContext context,
    String email,
    String pwd,
    String fname,
    String lname,
    String fullName,
    String mobile,
    String countryCode,
    String dob,
    Function(RegAPIModel) callback,
  }) async {
    try {
      final param = RegHelper().getParam(
        email: email,
        pwd: pwd,
        fname: fname,
        lname: lname,
        fullName: fullName,
        phone: mobile,
        dob: dob,
      );
      final jsonString = JsonString(json.encode(param));
      myLog(jsonString.source);
      await NetworkMgr()
          .req<RegAPIModel, Null>(
        context: context,
        url: APIAuthCfg.REG_URL,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
