import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UIHelper {
  drawLine({Color colr = Colors.grey, double h = .5}) {
    return Container(color: colr, height: h);
  }

  scrollUp(ScrollController scrollController, double h) {
    if (scrollController.hasClients) {
      scrollController.animateTo(
        0.0,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    }
  }

  scrollDown(ScrollController scrollController, double h) {
    if (scrollController.hasClients) {
      scrollController.animateTo(
        scrollController.position.maxScrollExtent + h + 50,
        duration: Duration(seconds: 1),
        curve: Curves.fastOutSlowIn,
      );
    }
  }

  drawAppbarTitle({String title, Color txtColor}) {
    if (txtColor == null) {
      txtColor = Colors.white;
    }
    return FittedBox(
      fit: BoxFit.fitWidth,
      child: AutoSizeText(title,
          style: TextStyle(color: txtColor, fontWeight: FontWeight.bold)),
    );
  }

  drawCircle({BuildContext context, Color color, double size = 3}) {
    double width = MediaQuery.of(context).size.width;
    //double height = MediaQuery.of(context).size.height;
    //var padding = MediaQuery.of(context).padding;
    //double newheight = height - padding.top - padding.bottom;
    return Container(
      width: width * size / 100,
      height: width * size / 100,
      decoration: BoxDecoration(shape: BoxShape.circle, color: color),
    );
  }

  getStarsRow(int rate, Color colr) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = 0; i < 5; i++)
          Icon(
            (i < rate) ? Icons.star : Icons.star_outline,
            color: colr,
            size: 15,
          ),
      ],
    );
  }

  getStarRatingView({
    int rate,
    int reviews,
    String reviewTxt1 = '',
    String reviewTxt2,
    Color starColor,
    Color txtColor = Colors.black,
    MainAxisAlignment align = MainAxisAlignment.center,
    bool isRow = true,
  }) {
    if (starColor == null) starColor = MyTheme.brandColor;
    if (reviewTxt2 == null) reviewTxt2 = " " + "reviews".tr;
    return Container(
      child: (isRow)
          ? Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: align,
              children: [
                getStarsRow(rate, starColor),
                SizedBox(width: 5),
                (reviews != null)
                    ? Flexible(
                        child: Txt(
                            txt: reviewTxt1 + reviews.toString() + reviewTxt2,
                            txtColor: txtColor,
                            txtSize: MyTheme.txtSize - .3,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      )
                    : SizedBox(),
              ],
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: align,
              children: [
                getStarsRow(rate, starColor),
                SizedBox(height: 10),
                (reviews != null)
                    ? Txt(
                        txt: reviewTxt1 + reviews.toString() + reviewTxt2,
                        txtColor: txtColor,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: false)
                    : SizedBox(),
              ],
            ),
    );
  }

  getCompletionText(
      {int pa,
      MainAxisAlignment align = MainAxisAlignment.center,
      Color txtColor = Colors.black,
      Function callbackInfo}) {
    return Center(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: align,
        children: [
          Flexible(
            child: Txt(
                txt: (pa > 0
                    ? (pa.toString() + "% " + "completion_rate".tr)
                    : "completion_rate_none".tr),
                txtColor: txtColor,
                txtSize: MyTheme.txtSize - .3,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          (callbackInfo != null)
              ? IconButton(
                  iconSize: 20,
                  icon: Icon(
                    Icons.info,
                    color: MyTheme.gray5Color.withOpacity(.6),
                  ),
                  onPressed: () {
                    callbackInfo();
                  })
              : SizedBox(),
        ],
      ),
    );
  }

  expandableTxt(String txt, Color txtColor, Color arrowColor) {
    var txt1 = '';
    var txt2 = '';
    if (txt.contains(".")) {
      txt1 = txt.substring(0, txt.indexOf(".")).trim();
      txt2 = txt.substring(txt.indexOf(".") + 1).trim();
    } else {
      final len = (txt.length / 2);
      txt1 = txt.substring(0, len.toInt()).trim();
      txt2 = txt.substring(len.toInt() + 1, txt.length).trim();
    }

    return Container(
      child: ListTileTheme(
        contentPadding: EdgeInsets.all(0),
        iconColor: arrowColor,
        child: Theme(
          data: ThemeData.light()
              .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
          child: ExpansionTile(
            title: Txt(
                txt: txt1 + '...',
                txtColor: txtColor,
                txtSize: MyTheme.txtSize - .3,
                txtAlign: TextAlign.start,
                maxLines: 3,
                isBold: false),
            children: <Widget>[
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Txt(
                      txt: txt2,
                      txtColor: txtColor,
                      txtSize: MyTheme.txtSize - .3,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawAppBarTitle({String title, Function callback}) {
    return Container(
      color: Colors.white,
      child: Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Flexible(
                    child: GestureDetector(
                  onTap: () {
                    callback();
                  },
                  child: Container(
                    width: 100,
                    //color: Colors.black,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.arrow_back_ios,
                            color: MyTheme.backBtnColor, size: 15),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.backBtnColor,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.left,
                            isBold: false)
                      ],
                    ),
                  ),
                )),
                title != null
                    ? Expanded(
                        //flex: 3,
                        child: Txt(
                          txt: title,
                          txtColor: MyTheme.bgColor3,
                          txtSize: MyTheme.txtSize + .5,
                          txtAlign: TextAlign.center,
                          fontWeight: FontWeight.w500,
                          isBold: false,
                        ),
                      )
                    : SizedBox()
              ])),
    );
  }
}
