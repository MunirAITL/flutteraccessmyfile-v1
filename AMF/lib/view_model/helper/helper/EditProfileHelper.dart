import 'dart:convert';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';

class EditProfileHelper {
  //  dropdown title
  DropListModel ddTitle = DropListModel([
    OptionItem(id: 1, title: "Mr"),
    OptionItem(id: 2, title: "Mrs"),
    OptionItem(id: 3, title: "Ms"),
    OptionItem(id: 4, title: "Miss"),
    OptionItem(id: 5, title: "Dr"),
  ]);

  OptionItem optTitle = OptionItem(id: null, title: "Select Title");

  //  dropdown gender
  DropListModel ddGender = DropListModel([
    OptionItem(id: 1, title: "Male"),
    OptionItem(id: 2, title: "Female"),
  ]);

  OptionItem optGender = OptionItem(id: null, title: "Select Gender");

  //  dropdown visa status
  DropListModel ddVisaStatus = DropListModel([
    OptionItem(id: 1, title: "Tier 1"),
    OptionItem(id: 2, title: "Tier 2"),
    OptionItem(id: 3, title: "Indefinite leave to remain"),
    OptionItem(id: 4, title: "Family or spouse"),
    OptionItem(id: 5, title: "EU Settlement Scheme"),
    OptionItem(id: 6, title: "Other visa"),
  ]);

  OptionItem optVisaStatus = OptionItem(id: null, title: "Select Visa Status");

  //  dropdown marital status
  DropListModel ddMaritalStatus = DropListModel([
    OptionItem(id: 1, title: "Married"),
    OptionItem(id: 2, title: "Co-Habiting"),
    OptionItem(id: 3, title: "Civil Partnership"),
    OptionItem(id: 4, title: "Divorced"),
    OptionItem(id: 5, title: "Single"),
    OptionItem(id: 6, title: "Seperated"),
    OptionItem(id: 7, title: "Widowed"),
  ]);

  OptionItem optMaritalStatus = OptionItem(id: null, title: "Marital Status");

  //  dropdown countries
  //  countries birth
  DropListModel ddMaritalCountriesBirth;
  OptionItem optCountriesBirth;
  //  countries residential
  DropListModel ddMaritalCountriesResidential;
  OptionItem optCountriesResidential;
  //  nationalities
  DropListModel ddMaritalNationalities;
  OptionItem optNationalities;

  getCountriesBirth({BuildContext context, String cap}) async {
    List<OptionItem> list = [];
    String data = await DefaultAssetBundle.of(context)
        .loadString("assets/json/countries.json");
    final jsonResult = json.decode(data);
    int i = 1;
    jsonResult.forEach((element) {
      final optItem = OptionItem(id: i++, title: element['name'].toString());
      list.add(optItem);
    });

    //  birth
    optCountriesBirth =
        OptionItem(id: null, title: (cap == '') ? "Country of Birth" : cap);
    ddMaritalCountriesBirth = DropListModel(list);
  }

  getCountriesResidential({BuildContext context, String cap}) async {
    List<OptionItem> list = [];
    String data = await DefaultAssetBundle.of(context)
        .loadString("assets/json/countries.json");
    final jsonResult = json.decode(data);
    int i = 1;
    jsonResult.forEach((element) {
      final optItem = OptionItem(id: i++, title: element['name'].toString());
      list.add(optItem);
    });
    //  residential
    optCountriesResidential =
        OptionItem(id: null, title: (cap == '') ? "Country of Residence" : cap);
    ddMaritalCountriesResidential = DropListModel(list);
  }

  getCountriesNationaity({BuildContext context, String cap}) async {
    List<OptionItem> list = [];
    String data = await DefaultAssetBundle.of(context)
        .loadString("assets/json/countries.json");
    final jsonResult = json.decode(data);
    int i = 1;
    jsonResult.forEach((element) {
      final optItem = OptionItem(id: i++, title: element['name'].toString());
      list.add(optItem);
    });
    optNationalities =
        OptionItem(id: null, title: (cap == '') ? "Nationality" : cap);
    ddMaritalNationalities = DropListModel(list);
  }

  //  *****************************

  getParam({
    String firstName,
    String lastName,
    String name,
    String email,
    String userName,
    String profileImageId,
    String coverImageId,
    String referenceId,
    String referenceType,
    String stanfordWorkplaceURL,
    String remarks,
    String cohort,
    String communityId,
    bool isFirstLogin,
    String mobileNumber,
    String dateofBirth,
    String middleName,
    String namePrefix,
    String areYouASmoker,
    String countryCode,
    String addressLine1,
    String addressLine2,
    String addressLine3,
    String town,
    String county,
    String postcode,
    String telNumber,
    String nationalInsuranceNumber,
    String nationality,
    String countryofBirth,
    String countryofResidency,
    String passportNumber,
    String maritalStatus,
    String occupantType,
    String livingDate,
    String password,
    int userCompanyId,
    String visaExpiryDate,
    String passportExpiryDate,
    String visaName,
    String otherVisaName,
    int id,
  }) {
    return {
      "IsMobileNumberVerified": true,
      "FirstName": firstName,
      "LastName": lastName,
      "Name": name,
      "Email": email,
      "UserName": userName,
      "ProfileImageId": profileImageId,
      "CoverImageId": coverImageId,
      "ReferenceId": referenceId,
      "ReferenceType": referenceType,
      "StanfordWorkplaceURL": stanfordWorkplaceURL,
      "Remarks": remarks,
      "Cohort": cohort,
      "CommunityId": communityId,
      "IsFirstLogin": isFirstLogin,
      "MobileNumber": mobileNumber,
      "DateofBirth": dateofBirth,
      "MiddleName": middleName,
      "NamePrefix": namePrefix,
      "AreYouASmoker": areYouASmoker,
      "CountryCode": countryCode,
      "AddressLine1": addressLine1,
      "AddressLine2": addressLine2,
      "AddressLine3": addressLine3,
      "Town": town,
      "County": county,
      "Postcode": postcode,
      "TelNumber": telNumber,
      "NationalInsuranceNumber": nationalInsuranceNumber,
      "Nationality": nationality,
      "CountryofBirth": countryofBirth,
      "CountryofResidency": countryofResidency,
      "PassportNumber": passportNumber,
      "MaritalStatus": maritalStatus,
      "OccupantType": occupantType,
      "LivingDate": livingDate,
      //"Password": password,
      "UserCompanyId": userCompanyId,
      "VisaExpiryDate": visaExpiryDate,
      "PassportExpiryDate": passportExpiryDate,
      "VisaName": visaName,
      "OtherVisaName": otherVisaName,
      "Id": id,
    };
  }
}
