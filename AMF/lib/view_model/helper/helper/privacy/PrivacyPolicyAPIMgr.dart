import 'dart:developer';
import 'package:aitl/config/server/APICreditReportCfg.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/help/PrivacyPolicyAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/material.dart';

class PrivacyPolicyAPIMgr {
  static final PrivacyPolicyAPIMgr _shared = PrivacyPolicyAPIMgr._internal();

  factory PrivacyPolicyAPIMgr() {
    return _shared;
  }

  PrivacyPolicyAPIMgr._internal();

  wsOnLoad({
    BuildContext context,
    Function(PrivacyPolicyAPIModel) callback,
  }) async {
    try {
      final url = PrivacyPolicyHelper().getUrl();
      log("PrivacyPolicyHelper= " + url);
      await NetworkMgr()
          .req<PrivacyPolicyAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}

class PrivacyPolicyHelper {
  getUrl() {
    var url = ServerUrls.TERMS_POLICY;
    url = url.replaceAll(
        "#UserCompanyId#", userData.userModel.userCompanyID.toString());
    return url;
  }
}
