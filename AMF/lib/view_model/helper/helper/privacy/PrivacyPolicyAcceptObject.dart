import 'package:aitl/data/app_data/UserData.dart';

class PrivacyPolicyAcceptObject {
  getParam({String refrenceUrl}) {
    return {
      "Title": "CustomerPrivacy",
      "Type": "CustomerPrivacy",
      "Description": "",
      "ReferenceId": "",
      "ReferenceType": "CustomerPrivacy",
      "RefrenceNumber": "",
      "RefrenceUrl": refrenceUrl,
      "Refrence": "",
      "AccountNumber": "",
      "AccountName": "",
      "SwiftCode": "",
      "IsVerified": true,
      "VerificationCode": "",
      "UserCompanyId": userData.userModel.userCompanyID,
      "UserId": userData.userModel.id,
    };
  }
}
