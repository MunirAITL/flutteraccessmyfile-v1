/*import 'dart:convert';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/cfg/StripeCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/mixin.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:stripe_payment/stripe_payment.dart';
//  https://dashboard.stripe.com/test/dashboard
//  Stripe Password = Hndxfg123456
import 'package:http/http.dart' as http;
//import 'package:flutter_credit_card/flutter_credit_card.dart';

class StripeMgr with Mixin {
  initStripe() {
    StripePayment.setOptions(StripeOptions(
        publishableKey: StripeCfg.PUBLISH_KEY,
        merchantId: StripeCfg.MERCHANT_ID,
        androidPayMode: (Server.isTest) ? 'test' : 'live'));
  }

  makePayment({
    double amount = 0.0,
    String description = 'Task price',
    Function(PaymentMethod, PaymentIntentResult, PaymentIntentResult) callback,
  }) async {
    try {
      final user = userData.userModel;
      BillingAddress billingAddress = BillingAddress(
          line1: user.address,
          line2: "",
          city: "",
          state: "",
          postalCode: "",
          name: user.name);
      PrefilledInformation prefilledInformation =
          PrefilledInformation(billingAddress: billingAddress);

      StripePayment.paymentRequestWithCardForm(CardFormPaymentRequest(
              prefilledInformation: prefilledInformation))
          .then((paymentMethod) async {
        final paymentIntentRes = await createPaymentIntent(amounts: amount);
        StripePayment.confirmPaymentIntent(
          PaymentIntent(
            clientSecret: paymentIntentRes['client_secret'],
            paymentMethodId: paymentMethod.id,
          ),
        ).then((paymentIntent) {
          print(paymentIntent);
          StripePayment.authenticatePaymentIntent(
                  clientSecret: paymentIntentRes['client_secret'])
              .then((authPaymentIntent) {
            print(authPaymentIntent);
            callback(paymentMethod, paymentIntent, authPaymentIntent);
          }).catchError((e) {
            showAlert(context:context,msg: "authenticatePaymentIntent\n\n" + e.toString());
          });
        }).catchError((e) {
          showAlert(context:context,msg: "confirmPaymentIntent\n\n" + e.toString());
        });
      }).catchError((e) {
        showAlert(context:context,msg: "paymentRequestWithCardForm\n\n" + e.toString());
      });
      /*createCharges(
            tokenId: paymentMethod.id,
            amounts: amount,
            description: description);*/
      //}).catchError(setError);

      /*StripePayment.createTokenWithCard(
        CreditCard(
          number: "",
          expMonth: 02,
          expYear: 22,
          //cvc: details["cvv"],
        ),
      ).then((token) {
        createCharge(
            tokenId: token.tokenId, amounts: amount, description: description);
      }).catchError(setError);*/
    } catch (e) {}
  }

  Future<Map<String, dynamic>> createPaymentIntent({double amounts = 0}) async {
    try {
      EasyLoading.show(status: "Payment Processing...");
      Map<String, dynamic> body = {
        'amount': amounts.round().toString(),
        'currency': AppDefine.CUR_ABBR,
        'payment_method_types[]': 'card'
      };
      var respose = await http.post('https://api.stripe.com/v1/payment_intents',
          body: body,
          headers: {
            'Authorization': 'Bearer ${StripeCfg.SECRET_KEY}',
            'Content-Type': 'application/x-www-form-urlencoded'
          });
      return jsonDecode(respose.body);
    } catch (e) {
      print("err charing user: " + e.toString());
      showAlert(context:context,
          msg: "https://api.stripe.com/v1/payment_intents\n\n" + e.toString());
    }
    EasyLoading.dismiss();
    return null;
  }

  Future<Map<String, dynamic>> createCharges(
      {String tokenId, double amounts = 0, String description = ''}) async {
    try {
      EasyLoading.show(status: "Payment Processing...");
      if (tokenId != null && amounts > 0) {
        Map<String, dynamic> body = {
          'amount': amounts.round().toString(),
          'currency': AppDefine.CUR_ABBR,
          'source': tokenId,
          'description': description
        };
        var response = await http
            .post('https://api.stripe.com/v1/charges', body: body, headers: {
          'Authorization': 'Bearer ${StripeCfg.SECRET_KEY}',
          'Content-Type': 'application/x-www-form-urlencoded'
        });
        return jsonDecode(response.body);
      }
    } catch (e) {
      print('err charging user: ${e.toString()}');
      showAlert(context:context,msg: "https://api.stripe.com/v1/charges\n\n" + e.toString());
    }
    EasyLoading.dismiss();
    return null;
  }
}*/
