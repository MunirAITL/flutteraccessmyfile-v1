import 'dart:async';

enum NavBarItem { NEW_TASK, MY_TASK, MSG, NOTI, MORE }

class BottomNavBarBloc {
  final StreamController<NavBarItem> _navBarController =
      StreamController<NavBarItem>.broadcast();

  //NavBarItem defaultItem = NavBarItem.NEW_CASE;

  Stream<NavBarItem> get itemStream => _navBarController.stream;

  void pickItem(int i) {
    switch (i) {
      case 0:
        _navBarController.sink.add(NavBarItem.NEW_TASK);
        break;
      case 1:
        _navBarController.sink.add(NavBarItem.MY_TASK);
        break;
      case 2:
        _navBarController.sink.add(NavBarItem.MSG);
        break;
      case 3:
        _navBarController.sink.add(NavBarItem.NOTI);
        break;
      case 4:
        _navBarController.sink.add(NavBarItem.MORE);
        break;
    }
  }

  close() {
    try {
      _navBarController?.close();
    } catch (e) {}
  }
}
