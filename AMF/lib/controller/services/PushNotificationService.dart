import 'dart:io';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

//  https://medium.com/comerge/implementing-push-notifications-in-flutter-apps-aef98451e8f1

enum enumFCM { onMessage, onLaunch, onResume }

class PushNotificationService with Mixin {
  final FirebaseMessaging _fcm;

  PushNotificationService(this._fcm);

  static const String fcmTokenKey = "fcmTokenKey";

  Future initialise({Function callback}) async {
    if (Platform.isIOS) {
      _fcm.requestPermission(sound: true, badge: true, alert: true);

      // _fcm.requestNotificationPermissions(
      //     IosNotificationSettings(sound: true, badge: true, alert: true));

      // _fcm.onIosSettingsRegistered.listen((IosNotificationSettings settings) {
      //   print("Settings registered: $settings");
      // });

    }

    // If you want to test the push notification locally,
    // you need to get the token and input to the Firebase console
    // https://console.firebase.google.com/project/YOUR_PROJECT_ID/notification/compose
    String token = await _fcm.getToken();

    //  store in shared pref
    await PrefMgr.shared.setPrefStr(fcmTokenKey, token);
    myLog("FirebaseMessaging token: $token");
    FirebaseMessaging.onMessage.listen((message) {
      myLog("onMessage 2 : ${message.data}");
      // callback(enumFCM.onMessage, message);
      showNotificationMessage(
          title: message.data['title'] == null
              ? "Mortgage Magic"
              : message.data['title'],
          body: message.data['body'] == null
              ? message.data['Message']
              : message.data['body']);
    });
/*    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        myLog("onMessage: ${message}");
        callback(enumFCM.onMessage, message);
        showNotificationMessage(
            title: message['notification']['title'] == null
                ? "Mortgage Magic"
                : message['notification']['title'],
            body: message['notification']['body'] == null
                ? message['data']['Message']
                : message['notification']['body']);
      },
      onLaunch: (Map<String, dynamic> message) async {
        myLog("onLaunch: $message");
        callback(enumFCM.onLaunch, message);
      },
      onResume: (Map<String, dynamic> message) async {
        myLog("onResume: $message");
        callback(enumFCM.onResume, message);
      },
    );*/
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) {}

  static Future selectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }

  Future<void> showNotificationMessage({String title, String body}) async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.max, priority: Priority.high, ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        DateTime.now().second, "$title", body, platformChannelSpecifics,
        payload: 'Default_Sound');
  }
}
