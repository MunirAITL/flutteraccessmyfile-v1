import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:path/path.dart';

class UserProfileVal with Mixin {
  static const int PHONE_LIMIT = 8;

  isEmpty(context, TextEditingController tf, arg) {
    if (tf.text.isEmpty) {
      showToast(context: context, msg: arg);
      return true;
    }
    return false;
  }

  isFullName(context, String name) {
    try {
      final isOk = (name.trim().contains(" ")) ? true : false;
      if (!isOk) {
        showToast(context: context, msg: "Invalid full name");
      }
      return isOk;
    } catch (e) {
      return false;
    }
  }

  isFNameOK(context, TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(context: context, msg: "Invalid first name");
      return false;
    }
    return true;
  }

  isLNameOK(context, TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(context: context, msg: "Invalid last name");
      return false;
    }
    return true;
  }

  isEmailOK(context, TextEditingController tf, String msg) {
    if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9-]+\.[a-zA-Z]+")
        .hasMatch(tf.text.trim())) {
      showToast(context: context, msg: msg);
      return false;
    } else if (tf.text.trim().contains("@help.mortgage-magic.co.uk")) {
      showToast(
        context: context,
        msg: "Email address does not exists",
      );
      return false;
    }
    return true;
  }

  isPhoneOK(context, TextEditingController tf) {
    if (tf.text.length < PHONE_LIMIT) {
      showToast(context: context, msg: "Invalid Mobile Number");
      return false;
    }
    return true;
  }

  isPwdOK(context, TextEditingController tf) {
    if (tf.text.length < 3) {
      showToast(
        context: context,
        msg: "Password should be greater by 4 characters",
      );
      return false;
    }
    return true;
  }

  isComNameOK(context, TextEditingController tf) {
    if (tf.text.length < 6) {
      showToast(context: context, msg: "Invalid Company Name");
      return false;
    }
    return true;
  }

  isDOBOK(context, str) {
    if (str == '') {
      showToast(context: context, msg: "Invalid Date of Birth");
      return false;
    }
    return true;
  }
}
