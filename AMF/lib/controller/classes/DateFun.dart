// ignore_for_file: file_names

import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

class DateFun {
  static const DT_CURRENT_QUARTER = "Current quarter";
  static const DT_LAST_QUARTER = "Last quarter";
  static const DT_CURRENT_FINANCIAL_YEAR = "Current financial year";
  static const DT_LAST_FINANCIAL_YEAR = "Last financial year";
  static const DT_ALL_TIME = "All time";

  static getMonthByDigit({int index, bool isFullMonthName = false}) {
    final now = DateTime.now();
    final dt = DateTime(now.year, index, now.day);
    return getDate(dt.toString(), "MMM");
  }

  static getDOBFormat() {
    return "MM/dd/yyyy";
  }

  static getDateUKFormat() {
    return "dd/MM/yyyy";
  }

  static getDateBDFormat() {
    return "dd/MM/yyyy";
  }

  static String getTimeAgoTxt(dt) {
    try {
      var t = Jiffy(dt).fromNow();
      t = t.replaceAll("hours", "hrs");
      t = t.replaceAll("minutes", "mins");
      t = t.replaceAll("seconds", "sec");
      t = t.replaceAll("hour", "hr");
      t = t.replaceAll("minute", "min");
      t = t.replaceAll("second", "sec");
      print(t);
      return t;
    } catch (e) {
      print(e.toString());
      return dt;
    }
  }

  static String getLastMondayDate() {
    var dayOfWeek = 1;
    DateTime date = DateTime.now();
    return date
        .subtract(Duration(days: date.weekday - dayOfWeek))
        .toIso8601String();
  }

  static String getFormatedDate({DateTime mDate, String format}) {
    if (mDate == null) mDate = DateTime.now();
    return Jiffy(mDate).format(format);
  }

  static String getDateWhatsAppFormat(String dt) {
    try {
      DateTime checkedTime = DateTime.parse(dt);
      DateTime currentTime = DateTime.now();
      if ((currentTime.year == checkedTime.year) &&
          (currentTime.month == checkedTime.month) &&
          (currentTime.day == checkedTime.day)) {
        dt = Jiffy(dt).format("hh:mm a");
      } else {
        dt = Jiffy(dt).format("dd-MMM-yyyy hh:mm a");
      }
    } catch (e) {}
    return dt;
  }

  static String getPaymentDate(String givenDateString) {
    try {
      int timeInMilliseconds = 0;
      final f = DateFormat('yyyy-MM-dd hh:mm:ss');
      var dateTimeString = givenDateString;
      if (givenDateString.contains("T")) {
        dateTimeString = givenDateString.replaceAll("T", " ");
      }

      final mDate = f.parse(dateTimeString);
      timeInMilliseconds = mDate.millisecondsSinceEpoch;

      //final f2 = DateFormat('dd-MM-yyyy hh:mm:ss');
      final now = DateTime.now();

      var ago = DateTime.fromMillisecondsSinceEpoch(timeInMilliseconds);

      //final currentDate = f2.parse(now.toString());
      //final createDate = f2.parse(ago.toString());
      var different =
          (now.millisecondsSinceEpoch - ago.millisecondsSinceEpoch).abs();
      //print(different);

      var secondsInMilli = 1000;
      var minutesInMilli = secondsInMilli * 60;
      var hoursInMilli = minutesInMilli * 60;
      var daysInMilli = hoursInMilli * 24;

      var elapsedDays = different / daysInMilli;
      different %= daysInMilli;

      var elapsedHours = different / hoursInMilli;
      different %= hoursInMilli;

      var elapsedMinutes = different / minutesInMilli;
      different %= minutesInMilli;

      var elapsedSeconds = different / secondsInMilli;

      different %= secondsInMilli;

      if (elapsedDays == 0) {
        if (elapsedHours == 0) {
          if (elapsedMinutes == 0) {
            if (elapsedSeconds < 0) {
              return DT_CURRENT_QUARTER;
            } else {
              if (elapsedDays > 0 && elapsedSeconds < 59) {
                return DT_CURRENT_QUARTER;
              }
            }
          } else {
            return DT_CURRENT_QUARTER;
          }
        } else {
          return DT_CURRENT_QUARTER;
        }
      } else {
        if (elapsedDays <= 29) {
          return elapsedDays.round().toString() + " d ago";
        } else if (elapsedDays >= 30 && elapsedDays <= 58) {
          return DT_CURRENT_QUARTER;
        }
        if (elapsedDays >= 59 && elapsedDays <= 87) {
          return DT_CURRENT_QUARTER;
        }
        if (elapsedDays >= 88 && elapsedDays <= 116) {
          return DT_CURRENT_QUARTER;
        }
        if (elapsedDays >= 117 && elapsedDays <= 145) {
          return DT_CURRENT_QUARTER;
        }
        //
        if (elapsedDays >= 146 && elapsedDays <= 174) {
          return DT_LAST_QUARTER;
        }
        if (elapsedDays >= 175 && elapsedDays <= 203) {
          return DT_LAST_QUARTER;
        }
        if (elapsedDays >= 204 && elapsedDays <= 232) {
          return DT_LAST_QUARTER;
        }
        if (elapsedDays >= 233 && elapsedDays <= 261) {
          return DT_LAST_QUARTER;
        }
        //
        if (elapsedDays >= 262 && elapsedDays <= 290) {
          return DT_CURRENT_FINANCIAL_YEAR;
        }
        if (elapsedDays >= 291 && elapsedDays <= 319) {
          return DT_CURRENT_FINANCIAL_YEAR;
        }
        if (elapsedDays >= 320 && elapsedDays <= 348) {
          return DT_CURRENT_FINANCIAL_YEAR;
        }
        if (elapsedDays >= 349 && elapsedDays <= 360) {
          return DT_CURRENT_FINANCIAL_YEAR;
        }
        //
        if (elapsedDays >= 361 && elapsedDays <= 720) {
          return DT_LAST_FINANCIAL_YEAR;
        }
      }
    } catch (e) {
      return null;
    }
  }

  static String getDate(String dt, String format) {
    try {
      return Jiffy(DateTime.parse(dt)).format(format);
    } catch (e) {}
    return dt;
  }

  static int daysBetween(DateTime from, DateTime to) {
    from = DateTime(from.year, from.month, from.day);
    to = DateTime(to.year, to.month, to.day);
    return (to.difference(from).inHours / 24).round();
  }

  static int hrBefore(DateTime from, DateTime to) {
    from = DateTime(from.year, from.month, from.day);
    to = DateTime(to.year, to.month, to.day);
    return (from.difference(to).inSeconds).round();
  }
}
