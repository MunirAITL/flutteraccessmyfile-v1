import 'package:aitl/Mixin.dart';
import 'package:aitl/config/server/APICreditReportCfg.dart';
import 'package:aitl/data/model/dashboard/credit_case/GetUserValidationOldUserApiModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/cupertino.dart';

class GetUserValidationOldUserApiMgr with Mixin {
  static final GetUserValidationOldUserApiMgr _shared =
      GetUserValidationOldUserApiMgr._internal();

  factory GetUserValidationOldUserApiMgr() {
    return _shared;
  }

  GetUserValidationOldUserApiMgr._internal();

  creditCaseOldUserInformation({
    BuildContext context,
    int userID,
    int userCompanyId,
    Function(GetUserValidationOldUserApiModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<GetUserValidationOldUserApiModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: CreditURLbHelper()
            .getUrl(userCompanyId: userCompanyId, userID: userID),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog("GET_USER_VALIDATION_FOR_OLD_USER_URL  ERROR = " + e.toString());
    }
  }
}

class CreditURLbHelper {
  getUrl({int userCompanyId, int userID}) {
    var url = APICreditReportCfg.GET_USER_VALIDATION_FOR_OLD_USER_URL;
    url = url.replaceAll("#UserId#", userID.toString());
    url = url.replaceAll("#UserCompanyId#", userCompanyId.toString());
    return url;
  }
}
