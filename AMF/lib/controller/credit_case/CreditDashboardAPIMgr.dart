import 'package:aitl/config/server/APICreditReportCfg.dart';
import 'package:aitl/data/model/dashboard/credit_case/CreditDertailsAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class CreditCaseDashboardApiMgr with Mixin {
  static final CreditCaseDashboardApiMgr _shared =
      CreditCaseDashboardApiMgr._internal();

  factory CreditCaseDashboardApiMgr() {
    return _shared;
  }

  CreditCaseDashboardApiMgr._internal();

  creditCaseDashboardLoad({
    BuildContext context,
    String userIdentifier,
    var userID,
    var userCompanyId,
    Function(CreditDetailsAPIModel) callback,
  }) async {
    Map<String, dynamic> prams = {
      "UserIdentifier": "$userIdentifier",
      "UserId": "$userID",
      "UserCompanyId": "$userCompanyId",
    };

    try {
      await NetworkMgr()
          .req<CreditDetailsAPIModel, Null>(
        context: context,
        reqType: ReqType.Post,
        param: prams,
        url: APICreditReportCfg.CREDIT_DATA_DASHBOARD_URL,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog("CREDIT_DATA_DASHBOARD_URL  ERROR = " + e.toString());
    }
  }
}
