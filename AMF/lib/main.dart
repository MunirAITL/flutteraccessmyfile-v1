import 'dart:io';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'config/cfg/AppDefine.dart';
import 'config/server/Server.dart';
import 'config/theme/theme_types/app_themes.dart';
import 'data/translations/LanguageTranslations.dart';
import 'view/splash/splash_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  //  device settings
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  //  error catching
  FlutterError.onError = (FlutterErrorDetails details) async {
    print(
        "************************** FlutterErrorDetails" + details.toString());
  };

  EasyLoading.instance
    ..displayDuration = const Duration(seconds: 3)
    ..loadingStyle = EasyLoadingStyle.custom
    ..textColor = Colors.white
    //..textStyle =
    //TextStyle(fontFamily: "Fieldwork", fontSize: 17, color: MyTheme.bgDark)
    ..backgroundColor = MyTheme.bgColor3
    ..indicatorColor = Colors.white
    ..lineWidth = 10
    //..maskColor = Colors.white
    ..indicatorType = EasyLoadingIndicatorType.chasingDots
    ..maskType = EasyLoadingMaskType.clear
    ..userInteractions = false;

  HttpOverrides.global = MyHttpOverrides();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        translations: LngTR(),
        locale: Get.deviceLocale,
        fallbackLocale:
            Locale(AppDefine.LANGUAGE, AppDefine.GEO_CODE.toUpperCase()),
        smartManagement: SmartManagement.full,
        enableLog: Server.isTest,
        defaultTransition: Transition.fade,
        title: AppDefine.APP_NAME,
        //theme: MyTheme.themeData,
        darkTheme: AppThemes.dark,
        theme: AppThemes.light,
        themeMode: ThemeMode.system,
        builder: (context, widget) {
          widget = EasyLoading.init()(context, widget);
          return widget;
        },
        home: SplashPage());
    //home: Test());
  }
}
