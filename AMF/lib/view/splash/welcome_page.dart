import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../auth/signin_page.dart';

class WelcomePage extends StatefulWidget {
  @override
  State createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> with Mixin, StateListener {
  StateProvider _stateProvider;
  @override
  void onStateChanged(ObserverState state, data) async {
    try {} catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    super.dispose();
  }

  appInit() async {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: MyTheme.bgColor3,
      statusBarIconBrightness: Brightness.light,
    ));
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor3,
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      child: Container(
        width: getW(context),
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 20),
              Txt(
                txt: "Welcome to",
                txtColor: Colors.white,
                txtSize: MyTheme.txtSize + 2,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 2),
              Txt(
                txt: "Access My File",
                txtColor: MyTheme.cyanColor,
                txtSize: MyTheme.txtSize + 1.5,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 60),
              Image.asset(
                "assets/images/img/welcome.png",
                width: getWP(context, 60),
                height: getHP(context, 30),
              ),
              SizedBox(height: 30),
              Txt(
                txt:
                    "Check your credit score and report\nwithout hurting your credit",
                txtColor: Colors.white,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 50),
              MMBtn(
                  txt: "GET STARTED",
                  bgColor: MyTheme.redColor,
                  txtColor: MyTheme.btnTxtColor,
                  width: getW(context),
                  height: getHP(context, 7),
                  callback: () {
                    Get.off(() => SigninPage());
                  }),
              SizedBox(height: 20),
              _drawSignup(),
            ],
          ),
        ),
      ),
    );
  }

  _drawSignup() {
    return RichText(
      text: new TextSpan(
        children: [
          new TextSpan(
            text: 'Already a member?',
            style: new TextStyle(color: Colors.white),
          ),
          new TextSpan(
            text: ' Sign in ',
            style: new TextStyle(color: MyTheme.cyanColor),
            recognizer: new TapGestureRecognizer()
              ..onTap = () {
                Get.off(() => SigninPage());
              },
          ),
        ],
      ),
    );
  }
}
