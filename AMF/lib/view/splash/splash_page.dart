import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/CommonData.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/noti/NotificationData.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/data/translations/LanguageTranslations.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/otp/sms_page2.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/splash/welcome_page.dart';
import 'package:aitl/view/widgets/views/case_alert_page.dart';
import 'package:aitl/view_model/helper/noti/InitializeAwesomeNotifications.dart';
import 'package:aitl/view_model/helper/noti/NotiHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../auth/otp/sms_page2.dart';

class SplashPage extends StatefulWidget {
  @override
  State createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with Mixin {
  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      LngTR().setLng();
      StateProvider().notify(ObserverState.STATE_RELOAD_LOCAL, null);
      //SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark,
      ));

      //notification int
      InitializeAwesomeNotifications.initAwesome();
      //allow notification
      // InitializeAwesomeNotifications.checkNotificationIsAllow();

      // Create the initialization for your desired push service here
      FirebaseMessaging.onBackgroundMessage(
          _firebaseMessagingBackgroundHandler);
      FirebaseMessaging.instance.getToken().then((value) async => {
            print("Token imaran = ${value.toString()}"),
            await PrefMgr.shared.setPrefStr("fcmTokenKey", value.toString())
          });

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        if (message.data["EntityName"] == "TestPushNotitification") {
          InitializeAwesomeNotifications.generateNotification(
              title: message.notification.title,
              body: message.notification.body);
        } else {
          notiCreator(message);
        }
        // debugPrint("Notification generation getting error ${message.data}");
      });

      try {
        //  cookie
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0 &&
            await DBMgr.shared.getTotalRow("User") > 0) {
          await userData.setUserModel();
          if (userData.userModel.isEmailVerified) {
            Get.off(() => DashboardPage());
          } else {
            if (Server.isOtp) {
              Get.off(() => WelcomePage());
            } else {
              Get.off(() => DashboardPage());
            }
          }
        } else {
          Future.delayed(const Duration(seconds: 2), () {
            Get.off(() => WelcomePage());
          });
        }
      } catch (e) {
        myLog(e.toString());
      }
    } catch (e) {}
  }

  fcmClickNoti(Map<String, dynamic> message) async {
    try {
      if (mounted && comData.isNotiTestPage != null) {
        if (comData.isNotiTestPage) {
          FlutterRingtonePlayer.play(
            android: AndroidSounds.notification,
            ios: IosSounds.glass,
            looping: false, // Android only - API >= 28
            volume: 0.1, // Android only - API >= 28
            asAlarm: false, // Android only - all APIs
          );

          Get.to(() => CaseAlertPage(
                message: message,
              )).then((value) async {
            await userData.setUserModel();
            Get.off(() => DashboardPage()).then((value) {});
          });
        } else {
          final msg = message['data']['Message'] +
              '\n\n' +
              message['data']['Description'];
          showSnake("Test notification!", msg);
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.white,
          body: Container(
            child: Center(
              child: Image.asset(
                "assets/images/welcome/splash-logo.png",
                //fit: BoxFit.cover,
                height: getHP(context, 10),
                width: getW(context),
              ),
            ),
          )),
    );
  }
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // Use this method to automatically convert the push data, in case you gonna use our data standard
  // AwesomeNotifications().createNotificationFromJsonData(message.data);

  notiCreator(message);
}

notiCreator(RemoteMessage message) {
  debugPrint("notification Message data get ${message.data}");

  try {
    NotificationData notiModel = new NotificationData.fromJson(message.data);

    Map<String, dynamic> notiMap =
        NotiHelper().getNotiMapLocalNotification(model: notiModel);

    String txt = notiMap['txt'] != null ? notiMap['txt'].toString() : "";

    InitializeAwesomeNotifications.generateNotification(
        title: notiModel.initiatorName, body: txt);
  } catch (e) {
    debugPrint("Notification generation getting error $e");
  }
}
