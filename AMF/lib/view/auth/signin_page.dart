import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/forgot_page.dart';
import 'package:aitl/view/auth/reg_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../config/server/APIAuthCfg.dart';
import '../../config/server/Server.dart';
import '../../config/theme/MyTheme.dart';
import '../../controller/form_validator/UserProfileVal.dart';
import '../../data/app_data/UserData.dart';
import '../../data/db/DBMgr.dart';
import '../../data/model/auth/LoginAPIModel.dart';
import '../../data/network/NetworkMgr.dart';
import '../../view_model/api/api_view_model.dart';
import '../../view_model/helper/auth/LoginHelper.dart';
import '../dashboard/dashboard_page.dart';
import '../widgets/textfield/InputBox.dart';
import '../widgets/txt/Txt.dart';
import 'package:get/get.dart';

import 'otp/sms_page2.dart';
import 'otp/verify_code_page.dart';

class SigninPage extends StatefulWidget {
  const SigninPage({Key key}) : super(key: key);

  @override
  State<SigninPage> createState() => _SigninPageState();
}

class _SigninPageState extends State<SigninPage> with Mixin {
  final email = TextEditingController();
  final pwd = TextEditingController();

  final focusEmail = FocusNode();
  final focusPwd = FocusNode();

  var isPwdVisable = false;

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: MyTheme.bgColor4,
      statusBarIconBrightness: Brightness.dark,
    ));
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    email.dispose();
    pwd.dispose();
    super.dispose();
  }

  appInit() async {}

  validate() {
    if (UserProfileVal().isEmpty(context, email, 'Missing Email/Phone')) {
      return false;
    } else if (!UserProfileVal().isPwdOK(context, pwd)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor4,
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      child: Container(
        width: getW(context),
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30),
              Center(
                child: Column(
                  children: [
                    Txt(
                      txt: "Sign in to",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize + .2,
                      txtAlign: TextAlign.center,
                      fontWeight: FontWeight.w500,
                      isBold: true,
                    ),
                    SizedBox(height: 2),
                    Txt(
                      txt: "Access My File",
                      txtColor: MyTheme.bgColor3,
                      txtSize: MyTheme.txtSize + .1,
                      txtAlign: TextAlign.center,
                      fontWeight: FontWeight.w500,
                      isBold: true,
                    ),
                  ],
                ),
              ),
              SizedBox(height: 40),
              InputBox(
                context: context,
                ctrl: email,
                lableTxt: "Email",
                kbType: TextInputType.emailAddress,
                inputAction: TextInputAction.next,
                focusNode: focusEmail,
                focusNodeNext: focusPwd,
                len: 50,
              ),
              InputBox(
                context: context,
                ctrl: pwd,
                lableTxt: "Password",
                kbType: TextInputType.emailAddress,
                inputAction: TextInputAction.done,
                focusNode: focusPwd,
                len: 20,
                isPwd: !isPwdVisable,
                suffixIcon: IconButton(
                    onPressed: () {
                      isPwdVisable = !isPwdVisable;
                      setState(() {});
                    },
                    icon: Icon(
                      !isPwdVisable
                          ? Icons.visibility_outlined
                          : Icons.visibility_off_outlined,
                      color: Colors.grey,
                    )),
              ),
              SizedBox(height: 10),
              Align(
                alignment: Alignment.centerRight,
                child: InkWell(
                    child: new Text('Forgot password?',
                        style: TextStyle(color: Colors.black87)),
                    onTap: () {
                      Get.to(() => ForgotPage(email: email.text.trim()));
                    }),
              ),
              SizedBox(height: 20),
              MMBtn(
                  txt: "SIGN IN",
                  bgColor: MyTheme.bgColor3,
                  txtColor: MyTheme.cyanColor,
                  width: getW(context),
                  height: getHP(context, 7),
                  callback: () async {
                    if (validate()) {
                      APIViewModel().req<LoginAPIModel>(
                        context: context,
                        url: APIAuthCfg.LOGIN_URL,
                        param: LoginHelper().getParam(
                            email: email.text.trim(), pwd: pwd.text.trim()),
                        reqType: ReqType.Post,
                        callback: (model) async {
                          if (mounted && model != null) {
                            if (model.success) {
                              try {
                                await DBMgr.shared.setUserProfile(
                                    user: model.responseData.user);
                                await userData.setUserModel();
                              } catch (e) {
                                myLog(e.toString());
                              }
                              if (!Server.isOtp) {
                                Get.off(() => DashboardPage());
                              } else {
                                if (model.responseData.user.isEmailVerified ||
                                    model.responseData.user
                                        .isMobileNumberVerified) {
                                  Get.off(() => DashboardPage());
                                } else {
                                  final user = model.responseData.user;
                                  Get.to(
                                      () => VerifyCodePage(email: user.email));
                                }
                              }
                            } else {
                              try {
                                final err = model.errorMessages.login[0];
                                showToast(context: context, msg: err);
                              } catch (e) {
                                myLog(e.toString());
                              }
                            }
                          }
                        },
                        isCookie: true,
                      );
                    }
                  }),
              SizedBox(height: getHP(context, 10)),
              _drawSocialLoginBtn(),
              SizedBox(height: 40),
              _drawSignup(),
            ],
          ),
        ),
      ),
    );
  }

  _drawSocialLoginBtn() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Row(
            children: [
              Expanded(child: Divider(color: Colors.grey)),
              Txt(
                  txt: "or you can use",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize - .6,
                  txtAlign: TextAlign.center,
                  isBold: false),
              Expanded(child: Divider(color: Colors.grey)),
            ],
          ),
        ),
        SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 5, bottom: 5, left: 10, right: 10),
                child: Image.asset(
                  "assets/images/icons/google_btn.png",
                  //fit: BoxFit.fill,
                  width: 100,
                  height: 25,
                ),
              ),
            ),
            SizedBox(width: 20),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 5, bottom: 5, left: 10, right: 10),
                child: Image.asset(
                  "assets/images/icons/fb_btn.png",
                  //fit: BoxFit.fill,
                  width: 100,
                  height: 25,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  _drawSignup() {
    return Center(
      child: RichText(
        text: new TextSpan(
          children: [
            new TextSpan(
              text: 'Don\'t have an account?',
              style: new TextStyle(color: Colors.black),
            ),
            new TextSpan(
              text: ' Sign up ',
              style: new TextStyle(color: MyTheme.bgColor3),
              recognizer: new TapGestureRecognizer()
                ..onTap = () {
                  Get.to(() => RegPage());
                },
            ),
          ],
        ),
      ),
    );
  }
}
