import 'package:aitl/Mixin.dart';
import 'package:aitl/config/server/APIHrefLoginCfg.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/txt/TxtBox.dart';
import 'package:aitl/view/widgets/views/TCView.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../config/server/Server.dart';
import '../../config/theme/MyTheme.dart';
import '../../data/app_data/PrefMgr.dart';
import '../../data/app_data/UserData.dart';
import '../../data/db/DBMgr.dart';
import '../../data/model/auth/LoginAPIModel.dart';
import '../../data/model/auth/hreflogin/PostEmailOtpAPIModel.dart';
import '../../data/model/auth/hreflogin/SendUserEmailOtpAPIModel.dart';
import '../../data/network/NetworkMgr.dart';
import '../../view_model/helper/auth/LoginApiMgr.dart';
import '../../view_model/helper/auth/RegAPIMgr.dart';
import '../../view_model/helper/helper/UIHelper.dart';
import '../widgets/datepicker/DatePickerView.dart';
import '../widgets/textfield/InputTitleBox.dart';
import '../widgets/textfield/InputTitleBoxfWithCountryCode.dart';
import 'otp/verify_code_page.dart';

class RegPage extends StatefulWidget {
  final bool isOTPComeThrough;
  String firstName = "";
  String lastName = "";
  String emailAddress = "";
  String phoneNumber = "";

  RegPage(
      {this.firstName,
      this.lastName,
      this.emailAddress,
      this.phoneNumber,
      this.isOTPComeThrough = false});

  @override
  State createState() => _RegPageState();
}

enum genderEnum { male, female }

class _RegPageState extends State<RegPage> with Mixin {
  final _fname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _mobile = TextEditingController();
  final _pwd = TextEditingController();
  //
  final focusFname = FocusNode();
  final focusLname = FocusNode();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();
  final focusPwd = FocusNode();

  String countryCode = "+44";
  String countryName = "GB";

  bool isobscureText = true;

  String dob = "";

  genderEnum _gender = genderEnum.male;

  bool isStep2 = false;
  bool isLoading = false;

  callCheckUserEmailAPI(LoginAPIModel model) async {
    try {
      await APIViewModel().req<PostEmailOtpAPIModel>(
        context: context,
        url: APIHrefLoginCfg.POSTEMAILOTP_POST_URL,
        reqType: ReqType.Post,
        param: {
          "Email": _email.text.trim(),
          "Status": "101",
        },
        callback: (model2) async {
          if (mounted) {
            if (model2 != null) {
              if (model2.success) {
                await APIViewModel().req<SendUserEmailOtpAPIModel>(
                    context: context,
                    url: APIHrefLoginCfg.SENDUSEREMAILOTP_GET_URL.replaceAll(
                        "#otpId#", model2.responseData.userOTP.id.toString()),
                    reqType: ReqType.Get,
                    callback: (model3) async {
                      if (mounted) {
                        if (model3 != null) {
                          if (model3.success) {
                            Get.off(() => VerifyCodePage(
                                  email: _email.text.trim(),
                                ));
                          } else {
                            final err =
                                model3.messages.getSenduseremailotplogin[0];
                            showToast(context: context, msg: err, which: 0);
                          }
                        }
                      }
                    });
              } else {
                final err = model2.messages.postUserotp[0];
                showToast(context: context, msg: err, which: 0);
              }
            }
          }
        },
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  getFullPhoneNumber({String countryCodeTxt, String number}) {
    var fullNumber = countryCodeTxt + number;
    return fullNumber;
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    _fname.dispose();
    _lname.dispose();
    _mobile.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.white,
      statusBarIconBrightness: Brightness.dark,
    ));
    getMobCode();
    try {
      _fname.text = widget.firstName;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      _lname.text = widget.lastName;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      _email.text = widget.emailAddress;
    } catch (e) {
      myLog(e.toString());
    }

    try {
      _mobile.text = widget.phoneNumber;
    } catch (e) {
      myLog(e.toString());
    }
  }

  getMobCode() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;
          countryCode = cd;
        });
      }
    } catch (e) {
      print("sms2 Screen country code and name problem ");
    }
  }

  validate1() {
    if (!UserProfileVal().isFNameOK(context, _fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, _lname)) {
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(context, _email, "Invalid email address")) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, _mobile)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(context, _pwd)) {
      return false;
    }
    return true;
  }

  /*validate2() {
    if (!UserProfileVal().isDOBOK(context, dob)) {
      return false;
    }
    return true;
  }*/

  refreshStep1() {
    setState(() {
      isStep2 = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor4,
        bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          child: (!isStep2)
              ? Container(
                  color: MyTheme.bgColor4,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 20, right: 20, top: 5, bottom: 5),
                    child: MMBtn(
                        txt: "CONTINUE",
                        height: getHP(context, 7),
                        width: getW(context),
                        bgColor: MyTheme.bgColor3,
                        txtColor: MyTheme.cyanColor,
                        radius: 10,
                        callback: () {
                          if (validate1()) {
                            isStep2 = true;
                            getMobCode();
                            setState(() {});
                          }
                        }),
                  ),
                )
              : Container(
                  color: MyTheme.bgColor4,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TCView(screenName: 'Continue'),
                        SizedBox(height: 30),
                        MMBtn(
                            txt: "CREATE ACCOUNT",
                            height: getHP(context, 7),
                            width: getW(context),
                            bgColor: MyTheme.bgColor3,
                            txtColor: MyTheme.cyanColor,
                            callback: () {
                              //if (validate2()) {
                              setState(() {
                                isLoading = true;
                              });
                              RegAPIMgr().wsRegAPI(
                                  context: context,
                                  email: _email.text.trim(),
                                  pwd: _pwd.text.trim(),
                                  fname: _fname.text.trim(),
                                  lname: _lname.text.trim(),
                                  mobile: getFullPhoneNumber(
                                          countryCodeTxt: countryCode,
                                          number: _mobile.text.toString())
                                      .trim(),
                                  countryCode: countryCode,
                                  dob: dob,
                                  callback: (model) async {
                                    if (model != null && mounted) {
                                      try {
                                        if (model.success) {
                                          //  recall login to get cookie
                                          LoginAPIMgr().wsLoginAPI(
                                              context: context,
                                              email: _email.text.trim(),
                                              pwd: _pwd.text.trim(),
                                              callback: (model2) async {
                                                if (model2 != null && mounted) {
                                                  try {
                                                    setState(() {
                                                      isLoading = false;
                                                    });
                                                    if (model2.success) {
                                                      //myLog(model.responseData.user.address);
                                                      try {
                                                        //DBMgr.shared.getUserProfile();
                                                        await DBMgr.shared
                                                            .setUserProfile(
                                                                user: model2
                                                                    .responseData
                                                                    .user);
                                                        await userData
                                                            .setUserModel();
                                                        if (model
                                                            .responseData
                                                            .user
                                                            .isMobileNumberVerified) {
                                                          Get.offAll(() =>
                                                              DashboardPage());
                                                        } else {
                                                          if (!Server.isOtp) {
                                                            Get.offAll(() =>
                                                                DashboardPage());
                                                          } else {
                                                            callCheckUserEmailAPI(
                                                                model2);
                                                          }
                                                        }
                                                      } catch (e) {
                                                        myLog(e.toString());
                                                      }
                                                    } else {
                                                      try {
                                                        if (mounted) {
                                                          final err = model2
                                                              .errorMessages
                                                              .login[0]
                                                              .toString();
                                                          showToast(
                                                              context: context,
                                                              msg: err);
                                                        }
                                                      } catch (e) {
                                                        myLog(e.toString());
                                                      }
                                                    }
                                                  } catch (e) {
                                                    myLog(e.toString());
                                                  }
                                                }
                                              });
                                        } else {
                                          try {
                                            setState(() {
                                              isLoading = false;
                                            });
                                            if (mounted) {
                                              final err = model
                                                  .errorMessages.register[0]
                                                  .toString();
                                              showToast(
                                                  context: context, msg: err);
                                            }
                                          } catch (e) {
                                            myLog(e.toString());
                                          }
                                        }
                                      } catch (e) {
                                        myLog(e.toString());
                                      }
                                    }
                                  });
                            }
                            //}
                            ),
                      ],
                    ),
                  ),
                ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: (!isStep2) ? drawRegView1() : drawRegView2(),
        ),
      ),
    );
  }

  drawRegView1() {
    return Container(
      //decoration: MyTheme.bgBlueColor,
      width: getW(context),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIHelper().drawAppBarTitle(
                title: "Create Account",
                callback: () {
                  Get.back();
                }),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    drawInputBox(
                      context: context,
                      bgColor: Colors.transparent,
                      title: "First Name",
                      input: _fname,
                      ph: "Celia",
                      kbType: TextInputType.name,
                      inputAction: TextInputAction.next,
                      focusNode: focusFname,
                      focusNodeNext: focusLname,
                      len: 20,
                    ),
                    SizedBox(height: 15),
                    drawInputBox(
                      context: context,
                      bgColor: Colors.transparent,
                      title: "Last Name",
                      input: _lname,
                      ph: "Rhodes",
                      kbType: TextInputType.name,
                      inputAction: TextInputAction.next,
                      focusNode: focusLname,
                      focusNodeNext: focusEmail,
                      len: 20,
                    ),
                    SizedBox(height: 15),
                    drawInputBox(
                      context: context,
                      bgColor: Colors.transparent,
                      title: "Email",
                      input: _email,
                      ph: "yourmail@mail.com",
                      kbType: TextInputType.emailAddress,
                      inputAction: TextInputAction.next,
                      focusNode: focusEmail,
                      focusNodeNext:
                          widget.phoneNumber == null ? focusMobile : focusPwd,
                      len: 50,
                    ),
                    SizedBox(height: 15),
                    widget.phoneNumber == null
                        ? drawInputBoxWithCountryCode(
                            context: context,
                            title: "Phone Number",
                            input: _mobile,
                            ph: "Phone Number",
                            kbType: TextInputType.phone,
                            inputAction: TextInputAction.next,
                            focusNode: focusMobile,
                            focusNodeNext: focusPwd,
                            len: 15,
                            isWhiteBG: false,
                            countryCode: countryCode,
                            countryName: countryName,
                            getCountryCode: (value) {
                              countryCode = value.toString();
                              print("Country Code Clik = " + countryCode);
                              PrefMgr.shared
                                  .setPrefStr("countryName", value.code);
                              PrefMgr.shared
                                  .setPrefStr("countryCode", value.toString());
                            })
                        : Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Txt(
                                  txt: "Mobile Number",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  fontWeight: FontWeight.w400,
                                  isBold: false),
                              SizedBox(height: 10),
                              TxtBox(txt: widget.phoneNumber),
                            ],
                          ),
                    /*  Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: drawInputBox(
                        title: "Phone Number",
                        input: _mobile,
                        ph: "0898215324232",
                        kbType: TextInputType.phone,
                        len: 20,
                      ),
                    ),*/
                    SizedBox(height: 15),
                    drawInputBox(
                      context: context,
                      bgColor: Colors.transparent,
                      title: "Password",
                      input: _pwd,
                      ph: "Type Your Password",
                      kbType: TextInputType.text,
                      inputAction: TextInputAction.done,
                      focusNode: focusPwd,
                      len: 20,
                      isPwd: true,
                    ),
                  ],
                ),
              ),
            ),
            /*SizedBox(height: 20),
            Txt(
              txt: "or",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
              child: ListTile(
                tileColor: Colors.blue.shade800,
                leading: Image(
                  image: AssetImage(
                    "assets/images/icons/fb_icon.png",
                  ),
                  //color: null,
                  width: 20,
                  height: 20,
                ),
                minLeadingWidth: 0,
                title: MaterialButton(
                  child: Container(
                    width: double.infinity,
                    child: Align(
                      alignment: Alignment(-1, 0),
                      child: Text(
                        "Continue with Facebook",
                      ),
                    ),
                  ),
                  onPressed: () async {
                    //
                  },
                ),
              ),
            ),*/
          ],
        ),
      ),
    );
  }

  //  ***************** RegView2  **********************

  drawRegView2() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);
    return Container(
      //decoration: MyTheme.bgBlueColor,
      width: getW(context),
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              UIHelper().drawAppBarTitle(
                  title: "Create Account",
                  callback: () {
                    setState(() {
                      isStep2 = false;
                    });
                  }),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // drawCompSwitch(),
                    SizedBox(height: 10),
                    DatePickerView(
                      cap: "Select Date of Birth",
                      dt: (dob == '') ? 'Select Date' : dob,
                      txtColor: Colors.black,
                      initialDate: dateDOBlast,
                      firstDate: dateDOBfirst,
                      lastDate: dateDOBlast,
                      topbotHeight: 8,
                      callback: (value) {
                        if (mounted) {
                          setState(() {
                            try {
                              dob = DateFormat('dd-MMM-yyyy')
                                  .format(value)
                                  .toString();
                            } catch (e) {
                              myLog(e.toString());
                            }
                          });
                        }
                      },
                    ),
                    SizedBox(height: 40),
                    drawGender(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawGender() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Txt(
            txt: "Gender",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            isBold: false,
            fontWeight: FontWeight.w400,
            txtAlign: TextAlign.start,
          ),
          //SizedBox(height: 10),
          Theme(
            data: MyTheme.radioThemeData,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.male;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.male,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Male",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.female;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.female,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Female",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
/*
  drawCompSwitch() {
    return Container(
      //color: MyTheme.brandColor,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 50),
                      child: Txt(
                        txt: "Do you have an adviser to assist you with your case? If not we can help you find one.",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: true,
                        //txtLineSpace: 1.5,
                      ),
                    ),
                  ),
                  //SizedBox(width: 10),
                  SwitchView(
                    onTxt: "Yes",
                    offTxt: "No",
                    value: _isSwitch,
                    onChanged: (value) {
                      _isSwitch = value;
                      if (mounted) {
                        setState(() {});
                      }
                    },
                  ),
                ],
              ),
            ),
            (_isSwitch)
                ? Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 30),
                        Txt(txt: "Company Name", txtColor: Colors.black, txtSize: MyTheme.txtSize, txtAlign: TextAlign.start, isBold: true),
                        SizedBox(height: 20),
                        InputBox(
                          ctrl: _compName,
                          lableTxt: "Birilliant Company",
                          kbType: TextInputType.text,
                          len: 100,
                          isPwd: false,
                        ),
                      ],
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }*/
}
