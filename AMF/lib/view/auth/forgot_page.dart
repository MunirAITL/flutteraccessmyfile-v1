import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/otp/verify_code_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../config/theme/MyTheme.dart';
import '../widgets/textfield/InputBox.dart';
import '../widgets/txt/Txt.dart';
import 'package:get/get.dart';

class ForgotPage extends StatefulWidget {
  final email;
  const ForgotPage({Key key, @required this.email}) : super(key: key);

  @override
  State<ForgotPage> createState() => _ForgotPageState();
}

class _ForgotPageState extends State<ForgotPage> with Mixin {
  final email = TextEditingController();
  final focusEmail = FocusNode();

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: MyTheme.bgColor4,
      statusBarIconBrightness: Brightness.dark,
    ));
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    email.dispose();
    super.dispose();
  }

  appInit() async {
    email.text = widget.email;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor4,
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      child: Container(
        width: getW(context),
        child: Column(
          children: [
            UIHelper().drawAppBarTitle(
                title: null,
                callback: () {
                  Get.back();
                }),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 30),
                  Center(
                    child: Column(
                      children: [
                        Txt(
                          txt: "Forgot Password?",
                          txtColor: MyTheme.bgColor3,
                          txtSize: MyTheme.txtSize + .2,
                          txtAlign: TextAlign.center,
                          isBold: true,
                        ),
                        SizedBox(height: 20),
                        Txt(
                          txt:
                              "Please enter your Email address\nassociated with your account.",
                          txtColor: MyTheme.lblackColor,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: false,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 40),
                  InputBox(
                    context: context,
                    ctrl: email,
                    lableTxt: "Email",
                    kbType: TextInputType.emailAddress,
                    inputAction: TextInputAction.done,
                    focusNode: focusEmail,
                    len: 50,
                    autofocus: true,
                  ),
                  SizedBox(height: 20),
                  MMBtn(
                      txt: "RESET PASSWORD",
                      bgColor: MyTheme.bgColor3,
                      txtColor: MyTheme.cyanColor,
                      width: getW(context),
                      height: getHP(context, 7),
                      callback: () {
                        if (UserProfileVal().isEmailOK(
                            context, email, "Invalid email entered")) {
                          Get.to(() => VerifyCodePage(email: email.text));
                        }
                      }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _drawSocialLoginBtn() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Row(
            children: [
              Expanded(child: Divider(color: Colors.grey)),
              Txt(
                  txt: "or you can use",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize - .6,
                  txtAlign: TextAlign.center,
                  isBold: false),
              Expanded(child: Divider(color: Colors.grey)),
            ],
          ),
        ),
        SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 5, bottom: 5, left: 10, right: 10),
                child: Image.asset(
                  "assets/images/icons/google_btn.png",
                  //fit: BoxFit.fill,
                  width: 100,
                  height: 25,
                ),
              ),
            ),
            SizedBox(width: 20),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 5, bottom: 5, left: 10, right: 10),
                child: Image.asset(
                  "assets/images/icons/fb_btn.png",
                  //fit: BoxFit.fill,
                  width: 100,
                  height: 25,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  _drawSignup() {
    return Center(
      child: RichText(
        text: new TextSpan(
          children: [
            new TextSpan(
              text: 'Don\'t have an account?',
              style: new TextStyle(color: Colors.black),
            ),
            new TextSpan(
              text: ' Sign up ',
              style: new TextStyle(color: MyTheme.bgColor3),
              recognizer: new TapGestureRecognizer()
                ..onTap = () {
                  //Get.to(() => SigninPage());
                },
            ),
          ],
        ),
      ),
    );
  }
}
