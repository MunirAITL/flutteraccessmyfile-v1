import 'dart:convert';

import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/otp/sms1/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms1/SendOtpNotiAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/otp/sms_page3.dart';
import 'package:aitl/view/splash/welcome_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/textfield/InputMobFlagBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_string/json_string.dart';

class Sms2Page extends StatefulWidget {
  final String mobile;
  Sms2Page({Key key, this.mobile = ''}) : super(key: key);
  @override
  State createState() => _Sms2PageState();
}

class _Sms2PageState extends State<Sms2Page> with APIStateListener, Mixin {
  Color btnBgColor;
  Color btnTxtColor;

  final mobile = TextEditingController();
  String countryDialCode = AppDefine.COUNTRY_DIALCODE;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.otp_post &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final otpId =
                (model as MobileUserOtpPostAPIModel).responseData.userOTP.id;
            APIViewModel().req<SendOtpNotiAPIModel>(
                context: context,
                url: APIAuthCfg.SEND_OTP_NOTI_URL
                    .replaceAll("#otpId#", otpId.toString()),
                reqType: ReqType.Get,
                callback: (model2) {
                  if (model2 != null && mounted) {
                    if (model2.success) {
                      Get.to(
                        () => Sms3Page(
                          mobileUserOTPModel: model.responseData.userOTP,
                        ),
                      );
                    } else {
                      try {
                        final err = model2.messages.postUserotp[0].toString();
                        showToast(context: context, msg: err);
                      } catch (e) {
                        myLog(e.toString());
                      }
                    }
                  }
                });
          } else {
            try {
              final err = model.messages.postUserotp[0].toString();
              showToast(context: context, msg: err);
            } catch (e) {
              myLog(e.toString());
            }
          }
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    mobile.dispose();
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      if (widget.mobile.length == 0) {
        btnBgColor = Colors.grey.shade300;
        btnTxtColor = Colors.black;
      } else {
        btnBgColor = MyTheme.bgDark;
        btnTxtColor = Colors.white;
      }

      mobile.text = Common.stripCountryCodePhone(widget.mobile);
      mobile.addListener(() {
        if (mounted) {
          if (mobile.text.trim().length < UserProfileVal.PHONE_LIMIT) {
            btnBgColor = Colors.grey.shade300;
            btnTxtColor = Colors.black;
          } else {
            btnBgColor = MyTheme.bgDark;
            btnTxtColor = Colors.white;
          }
          setState(() {});
        }
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgLColor,
        /*appBar: AppBar(
          //automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
        ),*/
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            SizedBox(width: 10),
            IconButton(
                onPressed: () {
                  if (widget.mobile == null)
                    Get.back();
                  else
                    Get.offAll(() => WelcomePage());
                },
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ))
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Txt(
                txt: "Enter your mobile number",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.start,
                //txtLineSpace: 1.0,
                isBold: false,
              ),
            ),
            SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: InputMobFlagBox(
                ctrl: mobile,
                lableTxt: "Mobile number",
                len: 15,
                getCountryCode: (code) {
                  countryDialCode = code.toString();
                  PrefMgr.shared.setPrefStr("countryName", code.code);
                  PrefMgr.shared.setPrefStr("countryCode", code.toString());
                },
              ),
            ),
            SizedBox(height: 20),
            Center(
              child: MMBtn(
                txt: "Next",
                txtColor: btnTxtColor,
                bgColor: btnBgColor,
                width: getWP(context, 90),
                height: getHP(context, MyTheme.btnHpa),
                radius: 5,
                callback: () async {
                  if (mobile.text.trim().length >= UserProfileVal.PHONE_LIMIT) {
                    final param = {
                      "MobileNumber": "+" +
                          countryDialCode.replaceAll("+", "") +
                          (mobile.text.trim().replaceAll(countryDialCode, "")),
                      "OTPCode": "",
                      "Status": 101,
                      "UserId": (userData.userModel != null)
                          ? userData.userModel.id
                          : 0,
                    };
                    myLog(param);
                    final jsonString = JsonString(json.encode(param));
                    myLog(jsonString.source);
                    APIViewModel().req<MobileUserOtpPostAPIModel>(
                      context: context,
                      apiState:
                          APIState(APIType.otp_post, this.runtimeType, null),
                      url: APIAuthCfg.LOGIN_MOBILE_OTP_POST_URL,
                      reqType: ReqType.Post,
                      param: param,
                    );
                  } else {
                    showToast(
                      context: context,
                      msg: 'Please enter your valid phone number',
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}
