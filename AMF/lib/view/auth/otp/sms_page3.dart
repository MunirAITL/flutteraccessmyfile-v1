import 'dart:io';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/otp/sms1/MobileUserOTPModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/splash/welcome_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/textfield/OTPTextField.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Sms3Page extends StatefulWidget {
  final bool isBack;
  final MobileUserOTPModel mobileUserOTPModel;

  const Sms3Page({
    Key key,
    @required this.mobileUserOTPModel,
    this.isBack = false,
  }) : super(key: key);

  @override
  State createState() => _Sms3ScreenState();
}

class _Sms3ScreenState extends State<Sms3Page>
    with Mixin, APIStateListener, SingleTickerProviderStateMixin {
  String countryCode = "+44";
  String countryName = "GB";

  String otpCode;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.otp_put &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            await APIViewModel().req<LoginRegOtpFBAPIModel>(
                context: context,
                url: APIAuthCfg.LIGIN_REG_FB_MOBILE,
                reqType: ReqType.Post,
                param: {
                  "MobileNumber": (model as MobileUserOtpPutAPIModel)
                      .responseData
                      .userOTP
                      .mobileNumber,
                  "OTPCode": otpCode,
                  "DeviceType": (Platform.isAndroid) ? 'Android' : 'iOS',
                  "Persist": true,
                },
                callback: (model2) async {
                  try {
                    await DBMgr.shared
                        .setUserProfile(user: model2.responseData.user);
                    await userData.setUserModel();
                  } catch (e) {}
                  Get.offAll(() => DashboardPage());
                });
          } else {
            try {
              final err = model.errorMessages.postUserotp[0].toString();
              showToast(context: context, msg: err);
            } catch (e) {}
          }
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;

          countryCode = cd;
        });
      }
    } catch (e) {
      print("Sms2 screen country code and name problem ");
    }
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgDark,
        body: drawLayout() /* add child content here */,
      ),
    );
  }

  drawLayout() {
    final box = getW(context) / 8;
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: getHP(context, 2)),
              child: Align(
                alignment: Alignment.centerRight,
                child: IconButton(
                    onPressed: () {
                      Get.back();
                    },
                    icon: Icon(
                      Icons.close,
                      color: Colors.white,
                      size: 30,
                    )),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: getHP(context, 10)),
              child: Container(
                width: getW(context),
                height: getHP(context, 85),
                decoration: BoxDecoration(
                  color: MyTheme.bgLColor,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 10),
                      Txt(
                        txt: "Verify Account",
                        txtColor: HexColor.fromHex("#1E263C"),
                        txtSize: MyTheme.txtSize + .5,
                        txtAlign: TextAlign.center,
                        isBold: true,
                      ),
                      SizedBox(height: 20),
                      Txt(
                        txt: "On your phone number, please replace it",
                        txtColor: HexColor.fromHex("#6B7285"),
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.center,
                        isBold: false,
                      ),
                      SizedBox(height: 35),
                      Container(
                          width: getWP(context, 35),
                          height: getWP(context, 35),
                          child:
                              Image.asset("assets/images/icons/sms3_ico.png")),
                      SizedBox(height: 35),
                      OTPTextField(
                        //numberOfFields: 6,
                        length: 6,
                        width: getW(context),
                        fieldWidth: box,
                        onChanged: (v) {},
                        onCompleted: (v) {
                          otpCode = v;
                          if (otpCode.length == 6) {
                            //signInWithPhoneNumber(otpCode);
                            APIViewModel().req<MobileUserOtpPutAPIModel>(
                              context: context,
                              apiState: APIState(
                                  APIType.otp_put, this.runtimeType, null),
                              url: APIAuthCfg.LOGIN_MOBILE_OTP_PUT_URL,
                              reqType: ReqType.Put,
                              param: {
                                "MobileNumber": countryCode +
                                    widget.mobileUserOTPModel.mobileNumber,
                                "OTPCode": otpCode,
                                "UserId": (userData.userModel != null)
                                    ? userData.userModel.id
                                    : 0,
                                "DeviceType":
                                    (Platform.isAndroid) ? 'Android' : 'iOS',
                                "Persist": true,
                              },
                            );
                          }
                        },
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold),
                        //borderColor: Color(0xFF512DA8),
                        //textStyle: TextStyle(
                        //color: Colors.black,
                        //fontSize: 20,
                        //fontWeight: FontWeight.bold),
                        //set to true to show as box or false to show as dash
                        //showFieldAsBox: true,
                        //runs when a code is typed in
                        //onCodeChanged: (String code) {
                        //handle validation or checks here
                        // },
                        //runs when every textfield is filled
                      ),
                      SizedBox(height: 15),
                      Flexible(
                        child: TextButton(
                          onPressed: () {
                            Get.back();
                          },
                          style: TextButton.styleFrom(
                            primary: Colors.blueAccent,
                          ),
                          child: Text(
                            'Didn\'t get the code?',
                            style: TextStyle(fontSize: 14),
                          ),
                        ),
                      ),
                      SizedBox(height: 15),
                      MMBtn(
                        txt: (!widget.isBack) ? "Continue" : "Back",
                        //bgColor: HexColor.fromHex("#F29A69"),
                        height: getHP(context, 7),
                        width: getW(context),
                        radius: 5,
                        callback: () {
                          if (!widget.isBack) {
                            Get.offAll(() => WelcomePage());
                          } else {
                            Get.back();
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
