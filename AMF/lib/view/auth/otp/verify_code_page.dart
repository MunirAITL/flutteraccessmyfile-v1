import 'package:aitl/mixin.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../config/server/APIHrefLoginCfg.dart';
import '../../../config/server/Server.dart';
import '../../../config/theme/MyTheme.dart';
import '../../../data/app_data/PrefMgr.dart';
import '../../../data/app_data/UserData.dart';
import '../../../data/db/DBMgr.dart';
import '../../../data/model/auth/hreflogin/PostEmailOtpAPIModel.dart';
import '../../../data/model/auth/hreflogin/SendUserEmailOtpAPIModel.dart';
import '../../../data/model/auth/otp/sms2/LoginRegOtpFBAPIModel.dart';
import '../../../data/network/NetworkMgr.dart';
import '../../../view_model/api/api_view_model.dart';
import '../../widgets/textfield/InputBox.dart';
import '../../widgets/txt/Txt.dart';
import 'package:get/get.dart';

class VerifyCodePage extends StatefulWidget {
  final email;
  const VerifyCodePage({Key key, @required this.email}) : super(key: key);

  @override
  State<VerifyCodePage> createState() => _VerifyCodePageState();
}

class _VerifyCodePageState extends State<VerifyCodePage>
    with SingleTickerProviderStateMixin, Mixin {
  String countryCode = "+44";
  String countryName = "GB";

  bool isLoading = false;

  // Variables
  static const int SMS_AUTH_CODE_LEN = 4;
  int _currentDigit;
  int _firstDigit;
  int _secondDigit;
  int _thirdDigit;
  int _fourthDigit;

  var userId = 0;
  var otpCode = "";

  getFullPhoneNumber({String countryCodeTxt, String number}) {
    var fullNumber = countryCodeTxt + number;

    return fullNumber;
  }

  sendEmailOtpAPI() async {
    try {
      await APIViewModel().req<PostEmailOtpAPIModel>(
        context: context,
        url: APIHrefLoginCfg.POSTEMAILOTP_POST_URL,
        reqType: ReqType.Post,
        param: {
          "Email": widget.email,
          "Status": "101",
        },
        callback: (model2) async {
          if (mounted) {
            if (model2 != null) {
              if (model2.success) {
                await APIViewModel().req<SendUserEmailOtpAPIModel>(
                    context: context,
                    url: APIHrefLoginCfg.SENDUSEREMAILOTP_GET_URL.replaceAll(
                        "#otpId#", model2.responseData.userOTP.id.toString()),
                    reqType: ReqType.Get,
                    callback: (model3) async {
                      if (mounted) {
                        if (model3 != null) {
                          if (model3.success) {
                            showToast(
                                context: context, msg: 'OTP sent successfully');
                          } else {
                            final err =
                                model3.messages.getSenduseremailotplogin[0];
                            showToast(context: context, msg: err, which: 0);
                          }
                          clearOtp();
                        }
                      }
                    });
              } else {
                final err = model2.messages.postUserotp[0];
                showToast(context: context, msg: err, which: 0);
              }
            }
          }
        },
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: MyTheme.bgColor4,
      statusBarIconBrightness: Brightness.dark,
    ));
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void clearOtp() {
    _fourthDigit = null;
    _thirdDigit = null;
    _secondDigit = null;
    _firstDigit = null;
    if (mounted) {
      setState(() {});
    }
  }

  appInit() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;

          countryCode = cd;
        });
      }
    } catch (e) {
      print("Sms2 screen country code and name problem ");
    }
    try {
      userId = userData.userModel.id;
    } catch (e) {
      debugPrint("User id getting problem $userId = " + e.toString());
    }
    sendEmailOtpAPI();
  }

  // Return "OTP" input field
  get _getInputField {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _otpTextField(_firstDigit),
        SizedBox(width: 10),
        _otpTextField(_secondDigit),
        SizedBox(width: 10),
        _otpTextField(_thirdDigit),
        SizedBox(width: 10),
        _otpTextField(_fourthDigit),
      ],
    );
  }

  // Returns "Otp" keyboard
  get _getOtpKeyboard {
    return new Container(
        height: getW(context) - 100,
        color: Color(0xFFE9EAEE),
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "1",
                      onPressed: () {
                        _setCurrentDigit(1);
                      }),
                  _otpKeyboardInputButton(
                      label: "2",
                      onPressed: () {
                        _setCurrentDigit(2);
                      }),
                  _otpKeyboardInputButton(
                      label: "3",
                      onPressed: () {
                        _setCurrentDigit(3);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "4",
                      onPressed: () {
                        _setCurrentDigit(4);
                      }),
                  _otpKeyboardInputButton(
                      label: "5",
                      onPressed: () {
                        _setCurrentDigit(5);
                      }),
                  _otpKeyboardInputButton(
                      label: "6",
                      onPressed: () {
                        _setCurrentDigit(6);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "7",
                      onPressed: () {
                        _setCurrentDigit(7);
                      }),
                  _otpKeyboardInputButton(
                      label: "8",
                      onPressed: () {
                        _setCurrentDigit(8);
                      }),
                  _otpKeyboardInputButton(
                      label: "9",
                      onPressed: () {
                        _setCurrentDigit(9);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardActionButton(
                      label: new Icon(
                        Icons.backspace,
                        color: Color(0xFF8C8C8C),
                        size: 30,
                      ),
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            otpCode = "";
                            if (_fourthDigit != null) {
                              _fourthDigit = null;
                            } else if (_thirdDigit != null) {
                              _thirdDigit = null;
                            } else if (_secondDigit != null) {
                              _secondDigit = null;
                            } else if (_firstDigit != null) {
                              _firstDigit = null;
                            }
                          });
                        }
                      }),
                  _otpKeyboardInputButton(
                      label: "0",
                      onPressed: () {
                        _setCurrentDigit(0);
                      }),
                  _otpKeyboardActionButton(
                      label: Container(
                        decoration: BoxDecoration(
                          color: MyTheme.bgColor3,
                          shape: BoxShape.circle,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: new Icon(
                            Icons.keyboard_return,
                            color: Colors.white,
                            size: 25,
                          ),
                        ),
                      ),
                      onPressed: () {
                        if (mounted) {
                          if (otpCode.length == SMS_AUTH_CODE_LEN) {
                            callOTPApi();
                          }
                        }
                      }),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        /*appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          iconTheme: IconThemeData(color: MyTheme.titleColor),
          backgroundColor: MyTheme.bgColor2,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          centerTitle: true,
        ),*/
        bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          child: _getOtpKeyboard,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        // width: getW(context),
        // height: getH(context),
        child: Column(
          // mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            UIHelper().drawAppBarTitle(
                title: null,
                callback: () {
                  Get.back();
                }),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
              child: Column(
                children: [
                  Txt(
                    txt: "Verify your code",
                    txtColor: MyTheme.bgColor3,
                    txtSize: MyTheme.txtSize + 1.2,
                    txtAlign: TextAlign.center,
                    isBold: true,
                  ),
                  SizedBox(height: 20),
                  Text(
                      "Please enter the 4 digit code\nsent to ${widget.email.toString()}",
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize),
                          color: MyTheme.lblackColor,
                          fontWeight: FontWeight.normal,
                          height: 1.5)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(
                        onPressed: () async {
                          otpCode = "";
                          sendEmailOtpAPI();
                        },
                        child: Text(
                          "Resend code",
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 14,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            SizedBox(height: 10),
            _getInputField,
            /*SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: MMBtn(
                  txt: "CONFIRMATION",
                  bgColor: otpCode.length < SMS_AUTH_CODE_LEN
                      ? Color(0xFF71ADB5).withOpacity(.5)
                      : MyTheme.bgColor3,
                  txtColor: otpCode.length < SMS_AUTH_CODE_LEN
                      ? MyTheme.bgColor3
                      : MyTheme.cyanColor,
                  width: getW(context),
                  height: getHP(context, 7),
                  callback: () {}),
            ),*/
          ],
        ),
      ),
    );
  }

  // Returns "Otp custom text field"
  Widget _otpTextField(int digit) {
    double boxW = (getWP(context, 100) / SMS_AUTH_CODE_LEN) - 30;
    return new Container(
      width: boxW,
      height: boxW,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.grey, width: 1),
        //color: MyTheme.brandColor,
      ),
      child: Text(
        digit != null ? digit.toString() : "",
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.black87,
          fontSize: 40,
          fontFamily: "Dosis Regular",
        ),
      ),
    );
  }

  // Returns "Otp keyboard input Button"
  Widget _otpKeyboardInputButton({String label, VoidCallback onPressed}) {
    final wbox = (getW(context) / 3) - 40;
    return Padding(
      padding: const EdgeInsets.all(2),
      child: new Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        color: Colors.white,
        elevation: 2,
        child: new InkWell(
          onTap: onPressed,
          child: new Container(
            height: wbox,
            width: wbox,
            decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(40.0),
                shape: BoxShape.rectangle),
            child: new Center(
              child: Text(
                label,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  fontFamily: "Dosis Bold",
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  // Returns "Otp keyboard action Button"
  _otpKeyboardActionButton({Widget label, VoidCallback onPressed}) {
    return new InkWell(
      onTap: onPressed,
      borderRadius: new BorderRadius.circular(40.0),
      child: new Container(
        height: 80.0,
        width: 80.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: new Center(
          child: label,
        ),
      ),
    );
  }

  // Current digit
  void _setCurrentDigit(int i) {
    if (mounted) {
      setState(() {
        _currentDigit = i;
        if (_firstDigit == null) {
          _firstDigit = _currentDigit;
        } else if (_secondDigit == null) {
          _secondDigit = _currentDigit;
        } else if (_thirdDigit == null) {
          _thirdDigit = _currentDigit;
        } else if (_fourthDigit == null) {
          _fourthDigit = _currentDigit;

          final otpCode2 = _firstDigit.toString() +
              _secondDigit.toString() +
              _thirdDigit.toString() +
              _fourthDigit.toString();
          if (otpCode2.length == SMS_AUTH_CODE_LEN) {
            otpCode = otpCode2;
            callOTPApi();
          }
        }
      });
    }
  }

  void callOTPApi() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
      await APIViewModel().req<LoginRegOtpFBAPIModel>(
        context: context,
        url: APIHrefLoginCfg.LOGINEMAILOTPBYMOBAPP_POST_URL,
        reqType: ReqType.Post,
        param: {
          "Email": widget.email,
          "OTPCode": otpCode,
        },
        callback: (model) async {
          if (mounted) {
            if (model != null) {
              if (model.success) {
                await DBMgr.shared
                    .setUserProfile(user: model.responseData.user);
                await userData.setUserModel();
                if (userData.userModel.communityID.toString() == "1") {
                  //Get.offAll(() => MainCustomerScreen());
                } else {
                  //Get.offAll(() => MainIntroducerScreen());
                }
              } else {
                final err = model.errorMessages.login[0];
                showToast(context: context, msg: err, which: 0);
              }
            }
            Future.delayed(Duration(seconds: 4), () {
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            });
          }
        },
      );
    }
  }
}
