import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';

class RattingPage extends StatefulWidget {
  const RattingPage({Key key}) : super(key: key);

  @override
  _RattingPageState createState() => _RattingPageState();
}

class _RattingPageState extends BaseDashboard<RattingPage> {
  double rate = 0;
  final TextEditingController cmt = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    cmt.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor4,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  Widget drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            UIHelper().drawAppBarTitle(
                title: "Rate The App",
                callback: () {
                  Get.back();
                }),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: RatingBar.builder(
                      initialRating: rate,
                      itemCount: 5,
                      itemBuilder: (context, index) {
                        switch (index) {
                          case 0:
                            return Icon(Icons.sentiment_very_dissatisfied,
                                color: Colors.red, size: 20);
                          case 1:
                            return Icon(Icons.sentiment_dissatisfied,
                                color: Colors.orange, size: 20);
                          case 2:
                            return Icon(Icons.sentiment_neutral,
                                color: Colors.amber, size: 20);
                          case 3:
                            return Icon(Icons.sentiment_satisfied,
                                color: Colors.lightGreen, size: 20);
                          case 4:
                            return Icon(Icons.sentiment_very_satisfied,
                                color: Colors.green, size: 20);
                          default:
                            return Container();
                        }
                      },
                      onRatingUpdate: (rating) {
                        rate = rating;
                      },
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    //margin: const EdgeInsets.only(left: 10.0, right: 10, top: 10),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: TextField(
                      controller: cmt,
                      minLines: 5,
                      maxLines: 10,
                      //expands: true,
                      autocorrect: false,
                      maxLength: 500,
                      keyboardType: TextInputType.multiline,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                      ),
                      decoration: InputDecoration(
                        hintText: 'Your comments',
                        hintStyle: TextStyle(color: Colors.grey),
                        //labelText: 'Your message',
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.only(
                            left: 15, bottom: 11, top: 11, right: 15),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  MMBtn(
                      txt: "Submit",
                      width: getW(context),
                      height: getHP(context, 7),
                      bgColor: MyTheme.btnRedColor,
                      txtColor: MyTheme.btnTxtColor,
                      radius: 10,
                      callback: () {}),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
