import 'package:aitl/Mixin.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../widgets/btn/MMBtnLeftIconDashBoard.dart';
import 'ResolutionScreen.dart';

class SupportCentreScreen extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: MyTheme.bgColor4, body: drawLayout(context)));
  }

  drawLayout(context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            UIHelper().drawAppBarTitle(
                title: "Support",
                callback: () {
                  Get.back();
                }),
            SizedBox(height: 40),
            Image.asset(
              "assets/images/img/support.png",
              width: getWP(context, 72),
              height: getHP(context, 35),
              fit: BoxFit.fill,
            ),
            Padding(
              padding: const EdgeInsets.all(40),
              child: Column(
                children: [
                  Card(
                    color: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: getW(context),
                      color: Colors.transparent,
                      child: MMBtnLeftIconDashBoard(
                        imageFile: null,
                        bgColor: Colors.white,
                        textColor: MyTheme.lblackColor,
                        txt: "CALL: " + AppDefine.SUPPORT_CALL1,
                        height: getHP(context, 7),
                        width: getW(context),
                        radius: 10,
                        callback: () {
                          launch("tel://" + AppDefine.SUPPORT_CALL1);
                        },
                      ),
                    ),
                  ),
                  Card(
                    color: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: getW(context),
                      color: Colors.transparent,
                      child: MMBtnLeftIconDashBoard(
                        imageFile: null,
                        bgColor: Colors.white,
                        textColor: MyTheme.lblackColor,
                        txt: "SEND MESSAGE",
                        height: getHP(context, 7),
                        width: getW(context),
                        radius: 10,
                        callback: () {
                          Get.to(
                            () => ResolutionScreen(),
                          );
                        },
                      ),
                    ),
                  ),
                  Card(
                    color: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: getW(context),
                      color: Colors.transparent,
                      child: MMBtnLeftIconDashBoard(
                        imageFile: null,
                        bgColor: MyTheme.btnRedColor,
                        txt: "EMAIL: " + AppDefine.SUPPORT_EMAIL,
                        textColor: MyTheme.btnTxtColor,
                        height: getHP(context, 7),
                        width: getW(context),
                        radius: 10,
                        callback: () {
                          launch("mailto:" + AppDefine.SUPPORT_EMAIL);
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
