import 'dart:developer';
import 'dart:io';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/res/ResolutionAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/helper/ResolutionHelper.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:device_info/device_info.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ResolutionScreen extends StatefulWidget {
  @override
  State createState() => _ResolutionScreenState();
}

class _ResolutionScreenState extends State<ResolutionScreen>
    with Mixin, APIStateListener {
  final TextEditingController _desc = TextEditingController();

  static const int MAX_FILE_UPLOAD = 5;

  List<MediaUploadFilesModel> listMediaUploadFilesModel = [];
  ResolutionHelper resolutionHelper;

  String deviceName = "";

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listMediaUploadFilesModel.add(model.responseData.images[0]);
            setState(() {});
          } else {
            final err = model.errorMessages.upload_pictures[0].toString();
            showToast(context: context, msg: err);
          }
        }
      } else if (apiState.type == APIType.resolution &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            _desc.text = "";
            listMediaUploadFilesModel.clear();
            final msg =
                "Thank you for submitting the support ticket.\n\nOur support member will contact you as soon as possible."; // model.messages.resolution_post[0].toString();
            showToast(context: context, msg: msg, which: 1);
            setState(() {});
          } else {
            final err = model.errorMessages.upload_pictures[0].toString();
            showToast(context: context, msg: err);
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }

    _desc.dispose();
    resolutionHelper = null;
    listMediaUploadFilesModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  /*browseFiles() async {
    try {
      if (listMediaUploadFilesModel.length >= MAX_FILE_UPLOAD) {
        showToast(
            context: context,
            msg: "Maximum file upload limit is " +
                MAX_FILE_UPLOAD.toString() +
                " files");
        return;
      }
      FilePickerResult result = await FilePicker.platform.pickFiles(
        allowMultiple: false,
        type: FileType.custom,
        allowedExtensions: [
          'jpg',
          'png',
        ],
        // type: FileType.any,
        /*allowedExtensions: [
                      'jpg',
                      'jpeg',
                      'png',
                      'pdf',
                      'doc',
                      'docx'
                    ],*/
      );

      if (result != null) {
        //wsMediaUploadFileAPI(File(result.files.single.path));
        if (listMediaUploadFilesModel.length >= MAX_FILE_UPLOAD) {
          showToast(
              context: context,
              msg: "Maximum file upload limit is " +
                  MAX_FILE_UPLOAD.toString() +
                  " files");
          return;
        }
        await APIViewModel().upload(
          context: context,
          apiState: APIState(APIType.media_upload_file, this.runtimeType, null),
          file: File(result.files.single.path),
        );
      }
    } catch (e) {
      myLog(e.toString());
    }
  }*/

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      resolutionHelper = ResolutionHelper();
      final deviceInfo = DeviceInfoPlugin();

      if (Platform.isAndroid) {
        final androidInfo = await deviceInfo.androidInfo;
        deviceName = androidInfo.model;
      } else {
        final iosInfo = await deviceInfo.iosInfo;
        deviceName = iosInfo.utsname.machine;
      }
      setState(() {});
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor4,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return (resolutionHelper == null)
        ? SizedBox()
        : Container(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  UIHelper().drawAppBarTitle(
                      title: "Send Message",
                      callback: () {
                        Get.back();
                      }),
                  SizedBox(height: 20),
                  Image.asset(
                    "assets/images/img/send_msg.png",
                    width: getWP(context, 80),
                    height: getHP(context, 35),
                    fit: BoxFit.cover,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        /* Padding(
                          padding: const EdgeInsets.only(
                              left: 10, right: 10, top: 20, bottom: 20),
                          child: DropDownListDialog(
                            context: context,
                            title: resolutionHelper.opt.title,
                            ddTitleList: resolutionHelper.dd,
                            callback: (optionItem) {
                              resolutionHelper.opt = optionItem;
                              setState(() {});
                            },
                          ),
                        ),
                        SizedBox(
                            width: getWP(context, 30),
                            height: getWP(context, 30),
                            child: Image.asset(
                              "assets/images/img/contact_us_header.png",
                              fit: BoxFit.contain,
                              //width: getWP(context, 40),
                              //height: getWP(context, 50),
                            )),*/
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Txt(
                              txt: "Type your message here",
                              txtColor: MyTheme.lblackColor,
                              txtSize: MyTheme.txtSize - .3,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ),
                        Container(
                          margin: const EdgeInsets.only(
                              left: 10.0, right: 10, top: 10),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey, width: .8),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          child: TextField(
                            controller: _desc,
                            minLines: 5,
                            maxLines: 10,
                            //expands: true,
                            autocorrect: false,
                            maxLength: 500,
                            keyboardType: TextInputType.multiline,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: getTxtSize(
                                  context: context, txtSize: MyTheme.txtSize),
                            ),
                            decoration: InputDecoration(
                              //hintText: 'Description',
                              //hintStyle: TextStyle(color: Colors.grey),
                              //labelText: 'Your message',
                              counterText: "",
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  left: 15, bottom: 11, top: 11, right: 15),
                            ),
                          ),
                        ),
                        /*Padding(
                          padding: const EdgeInsets.only(
                              left: 10, right: 10, top: 20),
                          child: Txt(
                              txt: "Attachments - " +
                                  listMediaUploadFilesModel.length.toString() +
                                  ' files added',
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ),
                        for (MediaUploadFilesModel model
                            in listMediaUploadFilesModel)
                          Container(
                            color: Colors.transparent,
                            child: ListTile(
                              leading: IconButton(
                                  icon: Icon(
                                    Icons.remove_circle,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    listMediaUploadFilesModel.remove(model);
                                    setState(() {});
                                  }),
                              title: Align(
                                alignment: Alignment(-1.2, 0),
                                child: Txt(
                                    txt: model.name.split('/').last ?? '',
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.center,
                                    isBold: false),
                              ),
                            ),
                          ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: DottedBorder(
                            borderType: BorderType.RRect,
                            radius: Radius.circular(5),
                            padding: EdgeInsets.all(5),
                            color: Colors.grey,
                            strokeWidth: 1,
                            child: GestureDetector(
                              onTap: () async {
                                await browseFiles();
                              },
                              child: Container(
                                height: getHP(context, 5),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Txt(
                                          txt: "Add up to 5 files",
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize,
                                          txtAlign: TextAlign.center,
                                          isBold: false),
                                    ),
                                    Flexible(
                                      child: Icon(
                                        Icons.attach_file,
                                        color: Colors.grey,
                                        size: 25,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),*/
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 10, left: 10, right: 10),
                          child: MMBtn(
                            txt: "SUBMIT",
                            width: getW(context),
                            height: getHP(context, 7),
                            bgColor: MyTheme.btnRedColor,
                            txtColor: MyTheme.btnTxtColor,
                            radius: 10,
                            callback: () {
                              /*if (resolutionHelper.opt.id == null) {
                                showToast(
                                    context: context,
                                    msg:
                                        "Please choose ticket type from the list");
                                return;
                              } else*/

                              if (_desc.text.trim().length == 0) {
                                showToast(
                                    context: context,
                                    msg: "Please enter message");
                                return;
                              }
                              List<String> listFileUrl = [];
                              for (MediaUploadFilesModel model
                                  in listMediaUploadFilesModel) {
                                listFileUrl.add(model.url);
                              }

                              final desc = _desc.text.trim() +
                                  "\n\n" +
                                  "From " +
                                  ((Platform.isAndroid) ? 'Android' : 'iPhone');
                              print(deviceName);
                              APIViewModel().req<ResolutionAPIModel>(
                                context: context,
                                apiState: APIState(
                                    APIType.resolution, this.runtimeType, null),
                                url: ResCfg.RES_POST_URL,
                                reqType: ReqType.Post,
                                param: {
                                  "Description":
                                      'Contact from App : ' + _desc.text.trim(),
                                  "InitiatorId": userData.userModel.id,
                                  "Remarks": "",
                                  "ResolutionType": "Other",
                                  "ServiceDate": DateTime.now().toString(),
                                  "Status": 101,
                                  "Title": resolutionHelper.opt.title,
                                  "FileUrl": listFileUrl.join(','),
                                  "UserId": userData.userModel.id,
                                },
                              );
                            },
                          ),
                        ),
                        SizedBox(height: getHP(context, 5)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}
