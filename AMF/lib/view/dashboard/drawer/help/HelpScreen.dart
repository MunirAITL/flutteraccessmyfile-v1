import 'package:aitl/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/btn/MMBtnLeftIconDashBoard.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'SupportCentreScreen.dart';

class HelpScreen extends StatefulWidget {
  @override
  State createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> with Mixin {
  final bottomViewHeight = 32;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(backgroundColor: MyTheme.bgColor4, body: drawLayout()));
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            UIHelper().drawAppBarTitle(
                title: "Help Centre",
                callback: () {
                  Get.back();
                }),
            SizedBox(height: 40),
            Image.asset(
              "assets/images/img/help_center.png",
              width: getWP(context, 72),
              height: getHP(context, 35),
              fit: BoxFit.fill,
            ),
            Padding(
              padding: const EdgeInsets.all(40),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Card(
                    color: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: getW(context),
                      color: Colors.transparent,
                      child: MMBtnLeftIconDashBoard(
                        imageFile: null,
                        bgColor: Colors.white,
                        textColor: MyTheme.lblackColor,
                        txt: "SUPPORT CENTRE",
                        height: getHP(context, 7),
                        width: getW(context),
                        radius: 10,
                        callback: () {
                          Get.to(
                            () => SupportCentreScreen(),
                          );
                        },
                      ),
                    ),
                  ),
                  Card(
                    color: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: getW(context),
                      color: Colors.transparent,
                      child: MMBtnLeftIconDashBoard(
                        imageFile: null,
                        bgColor: Colors.white,
                        txt: "TUTORIAL",
                        textColor: MyTheme.lblackColor,
                        height: getHP(context, 7),
                        width: getW(context),
                        radius: 10,
                        callback: () {
                          //Navigator.pop(context);
                          //StateProvider()
                          //.notify(ObserverState.STATE_CHANGED_tabbar1);
                        },
                      ),
                    ),
                  ),
                  Card(
                    color: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      width: getW(context),
                      color: Colors.transparent,
                      child: MMBtnLeftIconDashBoard(
                        imageFile: null,
                        bgColor: MyTheme.btnRedColor,
                        txt: "FAQ",
                        textColor: MyTheme.btnTxtColor,
                        height: getHP(context, 7),
                        width: getW(context),
                        radius: 10,
                        callback: () {
                          /*Get.to(() => WebScreen(
                              title: "FAQ",
                              url: Server.FAQ_URL,
                            ));*/
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
