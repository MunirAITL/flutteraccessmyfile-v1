import 'package:aitl/Mixin.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../profile/change_pwd_page.dart';
import '../profile/edit_profile_page.dart';
import 'NotiSettingsScreen.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State createState() => _SettingsScreenState();
}

class _SettingsScreenState extends BaseDashboard<SettingsScreen> {
  var listItems = [];

  @override
  void initState() {
    super.initState();
    listItems = [
      // "Edit Profile",
      {"head": "APP SETTINGS"},
      {
        "head": null,
        "title": "Profile Settings",
        "route": () => EditProfilePage()
      },
      {
        "head": null,
        "title": "Notification Settings",
        "route": () => NotiSettingsScreen()
      },
      {"head": null, "title": "Change PIN", "route": () => ChangePwdPage()},
      {"head": "GENERAL INFO"},
      //{"head": null, "title": "Help Center", "route": () => HelpScreen()},
      {
        "head": null,
        "title": "Terms & Conditions",
        "route": () => PDFDocumentPage(
              title: "Privacy",
              url: "https://mortgage-magic.co.uk/assets/img/privacy_policy.pdf",
            ),
      },
      {
        "head": null,
        "title": "Privacy Policy",
        "callback": () => openWSPrivacyPDF(context),
      },
      //"Test Notification"
    ];
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //userModel.countryofResidency = "ffff";
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        //resizeToAvoidBottomPadding: true,
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  elevation: 0,
                  backgroundColor: MyTheme.bgDark,
                  //backgroundColor: MyTheme.parallexToolbarColor,
                  iconTheme: IconThemeData(
                      color: (innerBoxIsScrolled)
                          ? Colors.black
                          : Colors.white //change your color here
                      ),
                  title: UIHelper().drawAppbarTitle(title: "Settings"),
                  centerTitle: false,
                  expandedHeight: getHP(context, 40),
                  floating: false,
                  pinned: true,
                  snap: false,
                  forceElevated: true,
                  flexibleSpace: FlexibleSpaceBar(
                    collapseMode: CollapseMode.parallax,
                    centerTitle: true,
                    background: drawUserProfileData(),
                  ),
                  //background:
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawUserProfileData() {
    final userModel = userData.userModel;
    return Container(
      decoration: BoxDecoration(color: MyTheme.bgDark),
      alignment: Alignment.center,
      child: ListView(
        primary: false,
        shrinkWrap: true,
        children: [
          SizedBox(height: 20),
          Container(
            decoration: MyTheme.picEmboseCircleDeco,
            child: CircleAvatar(
              radius: 50,
              backgroundColor: Colors.transparent,
              backgroundImage: new CachedNetworkImageProvider(
                MyNetworkImage.checkUrl(userModel.profileImageURL != null &&
                        userModel.profileImageURL.isNotEmpty
                    ? userModel.profileImageURL
                    : ServerUrls.MISSING_IMG),
              ),
            ),
          ),
          SizedBox(height: 15),
          Txt(
              txt: userModel.name,
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: true),
          SizedBox(height: 5),
          Txt(
              txt: userModel.mobileNumber,
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  drawLayout() {
    return Container(
      //decoration: MyTheme.bgBlueColor,
      width: getW(context),
      height: getH(context),
      child: ListView.builder(
        itemCount: listItems.length,
        itemBuilder: (context, index) {
          final item = listItems[index];
          final heading = item['head'];
          if (heading != null) {
            return Container(
              color: MyTheme.grayColor,
              width: getW(context),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Txt(
                    txt: heading,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .5,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            );
          } else {
            return GestureDetector(
              onTap: () async {
                if (item['route'] == null) {
                  Function.apply(item['callback'], []);
                } else {
                  Get.to(item['route']);
                }
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Card(
                  elevation: heading != null ? 0 : 1,
                  margin: EdgeInsets.zero,
                  clipBehavior: Clip.antiAlias,
                  child: ListTile(
                    title: Txt(
                        txt: item['title'],
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.black,
                      size: 20,
                    ),
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
