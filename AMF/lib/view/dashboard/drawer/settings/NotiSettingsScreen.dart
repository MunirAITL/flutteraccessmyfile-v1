import 'dart:ui';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/app/settings/NotiSettingsCfg.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/APINotiCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/noti/FcmTestNotiAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiSettingsAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiSettingsPostAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/BtnOutline.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotiSettingsScreen extends StatefulWidget {
  @override
  State createState() => _NotiSettingsScreenState();
}

class _NotiSettingsScreenState extends State<NotiSettingsScreen> with Mixin {
  NotiSettingsCfg notiSettingsCfg;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    notiSettingsCfg = null;
    super.dispose();
  }

  appInit() async {
    try {
      notiSettingsCfg = NotiSettingsCfg();
      var url = APINotiCfg.NOTI_SETTINGS_URL;
      url = url.replaceAll("#userId#", userData.userModel.id.toString());
      url = url.replaceAll(
          "#userCompanyId#", userData.userModel.userCompanyID.toString());
      await APIViewModel().req<NotiSettingsAPIModel>(
        context: context,
        url: url,
        reqType: ReqType.Get,
        callback: (model) async {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final caseUpdateMap = notiSettingsCfg.listOpt[0];
                  final caseRecomendationMap = notiSettingsCfg.listOpt[1];
                  final caseCompletedMap = notiSettingsCfg.listOpt[2];
                  final caseReminderMap = notiSettingsCfg.listOpt[3];
                  final userUpdateMap = notiSettingsCfg.listOpt[4];
                  final helpfulInfoMap = notiSettingsCfg.listOpt[5];
                  final updateNewsLetterMap = notiSettingsCfg.listOpt[6];

                  //  caseUpdate
                  caseUpdateMap['isEmail'] = model
                      .responseData.userNotificationSetting.isCaseUpdateEmail;
                  caseUpdateMap['isSms'] = model
                      .responseData.userNotificationSetting.isCaseUpdateSMS;
                  caseUpdateMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseUpdateNotification;
                  //  caseRecomendation
                  caseRecomendationMap['isEmail'] = model.responseData
                      .userNotificationSetting.isCaseRecomendationEmail;
                  caseRecomendationMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseRecomendationNotification;
                  //  caseCompleted
                  caseCompletedMap['isEmail'] = model.responseData
                      .userNotificationSetting.isCaseCompletedEmail;
                  caseCompletedMap['isSms'] = model
                      .responseData.userNotificationSetting.isCaseCompletedSMS;
                  caseCompletedMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseCompletedNotification;
                  //  caseReminder
                  caseReminderMap['isEmail'] = model
                      .responseData.userNotificationSetting.isCaseReminderEmail;
                  caseReminderMap['isSms'] = model
                      .responseData.userNotificationSetting.isCaseReminderSMS;
                  caseReminderMap['isPush'] = model.responseData
                      .userNotificationSetting.isCaseReminderNotification;
                  //  userUpdateMap
                  userUpdateMap['isEmail'] = model
                      .responseData.userNotificationSetting.isUserUpdateEmail;
                  userUpdateMap['isPush'] = model.responseData
                      .userNotificationSetting.isUserUpdateNotification;
                  //  helpfulInfoMap
                  helpfulInfoMap['isEmail'] = model.responseData
                      .userNotificationSetting.isHelpfulInformationEmail;
                  helpfulInfoMap['isPush'] = model.responseData
                      .userNotificationSetting.isHelpfulInformationNotification;
                  //  updateNewsLetterMap
                  updateNewsLetterMap['isEmail'] = model.responseData
                      .userNotificationSetting.isUpdateAndNewsLetterEmail;
                  updateNewsLetterMap['isPush'] = model
                      .responseData
                      .userNotificationSetting
                      .isUpdateAndNewsLetterNotification;

                  //notiSettingsCfg.listOpt[6] = updateNewsLetterMap;
                  //myLog(notiSettingsCfg.listOpt[6].toString());

                  setState(() {});
                } catch (e) {
                  myLog(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(
                        context: context,
                        msg: "Notifications Settings not found");
                  }
                } catch (e) {
                  myLog(e.toString());
                }
              }
            } catch (e) {
              myLog(e.toString());
            }
          }
        },
      );
    } catch (e) {}
    try {
      /*await NotiSettingsHelper().getSettings().then((listOpt) {
        if (listOpt != null) {
          notiSettingsCfg.listOpt = listOpt;
          setState(() {});
        } else {}
      });*/
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.bgDark,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Notification settings"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: getW(context),
      height: getH(context),
      child: (notiSettingsCfg == null)
          ? SizedBox()
          : Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Txt(
                              txt: "is it working ?".capitalizeFirstofEach,
                              txtColor: Colors.black87,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          BtnOutline(
                            txt: "Test it",
                            width: getWP(context, 22),
                            height: getHP(context, MyTheme.btnHpa - 1),
                            txtColor: Colors.black,
                            borderColor: MyTheme.bgDark,
                            callback: () async {
                              // showNotificationMessage("Mortgage Magic says hi!", "If you\'re seeing this, it means everything\'s OK!");

                              var url = APINotiCfg.FCM_TEST_NOTI_URL;
                              url = url.replaceAll(
                                  "#userId#", userData.userModel.id.toString());
                              await APIViewModel().req<FcmTestNotiAPIModel>(
                                context: context,
                                url: url,
                                reqType: ReqType.Get,
                                callback: (model) async {
                                  if (model != null && mounted) {
                                    try {
                                      if (model.success) {
                                        try {
                                          final msg = model.responseData
                                              .notification.description;
                                          showToast(context: context, msg: msg);
                                        } catch (e) {
                                          myLog(e.toString());
                                        }
                                      } else {
                                        try {
                                          final err = model
                                              .messages.pushMessage[0]
                                              .toString();
                                          showToast(
                                              context: context,
                                              msg: err,
                                              which: 0);
                                        } catch (e) {
                                          myLog(e.toString());
                                        }
                                      }
                                    } catch (e) {
                                      myLog(e.toString());
                                    }
                                  } else {
                                    myLog("Notification screen not in");
                                  }
                                },
                              );
                            },
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          // SizedBox(height: 10),
                          Expanded(
                            child: Txt(
                                txt:
                                    "make sure you're actually getting those all important push notifications",
                                txtColor: Colors.black87,
                                txtSize: MyTheme.txtSize - .2,
                                //txtLineSpace: 1.2,
                                maxLines: 10,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w300,
                                isBold: false),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(color: Colors.black, height: .5),
                    SizedBox(height: 10),
                    for (var listOpt in notiSettingsCfg.listOpt)
                      Padding(
                        padding: const EdgeInsets.only(top: 10, bottom: 10),
                        child: Container(
                          width: double.infinity,
                          //color: Colors.black,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Txt(
                                  txt: listOpt['title']
                                      .toString()
                                      .capitalizeFirstofEach,
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  isBold: true),
                              SizedBox(height: 10),
                              Txt(
                                  txt: listOpt['subTitle'],
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .2,
                                  //txtLineSpace: 1.2,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                              SizedBox(height: 10),
                              (listOpt['email'] as bool)
                                  ? Theme(
                                      data: MyTheme.radioThemeData,
                                      child: Transform.translate(
                                        offset: Offset(-10, 0),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Transform.scale(
                                              scale: 1.2,
                                              child: Checkbox(
                                                materialTapTargetSize:
                                                    MaterialTapTargetSize
                                                        .shrinkWrap,
                                                value: listOpt['isEmail'],
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    listOpt['isEmail'] =
                                                        newValue;
                                                  });
                                                },
                                              ),
                                            ),
                                            SizedBox(width: 10),
                                            Txt(
                                                txt: 'Email',
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize - .4,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                              (listOpt['sms'] as bool)
                                  ? Theme(
                                      data: MyTheme.radioThemeData,
                                      child: Transform.translate(
                                        offset: Offset(-10, 0),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Transform.scale(
                                              scale: 1.2,
                                              child: Checkbox(
                                                materialTapTargetSize:
                                                    MaterialTapTargetSize
                                                        .shrinkWrap,
                                                value: listOpt['isSms'],
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    listOpt['isSms'] = newValue;
                                                  });
                                                },
                                              ),
                                            ),
                                            SizedBox(width: 10),
                                            Txt(
                                                txt: 'Sms',
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize - .4,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                              (listOpt['push'] as bool)
                                  ? Theme(
                                      data: MyTheme.radioThemeData,
                                      child: Transform.translate(
                                        offset: Offset(-10, 0),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Transform.scale(
                                              scale: 1.2,
                                              child: Checkbox(
                                                materialTapTargetSize:
                                                    MaterialTapTargetSize
                                                        .shrinkWrap,
                                                value: listOpt['isPush'],
                                                onChanged: (newValue) {
                                                  setState(() {
                                                    listOpt['isPush'] =
                                                        newValue;
                                                  });
                                                },
                                              ),
                                            ),
                                            SizedBox(width: 10),
                                            Txt(
                                                txt: 'Push',
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize - .4,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ],
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                            ],
                          ),
                        ),
                      ),
                    SizedBox(height: 20),
                    MMBtn(
                      txt: "Save",
                      width: getW(context),
                      height: getHP(context, 6),
                      radius: 10,
                      callback: () async {
                        final caseUpdateMap = notiSettingsCfg.listOpt[0];
                        final caseRecomendationMap = notiSettingsCfg.listOpt[1];
                        final caseCompletedMap = notiSettingsCfg.listOpt[2];
                        final caseReminderMap = notiSettingsCfg.listOpt[3];
                        final userUpdateMap = notiSettingsCfg.listOpt[4];
                        final helpfulInfoMap = notiSettingsCfg.listOpt[5];
                        final updateNewsLetterMap = notiSettingsCfg.listOpt[6];

                        final param = {
                          "UserId": userData.userModel.id,
                          "UserCompanyId": userData.userModel.userCompanyID,
                          "Category": "User",
                          //
                          //"IsTransactionalEmail": transactionalMap['isEmail'] as bool,
                          //"IsTransactionalSMS": transactionalMap['isSms'] as bool,
                          //"IsTransactionalNotification": transactionalMap['isPush'] as bool,
                          //
                          "IsCaseCompletedEmail":
                              caseCompletedMap['isEmail'] as bool,
                          "IsCaseCompletedSMS":
                              caseCompletedMap['isSms'] as bool,
                          "IsCaseCompletedNotification":
                              caseCompletedMap['isPush'] as bool,
                          //
                          "IsCaseRecomendationEmail":
                              caseRecomendationMap['isEmail'] as bool,
                          "IsCaseRecomendationSMS":
                              caseRecomendationMap['isSms'] as bool,
                          "IsCaseRecomendationNotification":
                              caseRecomendationMap['isPush'] as bool,
                          //
                          "IsCaseUpdateEmail": caseUpdateMap['isEmail'] as bool,
                          "IsCaseUpdateSMS": caseUpdateMap['isSms'] as bool,
                          "IsCaseUpdateNotification":
                              caseUpdateMap['isPush'] as bool,
                          //
                          "IsCaseReminderEmail":
                              caseReminderMap['isEmail'] as bool,
                          "IsCaseReminderSMS": caseReminderMap['isSms'] as bool,
                          "IsCaseReminderNotification":
                              caseReminderMap['isPush'] as bool,
                          //
                          "IsUserUpdateEmail": userUpdateMap['isEmail'] as bool,
                          "IsUserUpdateSMS": userUpdateMap['isSms'] as bool,
                          "IsUserUpdateNotification":
                              userUpdateMap['isPush'] as bool,
                          //
                          "IsHelpfulInformationEmail":
                              helpfulInfoMap['isEmail'] as bool,
                          "IsHelpfulInformationSMS":
                              helpfulInfoMap['isSms'] as bool,
                          "IsHelpfulInformationNotification":
                              helpfulInfoMap['isPush'] as bool,
                          //
                          "IsUpdateAndNewsLetterEmail":
                              updateNewsLetterMap['isEmail'] as bool,
                          "IsUpdateAndNewsLetterSMS":
                              updateNewsLetterMap['isSms'] as bool,
                          "IsUpdateAndNewsLetterNotification":
                              updateNewsLetterMap['isPush'] as bool,
                          //
                          //"IsHelpFullEmail": helpFullMap['isEmail'] as bool,
                          //"IsHelpFullSMS": helpFullMap['isSms'] as bool,
                          //"IsHelpFullNotification": helpFullMap['isPush'] as bool,
                          //
                          //"IsLeadUpdateEmail": leadUpdateMap['isEmail'] as bool,
                          //"IsLeadUpdateSMS": leadUpdateMap['isSms'] as bool,
                          //"IsLeadUpdateNotification": leadUpdateMap['isPush'] as bool,
                          //
                          "Id": userData.userModel.id,
                        };
                        await APIViewModel().req<NotiSettingsPostAPIModel>(
                          context: context,
                          url: APINotiCfg.NOTI_SETTINGS_POST_URL,
                          reqType: ReqType.Post,
                          param: param,
                          callback: (model) async {
                            if (model != null && mounted) {
                              try {
                                if (model.success) {
                                  try {
                                    final msg = model
                                        .messages.postNotificationSetting[0]
                                        .toString();
                                    showToast(
                                        context: context, msg: msg, which: 1);
                                    //await NotiSettingsHelper().setSettings(notiSettingsCfg.listOpt);
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                } else {
                                  try {
                                    if (mounted) {
                                      final err = model
                                          .messages.postNotificationSetting[0]
                                          .toString();
                                      showToast(
                                          context: context, msg: err, which: 1);
                                    }
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                }
                              } catch (e) {
                                myLog(e.toString());
                              }
                            }
                          },
                        );
                      },
                    ),
                    SizedBox(height: 50),
                  ],
                ),
              ),
            ),
    );
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) {}
  static Future selectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }

  Future<void> showNotificationMessage(String title, String body) async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.max, priority: Priority.high, ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, title, body, platformChannelSpecifics,
        payload: 'Default_Sound');
  }
}
