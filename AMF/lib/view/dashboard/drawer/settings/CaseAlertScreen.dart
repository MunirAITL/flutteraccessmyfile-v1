import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class CaseAlertScreen extends StatelessWidget with Mixin {
  final Map<String, dynamic> message;

  const CaseAlertScreen({Key key, @required this.message}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme:
              IconThemeData(color: MyTheme.redColor //change your color here
                  ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: UIHelper().drawAppbarTitle(title: "Case alerts"),
          centerTitle: true,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: MyTheme.redColor),
              onPressed: () async {
                Navigator.pop(context);
              }),
        ),
        body: drawCaseUI(context),
      ),
    );
  }

  drawCaseUI(context) {
    return Center(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: getHP(context, 3)),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                  txt: message['data']['Message'].toString() ?? '',
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + .4,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Txt(
                  txt: message['data']['Description'].toString() ?? '',
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: MMBtn(
                txt: "Close",
                width: getW(context),
                height: getHP(context, 6),
                radius: 10,
                callback: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
