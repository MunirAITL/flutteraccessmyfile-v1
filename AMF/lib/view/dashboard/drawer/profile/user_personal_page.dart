import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserPersonalPage extends StatefulWidget {
  const UserPersonalPage({Key key}) : super(key: key);

  @override
  _UserPersonalPageState createState() => _UserPersonalPageState();
}

class _UserPersonalPageState extends BaseDashboard<UserPersonalPage> {
  final listBankAccount = [
    {
      "txt": "Your reported account",
      "amount": 0,
      "status": "Currently open",
      "color": MyTheme.bgColor3,
    },
    {
      "txt": "Starling Bank",
      "amount": 0,
      "status": "Currently open",
      "color": MyTheme.bgColor3,
    },
    {
      "txt": "Lloyds Bank (Was Lloyds TSB)",
      "amount": 592,
      "status": "Currently open",
      "color": MyTheme.bgColor3,
    },
  ];
  final listCreditCard = [
    {
      "txt": "NewDay Ltd Fluid MasterCard",
      "amount": 0,
      "status": "Currently open",
      "color": MyTheme.bgColor3,
    },
    {
      "txt": "NewDay Ltd",
      "amount": 0,
      "status": "Currently open",
      "color": MyTheme.bgColor3,
    },
    {
      "txt": "PayPle (Europe) Sari Et Cie SCA",
      "amount": 1419,
      "status": "Currently open",
      "color": MyTheme.bgColor3,
    },
    {
      "txt": "Halifax Credit Card",
      "amount": 0,
      "status": "Currently open",
      "color": MyTheme.bgColor3,
    },
    {
      "txt": "Barclaycard",
      "amount": 0,
      "status": "Currently closed",
      "color": MyTheme.bgColor3,
    },
  ];

  final listUtility = [
    {
      "txt": "Octopus Energy Limited",
      "amount": 0,
      "status": "Currently open",
      "color": MyTheme.bgColor3,
    },
    {
      "txt": "EE Limited",
      "amount": 0,
      "status": "Currently closed",
      "color": Colors.grey,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor4,
        resizeToAvoidBottomInset: false,
        bottomNavigationBar: drawFooter(),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIHelper().drawAppBarTitle(
                title: "My Account",
                callback: () {
                  Get.back();
                }),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10),
                  getTitle("Your Personal Details"),
                  SizedBox(height: 5),
                  Txt(
                      txt:
                          "This is the information used to generate your TransUnion credit report.",
                      txtColor: MyTheme.lblackColor.withOpacity(.5),
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 20),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: 'REPORT CREATED ON ',
                            style:
                                TextStyle(color: Colors.black87, fontSize: 12)),
                        TextSpan(
                          text: '29 Nov 2021',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              color: MyTheme.bgColor3,
                              fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  drawRowIconText(
                      image: Image.asset(
                        "assets/images/icons/user_ico.png",
                        width: 25,
                        height: 25,
                      ),
                      text: "John Doe"),
                  drawRowIconText(
                      image: Image.asset(
                        "assets/images/icons/user_dob_ico.png",
                        width: 25,
                        height: 25,
                      ),
                      text: "12 June 2023"),
                  drawRowIconText(
                      image: Image.asset(
                        "assets/images/icons/user_home_ico.png",
                        width: 25,
                        height: 25,
                      ),
                      text: "9a The Drive, Illford IG1 3EY"),
                  SizedBox(height: 10),
                ],
              ),
            ),
            Container(color: Colors.grey, height: .5),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10),
                  getTitle("Your Reported Account"),
                  SizedBox(height: 5),
                  Txt(
                      txt:
                          "These are the open account on your current TransUnion credit report.",
                      txtColor: MyTheme.lblackColor.withOpacity(.5),
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 20),
                  drawBankAccount(),
                  SizedBox(height: 30),
                  drawCreditCards(),
                  SizedBox(height: 30),
                  drawUtility(),
                  SizedBox(height: 20),
                  MMBtn(
                      txt: "SEE CLOSED ACCOUNTS",
                      bgColor: MyTheme.redColor,
                      txtColor: MyTheme.btnTxtColor,
                      width: getW(context),
                      height: getHP(context, 6),
                      radius: 10,
                      callback: () {
                        //Get.to(() => SigninPage());
                      }),
                  SizedBox(height: 10),
                ],
              ),
            ),
            Divider(color: Colors.grey),
            drawSearchBox(),
            SizedBox(height: 10),
            drawTextBox(
                "Addresses",
                "Your TransUnion credit report contains current and historical addresses. Most financial institutions requier 3 years worth of residential information to be able to provide financing offers.",
                "There are no reported addresses on your TransUnion report"),
            Divider(color: Colors.grey),
            drawTextBox(
                "Financial Connections",
                "According to your TransUnion credit report, these are the other names you've used in the past, such as a maiden name.",
                "No other reported names"),
            Divider(color: Colors.grey),
            drawTextBox(
                "Other Names",
                "This is where you would find people with whom you've shared a financial connection according to your TransUnion credit report.",
                "No financial connections reported"),
            Divider(color: Colors.grey),
            drawTextBox(
                "Electoral Roll",
                "Your current registration status on the electoral roll. This information is supplied to TransUnion by your local council.",
                ""),
            Divider(color: Colors.grey),
            SizedBox(height: 5),
            drawRowTxtBox("Name", "John Doe"),
            SizedBox(height: 5),
            Divider(color: Colors.grey),
            SizedBox(height: 5),
            drawRowTxtBox("Address", "9a The Drive, Illford IG1 3EY"),
            SizedBox(height: 5),
            Divider(color: Colors.grey),
            SizedBox(height: 5),
            drawRowTxtBox("Registered since", "28 Oct 2018"),
            SizedBox(height: 5),
            Divider(color: Colors.grey),
            SizedBox(height: 5),
            drawRowTxtBox("Current status", "Present"),
            SizedBox(height: 5),
            Divider(color: Colors.grey),
            SizedBox(height: 5),
            drawIconTextBox(
              Image.asset(
                "assets/images/icons/thumb_ico.png",
                width: 16,
                height: 16,
              ),
              "Public information",
              "Details of any bankruptcies, insolvencies or cour judgments provided by TransUnion.",
            ),
            drawIconTextBox(
              Image.asset(
                "assets/images/icons/thumb_ico.png",
                width: 16,
                height: 16,
              ),
              "Bankrupticies",
              "No bankrupticies or insolvencies",
            ),
            drawIconTextBox(
              Image.asset(
                "assets/images/icons/thumb_ico.png",
                width: 16,
                height: 16,
              ),
              "Judgements",
              "No court judgments",
            ),
            drawIconTextBox(
              Image.asset(
                "assets/images/icons/thumb_ico.png",
                width: 16,
                height: 16,
              ),
              "Notices of Correction",
              "This is where you would find statements you've asked to be added to your TransUnion credit report to provide details that help institutions better understand the information reported.",
            ),
            drawIconTextBox(
              Image.asset(
                "assets/images/icons/thumb_ico.png",
                width: 16,
                height: 16,
              ),
              "",
              "No notices of corrections reported",
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Txt(
                  txt:
                      "How do i add, amend or remove a Notice of correction from my credit report?",
                  txtColor: MyTheme.bgColor3,
                  txtSize: MyTheme.txtSize - .3,
                  txtAlign: TextAlign.start,
                  fontWeight: FontWeight.w400,
                  isBold: false),
            ),
            drawTextBox(
                "Cifas",
                "There are no Cifas warnings reported at your current address(es).",
                "If you take out protective registration with Cifas one of the organisations working with Cifas has registered you as a victim of fraud, that information will appear here. It doesn't affect your credit score."),
            Container(
              height: getHP(context, 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Txt(
                        txt: "Data from",
                        txtColor: MyTheme.lblackColor,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        fontWeight: FontWeight.w400,
                        isBold: false),
                  ),
                  SizedBox(width: 5),
                  Image.asset(
                    "assets/images/img/transunion_logo.png",
                    width: getWP(context, 30),
                    height: getHP(context, 10),
                  )
                ],
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  drawBankAccount() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(
              txt: "BANK ACCOUNTS",
              txtColor: MyTheme.lblackColor,
              txtSize: MyTheme.txtSize - .3,
              txtAlign: TextAlign.start,
              fontWeight: FontWeight.w500,
              isBold: true),
          ListView.builder(
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.vertical,
              itemCount: listBankAccount.length,
              itemBuilder: (BuildContext context, int index) {
                final map = listBankAccount[index];
                return _drawItem(map);
              })
        ],
      ),
    );
  }

  drawCreditCards() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          getTitle("CREDIT CARDS"),
          ListView.builder(
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.vertical,
              itemCount: listCreditCard.length,
              itemBuilder: (BuildContext context, int index) {
                final map = listCreditCard[index];
                return _drawItem(map);
              })
        ],
      ),
    );
  }

  drawUtility() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          getTitle("UTILITIES"),
          ListView.builder(
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.vertical,
              itemCount: listUtility.length,
              itemBuilder: (BuildContext context, int index) {
                final map = listUtility[index];
                return _drawItem(map);
              }),
          SizedBox(height: 20),
          Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              border: Border.all(color: HexColor.fromHex("#CBCBCB"), width: 1),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        width: 20,
                        height: 20,
                        child:
                            Image.asset("assets/images/icons/bulb_icon.png")),
                    SizedBox(width: 10),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Txt(
                              txt:
                                  "IG1 residents could save up to £230 on their energy bill",
                              txtColor: MyTheme.lblackColor,
                              txtSize: MyTheme.txtSize - .45,
                              txtAlign: TextAlign.start,
                              fontWeight: FontWeight.w500,
                              isBold: false),
                          SizedBox(height: 10),
                          GestureDetector(
                            onTap: () {},
                            child: Center(
                              child: Txt(
                                  txt: "Find out what I can save",
                                  txtColor: MyTheme.bgColor3,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.center,
                                  fontWeight: FontWeight.w500,
                                  isBold: false),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _drawItem(Map<String, Object> map) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Card(
        margin: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding:
              const EdgeInsets.only(left: 10, right: 10, top: 20, bottom: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    flex: 6,
                    child: Txt(
                        txt: map['txt'],
                        txtColor: MyTheme.lblackColor,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  Flexible(
                    child: Txt(
                        txt: AppDefine.CUR_SIGN + map['amount'].toString(),
                        txtColor: MyTheme.lblackColor,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.center,
                        fontWeight: FontWeight.w500,
                        isBold: false),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  UIHelper().drawCircle(
                      context: context, color: map['color'], size: 2.5),
                  SizedBox(width: 5),
                  Txt(
                      txt: map['status'],
                      txtColor: map['color'],
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  drawSearchBox() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            getTitle("Search History"),
            SizedBox(height: 5),
            Txt(
                txt:
                    "These are the enquiries to your credit that companies have made with TransUnion in the past 2 years. These searches let you know who's checking your report and when.",
                txtColor: MyTheme.lblackColor.withOpacity(.5),
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 20),
            getTitle("Hard Searches"),
            SizedBox(height: 5),
            Txt(
                txt:
                    "Hard searches are made by lenders when considering applications for credit. They don't affect your TransUnion credit score, but may be considered by lenders when working out how credit-worthy you are.",
                txtColor: MyTheme.lblackColor.withOpacity(.5),
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 20),
            _drawSearchItems(
                txt1: "Newday Cards Limited (CT, SA, SR, BSB,..",
                txt2: "Checking Credit Application",
                dt: "05 Nov 2020"),
            SizedBox(height: 20),
            getTitle("Soft Searches"),
            SizedBox(height: 5),
            Txt(
                txt:
                    "Soft searches are typically used to check your identity or assess whether you qualify for product offers. They never affect your credit score or your chance of being approved for credit.",
                txtColor: MyTheme.lblackColor.withOpacity(.5),
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 20),
            _drawSearchItems(
                txt1: "Credit Karma UK (CVAL, CR, TV)",
                txt2: "Consumer Credit File Request",
                dt: "05 Nov 2023"),
            /* SizedBox(height: 10),
            _drawSearchItems(
                txt1: "Credit Karma (LS) (CR API)",
                txt2: "Consumer Credit File Request",
                dt: "28 Nov 2021"),*/
            SizedBox(height: 15),
            Container(
                width: getW(context),
                decoration: BoxDecoration(
                  border: Border.all(
                      color: Colors.grey, width: .8, style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Center(
                    child: Text(
                      "SEE ALL",
                      style: TextStyle(
                          color: MyTheme.lblackColor,
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                )),
          ],
        ),
      ),
    );
  }

  _drawSearchItems({String txt1, String txt2, String dt}) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: HexColor.fromHex("#CBCBCB"), width: 1),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 3,
                  child: Text(
                    txt1,
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        color: MyTheme.lblackColor,
                        fontSize: 14),
                  ),
                ),
                Expanded(
                  child: Text(dt,
                      style: TextStyle(color: MyTheme.bgColor3, fontSize: 13)),
                ),
              ],
            ),
            SizedBox(height: 10),
            Txt(
                txt: txt2,
                txtColor: MyTheme.lblackColor.withOpacity(.5),
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
          ],
        ),
      ),
    );
  }
}
