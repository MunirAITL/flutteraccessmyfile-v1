import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/model/auth/ChangePwdAPIModel.dart';
import 'package:aitl/data/model/auth/profile/DeactivateProfileAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/textfield/InputTitleBox.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/Mixin.dart';
import 'package:get/get.dart';

import '../../../widgets/textfield/InputBoxAMF.dart';

class ChangePwdPage extends StatefulWidget {
  @override
  State createState() => _ChangePwdPageState();
}

class _ChangePwdPageState extends State<ChangePwdPage> with Mixin {
  final TextEditingController _curPwd = TextEditingController();
  final TextEditingController _newPwd = TextEditingController();
  final TextEditingController _newPwd2 = TextEditingController();
  //
  final focusPwd = FocusNode();
  final focusPwd1 = FocusNode();
  final focusPwd2 = FocusNode();

  bool isLoading = false;
  bool isobscureText = true;
  bool isobscureText1 = true;
  bool isobscureText2 = true;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _newPwd.dispose();
    _newPwd2.dispose();
    _curPwd.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {}

  validate() {
    if (UserProfileVal()
        .isEmpty(context, _curPwd, 'Please enter current password')) {
      return false;
    }
    if (UserProfileVal().isEmpty(context, _newPwd, 'Please enter password')) {
      return false;
    }
    if (UserProfileVal()
        .isEmpty(context, _newPwd2, 'Please enter cofirm password')) {
      return false;
    }
    if (_newPwd.text.trim() != _newPwd2.text.trim()) {
      showToast(context: context, msg: "Confirm password does not match");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor4,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            UIHelper().drawAppBarTitle(
                title: "Change Password",
                callback: () {
                  Get.back();
                }),
            Padding(
              padding: const EdgeInsets.all(20),
              child:
                  Column(mainAxisAlignment: MainAxisAlignment.start, children: [
                InputBoxAMF(
                    context: context,
                    ctrl: _curPwd,
                    lableTxt: "Current Password",
                    kbType: TextInputType.text,
                    inputAction: TextInputAction.next,
                    focusNode: focusPwd,
                    focusNodeNext: focusPwd1,
                    len: 20,
                    isPwd: true,
                    maxLines: 1),
                SizedBox(height: 20),
                InputBoxAMF(
                    context: context,
                    ctrl: _newPwd,
                    lableTxt: "New Password",
                    kbType: TextInputType.text,
                    inputAction: TextInputAction.next,
                    focusNode: focusPwd1,
                    focusNodeNext: focusPwd2,
                    len: 20,
                    isPwd: true,
                    maxLines: 1),
                SizedBox(height: 20),
                InputBoxAMF(
                    context: context,
                    ctrl: _newPwd2,
                    lableTxt: "Repeat Password",
                    kbType: TextInputType.text,
                    inputAction: TextInputAction.done,
                    focusNode: focusPwd2,
                    len: 20,
                    isPwd: true,
                    maxLines: 1),
                SizedBox(height: 10),
                MMBtn(
                    txt: "RESET PASSWORD",
                    height: getHP(context, 7),
                    bgColor: MyTheme.btnRedColor,
                    txtColor: MyTheme.btnTxtColor,
                    width: getW(context),
                    callback: () {
                      if (validate()) {
                        APIViewModel().req<ChangePwdAPIModel>(
                          context: context,
                          url: APIAuthCfg.CHANGE_PWD_URL,
                          reqType: ReqType.Put,
                          param: {
                            'CurrentPassword': _curPwd.text.trim(),
                            'password': _newPwd.text.trim(),
                            'confirmPassword': _newPwd2.text.trim(),
                          },
                          callback: (model) {
                            if (model != null && mounted) {
                              try {
                                if (model.success) {
                                  try {
                                    final msg = model.messages.changePassword[0]
                                        .toString();
                                    showToast(
                                        context: context, msg: msg, which: 1);
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                } else {
                                  try {
                                    if (mounted) {
                                      final err = model
                                          .errorMessages.changePassword[0]
                                          .toString();
                                      showToast(context: context, msg: err);
                                    }
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                }
                              } catch (e) {
                                myLog(e.toString());
                              }
                            }
                          },
                        );
                      }
                    }),
                SizedBox(height: 50),
              ]),
            ),
          ],
        ),
      ),
    );
  }
}
