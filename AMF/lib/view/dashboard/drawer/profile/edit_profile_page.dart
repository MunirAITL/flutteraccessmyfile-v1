import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/profile/DeactivateProfileAPIModel.dart';
import 'package:aitl/data/model/auth/profile/RegProfileAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/dialog/DeactivateProfileDialog.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geocoding.dart';
import 'package:intl/intl.dart';

import '../../../../view_model/helper/helper/UIHelper.dart';
import '../../../widgets/btn/BtnOutline.dart';
import '../../../widgets/btn/MMBtn.dart';
import 'edit_profile_base.dart';

class EditProfilePage extends StatefulWidget {
  @override
  State createState() => _EditProfileState();
}

enum SmokerEnum { yes, no }

class _EditProfileState extends BaseEditProfileStatefull<EditProfilePage>
    with APIStateListener {
  //  deactivate reason
  final deactivateReason = TextEditingController();

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.reg2 && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              await DBMgr.shared.setUserProfile(
                user: model.responseData.user,
              );
              await userData.setUserModel();
              showToast(
                  context: context,
                  msg: 'Profile updated successfully.',
                  which: 1);
              setState(() {});
            } catch (e) {
              myLog(e.toString());
            }
          } else {
            try {
              if (mounted) {
                final err = model.errorMessages.postUser[0].toString();
                showToast(context: context, msg: err);
              }
            } catch (e) {
              myLog(e.toString());
            }
          }
        }
      }

      if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final imageId = (model as MediaUploadFilesAPIModel)
                .responseData
                .images[0]
                .id
                .toString();
            wsUpdateProfile(imageId);
          }
        }
      }
    } catch (e) {}
  }

  bool validate() {
    if (!UserProfileVal().isFNameOK(context, fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, lname)) {
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(context, email, "Invalid email address")) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, mobile)) {
      return false;
    } else if (!UserProfileVal().isDOBOK(context, dob)) {
      return false;
    } else if (address == "" || address == "Search") {
      showToast(context: context, msg: "Please pick your location");
      return false;
    } else {
      return true;
    }
  }

  wsUpdateProfile(imageId) {
    try {
      APIViewModel().req<RegProfileAPIModel>(
          context: context,
          apiState: APIState(APIType.reg2, this.runtimeType, null),
          url: APIAuthCfg.REG_PROFILE_PUT_URL,
          reqType: ReqType.Put,
          param: {
            "Address": address.replaceAll("Search", "").trim(),
            "BriefBio": userData.userModel.briefBio,
            "CommunityId": userData.communityId,
            "DateofBirth": dob,
            "Email": email.text.trim(),
            "FirstName": fname.text.trim(),
            "Cohort": "",
            "Headline": userData.userModel.headline,
            "Id": userData.userModel.id,
            "LastName": lname.text.trim(),
            "Latitude": cord.lat,
            "Longitude": cord.lng,
            "MobileNumber": mobile.text.trim(),
            "DateCreatedLocal": userData.userModel.dateCreatedLocal,
            "ProfileImageId": imageId.toString(),
            //"CoverImageId": coverImageId.toString(),
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    //
    fname.dispose();
    lname.dispose();
    pwd.dispose();
    email.dispose();
    mobile.dispose();
    deactivateReason.dispose();

    dob = null;
    cord = null;
    address = null;
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    getMobCode();

    try {
      final user = userData.userModel;
      fname.text = user.firstName;
      lname.text = user.lastName;
      address = user.address;
      cord = Location(lat: 0, lng: 0);
      pwd.text = "";
      email.text = user.email;
      mobile.text = Common.getPhoneNumber(user.mobileNumber);
      dob = user.dateofBirth;
      final f1 = new DateFormat('dd-MMM-yyyy');
      final f2 = new DateFormat(DateFun.getDOBFormat());
      final d = f1.parse(dob);
      dob = f2.format(d);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor4,
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: Colors.white,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                      flex: 6,
                      child: UIHelper().drawAppBarTitle(
                          title: "Edit Profile",
                          callback: () {
                            Get.back();
                          })),
                  Flexible(
                      child: GestureDetector(
                          onTap: () async {
                            if (validate()) {
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              await wsUpdateProfile("");
                            }
                          },
                          child: Container(
                              width: 20,
                              height: 20,
                              color: Colors.transparent,
                              child: Icon(
                                Icons.edit_outlined,
                                size: 20,
                                color: MyTheme.bgColor3,
                              )))),
                  SizedBox(width: 1)
                ],
              ),
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  drawGenInfo(),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 20, right: 20, top: 20, bottom: 40),
                    child: MMBtn(
                      txt: "SAVE UPDATE",
                      bgColor: MyTheme.bgColor3,
                      txtColor: MyTheme.cyanColor,
                      width: getW(context),
                      height: getHP(context, 6),
                      radius: 10,
                      callback: () async {
                        if (validate()) {
                          FocusScope.of(context).requestFocus(new FocusNode());
                          await wsUpdateProfile("");
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 25, right: 25),
                    child: BtnOutline(
                      txt: "DEACTIVATE MY ACCOUNT",
                      borderColor: Colors.transparent,
                      bgColor: MyTheme.redColor,
                      txtColor: MyTheme.cyanColor,
                      width: getW(context),
                      height: getHP(context, 6),
                      radius: 10,
                      callback: () async {
                        showDeactivateProfileDialog(
                          context: context,
                          email: deactivateReason,
                          callback: (String reason) {
                            if (reason != null) {
                              APIViewModel().req<DeactivateProfileAPIModel>(
                                  context: context,
                                  url: APIProfileCFg.DEACTIVATE_PROFILE_URL,
                                  param: {
                                    'id': userData.userModel.id,
                                    'CancelReason': reason
                                  },
                                  reqType: ReqType.Post,
                                  callback: (model) {
                                    if (model != null && mounted) {
                                      try {
                                        if (model.success) {
                                          try {
                                            //  signout
                                            CookieMgr().delCookiee();
                                            DBMgr.shared.delTable("User");
                                            StateProvider().notify(
                                                ObserverState.STATE_LOGOUT,
                                                null);
                                          } catch (e) {
                                            myLog(e.toString());
                                          }
                                        } else {
                                          try {
                                            if (mounted) {
                                              final err = model
                                                  .messages.delete[0]
                                                  .toString();
                                              showToast(
                                                context: context,
                                                msg: err,
                                              );
                                            }
                                          } catch (e) {
                                            myLog(e.toString());
                                          }
                                        }
                                      } catch (e) {
                                        myLog(e.toString());
                                      }
                                    }
                                  });
                            }
                          },
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 50),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
