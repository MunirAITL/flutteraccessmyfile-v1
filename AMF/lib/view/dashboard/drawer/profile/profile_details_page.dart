import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/drawer/profile/user_personal_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../view_model/helper/helper/UIHelper.dart';
import 'edit_profile_page.dart';

class ProfileDetailsPage extends StatefulWidget {
  const ProfileDetailsPage({Key key}) : super(key: key);

  @override
  State createState() => _ProfileDetailsPageState();
}

class _ProfileDetailsPageState extends BaseDashboard<ProfileDetailsPage> {
  bool isCreditMonitoring = true;
  bool isBasicGuideInsight = true;
  bool isGetAccessMyFile = true;
  bool isGetSpecialDeal = true;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor4,
        resizeToAvoidBottomInset: false,
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIHelper().drawAppBarTitle(
                title: "Detail Information",
                callback: () {
                  Get.back();
                }),
            drawProfileInfo(),
            drawContactPref(),
            Container(color: MyTheme.bgColor2, height: 5),
            drawDataAccount(),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  drawProfileInfo() {
    return Container(
      width: getW(context),
      child: Padding(
        padding:
            const EdgeInsets.only(left: 10, right: 10, top: 20, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _drawInfoBox(title: "Name", val: "John Doe", callback: () {}),
            SizedBox(height: 10),
            _drawInfoBox(
                title: "Current Address",
                val: "9A The Drive, Illford, IG1 3EY",
                callback: () {}),
            SizedBox(height: 10),
            _drawInfoBox(
                title: "Email", val: "johndoe@mail.com", callback: () {}),
            SizedBox(height: 10),
            _drawInfoBox(
                title: "Password", val: "****************", callback: () {})
          ],
        ),
      ),
    );
  }

  drawContactPref() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            getTitle("Your Contact Preferences"),
            SizedBox(height: 5),
            Txt(
                txt:
                    "We'll always email you about your account and whenever there's something new with our service. The rest is up to you.",
                txtColor: MyTheme.lblackColor.withOpacity(.6),
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 10),
            _drawSwitchBox(
                title: "Credit Monitoring",
                txt:
                    "Get alerts whenever something changes with your finances and see what it means for you.",
                isOn: isCreditMonitoring,
                callback: (v) {
                  isCreditMonitoring = v;
                  setState(() {});
                }),
            SizedBox(height: 5),
            _drawSwitchBox(
                title: "Basic Guides And Insights",
                txt:
                    "Learn what impacts your financer and understand how to mack financial progress.",
                isOn: isBasicGuideInsight,
                callback: (v) {
                  isBasicGuideInsight = v;
                  setState(() {});
                }),
            SizedBox(height: 5),
            _drawSwitchBox(
                title: "Get The Most Out Of Access My File",
                txt:
                    "Mack progress faster with our best suggestion and personalised recommendations for you.",
                isOn: isGetAccessMyFile,
                callback: (v) {
                  isGetAccessMyFile = v;
                  setState(() {});
                }),
            SizedBox(height: 5),
            _drawSwitchBox(
                title: "Get Special Deals With Partners",
                txt:
                    "Get emails from us about special offers as our partners want to share with you.",
                isOn: isGetSpecialDeal,
                callback: (v) {
                  isGetSpecialDeal = v;
                  setState(() {});
                }),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }

  _drawInfoBox({
    String title,
    String val,
    Function callback,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Txt(
            txt: title,
            txtColor: MyTheme.lblackColor.withOpacity(.6),
            txtSize: MyTheme.txtSize - .4,
            txtAlign: TextAlign.start,
            isBold: false),
        SizedBox(height: 2),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 6,
              child: Txt(
                  txt: val,
                  txtColor: MyTheme.lblackColor,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            Flexible(
              child: GestureDetector(
                  onTap: () {
                    callback();
                  },
                  child: Txt(
                      txt: "Update",
                      txtColor: MyTheme.bgColor3,
                      txtSize: MyTheme.txtSize - .5,
                      txtAlign: TextAlign.start,
                      fontWeight: FontWeight.w500,
                      isBold: false)),
            )
          ],
        ),
      ],
    );
  }

  _drawSwitchBox(
      {String title, String txt, bool isOn, Function(bool) callback}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                flex: 6,
                child: Txt(
                    txt: title,
                    txtColor: MyTheme.lblackColor,
                    txtSize: MyTheme.txtSize - .3,
                    txtAlign: TextAlign.start,
                    fontWeight: FontWeight.w500,
                    isBold: true),
              ),
              Flexible(
                  child: drawSwitchView(
                      isOn: isOn,
                      callback: (v) {
                        callback(v);
                      })),
            ],
          ),
          Txt(
              txt: txt,
              txtColor: MyTheme.lblackColor.withOpacity(.6),
              txtSize: MyTheme.txtSize - .4,
              txtAlign: TextAlign.start,
              isBold: false),
        ],
      ),
    );
  }

  drawDataAccount() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            getTitle("Your Data And Your Account"),
            SizedBox(height: 5),
            Txt(
                txt:
                    "You can download the information you've given us at any time. You can also close your account, but this can't be undone and you'd have to sign up again if you want to use this app or hear from us in the future.",
                txtColor: MyTheme.lblackColor.withOpacity(.6),
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 20),
            GestureDetector(
              onTap: () {},
              child: Txt(
                  txt: "Download my information",
                  txtColor: MyTheme.bgColor3,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  fontWeight: FontWeight.w400,
                  isBold: false),
            ),
            SizedBox(height: 10),
            GestureDetector(
              onTap: () {},
              child: Txt(
                  txt: "Close my account",
                  txtColor: MyTheme.bgColor3,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  fontWeight: FontWeight.w400,
                  isBold: false),
            ),
            SizedBox(height: 10),
            GestureDetector(
              onTap: () {},
              child: Txt(
                  txt: "Contact member support",
                  txtColor: MyTheme.bgColor3,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  fontWeight: FontWeight.w400,
                  isBold: false),
            ),
            SizedBox(height: 30),
            Center(
              child: MMBtn(
                  txt: "VIEW OTHER DETAILS",
                  bgColor: MyTheme.redColor,
                  txtColor: MyTheme.btnTxtColor,
                  width: getWP(context, 90),
                  height: getHP(context, 6),
                  radius: 10,
                  callback: () {
                    Get.to(() => UserPersonalPage());
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
