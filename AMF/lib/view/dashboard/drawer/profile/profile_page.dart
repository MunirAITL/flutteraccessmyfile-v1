import 'dart:io';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/PublicProfileAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/drawer/profile/profile_details_page.dart';
import 'package:aitl/view/dashboard/drawer/profile/user_personal_page.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../config/server/APIAuthCfg.dart';
import '../../../../data/model/auth/profile/RegProfileAPIModel.dart';
import '../../../../data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import '../../../widgets/picker/CamPicker.dart';
import '../help/HelpScreen.dart';
import '../settings/SettingsScreen.dart';
import 'edit_profile_page.dart';

class ProfilePage extends StatefulWidget {
  @override
  State createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> with Mixin {
  bool isLoading = true;
  UserModel userModel;

  final menuItems = [
    /*{
      'index': 0,
      'icon': 'assets/images/ico/switch_acc_ico.png',
      'title': 'Switch Accounts'
    },*/
    {
      'icon': 'assets/images/ico/address_ico.png',
      'title': 'Details Information',
      'route': () => ProfileDetailsPage(),
    },
    {
      'icon': 'assets/images/ico/help_center_ico.png',
      'title': 'Help Center',
      'route': () => HelpScreen(),
    },
    {
      'icon': 'assets/images/ico/settings_ico.png',
      'title': 'My Account',
      'route': () => UserPersonalPage()
    },
  ];

  wsUpdateProfile(imageId) {
    try {
      APIViewModel().req<RegProfileAPIModel>(
          context: context,
          url: APIAuthCfg.REG_PROFILE_PUT_URL,
          reqType: ReqType.Put,
          param: {
            "Address": userModel.address ?? '',
            "BriefBio": userModel.briefBio ?? '',
            "CommunityId": userModel.communityID ?? '1',
            "DateofBirth": userModel.dateofBirth ?? '',
            "Email": userModel.email ?? '',
            "FirstName": userModel.firstName ?? '',
            "Cohort": userModel.cohort ?? 'Male',
            "Headline": userModel.headline ?? '',
            "Id": userModel.id,
            "LastName": userModel.lastName ?? '',
            "Latitude": userModel.latitude ?? '',
            "Longitude": userModel.longitude ?? '',
            "MobileNumber": userModel.mobileNumber ?? '',
            "DateCreatedLocal": userModel.dateCreatedLocal,
            "ProfileImageId": imageId.toString(),
            //"CoverImageId": coverImageId.toString(),
          },
          callback: (model) async {
            if (model != null && mounted) {
              if (model.success) {
                try {
                  await DBMgr.shared.setUserProfile(
                    user: model.responseData.user,
                  );
                  await userData.setUserModel();
                  userModel = userData.userModel;
                  showToast(
                      context: context,
                      msg: 'Profile updated successfully.',
                      which: 1);
                  setState(() {});
                } catch (e) {
                  myLog(e.toString());
                }
              } else {
                try {
                  if (mounted) {
                    final err = model.messages.postUser[0].toString();
                    showToast(context: context, msg: err);
                  }
                } catch (e) {
                  myLog(e.toString());
                }
              }
            }
          });
    } catch (e) {}
  }

  getUserProfile() async {
    await APIViewModel().req<PublicProfileAPIModel>(
      context: context,
      url: APIProfileCFg.PUBLIC_USER_GET_URL
          .replaceAll("#userId#", userData.userModel.id.toString()),
      reqType: ReqType.Get,
      callback: (model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                userModel = model.responseData.user;
                setState(() async {
                  isLoading = false;
                  await DBMgr.shared
                      .setUserProfile(user: model.responseData.user);
                  await userData.setUserModel();
                });
              } catch (e) {
                setState(() {
                  isLoading = false;
                });
              }
            } else {
              setState(() {
                isLoading = false;
              });

              try {
                if (mounted) {
                  final err = model.errorMessages.login[0].toString();
                  showToast(context: context, msg: err);
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          } catch (e) {
            myLog(e.toString());
          }
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    userModel = null;
    super.dispose();
  }

  appInit() async {
    try {
      userModel = userData.userModel;
      await getUserProfile();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    //userModel.countryofResidency = "ffff";
    return SafeArea(
        child: Scaffold(
            backgroundColor: MyTheme.bgColor4,
            /*bottomNavigationBar: BottomAppBar(
          color: MyTheme.bgColor,
          child: Container(
            width: getW(context),
            height: getHP(context, 35),
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(50),
                topRight: const Radius.circular(50),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 30, left: 10, right: 10, bottom: 10),
              child: ListView.builder(
                  shrinkWrap: true,
                  primary: false,
                  itemCount: menuItems.length,
                  itemBuilder: (context, index) {
                    final map = menuItems[index];
                    return GestureDetector(
                      onTap: () {
                        Get.to(map['route']);
                      },
                      child: Card(
                        elevation: 0,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Image.asset(
                                    map['icon'],
                                    width: 20,
                                    height: 20,
                                  ),
                                  SizedBox(width: 20),
                                  Expanded(
                                    child: Txt(
                                      txt: map['title'],
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize - .2,
                                      txtAlign: TextAlign.start,
                                      isBold: false,
                                    ),
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.grey,
                                    size: 20,
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 35, top: 10, bottom: 10),
                                child: Divider(
                                  color: Colors.black,
                                  height: 10,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            ),
          ),
        ),*/
            //resizeToAvoidBottomPadding: true,
            body: drawLayout()));
  }

  drawLayout() {
    return Container(
      //color: MyTheme.bgColor,
      width: getW(context),
      height: getH(context),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                UIHelper().drawAppBarTitle(
                    title: "Profile",
                    callback: () {
                      Get.back();
                    }),
                SizedBox(height: 20),
                Stack(
                  children: [
                    PhysicalModel(
                      color: Colors.grey[50],
                      elevation: 10,
                      shape: BoxShape.circle,
                      child: CircleAvatar(
                        radius: 50,
                        backgroundColor: Colors.transparent,
                        backgroundImage: new CachedNetworkImageProvider(
                          MyNetworkImage.checkUrl(
                              userModel.profileImageURL != null &&
                                      userModel.profileImageURL.isNotEmpty
                                  ? userModel.profileImageURL
                                  : ServerUrls.MISSING_IMG),
                        ),
                      ),
                    ),
                    Positioned(
                        right: 0,
                        child: GestureDetector(
                          onTap: () {
                            CamPicker().showCamDialog(
                              context: context,
                              isRear: false,
                              callback: (File path) {
                                if (path != null) {
                                  APIViewModel().upload(
                                      context: context,
                                      file: path,
                                      callback: (model) {
                                        if (model != null && mounted) {
                                          if (model.success) {
                                            final imageId = model
                                                .responseData.images[0].id
                                                .toString();
                                            wsUpdateProfile(imageId);
                                          }
                                        }
                                      });
                                }
                              },
                            );
                          },
                          child: Container(
                            width: 25,
                            height: 25,
                            child: Icon(
                              Icons.photo_camera_outlined,
                              size: 15,
                              color: Colors.white,
                            ),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFFBDBDBD)),
                          ),
                        ))
                  ],
                ),
                SizedBox(height: 10),
                Txt(
                  maxLines: 1,
                  txt: userModel.name,
                  txtColor: MyTheme.timelinePostCallerNameColor,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  fontWeight: FontWeight.w500,
                  isBold: false,
                ),
                userModel.address != ''
                    ? Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset("assets/images/icons/map_pin_icon.png",
                                color: MyTheme.lblackColor.withOpacity(.5),
                                width: 13,
                                height: 13),
                            SizedBox(width: 5),
                            Flexible(
                              child: Txt(
                                maxLines: 1,
                                txt: userModel.address,
                                txtColor: MyTheme.lblackColor.withOpacity(.5),
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                            ),
                          ],
                        ),
                      )
                    : SizedBox(),
                SizedBox(height: 5),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.phone_outlined,
                      color: MyTheme.lblackColor.withOpacity(.5),
                      size: 15,
                    ),
                    SizedBox(width: 5),
                    Flexible(
                      child: Txt(
                        maxLines: 1,
                        txt: getPhoneNumber(userModel.mobileNumber),
                        txtColor: MyTheme.lblackColor.withOpacity(.5),
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 2),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.email_outlined,
                      color: MyTheme.lblackColor.withOpacity(.5),
                      size: 15,
                    ),
                    SizedBox(width: 5),
                    Flexible(
                      child: Txt(
                        maxLines: 2,
                        txt: userModel.email,
                        txtColor: MyTheme.lblackColor.withOpacity(.5),
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 5),
                GestureDetector(
                  onTap: () {
                    Get.to(() => EditProfilePage()).then((value) async {
                      await getUserProfile();
                    });
                  },
                  child: Card(
                    elevation: 2,
                    child: Container(
                      width: getWP(context, 16),
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.edit_note,
                              color: MyTheme.lblackColor,
                              size: 15,
                            ),
                            SizedBox(width: 5),
                            Flexible(
                              child: Txt(
                                maxLines: 1,
                                txt: "Edit",
                                txtColor: MyTheme.lblackColor,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            _drawListView(),
          ],
        ),

        /*Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 15.0),
                            child: Txt(
                              txt: "Basic Information :",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true,
                            ),
                          ),
                          userModel.address.isNotEmpty &&
                                  userModel.address != null
                              ? SizedBox(height: 20)
                              : SizedBox(),
                          //address

                          userModel.address.isNotEmpty &&
                                  userModel.address != null
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Row(
                                    children: [
                                      // Icon(Icons.home_outlined,color: Colors.black,size: 36,),
                                      Image.asset(
                                        "assets/images/icons/home_icon.png",
                                        width: 36,
                                      ),

                                      SizedBox(width: 10),
                                      Expanded(
                                        child: Txt(
                                          txt: "${userModel.address}",
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize,
                                          txtAlign: TextAlign.start,
                                          isBold: false,
                                          maxLines: 5,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox(),
                          userModel.countryofResidency.isNotEmpty &&
                                  userModel.countryofResidency != null
                              ? SizedBox(
                                  height: 20,
                                )
                              : SizedBox(),
//country
                          userModel.countryofResidency.isNotEmpty &&
                                  userModel.countryofResidency != null
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 10),
                                  child: Row(
                                    children: [
                                      // Icon(Icons.flag,color: Colors.black,size: 36,),
                                      Image.asset(
                                        "assets/images/icons/glob_ic.png",
                                        width: 25,
                                        color: Colors.grey,
                                      ),

                                      SizedBox(
                                        width: 10,
                                      ),
                                      Txt(
                                        txt:
                                            "${userModel.countryofResidency}",
                                        txtColor: Colors.black,
                                        txtSize: MyTheme.txtSize,
                                        txtAlign: TextAlign.start,
                                        isBold: false,
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox(),
                          userModel.dateofBirth.isNotEmpty &&
                                  userModel.dateofBirth != null
                              ? SizedBox(
                                  height: 20,
                                )
                              : SizedBox(),
                          //Date of birth

                          userModel.dateofBirth.isNotEmpty &&
                                  userModel.dateofBirth != null
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Row(
                                    children: [
                                      // Icon(Icons.date_range,color: Colors.black,size: 36,),

                                      Image.asset(
                                        "assets/images/icons/date_icon.png",
                                        width: 36,
                                      ),

                                      SizedBox(
                                        width: 10,
                                      ),
                                      Txt(
                                        txt: "${userModel.dateofBirth}",
                                        txtColor: Colors.black,
                                        txtSize: MyTheme.txtSize,
                                        txtAlign: TextAlign.start,
                                        isBold: false,
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox(),

                          userModel.maritalStatus.isNotEmpty &&
                                  userModel.maritalStatus != null
                              ? SizedBox(
                                  height: 20,
                                )
                              : SizedBox(),
                          //marital status

                          userModel.maritalStatus.isNotEmpty &&
                                  userModel.maritalStatus != null
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Row(
                                    children: [
                                      // Icon(Icons.update,color: Colors.black,size: 36,),
                                      Image.asset(
                                        "assets/images/icons/gender_icon.png",
                                        width: 36,
                                      ),

                                      SizedBox(
                                        width: 10,
                                      ),
                                      Txt(
                                        txt: "${userModel.maritalStatus}",
                                        txtColor: Colors.black,
                                        txtSize: MyTheme.txtSize,
                                        txtAlign: TextAlign.start,
                                        isBold: false,
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox(),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            padding: EdgeInsets.all(10.0),
                            // margin: EdgeInsets.all(8.0),
                            color: Colors.grey[200],
                            child: Row(
                              // crossAxisAlignment: CrossAxisAlignment.s,
                              mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                              children: [
                                Txt(
                                  txt: "Badges",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: false,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Btn(
                                    txt: "Add More",
                                    txtColor: Colors.white,
                                    bgColor: MyTheme.redColor,
                                    width: null, //getWP(context, 35),
                                    height: getHP(context, 5),
                                    callback: () {
                                      navTo(
                                              context: context,
                                              page: () => BadgeScreen())
                                          .then((value) {});
                                    }),
                              ],
                            ),
                          ),

                          ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: listUserBadgeModelShowList.length,
                            itemBuilder: (context, index) {
                              var item;
                              debugPrint(
                                  "badge type  ${listUserBadgeModelShowList[index].type}");

                              if (listUserBadgeModelShowList[index].type ==
                                  "Mobile") {
                                item = listItems[0];
                              }
                              if (listUserBadgeModelShowList[index].type ==
                                  "Email") {
                                item = listItems[1];
                              }
                              if (listUserBadgeModelShowList[index].type ==
                                  "Passport") {
                                item = listItems[2];
                              }
                              if (listUserBadgeModelShowList[index].type ==
                                  "Facebook") {
                                item = listItems[3];
                              }
                              if (listUserBadgeModelShowList[index].type ==
                                  "CustomerPrivacy") {
                                item = listItems[4];
                              }
                              if (listUserBadgeModelShowList[index].type ==
                                  "NationalIDCard") {
                                item = listItems[5];
                              }
                              if (listUserBadgeModelShowList[index].type ==
                                  "ElectricId") {
                                item = listItems[5];
                              }
                              return (item == null)
                                  ? SizedBox()
                                  : Card(
                                      elevation: 0,
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            top: 10, bottom: 10),
                                        child: ListTile(
                                          // leading: Image.asset(item['icon']),
                                          title: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                  width: getWP(context, 8),
                                                  height: getWP(context, 8),
                                                  child: Image.asset(
                                                      item['icon'])),
                                              SizedBox(width: 10),
                                              Expanded(
                                                child: Txt(
                                                    txt: item["title"],
                                                    txtColor: Colors.black,
                                                    txtSize:
                                                        MyTheme.txtSize + .4,
                                                    txtAlign: TextAlign.start,
                                                    isOverflow: true,
                                                    isBold: false),
                                              ),
                                            ],
                                          ),
                                          subtitle: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Padding(
                                                padding:
                                                    const EdgeInsets.only(
                                                        top: 10, bottom: 10),
                                                child: Txt(
                                                    txt: item["title"] ==
                                                            "Customer Privacy"
                                                        ? item["desc"] +
                                                            " at ${DateFormat('dd-MMM-yyyy').format(DateTime.parse(listUserBadgeModelShowList[index].creationDate.toString()))}"
                                                        : item["desc"],
                                                    txtColor: Colors.grey,
                                                    txtSize:
                                                        MyTheme.txtSize - .2,
                                                    txtAlign: TextAlign.start,
                                                    //txtLineSpace: 1.4,
                                                    isBold: false),
                                              ),
                                              Container(
                                                color: Colors.grey[200],
                                                height: 1,
                                                width: getW(context),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                            },
                          ),
                        ],
                      ),
                    ),*/
      ),
    );
  }

  _drawListView() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Card(
          elevation: 1,
          margin: EdgeInsets.zero,
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: menuItems.length,
              itemBuilder: (context, index) {
                final map = menuItems[index];
                return GestureDetector(
                  onTap: () {
                    Get.to(map['route']);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(2),
                    child: Container(
                      decoration: BoxDecoration(
                        //color: Colors.transparent,
                        border: Border(
                          bottom: BorderSide(
                            color: index < menuItems.length - 1
                                ? Colors.grey
                                : Colors.transparent,
                            width: .7,
                          ),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Txt(
                                    txt: map['title'],
                                    txtColor: MyTheme.lblackColor,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: false,
                                  ),
                                ),
                                Icon(
                                  Icons.arrow_forward_ios,
                                  color: MyTheme.lblackColor,
                                  size: 20,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }),
        ),
      ),
    );
  }

  String getPhoneNumber(String mobileNumber) {
    var phoneNumber = mobileNumber;
    try {
      debugPrint("Phone number substring(0,2)= " + mobileNumber[0]);
      if (mobileNumber[0] == "+") {
        if (mobileNumber.substring(0, 3) == "+88") {
          phoneNumber = mobileNumber.substring(3);
        } else if (mobileNumber.substring(0, 3) == "+44") {
          phoneNumber = mobileNumber.substring(3);
        }
      } else {
        if (mobileNumber.substring(0, 2) == "88") {
          phoneNumber = mobileNumber.substring(2);
        } else if (mobileNumber.substring(0, 2) == "44") {
          phoneNumber = mobileNumber.substring(2);
        }
      }
      //remove double 00
      debugPrint("Phonenumber substring(0,2) 2= " + phoneNumber);
      if (phoneNumber.substring(0, 2) == "00") {
        phoneNumber = phoneNumber.substring(1);
      }
      // add a single  0
      //     if (phoneNumber.substring(0, 1) != "0") {
      //       phoneNumber = "0" + phoneNumber;
      //     }
    } catch (e) {}

    return phoneNumber;
  }
}
