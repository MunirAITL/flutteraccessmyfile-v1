import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/datepicker/DatePickerView.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/geocoding.dart';
import 'package:intl/intl.dart';

import '../../../widgets/textfield/InputBoxAMF.dart';
import '../../../widgets/textfield/MobileTitleLineCountryCode.dart';

abstract class BaseEditProfileStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final fname = TextEditingController();
  final lname = TextEditingController();
  final email = TextEditingController();
  final mobile = TextEditingController();
  final pwd = TextEditingController();

  //
  final focusFname = FocusNode();
  final focusLname = FocusNode();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();
  final focusPwd = FocusNode();

  String countryCode = "+44";
  String countryName = "GB";

  String dob = "";
  String address = "Search";
  Location cord;

  String countryDialCode = AppDefine.COUNTRY_DIALCODE;

  getMobCode() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;
          countryCode = cd;
        });
      }
    } catch (e) {
      print("sms2 Screen country code and name problem ");
    }
  }

  drawGenInfo() {
    final dateNow = DateTime.now();
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                InputBoxAMF(
                  context: context,
                  ctrl: fname,
                  lableTxt: "First name",
                  kbType: TextInputType.name,
                  inputAction: TextInputAction.next,
                  focusNode: focusFname,
                  focusNodeNext: focusLname,
                  len: 20,
                  //labelColor: Colors.black,
                ),
                SizedBox(height: 5),
                InputBoxAMF(
                  context: context,
                  ctrl: lname,
                  lableTxt: "Last name",
                  kbType: TextInputType.name,
                  inputAction: TextInputAction.next,
                  focusNode: focusLname,
                  focusNodeNext: focusEmail,
                  len: 20,
                ),
                SizedBox(height: 10),
                GPlacesView(
                  title: "Address",
                  address: address,
                  callback: (String _address, Location _loc) {
                    //callback(address);
                    address = _address;
                    cord = _loc;
                    setState(() {});
                  },
                ),
                SizedBox(height: 10),
                InputBoxAMF(
                    context: context,
                    ctrl: email,
                    lableTxt: "Email",
                    kbType: TextInputType.text,
                    inputAction: TextInputAction.next,
                    focusNode: focusEmail,
                    focusNodeNext: focusPwd,
                    len: 50),
                SizedBox(height: 5),
                InputBoxAMF(
                    context: context,
                    ctrl: pwd,
                    lableTxt: "Password",
                    kbType: TextInputType.text,
                    inputAction: TextInputAction.next,
                    focusNode: focusPwd,
                    focusNodeNext: focusMobile,
                    len: 20,
                    isPwd: true,
                    maxLines: 1),
                SizedBox(height: 10),
                DatePickerView(
                  cap: "Date of Birth",
                  capTxtColor: Colors.black,
                  dt: dob,
                  txtColor: Colors.black,
                  topbotHeight: 10,
                  borderWidth: 1,
                  initialDate:
                      DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
                  firstDate:
                      DateTime(dateNow.year - 100, dateNow.month, dateNow.day),
                  lastDate:
                      DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
                  callback: (value) {
                    if (mounted) {
                      setState(() {
                        try {
                          dob = DateFormat(DateFun.getDOBFormat())
                              .format(value)
                              .toString();
                        } catch (e) {
                          myLog(e.toString());
                        }
                      });
                    }
                  },
                ),
                SizedBox(height: 10),
                drawMobileTitleLineCountryCode(
                    context: context,
                    title: "Phone Number",
                    input: mobile,
                    ph: "Phone Number",
                    kbType: TextInputType.phone,
                    inputAction: TextInputAction.done,
                    focusNode: focusMobile,
                    len: 15,
                    isWhiteBG: false,
                    countryCode: countryCode,
                    countryName: countryName,
                    getCountryCode: (value) {
                      countryCode = value.toString();
                      print("Country Code Clik = " + countryCode);
                      PrefMgr.shared.setPrefStr("countryName", value.code);
                      PrefMgr.shared
                          .setPrefStr("countryCode", value.toString());
                    }),
                SizedBox(height: 10),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
