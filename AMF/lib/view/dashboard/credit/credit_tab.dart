import 'package:flutter/material.dart';

import '../../../Mixin.dart';
import '../../../config/theme/MyTheme.dart';
import '../../../data/network/NetworkMgr.dart';
import '../../../view_model/observer/StateProvider.dart';
import '../../widgets/txt/Txt.dart';
import '../dashboard_base.dart';

class CreditTab extends StatefulWidget {
  const CreditTab({Key key}) : super(key: key);

  @override
  State<CreditTab> createState() => _CreditTabState();
}

class _CreditTabState extends BaseDashboard<CreditTab> with StateListener {
  var scafoldKey = GlobalKey<ScaffoldState>();

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state, data) async {
    if (state == ObserverState.STATE_DRAWER && this.runtimeType != data) {
      Future.delayed(const Duration(seconds: 1), () {
        if (scafoldKey.currentState != null) {
          if (scafoldKey.currentState.isDrawerOpen) Navigator.pop(context);
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      try {
        RenderBox box3 = ccKey3.currentContext.findRenderObject() as RenderBox;
        posCCKey3 = box3.localToGlobal(Offset.zero); //this is global position
        setState(() {});
      } catch (e) {}
    });
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        bool isDrawerOpen = scafoldKey.currentState.isDrawerOpen;
        if (isDrawerOpen) {
          Navigator.pop(context); // close the drawer
          return Future.value(false); // don't allow app to navigate back
        } else {
          return Future.value(true); // allow app to navigate back
        }
      },
      child: Scaffold(
        //extendBodyBehindAppBar: true,
        //resizeToAvoidBottomInset: true,
        key: scafoldKey,
        backgroundColor: MyTheme.bgColor4,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white, //MyTheme.bgColor.withOpacity(.5),
          elevation: 0,
          titleSpacing: 0,
          title: Container(
            width: getW(context),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                    onPressed: () {
                      scafoldKey.currentState.openDrawer();
                    },
                    icon: Image.asset(
                      "assets/images/icons/drawer_ico.png",
                      fit: BoxFit.cover,
                      width: 25,
                      height: 25,
                    )),
                SizedBox(width: getWP(context, 15)),
                Text(
                  "Access My File",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: MyTheme.bgColor3, fontSize: 22),
                ),
              ],
            ),
          ),
        ),
        drawer: drawDrawer(),
        onDrawerChanged: (isOpen) {
          StateProvider().notify(ObserverState.STATE_DRAWER, this.runtimeType);
        },
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 10, left: 10),
                child: Txt(
                    txt: "Credit report summary",
                    txtColor: MyTheme.lblackColor,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
              SizedBox(height: 20),
              drawCreditCardTab2(),
              SizedBox(height: 10),
              GestureDetector(
                onTap: () {},
                child: Center(
                  child: Txt(
                      txt: "View my full report",
                      txtColor: MyTheme.bgColor3,
                      txtSize: MyTheme.txtSize - .3,
                      txtAlign: TextAlign.center,
                      fontWeight: FontWeight.w400,
                      isBold: false),
                ),
              ),
              SizedBox(height: 10),
              drawSummaryListView(),
            ],
          ),
        ),
      ),
    );
  }
}
