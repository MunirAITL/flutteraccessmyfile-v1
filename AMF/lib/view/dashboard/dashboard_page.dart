import 'dart:io';

import 'package:aitl/config/app/events/DeviceEventTypesCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/credit/credit_tab.dart';
import 'package:aitl/view/dashboard/marketplace/marketp_tab.dart';
import 'package:aitl/view/dashboard/today/today_tab.dart';
import 'package:aitl/view/splash/welcome_page.dart';
import 'package:aitl/view/widgets/botnav/bottomNavigation.dart';
import 'package:aitl/view/widgets/botnav/tabItem.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/BotNavController.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

//  next on https://pub.dev/packages/persistent_bottom_nav_bar

class DashboardPage extends StatefulWidget {
  static const int TAB_TODAY = 0;
  static const int TAB_CREDIT = 1;
  static const int TAB_MP = 4;

  @override
  State createState() => DashboardPageState();
}

class DashboardPageState extends State<DashboardPage>
    with Mixin, APIStateListener, StateListener {
  var _androidAppRetain = MethodChannel("android_app_retain");
  final List<Widget> listTabbar = [];
  static bool isDialogHelpOpenned = false;

  final botNavController = Get.put(BotNavController());

  //static int currentTab = 0;

  List<TabItem> tabs = [
    TabItem(
      tabName: "Today",
      icon: AssetImage("assets/images/tabbar/tab1.png"),
      //page: NewCaseTab(),
      page: TodayTabPage(),
    ),
    TabItem(
      tabName: "Credit",
      icon: AssetImage("assets/images/tabbar/tab2.png"),
      page: CreditTab(), //CreditReportSummaryPage(),
    ),
    TabItem(
      tabName: "Marketplace",
      icon: AssetImage("assets/images/tabbar/tab3.png"),
      page: MarketPlaceTab(), //MPPage(),
    ),
  ];

  DashboardPageState() {
    tabs.asMap().forEach((index, details) {
      details.setIndex(index);
    });
  }

  void _selectTab(int index) {
    //if (index == botNavController.index.value) {
    // pop to first route
    // if the user taps on the active tab
    //tabs[index].key.currentState.popUntil((route) => route.isFirst);
    //setState(() {});
    //} else {
    // update the state
    // in order to repaint

    if (mounted) {
      /*if (index == 0) {
        StateProvider().notify(ObserverState.STATE_RELOAD_TAB,
            PostTaskListPageState().runtimeType);
      } else if (index == 1) {
        StateProvider().notify(
            ObserverState.STATE_RELOAD_TAB, MyTasksPageState().runtimeType);
      } else if (index == 2) {
        StateProvider().notify(
            ObserverState.STATE_RELOAD_TAB, FindWorksPageState().runtimeType);
      } else if (index == 3) {
        StateProvider().notify(
            ObserverState.STATE_RELOAD_TAB, PrivateMsgPageState().runtimeType);
      } else if (index == 4) {
        StateProvider().notify(
            ObserverState.STATE_RELOAD_TAB, MorePageState().runtimeType);
      }*/
      setState(() => botNavController.index.value = index);
    }
    //}
  }

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state, dynamic data) {
    try {
      if (state == ObserverState.STATE_OPEN_TAB) {
        botNavController.isShowHelpDialogExtraHand.value = false;
        _selectTab(data ?? 0);
      }
      if (state == ObserverState.STATE_RELOAD_LOCAL) {
        setTabTitle();
      } else if (state == ObserverState.STATE_LOGOUT) {
        CookieMgr().delCookiee();
        DBMgr.shared.delTable("User");
        Get.offAll(
          () => WelcomePage(),
        ).then((value) {
          //callback(route);
        });
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.pending_review_rating_userid &&
          apiState.cls == this.runtimeType) {}
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      botNavController.dispose();
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.white,
      statusBarIconBrightness: Brightness.dark,
    ));

    setTabTitle();
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    try {
      //await APIHelper().wsFCMDeviceInfo(context, (model) {});
    } catch (e) {}
    /*try {
      APIViewModel().req<PendingReviewRatingByUserIdAPIModel>(
        context: context,
        apiState: APIState(
            APIType.pending_review_rating_userid, this.runtimeType, null),
        url: APIPostTaskCfg.PENDING_REVIEWRATING_BYUSERID_GET_URL
            .replaceAll("#userId#", userData.userModel.id.toString()),
        isLoading: false,
        reqType: ReqType.Get,
      );
    } catch (e) {}*/
  }

  setTabTitle() {
    int i = 0;
    tabs.asMap().forEach((index, details) {
      details.tabName = tabs[i++].tabName;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (Platform.isAndroid) {
          if (Navigator.of(context).canPop()) {
            return Future.value(true);
          } else {
            _androidAppRetain.invokeMethod("sendToBackground");
            return Future.value(false);
          }
        } else {
          return Future.value(true);
        }
      },
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.white,
          body:
              IndexedStack(
            index: botNavController.index.value,
            children: tabs.map((e) => e.page).toList(),
          ),
          // Bottom navigation
          bottomNavigationBar: BottomNavigation(
            context: context,
            onSelectTab: _selectTab,
            botNavController: botNavController,
            tabs: tabs,
            isHelpTut: isDialogHelpOpenned,
            totalMsg: 0,
            totalNoti: 0,
          ),
        ),
        //),
      ),
    );
  }
}
