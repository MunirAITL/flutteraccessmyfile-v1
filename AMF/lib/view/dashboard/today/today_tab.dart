import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/transunion_dialog.dart';
import 'package:aitl/view/widgets/grid/SliverGridDelegateWithFixedCrossAxisCountAndFixedHeight.dart';
import 'package:aitl/view/widgets/paint/CirclePainter.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:new_version/new_version.dart';
import 'offer_score_page.dart';

class TodayTabPage extends StatefulWidget {
  const TodayTabPage({Key key}) : super(key: key);
  @override
  State createState() => _TodayTabPageState();
}

class _TodayTabPageState extends BaseDashboard<TodayTabPage>
    with StateListener {
  var scafoldKey = GlobalKey<ScaffoldState>();

  final listOfferScore = [
    {
      "color": Colors.white,
      "image": Image.asset("assets/images/weekly_update/credit_cards.png",
          width: 50, height: 50),
      "title": "Credit Card",
    },
    {
      "color": Colors.white,
      "image": Image.asset("assets/images/weekly_update/personal_loans.png",
          width: 50, height: 50),
      "title": "Personal Loan",
    },
    {
      "color": Colors.white,
      "image": Image.asset("assets/images/weekly_update/car_buying.png",
          width: 65, height: 60),
      "title": "Car Buying",
    },
  ];

  final listExlusiveOffer = [
    {
      "card": "assets/images/img/today_card1.png",
      "txt1": "Get 6 months of 0% on Purchases with Master Card.",
      "txt2":
          "If you spend £1,200 at a purchase rate of 34.94%\n(variable p.a. your representative APR is 34.9%)",
    },
    {
      "card": "assets/images/img/today_card2.png",
      "txt1": "0% for 9 months on Balance Transfers.",
      "txt2":
          "If you spend £1,200 at a purchase rate of 34.94% (variable p.a. your representative APR is 34.9%)",
    },
  ];

  final listCreditScore = [
    {
      "url": Image.asset(
        "assets/images/weekly_update/logo1.png",
        width: 60,
        height: 60,
        fit: BoxFit.fill,
      ),
    },
    {
      "url": Image.asset(
        "assets/images/weekly_update/logo2.png",
        width: 60,
        height: 60,
        fit: BoxFit.fill,
      ),
    },
    {
      "url": Image.asset(
        "assets/images/weekly_update/logo3.png",
        width: 60,
        height: 60,
        fit: BoxFit.fill,
      ),
    },
    {
      "url": Image.asset(
        "assets/images/weekly_update/logo4.png",
        width: 60,
        height: 60,
        fit: BoxFit.fill,
      ),
    },
  ];

  final listBills = [
    {
      "url": Image.asset(
        "assets/images/ico/bill1_ico.png",
        width: 20,
        height: 20,
        fit: BoxFit.fill,
      ),
      "title": "Bill at a glance",
      "txt": "Split out your bill payments instantly",
    },
    {
      "url": Image.asset(
        "assets/images/ico/bill2_ico.png",
        width: 25,
        height: 20,
        fit: BoxFit.fill,
      ),
      "title": "Balance after bills",
      "txt": "Work out if you'll go into your overdraft",
    },
    {
      "url": Image.asset(
        "assets/images/ico/bill3_ico.png",
        width: 20,
        height: 20,
        fit: BoxFit.fill,
      ),
      "title": "Upcoming bills",
      "txt": "Look ahead and prepare for payments",
    },
    {
      "url": Image.asset(
        "assets/images/ico/bill4_ico.png",
        width: 20,
        height: 20,
        fit: BoxFit.fill,
      ),
      "title": "Secure connection",
      "txt": "Your data is protected by your bank",
    },
  ];

  final listCreditAlerts = [
    {
      "title": "Soft quotation search 1",
      "txt":
          "HSBC (SA,SR, TAC, CT BATCH) has carried out a search on your report so they can give you quotes.",
      "dt": "8 Nov",
    },
    {
      "title": "Soft quotation search 2",
      "txt":
          "HSBC (SA,SR, TAC, CT BATCH) has carried out a search on your report so they can give you quotes.",
      "dt": "9 Nov",
    },
    {
      "title": "Soft quotation search 3",
      "txt":
          "HSBC (SA,SR, TAC, CT BATCH) has carried out a search on your report so they can give you quotes.",
      "dt": "10 Nov",
    },
  ];

  final listWeeklyUpdate = [];

  checkNewerVersion() async {
    // Instantiate NewVersion manager object (Using GCP Console app as example)
    if (mounted) {
      final newVersion = NewVersion(
        iOSId: 'com.accessmyfile',
        androidId: 'com.accessmyfile',
      );
      newVersion.showAlertIfNecessary(context: context);
    }
  }

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state, data) async {
    if (state == ObserverState.STATE_DRAWER && this.runtimeType != data) {
      Future.delayed(const Duration(seconds: 1), () {
        if (scafoldKey.currentState != null) {
          if (scafoldKey.currentState.isDrawerOpen) Navigator.pop(context);
        }
      });
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      try {
        RenderBox box1 = ccKey1.currentContext.findRenderObject() as RenderBox;
        posCCKey1 = box1.localToGlobal(Offset.zero); //this is global position

        RenderBox box2 = ccKey2.currentContext.findRenderObject() as RenderBox;
        posCCKey2 = box2.localToGlobal(Offset.zero);

        RenderBox box3 = ccKey3.currentContext.findRenderObject() as RenderBox;
        posCCKey3 = box3.localToGlobal(Offset.zero); //this is global position

      } catch (e) {}

      listWeeklyUpdate.add(GestureDetector(
          onTap: () {
            //Get.to(() => ScoreFullReportPage());
          },
          child: drawListItems(
              image: Image.asset(
                "assets/images/ico/today_improve_score.png",
                width: 50,
                height: 20,
                fit: BoxFit.fill,
              ),
              text: "Improve my score")));

      listWeeklyUpdate.add(GestureDetector(
          onTap: () {
            //Get.to(() => ScoreFullReportPage());
          },
          child: drawListItems(
              image: Image.asset(
                "assets/images/ico/today_profile.png",
                width: 30,
                height: 25,
                fit: BoxFit.fill,
              ),
              text: "View my full profile")));

      listWeeklyUpdate.add(GestureDetector(
          onTap: () {
            //Get.to(() => ScoreFullReportPage());
          },
          child: drawListItems(
              image: Image.asset(
                "assets/images/ico/today_accesss_score.png",
                width: 22,
                height: 20,
                fit: BoxFit.fill,
              ),
              text: "About my score")));
      setState(() {});
    });
    /*WidgetsBinding.instance.addPostFrameCallback((_) async {
      await Get.dialog(TransUnionDialog());
      checkNewerVersion();
    });*/
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        bool isDrawerOpen = scafoldKey.currentState.isDrawerOpen;
        if (isDrawerOpen) {
          Navigator.pop(context); // close the drawer
          return Future.value(false); // don't allow app to navigate back
        } else {
          return Future.value(true); // allow app to navigate back
        }
      },
      child: Scaffold(
        //extendBodyBehindAppBar: true,
        //resizeToAvoidBottomInset: true,
        key: scafoldKey,
        backgroundColor: MyTheme.bgColor4,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white, //MyTheme.bgColor.withOpacity(.5),
          elevation: 0,
          titleSpacing: 0,
          title: Container(
            width: getW(context),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                    onPressed: () {
                      scafoldKey.currentState.openDrawer();
                    },
                    icon: Image.asset(
                      "assets/images/icons/drawer_ico.png",
                      fit: BoxFit.cover,
                      width: 25,
                      height: 25,
                    )),
                SizedBox(width: getWP(context, 15)),
                Text(
                  "Access My File",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: MyTheme.bgColor3, fontSize: 22),
                ),
              ],
            ),
          ),
        ),
        drawer: drawDrawer(),
        onDrawerChanged: (isOpen) {
          StateProvider().notify(ObserverState.STATE_DRAWER, this.runtimeType);
        },
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            drawWeeklyUpdates(),
            drawOfferScoreGrid(),
            drawExclusiveOffer(),
            drawCreditScore(),
            drawScoreCreditCard(isTitle: true),
            drawSmartHub(),
            drawBills(),
            drawMoreWays2Save(),
            drawCreditAlerts(),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  drawWeeklyUpdates() {
    return Container(
      color: MyTheme.skyColor,
      width: getW(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding:
                const EdgeInsets.only(left: 10, right: 10, top: 20, bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Txt(
                              txt: "Weekly Update",
                              txtColor: MyTheme.lblackColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 5),
                          Txt(
                            txt: "In 7 Days",
                            txtColor: Color(0xFF555574),
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: false,
                          ),
                          SizedBox(height: 15),
                          Text.rich(
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Your credit score went\nupto ',
                                  style: TextStyle(color: MyTheme.lblackColor),
                                ),
                                TextSpan(
                                  text: '20 points.\n',
                                  style: TextStyle(color: Colors.red),
                                ),
                                TextSpan(
                                  text: 'Great Job!',
                                  style: TextStyle(color: MyTheme.lblackColor),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                      child: Container(
                        height: getWP(context, 25),
                        width: getWP(context, 30),
                        child: CustomPaint(
                          foregroundPainter: CirclePainter(
                            context: context,
                            total: 750,
                            minPA: 30,
                            maxPA: 40,
                            width: getWP(context, 100),
                            height: getWP(context, 100),
                            fontSize: getTxtSize(
                                context: context, txtSize: MyTheme.txtSize),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.center,
                  child: Txt(
                    txt: "Your credit score is fair\nLet's make it better.",
                    txtColor: Color(0xFFDB3B61),
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.center,
                    fontWeight: FontWeight.w400,
                    isBold: false,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5, right: 5),
            child: Container(
              width: getW(context),
              height: getHP(context, 10),
              child: ListView.builder(
                shrinkWrap: true,
                primary: false,
                scrollDirection: Axis.horizontal,
                itemCount: listWeeklyUpdate.length,
                itemBuilder: (BuildContext context, int index) => Card(
                    elevation: 2,
                    color: Color(0xFFEFF0F6),
                    child: listWeeklyUpdate[index]),
              ),
            ),
          ),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  drawOfferScoreGrid() {
    final w = (getW(context) / 3) - 10;
    return Container(
      //color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding:
                const EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 10),
            child: getTitle("Offers Tailored To Score"),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5, right: 5),
            child: Container(
              width: getW(context),
              height: getHP(context, 18),
              child: ListView.builder(
                shrinkWrap: true,
                primary: false,
                scrollDirection: Axis.horizontal,
                itemCount: listOfferScore.length,
                itemBuilder: (BuildContext context, int index) {
                  final map = listOfferScore[index];
                  return Container(
                    width: w,
                    height: getHP(context, 18),
                    //color: Colors.black,
                    child: Column(
                      children: [
                        Container(
                          height: getHP(context, 12),
                          child: Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            color: map['color'],
                            child: Center(
                                child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                child: map['image'],
                              ),
                            )),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Txt(
                                txt: map['title'],
                                txtColor: MyTheme.lblackColor,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawExclusiveOffer() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding:
                const EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                getTitle("Zahirul's Exclusive Offers"),
                SizedBox(height: 5),
                Txt(
                    txt: "Top picks based on your score of 750",
                    txtColor: MyTheme.lblackColor.withOpacity(.5),
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.center,
                    isBold: false),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5, right: 5),
            child: ListView.builder(
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.vertical,
              itemCount: listExlusiveOffer.length,
              itemBuilder: (BuildContext context, int index) {
                final map = listExlusiveOffer[index];
                return Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: Card(
                    child: Container(
                      decoration: BoxDecoration(
                        color: MyTheme.skyColor,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          /*Transform.translate(
                            offset: Offset(-10, 0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/weekly_update/title_bg.png"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20,
                                        right: 40,
                                        bottom: 20,
                                        top: 10),
                                    child: Txt(
                                        txt: "Only on Access My File",
                                        txtColor: Colors.white,
                                        txtSize: MyTheme.txtSize - .6,
                                        txtAlign: TextAlign.center,
                                        isBold: true),
                                  ),
                                ),
                                Image.asset(
                                    "assets/images/weekly_update/stars.png",
                                    width: 50,
                                    height: 50)
                              ],
                            ),
                          ),*/
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: new Container(
                                width: getWP(context, 35),
                                decoration: new BoxDecoration(
                                    color: Color(0xFF0D3446),
                                    borderRadius: new BorderRadius.only(
                                      bottomLeft: const Radius.circular(5),
                                      bottomRight: const Radius.circular(5),
                                    )),
                                child: new Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4),
                                    child: new Text("Only on Access My File",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 9)),
                                  ),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 20, left: 20, bottom: 10, right: 20),
                            child: Container(
                              width: getW(context),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  index % 2 == 0
                                      ? drawTodayCreditCard1(
                                          map['card'].toString())
                                      : drawTodayCreditCard2(
                                          map['card'].toString()),
                                  /*Center(
                                    child: Image.asset(
                                      map['card'],
                                      width: getWP(context, 50),
                                      height: getWP(context, 30),
                                      fit: BoxFit.fill,
                                    ),
                                  ),*/
                                  SizedBox(height: 10),
                                  Padding(
                                    padding: const EdgeInsets.all(5),
                                    child: Txt(
                                        txt: map['txt1'],
                                        txtColor: HexColor.fromHex("#06152B"),
                                        txtSize: MyTheme.txtSize - .5,
                                        txtAlign: TextAlign.start,
                                        isBold: false),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(5),
                                    child: Txt(
                                        txt: map['txt2'],
                                        txtColor: HexColor.fromHex("#06152B"),
                                        txtSize: MyTheme.txtSize - .5,
                                        txtAlign: TextAlign.start,
                                        isBold: false),
                                  ),
                                  SizedBox(height: 20),
                                  Center(
                                    child: MMBtn(
                                        txt: "Check if i'm eligible",
                                        txtColor: MyTheme.btnTxtColor,
                                        bgColor: MyTheme.btnRedColor,
                                        height: getHP(context, 5.5),
                                        width: getWP(context, 45),
                                        radius: 10,
                                        callback: () {
                                          //Get.to(() => SigninPage());
                                        }),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  drawCreditScore() {
    final w = (getW(context) / 4) - 32;
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/images/icons/meter_ico.png",
                      width: 30,
                      height: 30,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(width: 10),
                    Txt(
                        txt: "Your Credit Score Is 750",
                        txtColor: MyTheme.lblackColor,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        fontWeight: FontWeight.w500,
                        isBold: false),
                  ],
                ),
                SizedBox(height: 5),
                Divider(height: 1, color: Colors.black),
                SizedBox(height: 10),
                Txt(
                    txt:
                        "Your score means you're likely to get 17 card offers from lenders like these.",
                    txtColor: MyTheme.lblackColor,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
                SizedBox(height: 10),
                Container(
                  width: getW(context),
                  height: w - 15,
                  //color: Colors.black,
                  child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.horizontal,
                    itemCount: listCreditScore.length,
                    itemBuilder: (BuildContext context, int index) {
                      final map = listCreditScore[index];
                      return Container(
                        //width: w + 15,
                        //height: w + 35,
                        child: Card(
                          elevation: .5,
                          /*shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),*/
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: map['url'],
                          ),
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(height: 20),
                MMBtn(
                    txt: "Check my offers",
                    txtColor: MyTheme.btnTxtColor,
                    bgColor: MyTheme.btnRedColor,
                    height: getHP(context, 5.5),
                    width: getWP(context, 50),
                    radius: 10,
                    callback: () {
                      //Get.to(() => OfferScorePage());
                    }),
                SizedBox(height: 10),
                Txt(
                    txt: "Checking offers never hurts your score",
                    txtColor: MyTheme.lblackColor,
                    txtSize: MyTheme.txtSize - .5,
                    txtAlign: TextAlign.start,
                    isBold: false),
                SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawSmartHub() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            getTitle("Your Smart Money Hub"),
            SizedBox(height: 10),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Txt(
                    txt: "Smart Move",
                    txtColor: Color(0xFF0D3446),
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.center,
                    fontWeight: FontWeight.w500,
                    isBold: true),
                GestureDetector(
                  onTap: () {},
                  child: Txt(
                      txt: "View all (17)",
                      txtColor: MyTheme.bgColor3.withOpacity(.6),
                      txtSize: MyTheme.txtSize - .4,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
              ],
            ),
            SizedBox(height: 10),
            Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                border:
                    Border.all(color: HexColor.fromHex("#CBCBCB"), width: 1),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 20,
                    height: 22,
                    child: Image.asset(
                      "assets/images/ico/bulb_ico.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(width: 10),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt:
                                "Your Octopus Energy Limited deal expired 400 days ago - Its likely overpaying.",
                            txtColor: MyTheme.lblackColor,
                            txtSize: MyTheme.txtSize - .5,
                            txtAlign: TextAlign.start,
                            txtLineSpace: 1.2,
                            isBold: false),
                        SizedBox(height: 10),
                        Txt(
                            txt: "Learn why it pays to switch now",
                            txtColor: MyTheme.bgColor3,
                            txtSize: MyTheme.txtSize - .5,
                            txtAlign: TextAlign.start,
                            fontWeight: FontWeight.w500,
                            isBold: false),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }

  drawBills() {
    return Container(
      color: Colors.white,
      width: getW(context),
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: MyTheme.btnRedColor,
              child: Padding(
                padding:
                    const EdgeInsets.only(left: 5, right: 5, top: 2, bottom: 2),
                child: Txt(
                    txt: "BETA",
                    txtColor: MyTheme.btnTxtColor,
                    txtSize: MyTheme.txtSize - .5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
            ),
            SizedBox(height: 5),
            getTitle("Stay On Top Of Your Bills"),
            SizedBox(height: 5),
            Txt(
                txt: "Gain early access to our new bill feature.",
                txtColor: MyTheme.lblackColor.withOpacity(.5),
                txtSize: MyTheme.txtSize - .6,
                txtAlign: TextAlign.center,
                isBold: false),
            SizedBox(height: 20),
            GridView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: listBills.length,
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCountAndFixedHeight(
                crossAxisCount: 2,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                height: getHP(context, 18),
              ),
              itemBuilder: (context, index) {
                final map = listBills[index];
                return new GridTile(
                  child: Container(
                      padding:
                          const EdgeInsets.only(top: 10, left: 10, right: 10),
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: HexColor.fromHex("#CBCBCB"), width: 1),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          map['url'],
                          SizedBox(height: 5),
                          Txt(
                              txt: map['title'],
                              txtColor: MyTheme.lblackColor,
                              txtSize: MyTheme.txtSize - .4,
                              txtAlign: TextAlign.start,
                              maxLines: 1,
                              fontWeight: FontWeight.w500,
                              isBold: true),
                          SizedBox(height: 5),
                          Txt(
                              txt: map['txt'].toString(),
                              txtColor: MyTheme.lblackColor,
                              txtSize: MyTheme.txtSize - .5,
                              txtAlign: TextAlign.start,
                              maxLines: 2,
                              isBold: false),
                        ],
                      )),
                );
              },
            ),
            SizedBox(height: 20),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        width: 40,
                        height: 40,
                        child: Image.asset("assets/images/ico/cards_ico.png")),
                    SizedBox(width: 10),
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Txt(
                              txt: "HALIFAX+ 2 Other Accounts",
                              txtColor: MyTheme.lblackColor,
                              txtSize: MyTheme.txtSize - .3,
                              txtAlign: TextAlign.start,
                              fontWeight: FontWeight.w500,
                              isBold: true),
                          SizedBox(height: 5),
                          Txt(
                              txt: "Ready to connect",
                              txtColor: MyTheme.lblackColor,
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Center(
                  child: MMBtn(
                      txt: "Connect my account",
                      txtColor: MyTheme.btnTxtColor,
                      bgColor: MyTheme.btnRedColor,
                      height: getHP(context, 5.5),
                      width: getWP(context, 70),
                      radius: 10,
                      callback: () {
                        //Get.to(() => SigninPage());
                      }),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  drawMoreWays2Save() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              getTitle("More Ways To Save"),
              SizedBox(height: 5),
              Txt(
                  txt: "Save money at 9A The Drive",
                  txtColor: MyTheme.lblackColor.withOpacity(.5),
                  txtSize: MyTheme.txtSize - .6,
                  txtAlign: TextAlign.center,
                  isBold: false),
              SizedBox(height: 20),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: HexColor.fromHex("#7EE6F4").withOpacity(.5),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          width: getWP(context, 45),
                          child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: Container(
                              width: 60,
                              height: 40,
                              child: Image.asset(
                                  "assets/images/weekly_update/wifi_ico.png"),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5, left: 5),
                          child: Txt(
                              txt: "Internet",
                              txtColor: MyTheme.lblackColor,
                              txtSize: MyTheme.txtSize - .4,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 20),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: HexColor.fromHex("#FFC9D6").withOpacity(.5),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          width: getWP(context, 45),
                          child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: Container(
                              width: 60,
                              height: 40,
                              child: Image.asset(
                                  "assets/images/weekly_update/mobile_ico.png"),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5, left: 5),
                          child: Txt(
                              txt: "Mobile",
                              txtColor: MyTheme.lblackColor,
                              txtSize: MyTheme.txtSize - .4,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ]),
          ),
          Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(0),
              ),
              child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: drawRowIconColText(
                      image: Image.asset(
                        "assets/images/ico/internet_ico.png",
                        width: 20,
                        height: 30,
                        fit: BoxFit.fill,
                      ),
                      title: "Find the fastest internet IG1 at the right price",
                      txt: "Find a faster, cheaper deal"))),
          SizedBox(height: 5),
          Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(0),
              ),
              child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: drawRowIconColText(
                      image: Image.asset(
                        "assets/images/ico/mobile_ico.png",
                        width: 20,
                        height: 30,
                        fit: BoxFit.fill,
                      ),
                      title: "Save money with a SIM-only data deal",
                      txt: "Pay less for my data"))),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  drawCreditAlerts() {
    return Container(
      width: getW(context),
      child: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(child: getTitle("Your Credit Card Alerts")),
                  GestureDetector(
                    onTap: () {},
                    child: Txt(
                        txt: "View all",
                        txtColor: MyTheme.bgColor3.withOpacity(.6),
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            ListView.builder(
                shrinkWrap: true,
                primary: false,
                scrollDirection: Axis.vertical,
                itemCount: listCreditAlerts.length,
                itemBuilder: (BuildContext context, int index) {
                  final map = listCreditAlerts[index];
                  return Card(
                    elevation: .5,
                    margin: EdgeInsets.all(1),
                    child: Container(
                        decoration: BoxDecoration(
                          color: (index % 2 == 0)
                              ? Colors.white
                              : MyTheme.bgColor4,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: drawRowIconColText(
                                image: Icon(Icons.search,
                                    color: MyTheme.lblackColor, size: 30),
                                title: map['title'],
                                txt: map['txt'],
                                dt: null))),
                  );
                }),
          ],
        ),
      ),
    );
  }
}
