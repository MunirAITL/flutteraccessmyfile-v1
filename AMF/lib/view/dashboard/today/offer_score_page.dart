import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OfferScorePage extends StatefulWidget {
  const OfferScorePage({Key key}) : super(key: key);

  @override
  State createState() => _OfferScorePageState();
}

class _OfferScorePageState extends BaseDashboard<OfferScorePage> {
  final listCreditScore = [
    {
      "url": "assets/images/weekly_update/logo1.png",
    },
    {
      "url": "assets/images/weekly_update/logo2.png",
    },
    {
      "url": "assets/images/weekly_update/logo3.png",
    },
    {
      "url": "assets/images/weekly_update/logo4.png",
    },
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        resizeToAvoidBottomInset: false,
        appBar: drawAppbar(
            bgColor: MyTheme.bgDark,
            onBack: () {
              Get.back();
            },
            backTitle: "Back"),
        bottomNavigationBar: drawFooter(),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            drawCreditScore(),
            drawOfferCreditCard(),
          ],
        ),
      ),
    );
  }

  drawCreditScore() {
    final w = (getW(context) / 4) - 32;
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      "assets/images/icons/meter_ico.png",
                      width: 40,
                      height: 40,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(width: 10),
                    Txt(
                        txt: "Your credit score is 602",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: true),
                  ],
                ),
                Divider(height: 1, color: Colors.black),
                SizedBox(height: 10),
                Txt(
                    txt:
                        "Your score means you're likely to get 17 card offers from lenders like these",
                    txtColor: HexColor.fromHex("#06152B"),
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
                SizedBox(height: 10),
                Container(
                  width: getW(context),
                  height: w - 15,
                  //color: Colors.black,
                  child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.horizontal,
                    itemCount: listCreditScore.length,
                    itemBuilder: (BuildContext context, int index) {
                      final map = listCreditScore[index];
                      return Container(
                        //width: w + 15,
                        //height: w + 35,
                        child: Card(
                          elevation: 1,
                          /*shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),*/
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: Container(
                                child: Image.asset(
                              map['url'],
                              //fit: BoxFit.fill,
                            )),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(height: 10),
                MMBtn(
                    txt: "Check my offers",
                    txtColor: Colors.white,
                    height: getHP(context, MyTheme.btnHpa),
                    width: getW(context),
                    radius: 10,
                    callback: () {
                      //Get.to(() => OfferScorePage());
                    }),
                SizedBox(height: 10),
                Txt(
                    txt: "Checking offers never hurts your score",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
                SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawOfferCreditCard() {
    final w = (getW(context) / 4) - 32;
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            Txt(
                txt: "CREDIT CARDS",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 5),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Image.asset(
                          "assets/images/icons/meter_ico.png",
                          width: 40,
                          height: 40,
                          fit: BoxFit.fill,
                        ),
                        SizedBox(width: 10),
                        Txt(
                            txt: "Your credit score is 602",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: true),
                      ],
                    ),
                    Divider(height: 1, color: Colors.black),
                    SizedBox(height: 10),
                    Txt(
                        txt:
                            "Given your score, you're likely to get 2 loan offers from lenders like these",
                        txtColor: HexColor.fromHex("#06152B"),
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(height: 10),
                    Container(
                      width: getW(context),
                      height: w - 15,
                      //color: Colors.black,
                      child: ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        scrollDirection: Axis.horizontal,
                        itemCount: listCreditCards.length,
                        itemBuilder: (BuildContext context, int index) {
                          final map = listCreditCards[index];
                          return Container(
                            //width: w + 15,
                            //height: w + 35,
                            child: Card(
                              elevation: 1,
                              /*shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),*/
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: map['url'],
                                //fit: BoxFit.fill,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(height: 10),
                    MMBtn(
                        txt: "See what i'm eligible for",
                        txtColor: Colors.white,
                        height: getHP(context, MyTheme.btnHpa),
                        width: getW(context),
                        radius: 10,
                        callback: () {
                          Get.to(() => OfferScorePage());
                        }),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
