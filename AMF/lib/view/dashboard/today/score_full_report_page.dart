import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/paint/CirclePainter.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class ScoreFullReportPage extends StatefulWidget {
  const ScoreFullReportPage({Key key}) : super(key: key);

  @override
  State createState() => _ScoreFullReportPageState();
}

class _ScoreFullReportPageState extends BaseDashboard<ScoreFullReportPage> {
  final listCreditFactors = [
    {
      "txt": "Credit limit",
      "status": "More to do",
      "color": HexColor.fromHex("#E69A24"),
      "process_txt": "£9,100",
      "rich_txt": Text.rich(
        TextSpan(
          children: [
            TextSpan(
                text:
                    'The combined credit limits reported for your 6 accounts.',
                style: TextStyle(color: HexColor.fromHex("#5A5A5A"))),
          ],
        ),
      ),
    },
    {
      "txt": "Payment history",
      "status": "Keep it going",
      "color": HexColor.fromHex("#2D4665"),
      "process_txt": "Perfect",
      "rich_txt": Text.rich(
        TextSpan(
          children: [
            TextSpan(
                text: 'You haven\'t missed any payments.',
                style: TextStyle(color: HexColor.fromHex("#5A5A5A"))),
          ],
        ),
      ),
    },
    {
      "txt": "Credit utilisation",
      "status": "More to do",
      "color": HexColor.fromHex("#E69A24"),
      "process_txt": "27%",
      "rich_txt": Text.rich(
        TextSpan(
          children: [
            TextSpan(
                text: 'You are using ',
                style: TextStyle(color: HexColor.fromHex("#5A5A5A"))),
            TextSpan(
              text: '27%',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: HexColor.fromHex("#5A5A5A")),
            ),
            TextSpan(
              text: ' of your total reported credit limit of ',
              style: TextStyle(color: HexColor.fromHex("#5A5A5A")),
            ),
            TextSpan(
              text: '£9,100.',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: HexColor.fromHex("#5A5A5A")),
            ),
          ],
        ),
      ),
    },
    {
      "txt": "Time on electoral roll",
      "status": "Keep it going",
      "color": HexColor.fromHex("#2D4665"),
      "process_txt": "9 yrs 1 mo",
      "rich_txt": Text.rich(
        TextSpan(
          children: [
            TextSpan(
                text:
                    'How long you\'ve been registered on the electoral roll at your current address.',
                style: TextStyle(color: HexColor.fromHex("#5A5A5A"))),
          ],
        ),
      ),
    },
    {
      "txt": "New accounts",
      "status": "Keep it going",
      "color": HexColor.fromHex("#2D4665"),
      "process_txt": "0",
      "rich_txt": Text.rich(
        TextSpan(
          children: [
            TextSpan(
                text:
                    'You haven\'t opened any new accounts in the last 6 months.',
                style: TextStyle(color: HexColor.fromHex("#5A5A5A"))),
          ],
        ),
      ),
    },
    {
      "txt": "Age of credit",
      "status": "Keep it going",
      "color": HexColor.fromHex("#2D4665"),
      "process_txt": "9 yrs 8 mo",
      "rich_txt": Text.rich(
        TextSpan(
          children: [
            TextSpan(
                text: 'Your ',
                style: TextStyle(color: HexColor.fromHex("#5A5A5A"))),
            TextSpan(
              text: 'Lloyds Bank (was Lloyds TSB)',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: HexColor.fromHex("#5A5A5A")),
            ),
            TextSpan(
              text: ' account is your oldest active account.',
              style: TextStyle(color: HexColor.fromHex("#5A5A5A")),
            ),
          ],
        ),
      ),
    },
    {
      "txt": "Mortgage",
      "status": "Take note",
      "color": Colors.red,
      "process_txt": "None",
      "rich_txt": Text.rich(
        TextSpan(
          children: [
            TextSpan(
                text:
                    'There\'s no active mortgage on your latest credit report.',
                style: TextStyle(color: HexColor.fromHex("#5A5A5A"))),
          ],
        ),
      ),
    },
  ];

  List<ChartData> chartData;

  @override
  void initState() {
    super.initState();
    chartData = <ChartData>[
      ChartData("Sept", 566),
      ChartData("Oct", 600),
      ChartData("Nov", 580),
      ChartData("Dec", 627),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        resizeToAvoidBottomInset: false,
        appBar: drawAppbar(
            bgColor: MyTheme.bgDark,
            onBack: () {
              Get.back();
            },
            backTitle: "Back",
            listBtn: [
              IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.info_outline,
                    color: Colors.white,
                  ))
            ]),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 10, right: 10, top: 20, bottom: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Txt(
                            txt:
                                "Your credit score is fair.\nLet's make it good.",
                            txtColor: HexColor.fromHex("#06152B"),
                            txtSize: MyTheme.txtSize + .2,
                            txtAlign: TextAlign.start,
                            isBold: true),
                        Container(
                          height: getWP(context, 25),
                          width: getWP(context, 25),
                          child: CustomPaint(
                            foregroundPainter: CirclePainter(
                              total: 602,
                              minPA: 30,
                              maxPA: 40,
                              width: getWP(context, 100),
                              height: getWP(context, 100),
                              fontSize: getTxtSize(
                                  context: context, txtSize: MyTheme.txtSize),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  drawTextBox("SCORE OVER TIME",
                      "Your score has fallen by 10 points since October.", ""),
                  drawTimeGraph(),
                ],
              ),
            ),
            SizedBox(height: 10),
            drawTextBox("Credit factors",
                "What's affecting your credit score and what needs work.", ""),
            SizedBox(height: 10),
            drawCreditFactorListItems(),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  drawTimeGraph() {
    return Container(
        child: Center(
            child: Container(
                child: SfCartesianChart(
                    primaryXAxis: CategoryAxis(),
                    series: <ChartSeries<ChartData, String>>[
          LineSeries<ChartData, String>(
              dataSource: chartData,
              xValueMapper: (ChartData sales, _) => sales.year,
              yValueMapper: (ChartData sales, _) => sales.sales),
        ]))));
  }

  drawCreditFactorListItems() {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        scrollDirection: Axis.vertical,
        itemCount: listCreditFactors.length,
        itemBuilder: (BuildContext context, int index) {
          final map = listCreditFactors[index];
          return Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                border:
                    Border.all(color: HexColor.fromHex("#CBCBCB"), width: 1),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        //flex: 2,
                        child: Txt(
                            txt: map['txt'],
                            txtColor: HexColor.fromHex("#5A5A5A"),
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            fontWeight: FontWeight.w500,
                            isBold: false),
                      ),
                      Spacer(),
                      Flexible(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Flexible(
                              child: Txt(
                                  txt: map['status'],
                                  txtColor: HexColor.fromHex("#5A5A5A"),
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.end,
                                  fontWeight: FontWeight.w500,
                                  isBold: false),
                            ),
                            SizedBox(width: 5),
                            UIHelper().drawCircle(
                                context: context, color: map['color'], size: 2),
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  Txt(
                      txt: map['process_txt'],
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize + .4,
                      txtAlign: TextAlign.start,
                      isBold: true),
                  SizedBox(height: 10),
                  map['rich_txt'],
                ],
              ),
            ),
          );
        });
  }
}

class ChartData {
  final String year;
  final double sales;
  ChartData(this.year, this.sales);
}
