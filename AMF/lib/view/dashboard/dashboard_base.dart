import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APINotiCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/credit_case/GetUserValidationForOldUserAPIMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/dashboard/credit_case/GetUserValidationOldUserResponse.dart';
import 'package:aitl/data/model/dashboard/more/help/PrivacyPolicyAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/help/TermsPrivacyNoticeSetups.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/splash/welcome_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:aitl/view_model/rx/SwitchController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'drawer/help/HelpScreen.dart';
import 'drawer/profile/change_pwd_page.dart';
import 'drawer/profile/profile_page.dart';
import 'drawer/star_rate/ratting_page.dart';

abstract class BaseDashboard<T extends StatefulWidget> extends State<T>
    with Mixin, UIHelper {
  final switchController = Get.put(SwitchController());
  final listCreditCards = [
    {
      "url": Image.asset(
        "assets/images/weekly_update/credit_card1.png",
        width: 60,
        height: 60,
        fit: BoxFit.fill,
      ),
    },
    {
      "url": Image.asset(
        "assets/images/weekly_update/credit_card2.png",
        width: 60,
        height: 60,
        fit: BoxFit.fill,
      ),
    },
    {
      "url": Image.asset(
        "assets/images/weekly_update/credit_card3.png",
        width: 60,
        height: 60,
        fit: BoxFit.fill,
      ),
    },
    {
      "url": Image.asset(
        "assets/images/weekly_update/credit_card4.png",
        width: 60,
        height: 60,
        fit: BoxFit.fill,
      ),
    },
  ];

  final listProducts = [
    {
      "color": HexColor.fromHex("#176D81").withOpacity(.2),
      "url": "assets/images/ico/report_sum_payoff.png",
      "title": "Pay off debt",
    },
    {
      "color": HexColor.fromHex("#DB3B61").withOpacity(.2),
      "url": "assets/images/ico/report_sum_buildcredit.png",
      "title": "Build your credit",
    },
    {
      "color": HexColor.fromHex("#E5D114").withOpacity(.2),
      "url": "assets/images/ico/report_sum_cutcost.png",
      "title": "Cut costs",
    },
  ];

  GlobalKey ccKey1 = GlobalKey();
  GlobalKey ccKey2 = GlobalKey();
  GlobalKey ccKey3 = GlobalKey();
  Offset posCCKey1, posCCKey2, posCCKey3;

  drawAppbar({
    Function onBack,
    String backTitle,
    String title,
    List<IconButton> listBtn,
    double elevation = 0,
    PreferredSizeWidget bottom,
    Color bgColor = Colors.transparent,
  }) {
    return AppBar(
      backgroundColor: bgColor,
      automaticallyImplyLeading: false,
      elevation: elevation,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          onBack != null
              ? Flexible(
                  child: GestureDetector(
                    onTap: () {
                      onBack();
                    },
                    child: Container(
                      color: Colors.transparent,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                            //size: 20,
                          ),
                          //SizedBox(width: 5),
                          /*(backTitle != null)
                              ? Padding(
                                  padding: const EdgeInsets.only(left: 0),
                                  child: Text(backTitle,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 17,
                                          color: Colors.white,
                                          fontWeight: FontWeight.normal)),
                                )
                              : SizedBox()*/
                        ],
                      ),
                    ),
                  ),
                )
              : SizedBox(),
          (title != null)
              ? Expanded(
                  flex: 3,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      //color: Colors.yellow,
                      child: Flexible(
                        child: FittedBox(
                          fit: BoxFit.fitWidth,
                          child: AutoSizeText(title,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal)),
                        ),
                      ),
                    ),
                  ),
                )
              : SizedBox(),
        ],
      ),
      actions: listBtn,
      bottom: bottom,
    );
  }

  drawDrawer() {
    return Container(
      width: getWP(context, 70),
      child: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: Container(
          color: MyTheme.drawerBGColor,
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 25, top: 60),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'SETTING',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 17),
                    ),
                    SizedBox(height: 15),
                    _drawerItems(
                        txt: "Profile",
                        callback: () {
                          Get.to(() => ProfilePage());
                        }),
                    SizedBox(height: 15),
                    /*_drawerItems(
                        txt: "Credit Report",
                        callback: () {
                          if (!Server.isOtp) {
                            /*Get.to(() => CreditDashBoardTabController(
          responseData: TestCreditReport().getUserValidationOldUser()));*/
                            /*GetUserValidationOldUserApiMgr()
                                .creditCaseOldUserInformation(
                              context: context,
                              userID: userData.userModel.id, //115767,
                              userCompanyId:
                                  userData.userModel.userCompanyID, //1003,
                              callback: (model) {
                                if (model != null && model.success) {
                                  GetUserValidationOldUser
                                      getUerValidationOldUser =
                                      model.responseData;
                                  if (getUerValidationOldUser
                                          .validateNewUser
                                          .validateNewUserResult
                                          .identityValidationOutcome ==
                                      CreditReportCfg
                                          .IdentityValidationOutcome10) {
                                    Get.to(
                                      () => CreditDashBoardTabController(
                                          responseData: model.responseData),
                                    );
                                  } else if (getUerValidationOldUser
                                          .validateNewUser
                                          .validateNewUserResult
                                          .identityValidationOutcome ==
                                      CreditReportCfg
                                          .IdentityValidationOutcome2) {
                                    Get.to(
                                      () => MultipleChoiceQuestions(
                                          responseData: model.responseData),
                                    );
                                  } else {
                                    Get.to(
                                      () => CreditUserInformation(
                                          responseData: model.responseData),
                                    );
                                  }
                                }
                              },
                            );*/
                          } else {
                            /*showAlrtDialog(
        context: context,
        title: "Unauthorised!",
        msg: "Your account is not authorised to use this feature.",
      );*/
                          }
                        }),
                    SizedBox(height: 15),*/
                    /*_drawerItems(
                        txt: "Change PIN",
                        callback: () {
                          Get.to(() => SigninPage());
                        }),
                    SizedBox(height: 15),*/
                    _drawerItems(
                        txt: "Reset Password",
                        callback: () {
                          Get.to(() => ChangePwdPage());
                        }),
                    SizedBox(height: 10),
                    Obx(
                      () => Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                              flex: 3,
                              child: _drawerItems(
                                  txt: "Allow push notifications",
                                  callback: () {})),
                          Flexible(
                            child: drawSwitchView(
                                isOn: switchController.isOn.value,
                                callback: (v) {
                                  switchController.isOn.value = v;
                                }),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    _drawerItems(
                        txt: "Rate the app",
                        callback: () {
                          Get.to(() => RattingPage());
                        }),
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: Divider(height: .5, color: Colors.grey),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20),
                    Text(
                      'RESOURCES',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 17),
                    ),
                    SizedBox(height: 15),
                    _drawerItems(
                        txt: "Help centre",
                        callback: () {
                          Get.to(() => HelpScreen());
                        }),
                    SizedBox(height: 15),
                    _drawerItems(txt: "Terms of service", callback: () {}),
                    SizedBox(height: 15),
                    _drawerItems(
                        txt: "Privacy policy",
                        callback: () {
                          openWSPrivacyPDF(context);
                        }),
                    SizedBox(height: 15),
                    _drawerItems(txt: "Licenses", callback: () {}),
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: Divider(height: .5, color: Colors.grey),
                    ),
                    SizedBox(height: 40),
                    GestureDetector(
                      onTap: () {
                        confirmDialog(
                            msg: "Are you sure, you want to log out ?",
                            title: "Alert !",
                            callbackYes: () async {
                              await CookieMgr().delCookiee();
                              await DBMgr.shared.delTable("User");
                              Get.offAll(() => WelcomePage());
                            },
                            callbackNo: () {
                              //Navigator.of(context, rootNavigator: true).pop();
                            },
                            context: context);
                      },
                      child: Container(
                        width: getW(context),
                        color: Colors.transparent,
                        child: Row(
                          children: [
                            Icon(Icons.logout,
                                color: MyTheme.bgColor3, size: 20),
                            SizedBox(width: 5),
                            Flexible(
                              child: Txt(
                                txt: "Sign out",
                                txtColor: MyTheme.bgColor3,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  getTitle(String title) {
    return Txt(
        txt: title,
        txtColor: MyTheme.lblackColor,
        txtSize: MyTheme.txtSize - .2,
        txtAlign: TextAlign.start,
        fontWeight: FontWeight.w500,
        isBold: false);
  }

  getFontSize(double size) {
    return ResponsiveFlutter.of(context).fontSize(size);
  }

  drawSwitchView({bool isOn, Function(bool) callback}) {
    return Transform.scale(
      scale: 0.55,
      child: CupertinoSwitch(
        //dragStartBehavior: widget.dragStartBehavior,

        value: isOn,
        onChanged: (v) {
          callback(v);
        },
        activeColor:
            isOn ? Color(0xFF0D3446).withOpacity(.5) : Colors.grey[100],
        trackColor: isOn ? MyTheme.bgColor : Colors.grey.shade400,
        thumbColor: Colors.white,
      ),
    );
  }

  _drawerItems({String txt, Function callback}) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Container(
        width: getW(context),
        color: Colors.transparent,
        child: Txt(
          txt: txt,
          txtColor: MyTheme.bgColor3,
          txtSize: MyTheme.txtSize - .2,
          txtAlign: TextAlign.start,
          isBold: false,
        ),
      ),
    );
  }

  drawListItems({Widget image, String text}) {
    return Container(
      width: getWP(context, 35),
      height: getHP(context, 10),
      //color: Colors.black,
      child: Padding(
        padding: const EdgeInsets.only(top: 5, left: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              child: image,
            ),
            SizedBox(width: 10),
            Expanded(
              flex: 4,
              child: Txt(
                  txt: text,
                  txtColor: MyTheme.lblackColor,
                  txtSize: MyTheme.txtSize - .5,
                  txtAlign: TextAlign.start,
                  maxLines: 2,
                  isBold: false),
            ),
          ],
        ),
      ),
    );
  }

  drawRowIconText({Widget image, String text}) {
    return Container(
      //width: getWP(context, 35),
      //height: getHP(context, 10),
      //color: Colors.black,
      child: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              child: image,
            ),
            SizedBox(width: 10),
            Expanded(
              flex: 4,
              child: Txt(
                  txt: text,
                  txtColor: MyTheme.lblackColor,
                  txtSize: MyTheme.txtSize - .5,
                  txtAlign: TextAlign.start,
                  maxLines: 2,
                  isBold: false),
            ),
          ],
        ),
      ),
    );
  }

  drawRowIconColText({Widget image, String title, String txt, String dt}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        image != null
            ? Padding(
                padding: const EdgeInsets.only(right: 20),
                child: image,
              )
            : SizedBox(),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Txt(
                        txt: title,
                        txtColor: MyTheme.lblackColor,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.start,
                        fontWeight: FontWeight.w400,
                        isBold: true),
                  ),
                  (dt != null)
                      ? Flexible(
                          child: Txt(
                              txt: dt,
                              txtColor: MyTheme.lblackColor.withOpacity(.5),
                              txtSize: MyTheme.txtSize - .5,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        )
                      : SizedBox(),
                ],
              ),
              SizedBox(height: 5),
              Txt(
                  txt: txt,
                  txtColor: MyTheme.lblackColor.withOpacity(.5),
                  txtSize: MyTheme.txtSize - .5,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ],
          ),
        ),
      ],
    );
  }

  drawTextBox(String title, String txt1, String txt2) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            getTitle(title),
            SizedBox(height: 5),
            Txt(
                txt: txt1,
                txtColor: MyTheme.lblackColor.withOpacity(.5),
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
            (txt2 != "")
                ? Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Txt(
                        txt: txt2,
                        txtColor: MyTheme.lblackColor.withOpacity(.5),
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  drawIconTextBox(Widget image, String title, String txt) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(
            top: title != '' ? 10 : 0, bottom: 10, left: 10, right: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            (title != '')
                ? Padding(
                    padding: const EdgeInsets.only(bottom: 5),
                    child: Txt(
                        txt: title,
                        txtColor: MyTheme.lblackColor,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        fontWeight: FontWeight.w500,
                        isBold: false),
                  )
                : SizedBox(),
            Txt(
                txt: txt,
                txtColor: MyTheme.lblackColor.withOpacity(.5),
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
          ],
        ),
      ),
    );
  }

  drawRowTxtBox(String txt1, String txt2) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
        child: Container(
          //color: Colors.green,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Txt(
                    txt: txt1,
                    txtColor: MyTheme.lblackColor,
                    txtSize: MyTheme.txtSize - .3,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Flexible(
                child: Txt(
                    txt: txt2,
                    txtColor: MyTheme.lblackColor,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    fontWeight: FontWeight.w400,
                    isBold: false),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawFooter() {
    return Container(
      height: getHP(context, 15),
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                  txt: "See any errors on your credit report?",
                  txtColor: MyTheme.lblackColor,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  fontWeight: FontWeight.w400,
                  isBold: false),
              SizedBox(height: 10),
              MMBtn(
                  txt: "RAISE A DISPUTE WITH TRANSUNION",
                  bgColor: MyTheme.redColor,
                  txtColor: MyTheme.btnTxtColor,
                  width: getW(context),
                  height: getHP(context, 7),
                  radius: 10,
                  callback: () {
                    //Get.to(() => SigninPage());
                  }),
            ],
          ),
        ),
      ),
    );
  }

  openWSPrivacyPDF(context) async {
    var url = ServerUrls.TERMS_POLICY;
    url = url.replaceAll(
        "#UserCompanyId#", userData.userModel.userCompanyID.toString());

    await APIViewModel().req<PrivacyPolicyAPIModel>(
      context: context,
      url: url,
      reqType: ReqType.Get,
      callback: (model) async {
        if (model != null) {
          try {
            if (model.success) {
              debugPrint(
                  "size = ${model.responseData.termsPrivacyNoticeSetupsList.length}");

              for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                  in model.responseData.termsPrivacyNoticeSetupsList) {
                if (termsPrivacyPoliceSetups.type.toString() ==
                        "Customer Privacy Notice" ||
                    termsPrivacyPoliceSetups.type.toString() ==
                        "Customer Case Agreement") {
                  Get.to(
                    () => PDFDocumentPage(
                      title: "Privacy",
                      url: termsPrivacyPoliceSetups.webUrl,
                    ),
                  ).then((value) {
                    //callback(route);
                  });
                  break;
                }
              }
            } else {
              //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.redColor,msg: model.errorMessages.toString());
              showToast(context: context, msg: "Sorry, something went wrong");
            }
          } catch (e) {
            myLog(e.toString());
          }
        } else {
          myLog("dashboard screen not in");
        }
      },
    );
  }

  //  ****************************************************************

  drawSummaryListView() {
    return Container(
      child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getTitle("Report Summary"),
              SizedBox(height: 5),
              Txt(
                  txt:
                      "See which products could help you out during this uncertain time.",
                  txtColor: MyTheme.lblackColor.withOpacity(.5),
                  txtSize: MyTheme.txtSize - .5,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 5),
              ListView.builder(
                shrinkWrap: true,
                primary: false,
                scrollDirection: Axis.vertical,
                itemCount: listProducts.length,
                itemBuilder: (BuildContext context, int index) {
                  final map = listProducts[index];
                  return GestureDetector(
                    onTap: () {},
                    child: Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: HexColor.fromHex("#CBCBCB"),
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(0),
                        ),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Container(
                                  //color: map['color'],
                                  child: Center(
                                      child: Image.asset(
                                map['url'],
                                width: 30,
                                height: 30,
                              ))),
                            ),
                            SizedBox(width: 10),
                            Flexible(
                              child: Txt(
                                  txt: map['title'],
                                  txtColor: MyTheme.lblackColor,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  maxLines: 1,
                                  isBold: false),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
              SizedBox(height: 20),
            ],
          )),
    );
  }

  drawScoreCreditCard({bool isTitle = true}) {
    final w = (getW(context) / 4) - 32;
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            isTitle
                ? Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: getTitle("Credit Cards"))
                : SizedBox(),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Image.asset(
                          "assets/images/icons/meter_ico.png",
                          width: 30,
                          height: 30,
                          fit: BoxFit.fill,
                        ),
                        SizedBox(width: 10),
                        Txt(
                            txt: "Your Credit Score Is 750",
                            txtColor: MyTheme.lblackColor,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            fontWeight: FontWeight.w500,
                            isBold: false),
                      ],
                    ),
                    SizedBox(height: 5),
                    Divider(height: 1, color: Colors.black),
                    SizedBox(height: 10),
                    Txt(
                        txt:
                            "Given your score, you're likely to get 2 loan offers from lenders like these",
                        txtColor: MyTheme.lblackColor,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(height: 10),
                    Container(
                      width: getW(context),
                      height: w - 15,
                      //color: Colors.black,
                      child: ListView.builder(
                        shrinkWrap: true,
                        primary: false,
                        scrollDirection: Axis.horizontal,
                        itemCount: listCreditCards.length,
                        itemBuilder: (BuildContext context, int index) {
                          final map = listCreditCards[index];
                          return Container(
                            //width: w + 15,
                            //height: w + 35,
                            child: Card(
                              elevation: .5,
                              /*shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),*/
                              child: Padding(
                                padding: const EdgeInsets.all(5),
                                child: map['url'],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(height: 10),
                    MMBtn(
                        txt: "See what i'm eligible for",
                        txtColor: MyTheme.btnTxtColor,
                        bgColor: MyTheme.btnRedColor,
                        height: getHP(context, 5.5),
                        width: getWP(context, 70),
                        radius: 10,
                        callback: () {
                          //Get.to(() => OfferScorePage());
                        }),
                    SizedBox(height: 10),
                    Txt(
                        txt:
                            "Searching our marketplace reveals your offers without hurting your score.",
                        txtColor: MyTheme.lblackColor,
                        txtSize: MyTheme.txtSize - .5,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  //  **************************************************  DRAW CREDIT CARD TEXT START HERE....

  drawCreditCardTab2() {
    final x = posCCKey3 == null ? 0.0 : posCCKey3.dx;
    final y = posCCKey3 == null ? 0.0 : posCCKey3.dy;
    return Stack(
      children: [
        Center(
          child: Container(
            key: ccKey3,
            width: getWP(context, 85),
            height: getHP(context, 25),
            child: Image.asset(
              "assets/images/img/card_bg.png",
              fit: BoxFit.fill,
            ),
          ),
        ),
        Positioned(
            top: y * .1,
            right: x + 25,
            child: Container(
              width: 22,
              height: 22,
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
              ),
            )),
        Positioned(
            top: y * .1,
            right: x + 15,
            child: Container(
              width: 22,
              height: 22,
              decoration: BoxDecoration(
                color: Color(0xFFDADADA).withOpacity(.5),
                shape: BoxShape.circle,
              ),
            )),
        Positioned(
          top: y * .35,
          left: x + 15,
          child: Txt(
              txt: "Balance",
              txtColor: HexColor.fromHex("#FFFFFF"),
              txtSize: getFontSize(.3),
              txtAlign: TextAlign.start,
              fontWeight: FontWeight.w400,
              isBold: false),
        ),
        Positioned(
          top: y * .5,
          left: x + 15,
          child: Txt(
              txt: AppDefine.CUR_SIGN + " 2,653.65",
              txtColor: HexColor.fromHex("#FFFFFF"),
              txtSize: getFontSize(.5),
              txtAlign: TextAlign.start,
              isBold: true),
        ),
        Positioned(
          top: y * .8,
          left: x + 15,
          child: Text("**** **** **** 1234",
              style: TextStyle(
                color: Colors.white,
                fontSize: getFontSize(2.5),
                fontFamily: "OCRAEXT",
                fontWeight: FontWeight.w500,
              )),
        )
      ],
    );
  }

  drawCreditCardTab3() {
    final x = posCCKey3 == null ? 0.0 : posCCKey3.dx;
    final y = posCCKey3 == null ? 0.0 : posCCKey3.dy;
    return Stack(
      children: [
        Center(
          child: Container(
            key: ccKey3,
            width: getWP(context, 85),
            height: getHP(context, 25),
            child: Image.asset(
              "assets/images/img/card_bg.png",
              fit: BoxFit.fill,
            ),
          ),
        ),
        Positioned(
            top: y * .05,
            right: x + 25,
            child: Container(
              width: 22,
              height: 22,
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
              ),
            )),
        Positioned(
            top: y * .05,
            right: x + 15,
            child: Container(
              width: 22,
              height: 22,
              decoration: BoxDecoration(
                color: Color(0xFFDADADA).withOpacity(.5),
                shape: BoxShape.circle,
              ),
            )),
        Positioned(
          top: y * .12,
          left: x + 15,
          child: Txt(
              txt: "Balance",
              txtColor: HexColor.fromHex("#FFFFFF"),
              txtSize: getFontSize(.3),
              txtAlign: TextAlign.start,
              fontWeight: FontWeight.w400,
              isBold: false),
        ),
        Positioned(
          top: y * .17,
          left: x + 15,
          child: Txt(
              txt: AppDefine.CUR_SIGN + " 2,653.65",
              txtColor: HexColor.fromHex("#FFFFFF"),
              txtSize: getFontSize(.5),
              txtAlign: TextAlign.start,
              isBold: true),
        ),
        Positioned(
          top: y * .28,
          left: x + 15,
          child: Text("**** **** **** 1234",
              style: TextStyle(
                color: Colors.white,
                fontSize: getFontSize(2.5),
                fontFamily: "OCRAEXT",
                fontWeight: FontWeight.w500,
              )),
        )
      ],
    );
  }

  drawTodayCreditCard1(String card) {
    final x = posCCKey1 == null ? 0.0 : posCCKey1.dx;
    final y = posCCKey1 == null ? 0.0 : posCCKey1.dy;
    return Stack(
      children: [
        Center(
          child: Container(
            key: ccKey1,
            width: getWP(context, 70),
            height: getWP(context, 40),
            child: Image.asset(
              card,
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned(
          top: y * .08,
          left: x + 15,
          child: Text("**** **** **** 1234",
              style: TextStyle(
                color: Colors.white,
                fontSize: getFontSize(1.8),
                fontFamily: "OCRAEXT",
                fontWeight: FontWeight.w500,
              )),
        ),
        Positioned(
          top: y * .13,
          left: x + 15,
          child: Txt(
              txt: "Cardholder Name",
              txtColor: HexColor.fromHex("#FFFFFF"),
              txtSize: getFontSize(.1),
              txtAlign: TextAlign.start,
              fontWeight: FontWeight.w400,
              isBold: false),
        ),
        Positioned(
          top: y * .13,
          right: x + 15,
          child: Txt(
              txt: "Expiry Date",
              txtColor: HexColor.fromHex("#FFFFFF"),
              txtSize: getFontSize(.1),
              txtAlign: TextAlign.start,
              fontWeight: FontWeight.w400,
              isBold: false),
        ),
        Positioned(
          top: y * .15,
          left: x + 15,
          child: Text("Zahirul Islam",
              style: TextStyle(
                color: Colors.white,
                fontSize: getFontSize(1.5),
                //fontFamily: "OCRAEXT",
                fontWeight: FontWeight.w500,
              )),
        ),
        Positioned(
          top: y * .15,
          right: x + 15,
          child: Text("01/28",
              style: TextStyle(
                color: Colors.white,
                fontSize: getFontSize(1.5),
                fontFamily: "OCRAEXT",
                fontWeight: FontWeight.w500,
              )),
        ),
      ],
    );
  }

  drawTodayCreditCard2(String card) {
    final x = posCCKey2 == null ? 0.0 : posCCKey2.dx;
    final y = posCCKey2 == null ? 0.0 : posCCKey2.dy;
    return Stack(
      children: [
        Center(
          child: Container(
            key: ccKey2,
            width: getWP(context, 70),
            height: getWP(context, 40),
            child: Image.asset(
              card,
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned(
          top: y * .05,
          left: x + 15,
          child: Text("**** **** **** 1234",
              style: TextStyle(
                color: Colors.white,
                fontSize: getFontSize(1.8),
                fontFamily: "OCRAEXT",
                fontWeight: FontWeight.w500,
              )),
        ),
        Positioned(
          top: y * .075,
          left: x + 15,
          child: Txt(
              txt: "Valid\nUntil",
              txtColor: HexColor.fromHex("#FFFFFF"),
              txtSize: getFontSize(.1),
              txtAlign: TextAlign.start,
              fontWeight: FontWeight.w400,
              isBold: false),
        ),
        Positioned(
          top: y * .078,
          left: x + 40,
          child: Text("01/28",
              style: TextStyle(
                color: Colors.white,
                fontSize: getFontSize(1.5),
                fontFamily: "OCRAEXT",
                fontWeight: FontWeight.w500,
              )),
        ),
        Positioned(
          top: y * .1,
          left: x + 15,
          child: Text("Zahirul Islam",
              style: TextStyle(
                color: Colors.white,
                fontSize: getFontSize(1.5),
                fontFamily: "OCRAEXT",
                fontWeight: FontWeight.w500,
              )),
        ),
      ],
    );
  }

  //  **************************************************  DRAW CREDIT CARD TEXT END
}
