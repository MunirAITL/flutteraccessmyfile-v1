import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/switchview/ToggleSwitch.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

class SwitchTitle extends StatefulWidget with Mixin {
  final txt;
  int switchIndex;
  final Function(bool) callback;
  SwitchTitle({
    Key key,
    @required this.txt,
    @required this.switchIndex,
    @required this.callback,
  }) : super(key: key);

  @override
  State createState() => _SwitchTitleState();
}

class _SwitchTitleState extends State<SwitchTitle> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    //_stateProvider.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Txt(
              txt: widget.txt,
              txtColor: MyTheme.gray5Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false,
              //txtLineSpace: 1.5,
            ),
          ),
          //SizedBox(width: 5),
          ToggleSwitch(
            minWidth: getWP(context, 15),
            minHeight: getHP(context, MyTheme.switchBtnHpa),
            initialLabelIndex: widget.switchIndex,
            cornerRadius: 50.0,
            activeFgColor: Colors.white,
            inactiveBgColor: HexColor.fromHex("#cad7dc"),
            inactiveFgColor: Colors.white,
            labels: ['Yes', 'No'],
            fontSize: 14,
            //icons: [FontAwesomeIcons.mars, FontAwesomeIcons.venus],
            activeBgColor: HexColor.fromHex("#1caade"),
            onToggle: (index) {
              widget.switchIndex = index;
              bool isSwitch = (index == 0) ? true : false;
              widget.callback(isSwitch);
              setState(() {});
            },
          ),
        ],
      ),
    );
  }
}
