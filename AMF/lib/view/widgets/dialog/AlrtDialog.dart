import 'dart:ui';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

showAlrtDialog({
  BuildContext context,
  String msg,
  String title,
}) {
  AwesomeDialog(
      dialogBackgroundColor: Colors.white,
      context: context,
      animType: AnimType.SCALE,
      dialogType: DialogType.NO_HEADER,
      //showCloseIcon: true,
      //buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
      //customHeader: Icon(Icons.info, size: 50),
      body: Center(
        child: Stack(
            overflow: Overflow.visible,
            alignment: Alignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  (title != null)
                      ? Padding(
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, top: 20),
                          child: Text(
                            title,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.w500),
                          ),
                        )
                      : SizedBox(),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Text(
                      msg,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 17),
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
              Positioned(
                top: -50,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child:
                        Icon(Icons.warning, color: MyTheme.bgColor, size: 40)),
              )
            ]),
      ),
      //title: 'This is Ignored',
      //desc: 'This is also Ignored',
      btnOkText: "Ok",
      btnOkColor: MyTheme.bgColor,
      btnOkOnPress: () {
        //Navigator.pop(context);
        //callback(email.text.trim());
      })
    ..show();

  /*return Dialog(
      backgroundColor: Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Card(
          color: widget.bgColor,
          elevation: 10,
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    widget.title != null
                        ? Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: Text(
                              widget.title,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: widget.txtColor,
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                height: 1.5,
                              ),
                            ),
                          )
                        : SizedBox(),
                    widget.title != null
                        ? Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: Divider(color: Colors.black, height: 10),
                          )
                        : SizedBox(),
                    Text(
                      widget.msg,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: widget.txtColor,
                        fontSize: 17,
                        fontWeight: FontWeight.normal,
                        height: 1.5,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: 150,
                alignment: Alignment.center,
                child: TextButton(
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(MyTheme.statusBarColor)),
                  onPressed: () {
                    Get.back();
                  },
                  child: Text(
                    "Ok",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );*/
}
