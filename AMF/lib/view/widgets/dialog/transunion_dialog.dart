import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TransUnionDialog extends StatefulWidget {
  const TransUnionDialog({Key key}) : super(key: key);

  @override
  State createState() => _TransUnionDialogState();
}

class _TransUnionDialogState extends State<TransUnionDialog> with Mixin {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.all(10),
      child: Container(
        height: getHP(context, 55),
        width: getW(context),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Container(
                      margin: const EdgeInsets.all(2),
                      padding: const EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Icon(Icons.close)),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Container(
                  //color: Colors.black,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                          child: Container(
                              width: getWP(context, 35),
                              height: getWP(context, 35),
                              child: Image.asset(
                                  "assets/images/weekly_update/dialog_transunion.png"))),
                      Txt(
                          txt: "About your credit score",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          fontWeight: FontWeight.w500,
                          isBold: true),
                      SizedBox(height: 10),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text: 'This is your ',
                                style: TextStyle(
                                    color: Colors.black54, fontSize: 15)),
                            TextSpan(
                              text: 'TransUnion',
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  color: MyTheme.bgColor,
                                  fontSize: 15),
                            ),
                            TextSpan(
                                text:
                                    ' score. We ask them for an update weekly, but it won\'t always have changed.',
                                style: TextStyle(
                                    color: Colors.black54, fontSize: 15)),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                          'You can find out what\'s behind it and how to improve it on your Finance pages.',
                          style:
                              TextStyle(color: Colors.black54, fontSize: 15)),
                      SizedBox(height: 10),
                      Text(
                          'Learn about the Needs Work, Fair, Good, and Excellent ratings we use.',
                          style: TextStyle(
                              color: HexColor.fromHex("#2D4665"),
                              fontSize: 15,
                              fontWeight: FontWeight.w500)),
                      SizedBox(height: 20),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
