import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

// ignore: non_constant_identifier_names
confirmDialog({
  BuildContext context,
  String title = "",
  String msg = "",
  int which = 0,
  Function callbackYes,
  Function callbackNo,
}) {
  AwesomeDialog(
      dialogBackgroundColor: Colors.white,
      context: context,
      animType: AnimType.SCALE,
      dialogType: DialogType.NO_HEADER,
      //showCloseIcon: true,
      //buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
      //customHeader: Icon(Icons.info, size: 50),
      body: Center(
        child: Stack(
            overflow: Overflow.visible,
            alignment: Alignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  title != null
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Center(
                            child: Txt(
                              txt: title ?? '',
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize + 1,
                              isBold: true,
                              txtAlign: TextAlign.center,
                            ),
                          ),
                        )
                      : SizedBox(),
                  SizedBox(height: 20.0),
                  Center(
                    child: Txt(
                      txtAlign: TextAlign.center,
                      txt: msg,
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      isBold: false,
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
              Positioned(
                top: -50,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.info, color: MyTheme.bgColor, size: 40)),
              )
            ]),
      ),
      //title: 'This is Ignored',
      //desc: 'This is also Ignored',
      btnOkText: "Yes",
      btnOkColor: MyTheme.bgColor,
      btnOkOnPress: () {
        callbackYes();
      },
      btnCancelText: "No",
      btnCancelColor: Colors.grey,
      btnCancelOnPress: () {
        if (callbackNo != null) callbackNo();
      })
    ..show();

  /*return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (ctx) => Dialog(
          insetAnimationDuration: const Duration(microseconds: 1000),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          child: Container(
            margin: EdgeInsets.only(left: 0.0, right: 0.0),
            child: Stack(alignment: Alignment.center, children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  top: 18.0,
                ),
                margin: EdgeInsets.only(top: 30),
                decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        color: Colors.black26,
                        blurRadius: 0.0,
                        offset: Offset(0.0, 0.0),
                      ),
                    ]),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          Center(
                            child: Txt(
                              txt: alertTitle,
                              txtColor: MyTheme.statusBarColor,
                              txtSize: MyTheme.txtSize + 1,
                              isBold: true,
                              txtAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(height: 20.0),
                          Center(
                            child: Txt(
                              txtAlign: TextAlign.center,
                              txt: alertBody,
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              isBold: false,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 15.0),
                    Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: cancelClick,
                            child: Container(
                              height: 60,
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(15.0),
                                ),
                              ),
                              child: Center(
                                child: Txt(
                                  txtAlign: TextAlign.center,
                                  txt: cancelTxt,
                                  txtColor: MyTheme.statusBarColor,
                                  txtSize: MyTheme.txtSize,
                                  isBold: false,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: InkWell(
                            onTap: deleteClick,
                            /*() {
                    // Navigator.of(context, rootNavigator: true).pop();
      
                  },*/
                            child: Container(
                              height: 60,
                              decoration: BoxDecoration(
                                color: MyTheme.statusBarColor,
                                borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(15.0),
                                ),
                              ),
                              child: Center(
                                child: Txt(
                                  txtAlign: TextAlign.center,
                                  txt: deleteTxt,
                                  txtColor: Colors.white,
                                  txtSize: MyTheme.txtSize,
                                  isBold: false,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 5,
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            10), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                        ),
                        child: Icon(Icons.info,
                            color: MyTheme.dBlueAirColor, size: 30)),
                  ),
                ),
              ),
            ]),
          )));*/
}
