import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

showDeactivateProfileDialog(
    {BuildContext context,
    TextEditingController email,
    Function callback}) async {
  AwesomeDialog(
      dialogBackgroundColor: Colors.white,
      context: context,
      animType: AnimType.SCALE,
      dialogType: DialogType.NO_HEADER,
      //buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
      //customHeader: Icon(Icons.info, size: 50),
      body: Center(
        child: Stack(
            overflow: Overflow.visible,
            alignment: Alignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
                child: TextField(
                  controller: email,
                  keyboardType: TextInputType.emailAddress,
                  minLines: 3,
                  maxLines: 5,
                  maxLength: 255,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize:
                        ResponsiveFlutter.of(context).fontSize(MyTheme.txtSize),
                  ),
                  decoration: InputDecoration(
                    isDense: true,
                    counterText: "",
                    labelText: 'Reason?',
                    labelStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: ResponsiveFlutter.of(context)
                          .fontSize(MyTheme.txtSize),
                    ),
                    hintMaxLines: 1,
                    enabledBorder: const OutlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.black, width: .5),
                    ),
                    border: new OutlineInputBorder(
                        borderSide: new BorderSide(color: Colors.blue)),
                  ),
                ),
              ),
              Positioned(
                top: -50,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child:
                        Icon(Icons.warning, color: MyTheme.bgColor, size: 40)),
              )
            ]),
      ),
      //title: 'This is Ignored',
      //desc: 'This is also Ignored',
      btnOkText: "Deactivate",
      btnOkColor: MyTheme.redColor,
      btnOkOnPress: () {
        //Navigator.pop(context);
        callback(email.text.trim());
      },
      btnCancelColor: Colors.grey,
      btnCancelOnPress: () {
        //Navigator.pop(context);
      })
    ..show();
  /*await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            backgroundColor: MyTheme.bgColor2,
            title: Row(
              children: [
                Expanded(
                  child: Txt(
                      txt: "My Profile",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                IconButton(
                    icon: Icon(Icons.close, color: Colors.black, size: 30),
                    onPressed: () {
                      Navigator.pop(context);
                      //callback(null);
                    })
              ],
            ),
            content: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: email,
                    keyboardType: TextInputType.emailAddress,
                    minLines: 3,
                    maxLines: 5,
                    maxLength: 255,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: ResponsiveFlutter.of(context)
                          .fontSize(MyTheme.txtSize),
                    ),
                    decoration: InputDecoration(
                      labelText: 'Cancel Reason?',
                      labelStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: ResponsiveFlutter.of(context)
                            .fontSize(MyTheme.txtSize),
                      ),
                      hintMaxLines: 1,
                      enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 0.0),
                      ),
                      border: new OutlineInputBorder(
                          borderSide: new BorderSide(color: Colors.blue)),
                    ),
                  ),
                  MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                      callback(email.text.trim());
                    },
                    child: Txt(
                        txt: "Deactivate",
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    color: MyTheme.brandColor,
                  ),
                ],
              ),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)));
      });*/
}
