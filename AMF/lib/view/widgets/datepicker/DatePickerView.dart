import 'package:aitl/view/widgets/txt/IcoTxtIco.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';

class DatePickerView extends StatelessWidget {
  final DateTime initialDate;
  final DateTime firstDate;
  final DateTime lastDate;
  final String cap;
  final String dt;
  Color capTxtColor;
  bool isCapBold;
  Color txtColor;
  Color bgColor;
  double topbotHeight;
  IconData leftIcon;
  IconData rightIcon;
  double borderWidth;
  final Function callback;

  DatePickerView({
    Key key,
    @required this.cap,
    @required this.dt,
    @required this.callback,
    @required this.initialDate,
    @required this.firstDate,
    @required this.lastDate,
    this.txtColor = Colors.black,
    this.bgColor,
    this.isCapBold = false,
    this.capTxtColor = Colors.black,
    this.topbotHeight = 0,
    this.leftIcon = Icons.calendar_today,
    this.rightIcon = Icons.keyboard_arrow_down,
    this.borderWidth,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          (this.cap != null)
              ? Txt(
                  txt: cap,
                  txtColor: MyTheme.lblackColor,
                  txtSize: MyTheme.txtSize - .3,
                  txtAlign: TextAlign.start,
                  fontWeight: FontWeight.w500,
                  isBold: false)
              : SizedBox(),
          GestureDetector(
            onTap: () {
              /*
                initialDate : Default Selected Date In Picker Dialog
                firstDate : From Minimum Date Of Your Date Picker
                lastDate : Max Date Of To-Date Of Date Picker
              */
              showDatePicker(
                context: context,
                //locale: LngTR().getLocale(),
                initialDate: initialDate,
                firstDate: firstDate,
                lastDate: lastDate,
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme: ColorScheme.light(primary: MyTheme.bgColor3),
                      buttonTheme:
                          ButtonThemeData(textTheme: ButtonTextTheme.primary),
                    ), // This will change to light theme.
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  callback(value);
                }
              });
            },
            child: IcoTxtIco(
                leftIcon: leftIcon,
                txt: this.dt,
                txtColor: txtColor,
                bgColor: bgColor,
                rightIcon: rightIcon,
                txtAlign: TextAlign.left,
                rightIconSize: 25,
                leftIconSize: 15,
                topbotHeight: topbotHeight,
                borderWidth: borderWidth ?? .5),
          ),
        ],
      ),
    );
  }
}
