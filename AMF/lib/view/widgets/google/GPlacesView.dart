// to get places detail (lat/lng)
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/google/GPlacesAPIKey.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/translations/LanguageTranslations.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:aitl/mixin.dart';
//import 'package:google_api_headers/google_api_headers.dart';
//  https://github.com/fluttercommunity/flutter_google_places/issues/140
//  https://stackoverflow.com/questions/55879550/how-to-fix-httpexception-connection-closed-before-full-header-was-received

class GPlacesView extends StatefulWidget {
  final String address;
  final String title;
  Color bgColor;
  Color txtColor;
  bool isTxtBold;
  double titlePadding;
  final Function(String address, Location loc) callback;
  GPlacesView({
    Key key,
    @required this.title,
    this.address = "",
    this.bgColor = Colors.transparent,
    this.txtColor = Colors.black,
    this.isTxtBold = true,
    this.titlePadding = 15,
    @required this.callback,
  }) : super(key: key);
  @override
  _GPlacesViewState createState() => _GPlacesViewState();
}

class _GPlacesViewState extends State<GPlacesView> with Mixin {
  @override
  Widget build(BuildContext context) {
    final address = (widget.address == "") ? "search".tr : widget.address;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        (widget.title != null)
            ? Txt(
                txt: widget.title,
                txtColor: MyTheme.lblackColor,
                txtSize: MyTheme.txtSize - .3,
                txtAlign: TextAlign.start,
                fontWeight: FontWeight.w500,
                isBold: false)
            : SizedBox(),
        new GestureDetector(
          onTap: () => _handlePressButton(),
          child: new Container(
            width: getW(context),
            //height: getHP(context, 9),
            //margin: const EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(
              //color: Colors.transparent,
              //borderRadius: BorderRadius.circular(5),
              border: Border(
                bottom: BorderSide(
                  color: (address.toLowerCase() == "search")
                      ? Colors.grey
                      : MyTheme.bgColor3,
                  width: .5,
                ),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(width: 5),
                  Icon(
                    Icons.search,
                    color: (address.toLowerCase() == "search")
                        ? Colors.grey
                        : widget.txtColor,
                    size: 20,
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Txt(
                        txt: address,
                        txtColor: (address.toLowerCase() == "search")
                            ? MyTheme.lgreyColor
                            : MyTheme.lblackColor,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  (address == "search")
                      ? SizedBox()
                      : GestureDetector(
                          child: Icon(
                            Icons.close,
                            size: 20,
                            color: (address.toLowerCase() == "search")
                                ? Colors.grey
                                : widget.txtColor,
                          ),
                          onTap: () {
                            widget.callback('', null);
                          },
                        ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  void onError(PlacesAutocompleteResponse response) {
    print(response.errorMessage.toString());
    //homeScaffoldKey.currentState.showSnackBar(
    //SnackBar(content: Text(response.errorMessage)),
    //);
  }

  Future<void> _handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: GPlacesAPIKey.ApiKey,
      onError: onError,
      mode: Mode.overlay,
      language: "en",
      types: [""],
      strictbounds: false,
      components: [Component(Component.country, "uk")],
    );
    if (p != null) {
      //displayPrediction(p, (String address, Location loc) {
      //widget.callback(address, loc);
      //});

      try {
        GoogleMapsPlaces _places = GoogleMapsPlaces(
          apiKey: GPlacesAPIKey.ApiKey,
          //apiHeaders: await GoogleApiHeaders().getHeaders(),
        );
        if (_places != null) {
          PlacesDetailsResponse detail =
              await _places.getDetailsByPlaceId(p.placeId);
          String address = detail.result.formattedAddress; //p.description;
          widget.callback(address, detail.result.geometry.location);
        }
      } catch (e) {
        myLog(e.toString());
      }
    }
  }
}
