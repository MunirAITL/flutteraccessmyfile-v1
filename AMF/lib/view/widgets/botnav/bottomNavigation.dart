import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/BotNavController.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:get/get.dart';
import 'tabItem.dart';
import 'package:badges/badges.dart';

class BottomNavigation extends StatefulWidget {
  final BuildContext context;
  final ValueChanged<int> onSelectTab;
  final botNavController;
  final List<TabItem> tabs;
  final bool isHelpTut;
  final int totalMsg;
  int totalNoti;
  BottomNavigation({
    @required this.context,
    @required this.onSelectTab,
    @required this.botNavController,
    @required this.tabs,
    @required this.isHelpTut,
    @required this.totalMsg,
    @required this.totalNoti,
  });

  @override
  State createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation>
    with Mixin, StateListener {
  int curTab = 0;
  int unreadNotiCount = 0;

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state, dynamic data) async {
    try {
      if (state == ObserverState.STATE_BOTNAV) {
        unreadNotiCount =
            await PrefMgr.shared.getPrefInt("unreadNotificationCount");
        setState(() {});
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    curTab = widget.botNavController.index.value;

    return BottomAppBar(
      //color: MyTheme.bgColor,
      shape: CircularNotchedRectangle(),
      //notchMargin: 4,
      //clipBehavior: Clip.antiAlias,
      child: Container(
        decoration: BoxDecoration(
            //color: Colors.white,
            border: Border(top: BorderSide(color: Colors.black, width: .5))),
        child: BottomNavigationBar(
          //selectedLabelStyle: TextStyle(fontSize: 14),
          selectedItemColor: Colors.white,
          currentIndex: curTab,
          //unselectedLabelStyle: TextStyle(fontSize: 14),
          elevation: MyTheme.botbarElevation,
          unselectedItemColor: Colors.white60,
          backgroundColor: MyTheme.bgColor3,
          type: BottomNavigationBarType.fixed,
          items: widget.tabs
              .map(
                (e) => _buildItem(
                  index: e.getIndex(),
                  icon: e.icon,
                  tabName: e.tabName,
                ),
              )
              .toList(),
          onTap: (index) => widget.onSelectTab(
            index,
          ),
        ),
      ),
    );
  }

  BottomNavigationBarItem _buildItem(
      {int index, AssetImage icon, String tabName}) {
    int totalBadge = (index == 3 && widget.totalMsg > 0) ? widget.totalMsg : 0;
    if (index == 4 && widget.totalNoti > 0) {
      totalBadge = widget.totalNoti - unreadNotiCount;
      if (widget.totalNoti < 0) totalBadge = 0;
    }
    //widget.totalNoti = 0;
    if (widget.botNavController.isShowHelpDialogExtraHand.value) curTab = 2;
    return BottomNavigationBarItem(
      icon: (widget.isHelpTut && curTab == index)
          ? Stack(clipBehavior: Clip.none, children: <Widget>[
              (index == 3 || index == 4)
                  ? Badge(
                      badgeColor: MyTheme.redColor,
                      showBadge: (totalBadge > 0) ? true : false,
                      //position: BadgePosition.topStart(start: -10),
                      badgeContent: Text(
                        totalBadge.toString(),
                        style: TextStyle(color: Colors.white, fontSize: 10),
                      ),
                      child: (index == 4 && totalBadge > 0)
                          ? null
                          : ImageIcon(
                              icon,
                              color: _tabColor(index: index),
                            ),
                    )
                  : ImageIcon(
                      icon,
                      color: _tabColor(index: index),
                    ),
              new Positioned(
                top: (widget.botNavController.isShowHelpDialogExtraHand.value)
                    ? -getHP(widget.context, 29)
                    : -getHP(widget.context, 9),
                right: -10,
                child: Image.asset(
                  "assets/images/icons/hand_" +
                      ((widget.botNavController.isShowHelpDialogExtraHand.value)
                          ? 'up'
                          : 'down') +
                      ".png",
                  width: 60,
                  height: 70,
                ),
              )
            ])
          : Badge(
              badgeColor: MyTheme.redColor,
              showBadge: (totalBadge > 0) ? true : false,
              //position: BadgePosition.topStart(start: -10),
              badgeContent: Text(
                totalBadge.toString(),
                style: TextStyle(color: Colors.white, fontSize: 10),
              ),
              child: (index == 4 && totalBadge > 0)
                  ? null
                  : ImageIcon(
                      icon,
                      color: _tabColor(index: index),
                    ),
            ),
      // ignore: deprecated_member_use
      label: tabName.tr,
    );
  }

  Color _tabColor({int index}) {
    return curTab == index ? Colors.white : Colors.white60;
  }
}
