import 'dart:io';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

class CamPicker with Mixin {
  List<String> _items = ['From Gallery', 'From Camera', 'Skip'];

  Future<File> _openCamera({isRear = true, String key}) async {
    final serviceStatus = await Permission.camera.status;
    if (!serviceStatus.isGranted) {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.camera,
      ].request();
    }
    final pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      //preferredCameraDevice: (isRear) ? CameraDevice.rear : CameraDevice.front,
    );

    if (pickedFile != null) {
      File path = File(pickedFile.path);
      await PrefMgr.shared.setPrefStr(key, pickedFile.path);
      return path;
    }
  }

  Future<File> _openGallery({String key}) async {
    final serviceStatus = await Permission.photos.status;
    if (!serviceStatus.isGranted) {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.photos,
      ].request();
    }
    final pickedFile = await ImagePicker().pickImage(
      source: ImageSource.gallery,
    );
    if (pickedFile != null) {
      File path = File(pickedFile.path);
      await PrefMgr.shared.setPrefStr(key, pickedFile.path);
      return path;
    }
  }

  void showCamDialog(
      {BuildContext context, Function callback, bool isRear = true}) {
    AwesomeDialog(
        dialogBackgroundColor: Colors.white,
        context: context,
        animType: AnimType.SCALE,
        dialogType: DialogType.NO_HEADER,
        //showCloseIcon: true,
        //buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
        //customHeader: Icon(Icons.info, size: 50),
        body: Center(
          child: Stack(
              overflow: Overflow.visible,
              alignment: Alignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 10),
                  child: Text(
                    "Choose image source",
                    style: TextStyle(color: MyTheme.bgColor, fontSize: 20),
                  ),
                ),
                Positioned(
                  top: -50,
                  child: Container(
                      //width: 50.0,
                      //height: 50.0,
                      padding: const EdgeInsets.all(
                          10), //I used some padding without fixed width and height
                      decoration: new BoxDecoration(
                        shape: BoxShape
                            .circle, // You can use like this way or like the below line
                        //borderRadius: new BorderRadius.circular(30.0),
                        color: Colors.white,
                      ),
                      child:
                          Icon(Icons.info, color: MyTheme.bgColor, size: 40)),
                )
              ]),
        ),
        //title: 'This is Ignored',
        //desc: 'This is also Ignored',
        btnOkText: "Camera",
        btnOkColor: MyTheme.bgColor,
        btnOkOnPress: () async {
          callback(await _openCamera(isRear: isRear));
        },
        btnCancelText: "Gallery",
        btnCancelColor: Colors.grey,
        btnCancelOnPress: () async {
          callback(await _openGallery());
        })
      ..show();

    /*showDialog<int>(
      context: context,
      builder: (context) => AlertDialog(
          backgroundColor: MyTheme.bgColor2,
          content: Text(
            "Choose image source",
            style: TextStyle(color: MyTheme.dBlueAirColor),
          ),
          actions: [
            TextButton(
              style: ButtonStyle(
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(
                              color: MyTheme.dBlueAirColor, width: .5)))),
              child: Text(
                "Gallery",
                style: TextStyle(color: MyTheme.dBlueAirColor),
              ),
              onPressed: () => Navigator.pop(context, 0),
            ),
            SizedBox(width: 10),
            TextButton(
              style: ButtonStyle(
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(
                              color: MyTheme.dBlueAirColor, width: .5)))),
              child: Text(
                "Camera",
                style: TextStyle(color: MyTheme.dBlueAirColor),
              ),
              onPressed: () => Navigator.pop(context, 1),
            ),
          ]),
    ).then((int choice) async {
      if (choice != null) {
        switch (choice) {
          case 0:
            callback(await _openGallery()); //  gallery
            break;
          case 1:
            callback(await _openCamera(isRear: isRear)); //  cam
            break;
          default:
            break;
        }
      }
    });*/
  }
}
