import 'package:aitl/config/theme/MyTheme.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

drawAppbarText({
  bool isNext = true,
  String txt,
  Color txtColor = Colors.white,
  Function callback,
}) =>
    GestureDetector(
      onTap: () {
        callback();
      },
      child: Center(
        child: Container(
          child: RichText(
            text: (isNext)
                ? TextSpan(
                    children: [
                      TextSpan(
                          text: txt,
                          style: TextStyle(color: txtColor, fontSize: 17)),
                      TextSpan(
                        text: " >",
                        style: TextStyle(color: txtColor, fontSize: 17),
                      ),
                    ],
                  )
                : TextSpan(
                    children: [
                      TextSpan(
                          text: "< ",
                          style: TextStyle(color: txtColor, fontSize: 17)),
                      TextSpan(
                        text: txt,
                        style: TextStyle(color: txtColor, fontSize: 17),
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
