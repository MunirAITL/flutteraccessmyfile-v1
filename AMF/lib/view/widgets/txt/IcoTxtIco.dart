import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

class IcoTxtIco extends StatelessWidget with Mixin {
  final IconData leftIcon;
  final IconData rightIcon;
  final String txt;
  TextAlign txtAlign;
  double leftIconSize;
  double rightIconSize;
  double height;
  double topbotHeight;
  Color iconColor;
  Color txtColor;
  Color bgColor;
  double borderWidth;

  IcoTxtIco({
    Key key,
    @required this.txt,
    @required this.leftIcon,
    @required this.rightIcon,
    this.iconColor = Colors.white,
    this.txtColor = Colors.white60,
    this.txtAlign = TextAlign.center,
    this.leftIconSize = 15,
    this.rightIconSize = 20,
    this.height = 10,
    this.topbotHeight = 0,
    this.borderWidth = .5,
    this.bgColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //height: getHP(context, MyTheme.btnHpa),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Colors.grey,
            width: .5,
          ),
        ),
      ),
      alignment: Alignment.center,
      child: Container(
        color: bgColor,
        child: Padding(
          padding: EdgeInsets.only(
              left: (leftIcon != null) ? 10 : 0,
              right: 0,
              top: topbotHeight,
              bottom: topbotHeight),
          child: Row(
            //dense: true,
            //contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
            children: [
              (leftIcon != null)
                  ? Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Icon(
                        leftIcon,
                        color: (txt.toLowerCase().contains("date"))
                            ? Colors.grey
                            : txtColor,
                        size: leftIconSize,
                      ),
                    )
                  : SizedBox(),
              Expanded(
                child: Txt(
                  txt: txt,
                  txtColor: (txt.toLowerCase().contains("date"))
                      ? Colors.grey
                      : MyTheme.lblackColor,
                  txtSize: MyTheme.txtSize - .3,
                  txtAlign: TextAlign.start,
                  isBold: false,
                ),
              ),
              (rightIcon != null)
                  ? Icon(
                      rightIcon,
                      color: (txt.toLowerCase().contains("date"))
                          ? Colors.grey
                          : txtColor,
                      size: rightIconSize,
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
