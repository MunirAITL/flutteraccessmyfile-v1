import 'dart:io';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/webview/WebControl.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/WebviewController.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

class WebScreen extends StatefulWidget {
  final String url;
  final String title;
  final String caseID;

  const WebScreen({
    Key key,
    this.caseID,
    @required this.url,
    @required this.title,
  }) : super(key: key);

  @override
  State createState() => _WebScreenState();
}

class _WebScreenState extends State<WebScreen> with Mixin {
  final webviewController = Get.put(WebviewController());
  final CookieManager cookieManager = CookieManager.instance();

  var cookieStr;
  var listCookies;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    webviewController.dispose();
    cookieStr = null;
    //flutterWebviewPlugin.close();
    //flutterWebviewPlugin.dispose();
    //webView = null;
    super.dispose();
  }

  appInit() async {
    try {
      print(widget.caseID);

      CookieJar cj = await CookieMgr().getCookiee();
      listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();
      myLog(cookieStr);
      if (cookieStr.length > 0) {
        cookieManager.setCookie(
          url: Uri.parse(Server.BASE_URL),
          name: listCookies[0].name,
          value: listCookies[0].value,
          domain: listCookies[0].domain,
          path: listCookies[0].path,
          maxAge: listCookies[0].maxAge,
          //expiresDate: listCookies[0].expires,
          isSecure: true,
          //isHttpOnly: true,
        );
      }
      setState(() {});
    } catch (e) {
      cookieStr = "";
      if (mounted) setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      //resizeToAvoidBottomPadding :true,
      //resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: MyTheme.appbarElevation,
        backgroundColor: MyTheme.bgColor2,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(
              webviewController.appTitle.value == ""
                  ? Icons.arrow_back
                  : Icons.close,
              color: Colors.black),
          onPressed: () async {
            if (webviewController.appTitle.value == "")
              Get.back();
            else {
              StateProvider().notify(ObserverState.STATE_WEBVIEW_BACK, null);
              webviewController.appTitle.value = "";
            }
          },
        ),
        title: Obx(() => (webviewController.progress.value < 100)
            ? Container(
                width: getW(context),
                child: Text(webviewController.progress.value.toString() + "%",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.black,
                    )),
              )
            : UIHelper().drawAppbarTitle(
                title: (webviewController.appTitle.value == "")
                    ? widget.title
                    : webviewController.appTitle.value,
                txtColor: Colors.black)),
        centerTitle: true,
        bottom: PreferredSize(
          preferredSize:
              Size.fromHeight((webviewController.isLoading.value) ? .5 : 0),
          child: Obx(() => (webviewController.isLoading.value)
              ? AppbarBotProgBar(
                  backgroundColor: MyTheme.appbarProgColor,
                )
              : SizedBox()),
        ),
      ),
      body: Opacity(
          opacity: (cookieStr != null) ? 1 : 0,
          child: (cookieStr != null)
              ? WebControl(
                  url: widget.url,
                  cookieStr: cookieStr,
                  listCookies: listCookies,
                  caseID: widget.caseID,
                )
              : SizedBox()),
    );
  }
}
