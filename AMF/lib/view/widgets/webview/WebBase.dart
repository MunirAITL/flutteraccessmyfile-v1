import 'dart:convert';
import 'dart:io';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/data/model/dashboard/more/help/PrivacyPolicyAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/dialog/alert_dialog.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view_model/helper/helper/privacy/PrivacyPolicyAPIMgr.dart';
import 'package:aitl/view_model/rx/WebviewController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:path/path.dart' as p;
//import 'package:webview_flutter/webview_flutter.dart';  //  FOR IOS
import 'package:flutter_webview_pro/webview_flutter.dart'; //  FOR ANDROID

abstract class WebBase<T extends StatefulWidget> extends State<T> with Mixin {
  final webviewController = Get.put(WebviewController());
  WebViewController wvc;
  Map<String, String> header;

  onHideWebview();
  onShowWebview();

  loadUrl(String url) {
    if (Platform.isAndroid) {
      //  FOR ANDROID
      wvc.loadUrl(url, headers: header);
    } else {
      //  FOR IOS
      /*wvc
    .loadRequest(WebViewRequest(
        uri: Uri.parse(url),
        headers: header,
        method: WebViewRequestMethod.get))
    .then((value) =>
        webviewController.isLoading.value = false);*/
    }
  }

  //  HANDLERS
  onBack() {
    Navigator.of(context).pop();
  }

  onSubmitApplication(caseID, String message) {}

  onDownloadFileModule(String message) async {
    await NetworkMgr.shared.downloadFile(
      context: context,
      url: message,
      callback: (f) {
        showToast(
            context: context, msg: "File downloaded successfully", which: 1);
      },
    );
  }

  onFullViewFileHandler(String message, Function(String) callbackReload) {
    final extension = p.extension(message);
    print(extension);
    if (extension.toLowerCase() == ".pdf") {
      onHideWebview();
      Get.to(
        () => PDFDocumentPage(
          title: "Document View",
          url: message,
        ),
      ).then((value) => onShowWebview());
    } else if (extension.toLowerCase() == ".doc" ||
        extension.toLowerCase() == ".docx" ||
        extension.toLowerCase() == ".xls") {
      callbackReload(
          "http://drive.google.com/viewerng/viewer?embedded=true&url=" +
              message);
    } else {
      onHideWebview();
      Get.to(() => PicFullView(
            url: message,
            title: "Document View",
          )).then((value) {
        onShowWebview();
      });
    }
  }

  onClickTermsOfBusiness(
      String message, Function(String) callbackReload) async {
    final url = PrivacyPolicyHelper().getUrl();
    myLog("PrivacyPolicyHelper= " + url);
    onHideWebview();
    await NetworkMgr()
        .req<PrivacyPolicyAPIModel, Null>(
      context: context,
      reqType: ReqType.Get,
      url: url,
      isLoading: true,
    )
        .then((model) async {
      for (var m in model.responseData.termsPrivacyNoticeSetupsList) {
        if (m.type == "Terms of Business") {
          final extension = p.extension(m.webUrl);
          print(extension);
          if (extension.toLowerCase() == ".pdf") {
            callbackReload(
                "http://drive.google.com/viewerng/viewer?embedded=true&url=" +
                    m.webUrl);
          } else {
            callbackReload(m.webUrl);
          }
          webviewController.appTitle.value = "Terms of Business";
          break;
        }
      }
      Future.delayed(Duration(seconds: AppConfig.AlertDismisSec), () {
        onShowWebview();
      });
    });
  }

  onCallbackclientagreement() {
    //Get.offAll(MainCustomerScreen());
  }

  onCallbackclientrecommendationagreement() {
    //Get.offAll(MainCustomerScreen());
  }
}
