import 'dart:async';
import 'dart:io';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/webview/WebBase.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:webview_cookie_manager/webview_cookie_manager.dart';
import 'package:get/get.dart';
//import 'package:webview_flutter/webview_flutter.dart';  //  FOR IOS
import 'package:flutter_webview_pro/webview_flutter.dart'; //  FOR ANDROID

class WebControl extends StatefulWidget {
  final String url;
  final cookieStr;
  final listCookies;
  final String caseID;
  const WebControl({
    Key key,
    @required this.url,
    @required this.cookieStr,
    @required this.listCookies,
    @required this.caseID,
  }) : super(key: key);

  @override
  State createState() => _WebControlState();
}

class _WebControlState extends WebBase<WebControl> with StateListener {
  StateProvider _stateProvider;

  @override
  onStateChanged(ObserverState state, data) async {
    if (state == ObserverState.STATE_WEBVIEW_BACK) {
      wvc.goBack();
      Future.delayed(Duration(seconds: 1), () {
        webviewController.isLoading.value = false;
      });
    }
  }

  onHideWebview() {
    //_webViewController.hide();
  }

  onShowWebview() {
    //_webViewController.show();
  }

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    _stateProvider = new StateProvider();
    _stateProvider.subscribe(this);
  }

  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        WebView(
          debuggingEnabled: true,
          gestureNavigationEnabled: true,
          allowsInlineMediaPlayback: true,
          initialMediaPlaybackPolicy: AutoMediaPlaybackPolicy.always_allow,
          //initialUrl: widget.url,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            wvc = webViewController;
            header = {'Cookie': widget.cookieStr};
            loadUrl(widget.url);
          },
          onProgress: (progress2) {
            webviewController.progress.value = progress2;
          },
          onPageStarted: (url) {
            webviewController.isLoading.value = true;
          },
          onPageFinished: (url) {
            webviewController.isLoading.value = false;
          },
          javascriptChannels: [
            JavascriptChannel(
                name: 'Back',
                onMessageReceived: (message) {
                  onBack();
                }),
            JavascriptChannel(
                //  HELP:
                name: 'SubmitApplication',
                onMessageReceived: (message) {
                  onSubmitApplication(widget.caseID, message.message);
                }),
            JavascriptChannel(
                name: 'DownloadFileModule',
                onMessageReceived: (message) {
                  onDownloadFileModule(message.message);
                }),
            JavascriptChannel(
                name: 'onFullViewFileHandler',
                onMessageReceived: (message) {
                  onFullViewFileHandler(message.message, (url) {
                    webviewController.appTitle.value = "Document View";
                    loadUrl(url);
                  });
                }),
            JavascriptChannel(
                name: 'onClickTermsOfBusiness',
                onMessageReceived: (message) {
                  onClickTermsOfBusiness(message.message, (url) {
                    webviewController.appTitle.value = "Document View";
                    loadUrl(url);
                  });
                }),
            JavascriptChannel(
                name: 'callbackclientrecommendationagreement',
                onMessageReceived: (message) {
                  onCallbackclientrecommendationagreement();
                }),
            JavascriptChannel(
                name: 'callbackclientagreement',
                onMessageReceived: (message) {
                  onCallbackclientagreement();
                }),
          ].toSet(),
        ),
        Obx(() => Visibility(
              visible: webviewController.isLoading.value,
              child: Center(
                child: CircularProgressIndicator(color: MyTheme.brandColor),
              ),
            ))
      ],
    );
  }
}
