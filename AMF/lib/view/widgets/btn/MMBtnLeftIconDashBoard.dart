import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class MMBtnLeftIconDashBoard extends StatelessWidget with Mixin {
  final String txt;
  final String imageFile;
  Color bgColor;
  final double width;
  final double height;
  final double radius;
  final Function callback;
  Color textColor;

  MMBtnLeftIconDashBoard({
    Key key,
    @required this.txt,
    @required this.imageFile,
    @required this.width,
    @required this.height,
    this.radius = 10,
    @required this.callback,
    this.bgColor,
    this.textColor,
  }) {
    if (textColor == null) textColor = MyTheme.txtColor;
    if (bgColor == null) bgColor = MyTheme.bgColor2;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: (height != null) ? height : getHP(context, 6),
      alignment: Alignment.center,
      decoration: new BoxDecoration(
        color: bgColor,
        borderRadius: new BorderRadius.circular(radius),
      ),
      child: ElevatedButton(
        style: ButtonStyle(
            overlayColor: MaterialStateProperty.all<Color>(Colors.grey),
            backgroundColor: MaterialStateProperty.all<Color>(bgColor),
            elevation: MaterialStateProperty.all<double>(0),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(radius)),
              // side: BorderSide(color: Colors.red)
            ))),
        onPressed: () {
          callback();
        },
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              (imageFile != null)
                  ? Padding(
                      padding: const EdgeInsets.only(right: 3),
                      child: Image.asset(
                        imageFile,
                        fit: BoxFit.fitWidth,
                        color: textColor,
                        width: 30,
                      ),
                    )
                  : SizedBox(),
              Flexible(
                child: Txt(
                  maxLines: 1,
                  txt: txt,
                  txtColor: textColor,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isOverflow: true,
                  isBold: false,
                ),
              ),
              SizedBox(width: (imageFile != null) ? 43 : 0),
            ],
          ),
        ),
      ),
    );
  }
}
