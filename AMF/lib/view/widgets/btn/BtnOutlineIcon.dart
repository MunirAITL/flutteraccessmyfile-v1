import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BtnOutlineIcon extends StatelessWidget with Mixin {
  final String txt;
  final Color txtColor;
  final Color borderColor;
  final Color bgColor;
  final String leftIcon;
  final String rightIcon;
  final Color leftIconColor;
  final Color rightIconColor;
  final double radius;
  final Function callback;

  BtnOutlineIcon({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.borderColor,
    @required this.callback,
    this.leftIcon,
    this.rightIcon,
    this.leftIconColor = Colors.grey,
    this.rightIconColor = Colors.grey,
    this.bgColor = Colors.transparent,
    this.radius = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialButton(
        color: bgColor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            (leftIcon != null)
                ? Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: SvgPicture.asset(
                      leftIcon,
                      color: leftIconColor,
                    ),
                  )
                : SizedBox(),
            Expanded(
              child: Txt(
                  txt: txt,
                  txtColor: txtColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            (rightIcon != null)
                ? SvgPicture.asset(
                    rightIcon,
                    color: rightIconColor,
                  )
                : SizedBox(),
          ],
        ),

        onPressed: () {
          callback();
        },
        //borderSide: BorderSide(color: borderColor),
        shape: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(radius),
          borderSide: BorderSide(
              style: BorderStyle.solid, width: 1.0, color: borderColor),
        ),

        //color: Colors.black,
      ),
    );
  }
}
