import 'package:aitl/config/theme/MyTheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget drawMatBtn(text, callback) {
  return Padding(
    padding: const EdgeInsets.only(left: 50, right: 50),
    child: MaterialButton(
      color: MyTheme.gray3Color,
      child: new Text(text,
          style: new TextStyle(fontSize: 20, color: Colors.black)),
      onPressed: () {
        callback();
      },
    ),
  );
}
