import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';

class BtnOutline extends StatelessWidget with Mixin {
  final String txt;
  final Color txtColor;
  final Color borderColor;
  final Color bgColor;
  final double width;
  final double height;
  double radius;
  final Function callback;

  BtnOutline({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.borderColor,
    @required this.width,
    @required this.height,
    this.bgColor = Colors.transparent,
    this.radius = 0,
    @required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: (height != null) ? height : getHP(context, 6),
      //color: bgColor,
      child: MaterialButton(
        color: bgColor,
        child: new Txt(
            txt: txt,
            txtColor: txtColor,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.start,
            isBold: false),

        onPressed: () {
          callback();
        },
        //borderSide: BorderSide(color: borderColor),
        shape: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(this.radius),
          borderSide: BorderSide(style: BorderStyle.solid, color: borderColor),
        ),

        //color: Colors.black,
      ),
    );
  }
}
