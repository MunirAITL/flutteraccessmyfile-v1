import 'package:flutter/material.dart';

class IconBtnQ extends IconButton {
  final int tag;
  final icon;
  final onPressed;
  IconBtnQ({this.icon, this.onPressed, @required this.tag})
      : super(icon: icon, onPressed: onPressed);
}
