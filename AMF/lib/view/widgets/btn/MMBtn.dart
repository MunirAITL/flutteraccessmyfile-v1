import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';

class MMBtn extends StatelessWidget with Mixin {
  final String txt;
  Color txtColor;
  Color bgColor;
  final double width;
  final double height;
  final double radius;
  final Function callback;

  MMBtn({
    Key key,
    @required this.txt,
    @required this.width,
    @required this.height,
    this.radius = 10,
    @required this.callback,
    this.txtColor = Colors.white,
    this.bgColor,
  }) {
    if (bgColor == null) bgColor = MyTheme.btnColor;
    if (txtColor == null) txtColor = Colors.white;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Card(
        elevation: 2,
        color: bgColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius),
        ),
        child: Container(
          width: width,
          height: (height != null) ? height : getHP(context, 6),
          alignment: Alignment.center,
          //color: MyTheme.brownColor,

          child: Txt(
            txt: txt,
            txtColor: txtColor,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: false,
          ),
        ),
      ),
    );
  }
}
