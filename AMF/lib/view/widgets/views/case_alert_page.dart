import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/helper/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';

class CaseAlertPage extends StatelessWidget with Mixin {
  final Map<String, dynamic> message;

  const CaseAlertPage({Key key, @required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: 'case_alert'.tr),
          centerTitle: false,
        ),
        body: drawLayout(context),
      ),
    );
  }

  drawLayout(context) {
    return Center(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: getHP(context, 3)),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                  txt: message['data']['Message'].toString() ?? '',
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + .8,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Txt(
                  txt: message['data']['Description'].toString() ?? '',
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: MMBtn(
                txt: "close".tr,
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                radius: 10,
                callback: () {
                  Get.back();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
