import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/Mixin.dart';

class TCView extends StatelessWidget with Mixin {
  final screenName;

  TCView({@required this.screenName});

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(
                  text: "By clicking you confirm that you accept the ",
                  style: TextStyle(
                    height: MyTheme.txtLineSpace,
                    color: Colors.grey,
                    fontSize: getTxtSize(
                        context: context, txtSize: MyTheme.txtSize - .4),
                  ),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'AMF Terms and Conditions',
                        style: TextStyle(
                            height: MyTheme.txtLineSpace,
                            decoration: TextDecoration.underline,
                            decorationThickness: 2,
                            color: MyTheme.bgColor3,
                            fontSize: getTxtSize(
                                context: context,
                                txtSize: MyTheme.txtSize - .4),
                            fontWeight: FontWeight.w500),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                              () => PDFDocumentPage(
                                title: "Privacy",
                                url:
                                    "https://mortgage-magic.co.uk/assets/img/privacy_policy.pdf",
                              ),
                            ).then((value) {
                              //callback(route);
                            });
                          })
                  ]),
            ),
          ),
        ]);
  }
}
