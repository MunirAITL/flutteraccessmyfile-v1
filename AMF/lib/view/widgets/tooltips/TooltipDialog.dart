import 'package:aitl/config/theme/MyTheme.dart';
import 'package:flutter/material.dart';

/*
//  Usage
Future.delayed(const Duration(milliseconds: 500), () {
      Get.dialog(ToolTipDialog(
          //title: "MM3",
          text:
              "testing.....sadf asfasdf asdf asf asdf asdf asdf asdf asdf asdfsadf asdf asdfsdfasd f asdf sdf asdf asdf asdf asf asdf asdf sad fsda fasdf asdfasdfasdf as testing.....sadf asfasdf asdf asf asdf asdf asdf asdf asdf asdfsadf asdf asdfsdfasd f asdf sdf asdf asdf asdf asf asdf asdf sad fsda fasdf asdfasdfasdf astesting.....sadf asfasdf asdf asf asdf asdf asdf asdf asdf asdfsadf asdf asdfsdfasd f asdf sdf asdf asdf asdf asf asdf asdf sad fsda fasdf asdfasdfasdf astesting.....sadf asfasdf asdf asf asdf asdf asdf asdf asdf asdfsadf asdf asdfsdfasd f asdf sdf asdf asdf asdf asf asdf asdf sad fsda fasdf asdfasdfasdf astesting.....sadf asfasdf asdf asf asdf asdf asdf asdf asdf asdfsadf asdf asdfsdfasd f asdf sdf asdf asdf asdf asf asdf asdf sad fsda fasdf asdfasdfasdf astesting.....sadf asfasdf asdf asf asdf asdf asdf asdf asdf asdfsadf asdf asdfsdfasd f asdf sdf asdf asdf asdf asf asdf asdf sad fsda fasdf asdfasdfasdf astesting.....sadf asfasdf asdf asf asdf asdf asdf asdf asdf asdfsadf asdf asdfsdfasd f asdf sdf asdf asdf asdf asf asdf asdf sad fsda fasdf asdfasdfasdf astesting.....sadf asfasdf asdf asf asdf asdf asdf asdf asdf asdfsadf asdf asdfsdfasd f asdf sdf asdf asdf asdf asf asdf asdf sad fsda fasdf asdfasdfasdf astesting.....sadf asfasdf asdf asf asdf asdf asdf asdf asdf asdfsadf asdf asdfsdfasd f asdf sdf asdf asdf asdf asf asdf asdf sad fsda fasdf asdfasdfasdf astesting.....sadf asfasdf asdf asf asdf asdf asdf asdf asdf asdfsadf asdf asdfsdfasd f asdf sdf asdf asdf asdf asf asdf asdf sad fsda fasdf asdfasdfasdf as"));
    });
*/

class ToolTipDialog extends StatelessWidget {
  final String title;
  final String text;
  final Widget wid;
  const ToolTipDialog({
    Key key,
    this.title,
    this.text = "",
    this.wid,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          const EdgeInsets.only(left: 50, right: 50, top: 100, bottom: 100),
      child: Center(
        child: Container(
          decoration: ShapeDecoration(
            color: MyTheme.bgColor2,
            shape: _TooltipShapeBorder(arrowArc: 0.5),
            shadows: [
              BoxShadow(
                  color: Colors.black26, blurRadius: 4.0, offset: Offset(2, 2))
            ],
          ),
          child: Padding(
            padding: EdgeInsets.all(20),
            child: (wid == null)
                ? Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        child: (title != null)
                            ? Column(
                                children: [
                                  Text(title,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold)),
                                  Divider(
                                    color: Colors.white,
                                    height: 1,
                                  ),
                                  SizedBox(height: 10),
                                ],
                              )
                            : SizedBox(),
                      ),
                      Flexible(
                          child: SingleChildScrollView(
                        child:
                            Text(text, style: TextStyle(color: Colors.black)),
                      )),
                    ],
                  )
                : SingleChildScrollView(child: wid),
          ),
        ),
      ),
    );
  }
}

class _TooltipShapeBorder extends ShapeBorder {
  final double arrowWidth;
  final double arrowHeight;
  final double arrowArc;
  final double radius;

  _TooltipShapeBorder({
    this.radius = 16.0,
    this.arrowWidth = 20.0,
    this.arrowHeight = 10.0,
    this.arrowArc = 0.0,
  }) : assert(arrowArc <= 1.0 && arrowArc >= 0.0);

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.only(bottom: arrowHeight);

  @override
  Path getInnerPath(Rect rect, {TextDirection textDirection}) => null;

  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    rect = Rect.fromPoints(
        rect.topLeft, rect.bottomRight - Offset(0, arrowHeight));
    double x = arrowWidth, y = arrowHeight, r = 1 - arrowArc;
    return Path()
      ..addRRect(RRect.fromRectAndRadius(rect, Radius.circular(radius)))
      ..moveTo(rect.bottomCenter.dx + x / 2, rect.bottomCenter.dy)
      ..relativeLineTo(-x / 2 * r, y * r)
      ..relativeQuadraticBezierTo(
          -x / 2 * (1 - r), y * (1 - r), -x * (1 - r), 0)
      ..relativeLineTo(-x / 2 * r, -y * r);
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection textDirection}) {}

  @override
  ShapeBorder scale(double t) => this;
}
