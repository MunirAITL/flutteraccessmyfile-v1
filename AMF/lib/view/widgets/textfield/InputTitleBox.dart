import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/theme/MyTheme.dart';

import 'InputBox.dart';

drawInputBox({
  @required BuildContext context,
  @required String title,
  @required TextEditingController input,
  @required TextInputType kbType,
  @required TextInputAction inputAction,
  @required FocusNode focusNode,
  FocusNode focusNodeNext,
  @required int len,
  String ph,
  Color bgColor,
  bool isPwd = false,
  bool autofocus = false,
}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Txt(
          txt: title,
          txtColor: MyTheme.lblackColor,
          txtSize: MyTheme.txtSize,
          txtAlign: TextAlign.start,
          isBold: false),
      SizedBox(height: 10),
      InputBox(
        context: context,
        ctrl: input,
        lableTxt: title,
        ph: ph,
        kbType: kbType,
        inputAction: inputAction,
        focusNode: focusNode,
        focusNodeNext: focusNodeNext,
        len: len,
        isPwd: isPwd,
        bgColor: bgColor,
        autofocus: autofocus,
      ),
    ],
  );
}
