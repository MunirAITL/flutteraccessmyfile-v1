import 'package:aitl/view/widgets/picker/country_code_picker/CountryCodePicker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/Mixin.dart';

import '../../../config/theme/MyTheme.dart';

InputBoxWithCountryCode({
  context,
  ctrl,
  lableTxt,
  ph,
  kbType,
  inputAction,
  FocusNode focusNode,
  focusNodeNext,
  len,
  isPwd,
  countryCode,
  countryName,
  getCountryCode,
  autofocus = false,
  isWhiteBG = false,
  radius = 5,
  fontSize = 14,
}) {
  double width = MediaQuery.of(context).size.width;
  double height = MediaQuery.of(context).size.height;
  return Container(
    decoration: BoxDecoration(
      color: (isWhiteBG) ? Colors.white : null,
      border: Border.all(
          color: focusNode.hasFocus ? MyTheme.bgColor3 : Colors.grey, width: 1),
      borderRadius: BorderRadius.all(Radius.circular(radius)),
    ),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(radius),
              bottomLeft: Radius.circular(radius),
            ),
          ),
          child: CountryCodePicker(
            dialogSize: Size(width * 70 / 100, height * 70 / 100),
            padding: EdgeInsets.all(0),
            showFlagDialog: true,
            showDropDownButton: true,
            textStyle: TextStyle(color: Colors.black),
            backgroundColor: Colors.transparent,
            dialogBackgroundColor: MyTheme.bgColor2,
            dialogTextStyle: TextStyle(color: Colors.black),
            searchDecoration: const InputDecoration(
              isDense: true,
              contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
              hintText: 'Search',
              hintStyle: TextStyle(
                color: Colors.grey,
                fontSize: 15,
              ),
              labelStyle: TextStyle(
                color: Colors.black,
                fontSize: 15,
              ),
              prefixIcon: Icon(
                Icons.search,
                color: Colors.grey,
                size: 20,
              ),
              //contentPadding: EdgeInsets.only(left: 20, right: 20),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(0),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black54),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(0),
                ),
              ),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(0),
                ),
              ),
            ),
            searchStyle: TextStyle(color: Colors.black),
            initialSelection: countryName,
            showOnlyCountryWhenClosed: false,
            hideMainText: false,
            showFlagMain: true,
            showFlag: true,
            hideSearch: false,
            showCountryOnly: false,
            alignLeft: false,
            closeIcon: Icon(
              Icons.close,
              color: Colors.grey,
            ),
            onChanged: getCountryCode,
            favorite: [countryCode, countryName],
            //flagWidth: 20,
          ),
        ),
        //SizedBox(width: 10),
        Expanded(
          flex: 5,
          child: TextField(
            textAlignVertical: TextAlignVertical.center,
            controller: ctrl,
            focusNode: focusNode,
            style: TextStyle(color: Colors.black, fontSize: fontSize),
            maxLength: len,
            obscureText: false,
            keyboardType: TextInputType.phone,
            textInputAction: inputAction,
            onEditingComplete: () {
              // Move the focus to the next node explicitly.
              if (focusNode != null) {
                focusNode.unfocus();
              } else {
                FocusScope.of(context).requestFocus(new FocusNode());
              }
              if (focusNodeNext != null) {
                FocusScope.of(context).requestFocus(focusNodeNext);
              } else {
                FocusScope.of(context).requestFocus(new FocusNode());
              }
            },
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            decoration: InputDecoration(
              isDense: true,
              filled: true,
              isCollapsed: true,
              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
              counterText: "",
              fillColor: Colors.transparent,
              focusColor: Colors.black,
              hoverColor: Colors.black,
              hintStyle: new TextStyle(
                color: Colors.grey[400],
                fontSize: 17,
                height: MyTheme.txtLineSpace,
              ),
              hintText: ph ?? lableTxt,
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
            ),
          ),
        ),
      ],
    ),
  );
}
