import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/view/widgets/picker/country_code_picker/CountryCodePicker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/Mixin.dart';

// ignore: must_be_immutable
class InputMobFlagBox extends StatefulWidget {
  final ctrl, lableTxt, len;
  Function getCountryCode;
  bool autofocus;
  final inputAction, focusNode, focusNodeNext;

  InputMobFlagBox({
    Key key,
    @required this.ctrl,
    @required this.lableTxt,
    @required this.len,
    @required this.getCountryCode,
    this.autofocus = false,
    this.inputAction,
    this.focusNode,
    this.focusNodeNext,
  }) : super(key: key);

  @override
  State<InputMobFlagBox> createState() => _InputMobFlagBoxState();
}

class _InputMobFlagBoxState extends State<InputMobFlagBox> with Mixin {
  String countryCode = AppDefine.COUNTRY_DIALCODE;
  String countryName = AppDefine.COUNTRY_FLAG.toUpperCase();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;
          countryCode = cd;
        });
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        border: Border.all(
          color: Colors.grey,
          width: 1,
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            decoration: BoxDecoration(
              color: MyTheme.greyColor,
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            child: CountryCodePicker(
              dialogSize: Size(getWP(context, 70), getHP(context, 70)),
              padding: EdgeInsets.all(0),
              showFlagDialog: true,
              showDropDownButton: true,
              textStyle: TextStyle(color: Colors.black, height: 1.0),
              backgroundColor: Colors.transparent,
              dialogBackgroundColor: Colors.white,
              dialogTextStyle: TextStyle(color: Colors.black),
              searchDecoration: InputDecoration(
                isDense: true,
                hintText: "Search",
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontSize: 15,
                ),
                labelStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                ),
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.grey,
                  size: 20,
                ),
                contentPadding: EdgeInsets.only(left: 20, right: 20),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(0),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black54),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(0),
                  ),
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(0),
                  ),
                ),
              ),
              searchStyle: TextStyle(color: Colors.black, height: 1.0),
              initialSelection: countryName,
              showOnlyCountryWhenClosed: false,
              hideMainText: false,
              showFlagMain: true,
              showFlag: true,
              hideSearch: false,
              showCountryOnly: false,
              alignLeft: false,
              closeIcon: Icon(
                Icons.close,
                color: Colors.grey,
              ),
              onChanged: widget.getCountryCode,
              favorite: [countryCode, countryName],
              //flagWidth: 20,
            ),
          ),
          Expanded(
            flex: 5,
            child: TextField(
              controller: widget.ctrl,
              focusNode: widget.focusNode,
              style: TextStyle(color: Colors.black, height: 1.0),
              maxLength: widget.len,
              keyboardType: TextInputType.phone,
              textInputAction: widget.inputAction,
              onEditingComplete: () {
                // Move the focus to the next node explicitly.
                if (widget.focusNode != null) {
                  widget.focusNode.unfocus();
                } else {
                  FocusScope.of(context).requestFocus(new FocusNode());
                }
                if (widget.focusNodeNext != null) {
                  FocusScope.of(context).requestFocus(widget.focusNodeNext);
                } else {
                  FocusScope.of(context).requestFocus(new FocusNode());
                }
              },
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ],
              decoration: InputDecoration(
                isDense: true,
                counterText: "",
                fillColor: Colors.grey,
                focusColor: Colors.black,
                hoverColor: Colors.black,
                labelStyle: TextStyle(color: Colors.grey),
                hintText: "${widget.lableTxt}",
                hintStyle: TextStyle(color: Colors.grey),
                contentPadding: EdgeInsets.all(5),
                focusedBorder: InputBorder.none,
                border: InputBorder.none,
              ),
              obscureText: false,
            ),
          ),
        ],
      ),
    );
  }
}
