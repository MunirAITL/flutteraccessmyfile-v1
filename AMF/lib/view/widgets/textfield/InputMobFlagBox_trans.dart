import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/view/widgets/picker/country_code_picker/CountryCodePicker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/Mixin.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

// ignore: must_be_immutable
class InputMobFlagBoxTrans2 extends StatefulWidget {
  final TextEditingController ctrl;
  final lableTxt, len;
  Function getCountryCode;
  bool autofocus;
  final inputAction, focusNode, focusNodeNext;

  InputMobFlagBoxTrans2({
    Key key,
    @required this.ctrl,
    @required this.lableTxt,
    @required this.len,
    @required this.getCountryCode,
    this.autofocus = false,
    this.inputAction,
    this.focusNode,
    this.focusNodeNext,
  }) : super(key: key);

  @override
  State<InputMobFlagBoxTrans2> createState() => _InputMobFlagBoxTransState();
}

class _InputMobFlagBoxTransState extends State<InputMobFlagBoxTrans2>
    with Mixin {
  String countryCode = AppDefine.COUNTRY_DIALCODE;
  String countryName = AppDefine.COUNTRY_FLAG.toUpperCase();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    widget.focusNode.removeListener(_onFocusChange);
    widget.focusNode.dispose();
    super.dispose();
  }

  appInit() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;
          countryCode = cd;
        });
      }
    } catch (e) {}

    widget.focusNode.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    debugPrint("Focus: ${widget.focusNode.hasFocus.toString()}");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        border: Border.all(
          color: Colors.grey,
          width: 1,
        ),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CountryCodePicker(
            //isSelected: widget.focusNode.hasFocus ? true : false,
            dialogSize: Size(getWP(context, 70), getHP(context, 70)),
            padding: EdgeInsets.all(0),
            showFlagDialog: true,
            showDropDownButton: true,
            textStyle: TextStyle(color: Colors.black, height: 1.0),
            backgroundColor: Colors.transparent,
            dialogBackgroundColor: MyTheme.bgColor2,
            dialogTextStyle: TextStyle(color: Colors.black),
            //boxDecoration: BoxDecoration(color: Colors.black, ba),
            //flagDecoration: BoxDecoration(color: Colors.black),
            searchDecoration: InputDecoration(
              isDense: true,
              hintText: 'Search',
              hintStyle: TextStyle(
                color: Colors.grey,
                fontSize: 15,
              ),
              labelStyle: TextStyle(
                color: Colors.black,
                fontSize: 15,
              ),
              prefixIcon: Icon(
                Icons.search,
                color: Colors.grey,
                size: 20,
              ),
              contentPadding: EdgeInsets.only(left: 20, right: 20),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(0),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(0),
                ),
              ),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey),
                borderRadius: const BorderRadius.all(
                  const Radius.circular(0),
                ),
              ),
            ),
            searchStyle: TextStyle(color: Colors.black, height: 1.0),
            initialSelection: countryName,
            showOnlyCountryWhenClosed: false,
            hideMainText: false,
            showFlagMain: true,
            showFlag: true,
            hideSearch: false,
            showCountryOnly: false,
            alignLeft: false,
            closeIcon: Icon(
              Icons.close,
              color: Colors.black,
            ),
            onChanged: widget.getCountryCode,
            favorite: [countryCode, countryName],
            flagWidth: 20,
          ),
          Expanded(
            flex: 5,
            child: TextFormField(
              controller: widget.ctrl,
              style: TextStyle(color: Colors.black, height: 1.0),
              maxLength: widget.len,
              keyboardType: TextInputType.phone,
              focusNode: widget.focusNode,
              textInputAction: widget.inputAction,
              onEditingComplete: () {
                // Move the focus to the next node explicitly.
                if (widget.focusNode != null) {
                  widget.focusNode.unfocus();
                } else {
                  FocusScope.of(context).requestFocus(new FocusNode());
                }
                if (widget.focusNodeNext != null) {
                  FocusScope.of(context).requestFocus(widget.focusNodeNext);
                } else {
                  FocusScope.of(context).requestFocus(new FocusNode());
                }
              },
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ],
              decoration: InputDecoration(
                //isDense: true,
                counterText: "",
                fillColor: Colors.grey,
                focusColor: Colors.white,
                hoverColor: Colors.white,
                labelStyle: TextStyle(color: Colors.grey),
                hintText: "${widget.lableTxt}",
                hintStyle: TextStyle(color: Colors.grey),
                // contentPadding: EdgeInsets.all(15.0),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: MyTheme.lgreyColor),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent, width: 1),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
              ),
              obscureText: false,
            ),
          ),
        ],
      ),
    );
  }
}
