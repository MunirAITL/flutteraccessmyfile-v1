import 'package:flutter/material.dart';
import '../../../config/theme/MyTheme.dart';
import '../txt/Txt.dart';
import 'InputBoxWithCountryCode.dart';
import 'MobileInputField.dart';

drawMobileTitleLineCountryCode({
  @required BuildContext context,
  @required String title,
  @required TextEditingController input,
  @required TextInputType kbType,
  @required TextInputAction inputAction,
  @required FocusNode focusNode,
  FocusNode focusNodeNext,
  @required int len,
  @required String countryCode,
  @required String countryName,
  @required Function getCountryCode,
  String ph,
  bool isWhiteBG = false,
  bool isPwd = false,
  bool autofocus = false,
  double radius = 8,
  double fontSize = 17,
}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      (title != null)
          ? Txt(
              txt: title,
              txtColor: MyTheme.lblackColor,
              txtSize: MyTheme.txtSize - .3,
              txtAlign: TextAlign.start,
              fontWeight: FontWeight.w500,
              isBold: false)
          : SizedBox(),
      MobileInputField(
        context: context,
        countryCode: countryCode,
        countryName: countryName,
        getCountryCode: getCountryCode,
        ctrl: input,
        lableTxt: title,
        ph: ph,
        kbType: kbType,
        inputAction: inputAction,
        focusNode: focusNode,
        focusNodeNext: focusNodeNext,
        len: len,
        isPwd: isPwd,
        autofocus: autofocus,
        isWhiteBG: isWhiteBG,
        radius: radius,
        fontSize: fontSize,
      ),
    ],
  );
}
