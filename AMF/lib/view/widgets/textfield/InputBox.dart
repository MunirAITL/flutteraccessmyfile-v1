import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'InputBoxAMF.dart';

InputBox({
  ctrl,
  context,
  lableTxt,
  ph,
  kbType,
  inputAction,
  focusNode,
  focusNodeNext,
  bgColor,
  len,
  isPwd = false,
  autofocus = false,
  labelColor = Colors.black,
  align = TextAlign.left,
  isShowHint = false,
  isFirstLetterCap = false,
  prefixIcon,
  suffixIcon,
  ecap = eCap.None,
}) =>
    Theme(
      data: Theme.of(context).copyWith(splashColor: Colors.transparent),
      child: TextField(
        controller: ctrl,
        focusNode: focusNode,
        autofocus: autofocus,
        keyboardType: kbType,
        textInputAction: inputAction,
        onEditingComplete: () {
          // Move the focus to the next node explicitly.
          if (focusNode != null) {
            focusNode.unfocus();
          } else {
            FocusScope.of(context).requestFocus(new FocusNode());
          }
          if (focusNodeNext != null) {
            FocusScope.of(context).requestFocus(focusNodeNext);
          } else {
            FocusScope.of(context).requestFocus(new FocusNode());
          }
        },
        inputFormatters: (kbType == TextInputType.phone)
            ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
            : (kbType == TextInputType.emailAddress)
                ? [
                    FilteringTextInputFormatter.allow(RegExp(
                        "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                  ]
                : (ecap != eCap.None)
                    ? [
                        (ecap == eCap.All)
                            ? AllUpperCaseTextFormatter()
                            : (ecap == eCap.Sentence)
                                ? FirstUpperCaseTextFormatter()
                                : WordsUpperCaseTextFormatter()
                      ]
                    : null,
        obscureText: isPwd,
        maxLength: len,
        autocorrect: false,
        enableSuggestions: false,
        textAlign: align,
        style: TextStyle(
          color: Colors.black,
          fontSize: 17,
          height: MyTheme.txtLineSpace,
        ),
        decoration: new InputDecoration(
          filled: true,
          fillColor: bgColor ?? Colors.white,
          isDense: true,
          counterText: "",
          counter: Offstage(),
          hintText: ph ?? lableTxt,
          //labelText: (!isShowHint) ? lableTxt : '',
          prefixIcon: (prefixIcon != null) ? prefixIcon : null,
          suffixIcon: (suffixIcon != null) ? suffixIcon : null,
          hintStyle: new TextStyle(
            color: Colors.grey[400],
            fontSize: 17,
            height: MyTheme.txtLineSpace,
          ),
          /*labelStyle: new TextStyle(
            color: Colors.grey,
            fontSize: 17,
            height: MyTheme.txtLineSpace,
          ),*/
          //contentPadding: EdgeInsets.only(left: 20, right: 20),
          contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color: bgColor == null ? Colors.transparent : Colors.grey),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: MyTheme.bgColor3, width: 1),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10),
            ),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(
                color: bgColor == null ? Colors.transparent : Colors.grey,
                width: 1),
            borderRadius: const BorderRadius.all(
              const Radius.circular(10),
            ),
          ),
        ),
      ),
    );

class _UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
