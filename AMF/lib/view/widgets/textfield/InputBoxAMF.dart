import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';

import '../txt/Txt.dart';

enum eCap {
  All,
  Word,
  Sentence,
  None,
}

InputBoxAMF({
  ctrl,
  context,
  lableTxt,
  kbType,
  inputAction,
  focusNode,
  focusNodeNext,
  len,
  minLines,
  maxLines,
  isPwd = false,
  autofocus = false,
  ecap = eCap.None,
  prefixIco,
  txtAlign = TextAlign.start,
  onChange,
}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Transform.translate(
        offset: Offset(0, 8),
        child: Txt(
            txt: lableTxt,
            txtColor: MyTheme.lblackColor,
            txtSize: MyTheme.txtSize - .3,
            txtAlign: TextAlign.start,
            fontWeight: FontWeight.w500,
            isBold: false),
      ),
      TextField(
        controller: ctrl,
        focusNode: focusNode,
        autofocus: autofocus,
        keyboardType: kbType,
        minLines: minLines,
        maxLines: maxLines,
        onChanged: onChange,
        textInputAction: inputAction,
        onEditingComplete: () {
          // Move the focus to the next node explicitly.
          if (focusNode != null) {
            focusNode.unfocus();
          } else {
            FocusScope.of(context).requestFocus(new FocusNode());
          }
          if (focusNodeNext != null) {
            FocusScope.of(context).requestFocus(focusNodeNext);
          } else {
            FocusScope.of(context).requestFocus(new FocusNode());
          }
        },
        inputFormatters: (kbType == TextInputType.phone)
            ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
            : (kbType == TextInputType.emailAddress)
                ? [
                    FilteringTextInputFormatter.allow(RegExp(
                        "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                  ]
                : (ecap != eCap.None)
                    ? [
                        (ecap == eCap.All)
                            ? AllUpperCaseTextFormatter()
                            : (ecap == eCap.Sentence)
                                ? FirstUpperCaseTextFormatter()
                                : WordsUpperCaseTextFormatter()
                      ]
                    : null,
        obscureText: isPwd,
        maxLength: len,
        autocorrect: false,
        enableSuggestions: false,
        textAlign: txtAlign,
        style: TextStyle(
          color: MyTheme.lblackColor,
          fontSize: 14,
          height: MyTheme.txtLineSpace,
        ),
        decoration: new InputDecoration(
          isDense: true,
          counter: Offstage(),
          //contentPadding: const EdgeInsets.symmetric(vertical: 20.0),
          prefixIcon: (prefixIco != null)
              ? Padding(
                  padding: const EdgeInsets.only(left: 10, top: 10, bottom: 10),
                  child: prefixIco,
                )
              : null,
          counterText: "",
          //filled: true,
          //fillColor: Colors.white,
          hintText: lableTxt,
          hintStyle: new TextStyle(
            color: Colors.grey,
            fontSize: 12,
          ),
          contentPadding: EdgeInsets.only(
              left: 0, right: (prefixIco != null) ? 50 : 0, top: 10, bottom: 2),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: .5),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: MyTheme.bgColor3, width: .5),
          ),
        ),
      ),
    ],
  );
}

class AllUpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return newValue.copyWith(text: newValue.text.allInCaps);
  }
}

class FirstUpperCaseTextFormatter extends TextInputFormatter {
  /*@override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var newText = toBeginningOfSentenceCase(newValue.text.inCaps);
    return newValue.copyWith(text: newText);
  }*/
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return newValue.copyWith(text: newValue.text.capitalizeFirstofEachDot);
  }
}

class WordsUpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return newValue.copyWith(text: newValue.text.capitalizeFirstofEach2);
  }
}

extension CapExtension on String {
  String get capitalizeFirstofEach2 =>
      this.split(" ").map((str) => str.inCaps).join(" ");
}

extension SentenceExtension on String {
  String get capitalizeFirstofEachDot =>
      this.split(". ").map((str) => str.inCaps).join(". ");
}
