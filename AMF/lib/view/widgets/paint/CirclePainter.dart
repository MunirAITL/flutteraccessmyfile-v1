import 'dart:ui';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'dart:ui' as ui;

//  https://medium.com/flutter-community/playing-with-paths-in-flutter-97198ba046c8

class CirclePainter extends CustomPainter {
  final BuildContext context;
  final int total;
  final double minPA;
  final double maxPA;
  final double width;
  final double height;
  final double fontSize;

  CirclePainter({
    @required this.context,
    @required this.total,
    @required this.minPA,
    @required this.maxPA,
    @required this.width,
    @required this.height,
    @required this.fontSize,
  });

  final TextPainter textPainter = TextPainter(
    textDirection: TextDirection.ltr,
  );

  @override
  void paint(Canvas canvas, Size size) {
    //double totalLeadPA = 100 - (completePA + fmaPA + offeredPA);

    double borderWidth = 10;

    Paint transPaint = new Paint()
      ..color = Colors.transparent
      ..strokeCap = StrokeCap.square
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    final rect = new Rect.fromLTWH(0.0, 0.0, size.width / 2, size.height / 2);

    final gradient1 = new SweepGradient(
      startAngle: 1 * math.pi / 2,
      endAngle: 5 * math.pi / 2,
      tileMode: TileMode.repeated,
      colors: [
        Color(0xFFFC5050),
        Colors.orange,
        Color(0xFFCEDB38),
        Color(0xFF4AC99B),
        Colors.cyan,
      ],
    );
    Paint shadePaint = new Paint();
    shadePaint.shader = gradient1.createShader(rect);
    shadePaint.strokeCap = StrokeCap.round;
    shadePaint.style = PaintingStyle.stroke;
    shadePaint.strokeWidth = borderWidth;

    Paint whitePaint = new Paint()
      ..color = Colors.white
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    Offset center = new Offset(size.width / 2, size.height / 2);
    double radius = math.min(size.width / 2, size.height / 2);
    canvas.drawCircle(center, radius, transPaint);

    //  red light = fma completed
    double arcAngle = 2 * math.pi * (minPA / 100);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius),
        -(math.pi / 2), arcAngle, false, whitePaint);

    double arcAngle2 = 2 * math.pi * (maxPA / 100);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius),
        -(math.pi / 2) + arcAngle + arcAngle2, arcAngle2, false, shadePaint);

    drawText(arcAngle, canvas, arcAngle2, size, total.toString());

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    double x = (width * .23);
    double y = height * .017;

    //
    Paint brush = new Paint()
      ..color = Color(0xFF25DBA4)
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill
      ..strokeWidth = 0;
    canvas.drawCircle(Offset(x, y), 7, brush);

    Paint brushSmall = new Paint()
      ..color = Colors.white
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = .8;
    canvas.drawCircle(Offset(x, y), 4, brushSmall);

    //_drawDashedLine(canvas, size);
  }

  void drawText(
      double pos, Canvas canvas, double arcAngle, Size size, String text) {
    final style0 = TextStyle(
      color: MyTheme.bgColor3,
      fontSize: 12,
    );

    final style1 = TextStyle(
        color: MyTheme.bgColor3,
        fontSize: 30,
        letterSpacing: 1.2,
        fontWeight: FontWeight.bold);

    final style2 = TextStyle(
        color: Color(0xFF19AC5D), fontSize: 15, fontWeight: FontWeight.w400);

    final style3 = TextStyle(
        color: Colors.black, fontSize: 15, fontWeight: FontWeight.w400);

    textPainter.text = TextSpan(style: style0, text: "Your credit");
    textPainter.layout();
    textPainter.paint(canvas, Offset(arcAngle * 12, size.height / 3.5));

    textPainter.text = TextSpan(style: style0, text: "score is");
    textPainter.layout();
    textPainter.paint(canvas, Offset(arcAngle * 15, size.height / 2.4));

    textPainter.text = TextSpan(style: style1, text: text);
    textPainter.layout();
    textPainter.paint(canvas, Offset(arcAngle * 13, size.height / 1.7));

    textPainter.text = TextSpan(style: style2, text: "Its Good!");
    textPainter.layout();
    textPainter.paint(canvas, Offset(arcAngle * 12, size.height / 1.1));

    textPainter.text = TextSpan(style: style3, text: "000");
    textPainter.layout();
    textPainter.paint(canvas, Offset(arcAngle - 16, size.height / 1.4));

    textPainter.text = TextSpan(style: style3, text: "900");
    textPainter.layout();
    textPainter.paint(canvas, Offset(arcAngle * 42, size.height / 1.4));
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class CircleDotPainter extends CustomPainter {
  final int total;

  CircleDotPainter({@required this.total});

  @override
  void paint(Canvas canvas, Size size) {
    Paint innerPaint1 = new Paint()
      ..color = Colors.purple
      ..strokeCap = StrokeCap.butt
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;
    Paint innerPaint2 = new Paint()
      ..color = Colors.white
      ..strokeCap = StrokeCap.butt
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;

    int i = 0;
    Offset center = new Offset(size.width / 2, size.height / 2);
    double radius = math.min(size.width / 2, size.height / 2);
    while (i < total) {
      if (i % 2 == 0) {
        drawDashLine(canvas, center, radius, innerPaint1, i, i + 1);
      } else {
        drawDashLine(canvas, center, radius, innerPaint2, i, i + 1);
      }
      i++;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

  drawDashLine(Canvas canvas, Offset center, double radius, Paint paint,
      int start, int end) {
    double innerAngle1 = 2 * math.pi * (start / 100);
    double innerAngle2 = 2 * math.pi * ((end - start) / 100);
    double innerAngle = (math.pi / 2) - innerAngle1;
    canvas.drawArc(
      new Rect.fromCircle(center: center, radius: radius),
      -innerAngle,
      innerAngle2,
      false,
      paint,
    );
  }
}

class ColorCfg {
  static final Color sky = MyTheme.skyColor;
  static final Color yellow = HexColor.fromHex("#ffcc00");
  static final Color gray = Colors.grey;
}
