import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';

import '../../../config/theme/MyTheme.dart';

// ignore: non_constant_identifier_names
DropDownListDialog({
  BuildContext context,
  String h1,
  String title,
  String id,
  Color titleColor,
  DropListModel ddTitleList,
  double radius = 5,
  double vPadding = 8,
  double txtSize,
  Function(OptionItem optionItem) callback,
}) {
  Color txtColor = (titleColor != null) ? titleColor : Colors.grey;
  ddTitleList.listOptionItems.forEach((element) {
    if (element.title == title) {
      txtColor = Colors.black;
      return;
    }
  });

  return Container(
    padding: EdgeInsets.symmetric(horizontal: 0, vertical: vPadding),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        border: Border(
            left: BorderSide(color: Colors.grey, width: 1),
            right: BorderSide(color: Colors.grey, width: 1),
            top: BorderSide(color: Colors.grey, width: 1),
            bottom: BorderSide(color: Colors.grey, width: 1)),
        color: Colors.transparent),
    child: GestureDetector(
      onTap: () {
        showGeneralDialog(
            barrierColor: Colors.black.withOpacity(0.2),
            transitionBuilder: (context, a1, a2, widget) {
              return Transform.scale(
                scale: a1.value,
                child: Opacity(
                    opacity: a1.value,
                    child: AlertDialog(
                      title: Column(
                        children: [
                          Text(h1 ?? 'Choose from the list',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500)),
                        ],
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      backgroundColor: MyTheme.bgColor2,
                      content: SingleChildScrollView(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ...ddTitleList.listOptionItems.map(
                                (e) => GestureDetector(
                                    onTap: () {
                                      callback(e);
                                      Navigator.of(context, rootNavigator: true)
                                          .pop();
                                    },
                                    child: SizedBox(
                                      width: double.infinity,
                                      child: Card(
                                          elevation: .5,
                                          color: MyTheme.bgColor2,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 5, bottom: 5),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Icon(
                                                  e.title == title
                                                      ? Icons
                                                          .radio_button_checked
                                                      : Icons
                                                          .radio_button_unchecked,
                                                  color: Colors.black,
                                                  size: 15,
                                                ),
                                                SizedBox(width: 5),
                                                Flexible(
                                                  child: Txt(
                                                      txt: "${e.title}",
                                                      txtColor: Colors.black,
                                                      txtSize: txtSize == null
                                                          ? (MyTheme.txtSize -
                                                              .2)
                                                          : txtSize,
                                                      txtAlign: TextAlign.start,
                                                      isBold: false),
                                                ),
                                              ],
                                            ),
                                          )),
                                    )),
                              ),
                            ]),
                      ),
                    )),
              );
            },
            transitionDuration: Duration(milliseconds: 300),
            barrierDismissible: true,
            barrierLabel: '',
            context: context,
            pageBuilder: (context, animation1, animation2) {});
      },
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Txt(
                  txt: title,
                  txtColor: txtColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ),
          Align(
            alignment: Alignment(1, 0),
            child: Icon(
              Icons.keyboard_arrow_down,
              color: Colors.grey,
              size: 30,
            ),
          ),
        ],
      ),
    ),
  );
}
