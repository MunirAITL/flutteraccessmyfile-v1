import 'package:flutter/material.dart';

class DropListModel {
  DropListModel(this.listOptionItems);
  final List<OptionItem> listOptionItems;
}

class OptionItem {
  int id;
  String title;
  OptionItem({@required this.id, @required this.title});
}
