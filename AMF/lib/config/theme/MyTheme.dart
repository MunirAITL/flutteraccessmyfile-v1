import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';

class MyTheme {
  static final bool isBoxDeco = false;
  static final double txtSize = 2;
  static final double btnHpa = 6.5;
  static final double switchBtnHpa = 6;
  static final String fontFamily = "sans";
  static final double appbarElevation = .5;
  static final double botbarElevation = 0;
  static final double txtLineSpace = 1.3;

  static final bgBlueColor = BoxDecoration(
      gradient: LinearGradient(
          colors: [
            HexColor.fromHex("#102948"),
            HexColor.fromHex("#3C5573"),
          ],
          begin: const FractionalOffset(0.0, 0.0),
          end: const FractionalOffset(0.0, 0.0),
          stops: [0.0, 1.0],
          tileMode: TileMode.clamp));

  static final Color bgColor = HexColor.fromHex("#2D4665").withAlpha(190);
  static final Color bgColor2 = HexColor.fromHex("#EEF0F3");
  static final Color bgColor3 = Color(0xFF176D81);
  static final Color bgColor4 = Color(0xFFF7F7FC);
  static final Color bgLColor = HexColor.fromHex("#F5F9FF");
  static final Color bgDark = Color(0xFF2D4665);
  static final Color drawerBGColor = Color(0xFFF7F7FC);
  static final Color txtColor = HexColor.fromHex("#CCCCCC");
  static final Color skyColor = HexColor.fromHex("#DEEAF7");
  static final Color cyanColor = Color(0xFF7EE6F4);
  static final Color lblackColor = Color(0xFF3A3A59);
  static final Color backBtnColor = Color(0xFF71ADB5);
  static final Color btnRedColor = Color(0xFFDB3B61);
  static final Color btnTxtColor = Color(0xFFFFC9D6);

  static final Color blueColor = HexColor.fromHex("#1caade");
  static final Color redColor = HexColor.fromHex("#E73458");
  static final Color airOrange = HexColor.fromHex("#f5ab5d");
  static final Color pinkColor = HexColor.fromHex("#FFA9AC");
  static final Color greyColor = HexColor.fromHex("#F1F1F4");
  static final Color gray1Color = HexColor.fromHex("#f5f9fb");
  static final Color gray2Color = HexColor.fromHex("#e7eef1");
  static final Color gray3Color = HexColor.fromHex("#cad7dc");
  static final Color gray4Color = HexColor.fromHex("#839094");
  static final Color gray5Color = HexColor.fromHex("#3a3d3e");
  static final Color lgreyColor = HexColor.fromHex("#839094");
  static final Color dgreyColor = HexColor.fromHex("#FFA9AC");
  static final Color l2greyColor = HexColor.fromHex("#D3D3D3");

  static final Color appbarColor = redColor;
  static final Color appbarDarkColor = HexColor.fromHex("#65000000");

  static final Color appbarProgColor = Colors.orangeAccent.withOpacity(.5);
  static final Color appbarTxtColor = Colors.white;
  static final Color btnColor = HexColor.fromHex("#2D4665");

  static final Color heroTheamColors = HexColor.fromHex("#E73458");
  static final Color brandColor = HexColor.fromHex("#1caade");
  static final Color airBlueColor = HexColor.fromHex("#008fb4");
  static final Color airBlueMidColor = HexColor.fromHex("#186b8e");
  static final Color airGreenColor = HexColor.fromHex("#9fc104");
  static final Color dGreenColor = HexColor.fromHex("#2da346");
  static final Color onlineColor = HexColor.fromHex("#4CBB17");
  static final Color offlineColor = HexColor.fromHex("#858585");
  static final Color fbColor = HexColor.fromHex("#3B5998");
  static final Color greenAlertWidgetText = HexColor.fromHex("#7a9a1d");
  static final Color greenAlertWidgetBackground = HexColor.fromHex("#daf196");
  static final Color hotdipPink = HexColor.fromHex("#1caade");
  static final Color parallexToolbarColor = HexColor.fromHex("#11000000");
  static final Color grayColor = HexColor.fromHex("#909090");
  static final Color ivory = HexColor.fromHex("#f8efe6");
  static final Color ivory_dark = HexColor.fromHex("#f7e8d9");
  static final Color cornflower_blue_dark = HexColor.fromHex("#303F9F");
  //  pages color theme

  // dashboard->post task theme
  //static final Color titlePostTaskColor = HexColor.fromHex("#61000000");
  static final Color iconTextColor = HexColor.fromHex("#839094");

  // mycases
  static final Color mycasesNFBtnColor = HexColor.fromHex("#B4B4B4");

  //  timeline
  static final Color timelineTitleColor = HexColor.fromHex("#1F3548");
  static final Color timelinePostCallerNameColor = HexColor.fromHex("#2D2D2D");

  //  more
  static final Color moreTxtColor = HexColor.fromHex("#1F3548");

  //  lead
  static final Color leadSubTitle = HexColor.fromHex("#151522");

  static final radioThemeData = ThemeData(
    unselectedWidgetColor: MyTheme.bgColor3,
    disabledColor: MyTheme.bgColor3,
    selectedRowColor: MyTheme.bgColor3,
    indicatorColor: MyTheme.bgColor3,
    toggleableActiveColor: MyTheme.bgColor3,
  );

  static final radioBlackThemeData = ThemeData(
    unselectedWidgetColor: Colors.black,
    disabledColor: Colors.black,
    selectedRowColor: Colors.black,
    indicatorColor: Colors.black,
    toggleableActiveColor: Colors.black,
  );

  static final checkboxThemeData = ThemeData(
    unselectedWidgetColor: Colors.black,
    disabledColor: Colors.black,
    selectedRowColor: Colors.black,
    indicatorColor: Colors.black,
    toggleableActiveColor: Colors.black,
  );

  static final ThemeData themeData = ThemeData(
    // Define the default brightness and colors.
    brightness: Brightness.dark,
    primaryColor: Colors.white,
    accentColor: HexColor.fromHex("#F5F5F7"),

    //iconTheme: IconThemeData(color: Colors.white),
    //primarySwatch: Colors.grey,
    //primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
    //visualDensity: VisualDensity.adaptivePlatformDensity,
    //scaffoldBackgroundColor: Colors.pink,

    cardTheme: CardTheme(
      color: Colors.white,
    ),

    //primarySwatch: Colors.white,
    primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
    iconTheme: IconThemeData(color: Colors.white),

    /*inputDecorationTheme: InputDecorationTheme(
      focusedBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
    ),*/

    // Define the default font family.
    fontFamily: fontFamily,

    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    /*textTheme: TextTheme(
      headline1: TextStyle(
          fontSize: 72.0, fontWeight: FontWeight.bold, color: Colors.black),
      headline6: TextStyle(
          fontSize: 36.0, fontStyle: FontStyle.italic, color: Colors.black),
      bodyText2: TextStyle(
          fontSize: 14.0, fontFamily: fontFamily, color: Colors.black),
    ),

    buttonTheme: ButtonThemeData(
      buttonColor: Colors.blueAccent,
      shape: RoundedRectangleBorder(),
      textTheme: ButtonTextTheme.accent,
      splashColor: Colors.lime,
    ),*/
  );

  //static final BoxDecoration boxDeco = BoxDecoration(
  //color: Colors.white,
  //);

  static final boxDeco = BoxDecoration(
      border: Border(
          left: BorderSide(color: Colors.grey, width: 1),
          right: BorderSide(color: Colors.grey, width: 1),
          top: BorderSide(color: Colors.grey, width: 1),
          bottom: BorderSide(color: Colors.grey, width: 1)),
      color: Colors.transparent);

  static final picEmboseCircleDeco =
      BoxDecoration(color: Colors.grey, shape: BoxShape.circle, boxShadow: [
    BoxShadow(
      color: Colors.grey.shade500,
      blurRadius: 5.0,
      spreadRadius: 2.0,
    ),
  ]);
}
