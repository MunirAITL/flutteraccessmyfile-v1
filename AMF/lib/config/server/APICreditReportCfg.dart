import 'package:aitl/config/server/Server.dart';

class APICreditReportCfg {
  //  credit report
  static const String CREDIT_DATA_DASHBOARD_URL =
      Server.BASE_URL + "/api/crcreditdata/post/getreport";
  static const String CREDIT_USER_INFO_POST_URL =
      Server.BASE_URL + "/api/crusermanage/post/validatenewuser";
  static const String GET_KBA_QUESTIONS_URL =
      Server.BASE_URL + "/api/crusermanage/post/getkbaquestions";
  static const POST_KBA_QUESTIONS_URL =
      Server.BASE_URL + "/api/crusermanage/post/answerkbaquestions";
  static const String GET_SUMMARY_REPORT_URL =
      Server.BASE_URL + "/api/crcreditdata/post/getsummaryreport";
  static const GET_SIMSCOREWITHUSERID =
      Server.BASE_URL + "/api/crcreditdata/post/getsimulatedscorewithuserid";
  static const String GET_USER_VALIDATION_FOR_OLD_USER_URL = Server.BASE_URL +
      "/api/crusermanage/get/getuservalidationforolduser?UserId=#UserId#&UserCompanyId=#UserCompanyId#";
}
