import 'package:aitl/config/server/Server.dart';

class APIHrefLoginCfg {
  //  href login
  static const LOGINACCEXISTSBYEMAIL_POST_URL = Server.BASE_URL +
      "/api/authentication/post/loginaccountexistscheckbyEmail";
  static const POSTEMAILOTP_POST_URL =
      Server.BASE_URL + "/api/userotp/postemailotp";
  static const SENDUSEREMAILOTP_GET_URL =
      Server.BASE_URL + "/api/userotp/senduseremailotp?otpId=#otpId#";
  static const LOGINEMAILOTPBYMOBAPP_POST_URL =
      Server.BASE_URL + "/api/authentication/loginemailotpbymobileapp";
}
