class Server {
  static const bool isTest = true;
  static const bool isOtp = false;

  //  BaseUrl
  static const String BASE_URL = (isTest)
      ? "https://app.mortgage-magic.co.uk"
      : "https://mortgage-magic.co.uk";

  static const String WSAPIKEY = "ABCXYZ123TEST";
}
