import 'Server.dart';

class ServerUrls {
  static const String MISSING_IMG =
      Server.BASE_URL + "/api/content/media/default_avatar.png";

  ///static const String ABOUTUS_URL =
  //"https://app.mortgage-magic.co.uk/apps/about-me/";
  //static const String HELP_INFO_URL =
  //"https://www.mortgagemagic.com/privacy-policy/";
  static const String TC_URL =
      "https://support.shohokari.com/terms-and-conditions/";
  static const String PRIVACY_URL =
      "https://support.shohokari.com/privacy-policy";
  static const HELP_URL = "https://support.shohokari.com/help/";
  static const String BADGE_SUPPORT_URL =
      "https://support.shohokari.com/badges";

  static const String TERMS_POLICY = Server.BASE_URL +
      "/api/termsprivacynoticesetup/getbycompanyid?UserCompanyId=#UserCompanyId#";
}
