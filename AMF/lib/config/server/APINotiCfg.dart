import 'package:aitl/config/server/Server.dart';

class APINotiCfg {
  static const NOTI_GET_URL =
      Server.BASE_URL + "/api/notifications/get/?UserId=#UserId#";
  static const NOTI_PUT_URL = Server.BASE_URL + "/api/notifications/put";
  static const NOTI_SETTINGS_GET_URL =
      Server.BASE_URL + "/api/usernotificationsetting/get";
  static const NOTI_SETTINGS_PUT_URL =
      Server.BASE_URL + "/api/usernotificationsetting/put";
  static const NOTI_SETTINGS_TEST_URL = Server.BASE_URL +
      "/api/notifications/sendtestpushnotificationtouser/#userId#";

  static const String NOTI_SETTINGS_URL = Server.BASE_URL +
      "/api/usernotificationsetting/getnotificationsettingbyusercompanyidanduserid?UserCompanyId=#userCompanyId#&UserId=#userId#";
  static const String NOTI_SETTINGS_POST_URL =
      Server.BASE_URL + "/api/usernotificationsetting/post";
  static const String FCM_TEST_NOTI_URL = Server.BASE_URL +
      "/api/notifications/sendtestpushnotificationtouser/#userId#";
}
