import 'Server.dart';

class APITimelineCfg {
  //  timeline
  static const String GET_TIMELINE_URL =
      Server.BASE_URL + "/api/timeline/gettimelinebyapp";

  static const String TIMELINE_POST_URL =
      Server.BASE_URL + "/api/timeline/post";

  //  comments
  static const String COMMENTS_POST_URL =
      Server.BASE_URL + "/api/comments/post";

  static const String GET_COMMENTS_URL = Server.BASE_URL + "/api/comments/get";
}
