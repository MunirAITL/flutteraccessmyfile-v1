import 'Server.dart';

class APIAuthCfg {
  //  login reg fb mobile
  static const String LIGIN_REG_FB_MOBILE =
      Server.BASE_URL + "/api/authentication/loginregistrationfacebookmobile";

  //  Login
  static const String LOGIN_URL = Server.BASE_URL + "/api/authentication/login";

  //  otp: sms1
  static const String LOGIN_MOBILE_OTP_POST_URL =
      Server.BASE_URL + "/api/userotp/post";
  static const String SEND_OTP_NOTI_URL =
      Server.BASE_URL + "/api/userotp/sendotpnotification?otpId=#otpId#";

  //  otp: sms2
  static const String LOGIN_MOBILE_OTP_PUT_URL =
      Server.BASE_URL + "/api/userotp/put";
  //static const String LOGIN_REG_OTP_FB_URL =
  //Server.BASE_URL + "/api/authentication/loginregistrationfacebookmobile";

  //  Forgot
  static const String FORGOT_URL =
      Server.BASE_URL + "/api/authentication/forgotpassword";

  // Change password
  static const String CHANGE_PWD_URL =
      Server.BASE_URL + "/api/users/put/change-password";

  //  Register
  static const String REG_URL =
      Server.BASE_URL + "/api/authentication/register";

  //  reg
  static const String REG_PROFILE_PUT_URL = Server.BASE_URL + "/api/users/put";
}
