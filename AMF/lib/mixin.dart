import 'dart:async';
import 'dart:developer' as dev;
import 'dart:math';
import 'package:aitl/view/widgets/dialog/AlrtDialog.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:package_info/package_info.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';

import 'config/theme/MyTheme.dart';
import 'view/widgets/tooltips/TooltipDialog.dart';

mixin Mixin {
  myLog(str) {
    if (const String.fromEnvironment('DEBUG') != null) {
      dev.log(str.toString());
      //final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
      //pattern.allMatches(str).forEach((match) => print(match.group(0)));
    }
  }

  getW(context) {
    return ResponsiveFlutter.of(context).wp(100);
  }

  getH(context) {
    return ResponsiveFlutter.of(context).hp(100);
  }

  getWP(context, p) {
    return ResponsiveFlutter.of(context).wp(p);
  }

  getHP(context, p) {
    return ResponsiveFlutter.of(context).hp(p);
  }

  getTxtSize({BuildContext context, double txtSize = 0}) {
    if (txtSize == 0) txtSize = 2;
    return ResponsiveFlutter.of(context).fontSize(txtSize);
  }

  openUrl(context, url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      Alert(context: context, title: "Alert", desc: 'Could not launch $url')
          .show();
    }
  }

  openMap(context, double latitude, double longitude) async {
    openUrl(context,
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude');
  }

  Iterable<E> mapIndexed<E, T>(
      Iterable<T> items, E Function(int index, T item) f) sync* {
    var index = 0;
    for (final item in items) {
      yield f(index, item);
      index = index + 1;
    }
  }

  void startLoading() {
    EasyLoading.show(status: "Loading...");
  }

  void stopLoading() {
    EasyLoading.dismiss();
  }

  void showAlert({BuildContext context, String msg}) {
    showAlrtDialog(
      context: context,
      msg: msg,
    );
  }

  void showSnake(String title, String msg) {
    Get.snackbar(title, msg, colorText: Colors.white);
  }

  void showToast({BuildContext context, String msg, int which = 2}) {
    FToast fToast;
    fToast = FToast();
    fToast.init(context);
    Widget toast = Stack(
      overflow: Overflow.visible,
      alignment: Alignment.center,
      children: <Widget>[
        Card(
          //shape: BoxShape.rectangle,
          shape: BeveledRectangleBorder(
            side: new BorderSide(color: MyTheme.bgColor2, width: .5),
            borderRadius: BorderRadius.circular(5),
          ),
          elevation: 50,
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(30),
            child: Text(msg,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.normal)),
          ),
        ),
        Positioned(
          top: -25,
          child: (which == 1)
              ? Icon(Icons.check, color: Colors.green, size: 50)
              : (which == 0)
                  ? Icon(Icons.error, color: Colors.red, size: 50)
                  : Icon(Icons.info, color: Colors.orange, size: 50),
        )
      ],
    );
    fToast.showToast(
      child: toast,
      gravity: ToastGravity.CENTER,
      toastDuration: Duration(seconds: 3),
    );
  }

  showToolTips({BuildContext context, String txt = "", Widget w}) {
    /*return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: (w == null)
            ? Text(
                txt,
                softWrap: true,
                style: TextStyle(color: Colors.white),
              )
            : w,
      ),
    );*/
    Get.dialog((w == null)
        ? ToolTipDialog(
            //title: "MM3",
            text: txt)
        : ToolTipDialog(
            //title: "MM3",
            wid: w));

    //key.currentState.ensureTooltipVisible();
  }

  drawCircle({BuildContext context, Color color, double size = 2}) {
    return Container(
      width: getWP(context, size),
      height: getWP(context, size),
      decoration: BoxDecoration(shape: BoxShape.circle, color: color),
    );
  }
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

extension CapExtension on String {
  String get inCaps =>
      this.length > 0 ? '${this[0].toUpperCase()}${this.substring(1)}' : '';
  String get allInCaps => this.toUpperCase();
  String get capitalizeFirstofEach => this.toLowerCase().split(' ').map((word) {
        String leftText =
            (word.length > 1) ? word.substring(1, word.length) : '';
        return word[0].toUpperCase() + leftText;
      }).join(' ');
}

extension StringExtension on String {
  String uFirst() {
    var str = "";
    for (var s in this.split(" ")) {
      try {
        str += "${s[0].toUpperCase()}${s.substring(1)}";
        str += " ";
      } catch (e) {}
    }
    return str.trim();
  }
}
