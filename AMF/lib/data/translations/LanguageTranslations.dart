import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'LanguageTransBase.dart';

class LngTRController extends GetxController {
  var txt = "".obs;
  var indexLng = 0.obs;
  var local = Locale(AppDefine.LANGUAGE, AppDefine.GEO_CODE).obs;
}

class LngTR extends BaseLngTR {
  final controller = Get.put(LngTRController());
  static final locales = [
    {
      'index': 0,
      'name': 'বাংলা',
      'abbr': 'বাংলা',
      'locale': Locale(AppDefine.LANGUAGE, AppDefine.GEO_CODE)
    },
    {
      'index': 1,
      'name': 'English',
      'abbr': 'Eng',
      'locale': Locale('en', 'UK')
    },
  ];

  getLngTitle() {
    int i = 0;
    if (controller.indexLng.value == 0)
      i = 1;
    else
      i = 0;
    controller.txt.value = locales[i]['abbr'];
    return controller.txt.value;
  }

  Locale getLocale() {
    return controller.local.value;
  }

  /*getLng() async {
    indexLng = await PrefMgr.shared.getPrefInt("local");
    controller.txt.value = locales[indexLng]['abbr'];
    final Locale locale = locales[indexLng]['locale'];
    controller.local.value = locale;
    //Get.updateLocale(locale);
  }*/

  setLng() async {
    int i = await PrefMgr.shared.getPrefInt("local");
    if (controller.indexLng.value == 0)
      i = 1;
    else
      i = 0;
    final Locale locale = locales[i]['locale'];
    controller.txt.value = locales[i]['abbr'];
    controller.local.value = locale;
    Get.updateLocale(locale);
    await PrefMgr.shared.setPrefInt("local", i);
    controller.indexLng.value = i;
  }
}
