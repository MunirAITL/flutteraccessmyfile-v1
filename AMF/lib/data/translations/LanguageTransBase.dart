import 'package:aitl/data/translations/bd.dart';
import 'package:aitl/data/translations/en.dart';
import 'package:get/get.dart';

class BaseLngTR extends Translations {
  //  Repository -> BD and UK
  @override
  Map<String, Map<String, String>> get keys => {
        'en_UK': EN().get(),
        'bn_BD': BD().get(),
      };
}
