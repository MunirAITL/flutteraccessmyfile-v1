class BD {
  get() {
    return {
      //  Keep these on top for enum popup menu
      'Edit': 'সংশোধন',
      'Share task': 'কাজ শেয়ার করুন',
      'Copy': 'কপি',
      'Report': 'রিপোর্ট',
      'Cancel task': 'কাজ বাতিল',
      //  Tabbar
      'tab_new_task': 'কাজ পোস্ট করুন',
      'tab_mytasks': 'আমার কাজ',
      'tab_findwork': 'কাজ খুঁজুন',
      'tab_msg': 'ইনবক্স',
      'tab_more': 'আরও',
      //  Status code
      'status_receipt': 'প্রাপ্তি',
      'status_payment': 'Payment',
      'status_post': 'Payment',
      'status_purchase_order': 'ক্রয় আদেশ',
      'status_active': 'সক্রিয়',
      'status_assigned': 'নিযুক্ত',
      'status_paid': 'পেমেন্ট',
      'status_cancelled': 'বাতিলকৃত',
      'status_deleted': 'মুছে ফেলা হয়েছে',
      'status_alltask': 'সমস্ত কাজ',
      'status_pendingtask': 'পেন্ডিং টাস্ক',
      'status_draft': 'খসড়া',
      'status_private_msg': 'ব্যক্তিগত সংবাদ',
      'status_req_payment': 'পেমেন্ট অনুরোধ',
      'status_rec_payment': 'রেমিভেড পেমেন্ট',
      'status_funded': 'ফান্ডেড পেমেন্ট',
      'status_cash': 'নগদ',
      'status_funded_req_payment': 'ফান্ডেড রিকুয়েস্ট পেমেন্ট',
      'status_release_payment': 'ফান্ডেড রিলিজ পেমেন্ট',
      'status_verified': 'যাচাই',
      'status_unverfied': 'যাচাই করা হয়নি',
      //  Welcome Page
      'loading': 'লোড হচ্ছে ...',
      'lng': 'বাংলা',
      'introduction_screen_catch_phrase_title':
          'সহকারী অ্যাপ এ আপনাকে স্বাগতম !',
      'introduction_screen_catch_phrase_text':
          'সহকারী অ্যাপ এর মাধ্যমে আপনি বহু ধরনের কাজ করতে ও করিয়ে নিতে পারবেন...',
      'start': 'শুরু করুন',
      //  Login Page
      'join_shohokari_title': 'যোগ দিন Shohokari তে',
      'sign_up_create_account': 'একটি একাউন্ট খুলুন',
      'log_in': 'লগ ইন',
      'login': 'লগ ইন',
      'introduction_screen_login_button': 'লগ ইন',
      'email_or_phone_number': 'ইমেইল বা ফোন নম্বর',
      'password': 'পাসওয়ার্ড',
      'sign_up_login_button_forgot_password': 'পাসওয়ার্ড ভুলে গেছেন?',
      'sign_up_login_separator_or': 'অথবা',
      'or': 'অথবা',
      'sign_up_login_main_button_label_mobile_login': 'মোবাইল দিয়ে লগ ইন',
      'fb_login': 'ফেসবুক দিয়ে লগ ইন করুন',
      'com_facebook_loginview_log_in_button_continue': 'ফেসবুকের মাধ্যমে চালান',
      'com_google_loginview_log_in_button_continue': 'গুগলের সাথে চালিয়ে যান',
      'missing_email': 'ইমেইল অনুপস্থিত',
      'dialog_password_reset_title': 'পাসওয়ার্ডটি পরিবর্তন করুন',
      'forgot_pwd_txt1': 'এ একটি ইমেল পাঠান ',
      'forgot_pwd_txt2': ' আপনার পাসওয়ার্ড পুনরায় সেট করার নির্দেশ দিয়ে?',
      'ok': 'ঠিক আছে',
      'cancel': 'বাতিল করুন',
      'missing_ep': 'ইমেল/ফোন অনুপস্থিত',
      'pwd_wrong': 'পাসওয়ার্ড 4 অক্ষরের দ্বারা বড় হওয়া উচিত',
      'dob_invalid': 'অবৈধ জন্ম তারিখ',
      'mobile_invalid': 'অকার্যকর মোবাইল নম্বর',
      'email_not_exists': 'ইমেল ঠিকানা বিদ্যমান নেই',
      'lname_invalid': 'অবৈধ শেষ নাম',
      'fname_invalid': 'অবৈধ প্রথম নাম',
      'fullname_invalid': 'অবৈধ পুরো নাম',
      'create_profile_first_name_hint': 'নামের প্রথম অংশ',
      'create_profile_last_name_hint': 'নামের শেষ অংশ',
      'email_txt': 'ইমেইল',
      'mobile_number_label': 'মোবাইল নম্বর',
      'email_invalid': 'ইমেইল সঠিক হয়নি',
      'tc_reg1': 'সাইন আপ করে, আমি রাজি ',
      'tc_reg2': ' শর্তাবলী',
      'tc': 'শর্তাবলী',
      'tc_sms1': 'মোবাইল দিয়ে লগ ইন\" বাটনে ক্লিক করে আপনি ',
      'login_page_title': 'অ্যাপটি ব্যবহার শুরু করতে আপনার মোবাইল নম্বর লিখুন',
      'please_give_your_mobile_number':
          'আপনি আপনার মোবাইল নম্বর লিখে লগ ইন করতে পারেন',
      'add_mobile_number_mobile_number_hint':
          'আপনার মোবাইল নম্বরটি প্রবেশ করান',
      'next_btn': 'পরবর্তী',
      'otp_3_sent': 'যে কোডটি পাঠানো হয়েছিল তা লিখুন',
      'sms_verification_resendcode_title': 'আমি কোনও কোড পাইনি',
      'loading': 'লোড হচ্ছে ...',
      'create_profile': 'প্রোফাইল তৈরি করুন',
      'confirm_your_offer_add_date_of_birth': 'আপনার জন্ম তারিখ দিন',
      'create_profile_dob_info':
          'সাইন আপ করতে হলে আপনার বয়স ১৮ বা তার বেশি হতে হবে। অন্য কেউ আপনার জন্মের তারিখ দেখতে পারবে না।.',
      'post_task_schedule_select_due_date': 'তারিখ নির্বাচন করুন',
      'create_profile_your_location_label': 'আপনার এলাকা দিন',
      'pick_addr': 'দয়া করে আপনার ঠিকানাটি বেছে নিন',
      'on_boarding_poster_four_continue_button_label': 'যাত্রা শুরু করুন',
      'search': 'অনুসন্ধান করুন',
      'create_profile_gender': 'িঙ্গ',
      'create_profile_gender_type_male': 'পুরুষ',
      'create_profile_gender_type_female': 'মহিলা',
      //  Post Task
      'tutorial_text_task_post':
          'যে কাজের জন্য সহকারী প্রয়োজন তা নিচ থেকে বেছে নিন।',
      'navigation_bar_post_task_label': 'কাজ পোস্ট করুন',
      'task_post_i_am_worker': 'আমি কাজ করতে চাই',
      'task_post_i_need_employee': 'আমি কাজ করাতে চাই',
      //  Task list...
      'suggestions_cleaning_pest_control_label': 'পরিষ্কার',
      'suggestion_example_cleaning_pest_control':
          'যেমন, আমার দুই বেডরুম এর এপার্টমেন্ট পরিষ্কার করতে হবে',
      'suggestions_anything_else_label': 'অন্যান্য',
      'suggestion_example_anything_else': 'যেমন, ফটোগ্রাফার প্রয়োজন',
      'browse_tasks_chip_categories_all_categories': 'সব ক্যাটাগরি',
      'building_contruction': 'ভবন ও নির্মাণ',
      'building_contruction_txt':
          'যেমন আমার প্যাটিও পাড়ার জন্য বাড়তি হাত দরকার',
      'housemaids': 'গৃহকর্মী',
      'housemaids_txt':
          'যেমন সোমবার-শুক্রবার দিনের বেলায় আমার বাচ্চাদের যত্ন নিন',
      'repairs': 'মেরামত',
      'repairs_txt': 'যেমন কেউ কি আমার ওয়াশিং মেশিন বা টিভি ঠিক করতে পারে!',
      'suggestions_part_time_job_label': 'পার্ট টাইম জব',
      'suggestion_example_part_time_job':
          'যেমন, কয়েকজন সেল্‌স রিপ্রেজেন্টেটিভ প্রয়োজন',
      'delivery_removals': 'ডেলিভারি এবং অপসারণ',
      'delivery_removals_txt':
          'যেমন বাড়ি চলে যাচ্ছি এবং আমার জন্য আমার ভারী আসবাবপত্র নেওয়ার জন্য কাউকে দরকার',
      'suggestions_computer_it_label': 'কম্পিউটার/ আই টি',
      'suggestions_computer_it_label_txt':
          'যেমন, আমার কম্পিউটার মেরামত করতে হবে/ একটি আর্টিকেল লিখে দিতে হবে',
      'suggestions_cleaning_pest_control_label_1': 'পোকা দমন',
      'suggestions_cleaning_pest_control_label_1_txt':
          'যেমন ফ্রুটফ্লাই ইনফেকশন- এসওএস!',
      'suggestions_shifting_moving_label': 'বাসা/অফিস',
      'suggestion_example_shifting_moving':
          'যেমন, মালামাল বহনের জন্য একটি ট্রাক চাই ইত্যাদি',
      'hair_beauty': 'চুল এবং সৌন্দর্য',
      'hair_beauty_txt': 'যেমন যত তাড়াতাড়ি সম্ভব আমার নখ করা দরকার!',
      'tutor_trainer': 'গৃহশিক্ষক ও প্রশিক্ষক',
      'tutor_trainer_txt': 'যেমন ইংরেজি শেখানোর জন্য একজন গৃহশিক্ষকের প্রয়োজন',
      'suggestions_marketing_label': 'মার্কেটিং',
      'suggestions_marketing_label_txt':
          'যেমন বিপণন প্রচারণার জন্য একটি পরিকল্পনা প্রস্তাব করার জন্য কারো প্রয়োজন',
      'suggestions_tailor_label': 'টেইলার',
      'suggestion_example_tailor':
          'যেমন কয়েকটি শার্ট ইত্যাদি তৈরির জন্য দর্জি প্রয়োজন।',
      'suggestions_driver_car_rental_label': 'ড্রাইভার',
      'suggestion_example_car_rental_it':
          'যেমন, এক দিনের জন্য একটি প্রাইভেট কার ভাড়া চাই ইত্যাদি',
      'task_category_car_label': 'গাড়ি ভাড়া',
      'task_category_truck_medium_label': 'ট্রাক ভাড়া',
      'security_gaurd': 'চৌকিদার',
      'suggestion_example_security_gaurd':
          'যেমন, অফিসিয়াল কাজের জন্য সিকিউরিটি গার্ড চাই',
      'suggestions_admin_label': 'অ্যাডমিন',
      'suggestion_example_admin': 'যেমন, একজন পার্সোনাল এসিস্ট্যান্ট প্রয়োজন',
      'suggestions_shopping_control_label': 'শপিং',
      'suggestions_shopping_control_label_txt':
          'যেমন প্রতি সোমবার আমার জন্য কিছু কেনাকাটা করার জন্য কাউকে দরকার',
      'events_entertainment': 'অনুষ্ঠান এবং বিনোদন',
      'events_entertainment_txt':
          'যেমন আমার বিয়ের জন্য কেউ কি ডিজে দিতে পারে?',
      'suggestions_laundry_label': 'লন্ড্রি',
      'suggestion_example_laundry_service': 'যেমন, লন্ড্রি সহায়তা চাই',
      'hire_goods': 'পণ্য ভাড়া',
      'hire_goods_txt':
          'যেমন একটি বাগান পার্টি জন্য একটি মার্কি ভাড়া প্রয়োজন!',
      'other': 'অন্যান্য',
      'other_txt':
          'যেমন আমার নতুন কোম্পানির জন্য একটি নাম প্রস্তাব করুন ইত্যাদি।',
      //  Post Task 1
      'post_task_screen_title_new': 'পোস্ট টাস্ক',
      'delete_task_confirm': 'আপনি কি নিশ্চিত, আপনি এই কাজটি মুছে ফেলতে চান?',
      'post_task_segment_title_details': 'বিস্তারিত',
      'post_task_segment_title_date': 'সময়সীমা',
      'post_task_segment_title_price': 'বাজেট',
      'continue': 'ওকে',
      'task_headline': 'টাস্ক হেডলাইন',
      'contact_fragment_description_hint': 'বর্ণনা',
      'online_job': 'চাকরি কি অনলাইন ভিত্তিক হবে?',
      'map_action_bar_title': 'কাজ এর স্থান',
      'yes': 'হ্যাঁ',
      'no': 'না',
      'post_task_details_error_title':
          'দয়া করে একটি সঠিক শিরোনাম দিন(at least 10 characters)',
      'post_task_details_error_description':
          'দয়া করে একটি সঠিক বর্ণনা দিন (at least 25 characters)',
      //  Post Task 2
      'post_task_schedule_due_date_when_do_you_label':
          'কাজের অফার নেওয়ার শেষ তারিখটি দিন',
      'propose_new_time_due_date_label': 'সময়সীমা',
      //  Post Task 3
      'post_task_price_invalid_price_text':
          'দয়া করে কাজ এর আনুমানিক দর উল্লেখ করুন! দর- ',
      'post_task_price_rate_label': 'ঘন্টাপ্রতি মূল্য',
      'post_task_price_error_hours': 'দয়া করে সঠিক সময় (ঘন্টা) জানান',
      'post_task_price_state_label_total': 'সর্বমোট',
      'post_task_price_state_label_hourly_rate': 'ঘন্টাপ্রতি দর',
      'post_task_price_price_label':
          'আপনার আনুমানিক বাজেট কেমন?\n(আপনার বাজেটের মধ্যে হতে হবে ',
      'post_task_price_hours_label': 'ঘন্টা',
      'taskers_need': 'আপনার কতজন টাস্কার দরকার?',
      'post_task_price_estimated_budget_label': 'আনুমানিক বাজেট:',
      'post_task_price_per_tasker_label': 'সহকারী প্রতি',
      'offer_accepted_confirmation_title': 'অফার গৃহীত',
      'offer_accept': 'অফার গ্রহণ',
      'accept_payment_methods_accept_button': 'নিশ্চিত এবং পেমেন্ট করুন ।',
      'done': 'সমাপ্ত',
      'submit': 'জমা দিন',
      'offer_successful_title': 'অভিনন্দন!',
      //  Post Task 4
      'post_task_finished_title_message': 'অভিনন্দন!',
      'post_task_finished_your_task_was_posted_text':
          'আপনার কাজটি পোস্ট হয়েছে। অফার পাওয়া মাত্রই আপনাকে জানানো হবে।',
      'task_attachments': 'কাজ এর সংযুক্তিসমূহ',
      'attachments': 'সংযুক্তিসমূহ',
      'chooseopt': 'একটি বিকল্প নির্বাচন করুন',
      'view': 'দেখুন',
      'delete': 'মুছে ফেলা',
      'files_added': 'ফাইল যোগ করা হয়েছে',
      'max_file_limit': 'সর্বোচ্চ ফাইল আপলোড সীমা',
      'files': 'নথি পত্র',
      'add_upto5_files': '5 টি ফাইল যোগ করুন',
      'confirmation': 'নিশ্চিতকরণ',
      'confirm_del_task_attachment':
          'আপনি কি নিশ্চিত, আপনি এই সংযুক্তিটি সরাতে চান?',
      'resolution_post_msg':
          'সমর্থন টিকিট জমা দেওয়ার জন্য আপনাকে ধন্যবাদ.\n\nআমাদের সমর্থন সদস্য যত তাড়াতাড়ি সম্ভব আপনার সাথে যোগাযোগ করবে.',
      //  Cam
      'choose_cam': 'ছবির উৎস নির্বাচন করুন',
      'gallery': 'গ্যালারি',
      'camera': 'ক্যামেরা',
      'from_gallery': 'গ্যালারি থেকে',
      'from_cam': 'ক্যামেরা থেকে',
      'skip': 'এড়িয়ে যান',
      //  My Tasks
      'navigation_bar_my_tasks_label': 'আমার কাজ',
      'search_by_title': 'শিরোনাম অনুসারে অনুসন্ধান করুন',
      'all': 'সব',
      'posted': 'পোস্টকৃত',
      'draft': 'খসড়া',
      'assigned': 'িযুক্ত',
      'offered': 'অফারকৃত',
      'completed': 'সম্পন্ন',
      'mytasks_nf':
          'মনে হচ্ছে আপনি এখনও কোনো কাজ পোস্ট করেননি বা কোনো অফার করেননি। এখন কিভাবে যে সম্পর্কে?',
      //  Find works
      'browse_tasks_screen_title': 'কাজ খুঁজুন',
      'tutorial_text_browse_task':
          'টাকা আয় করতে চাইলে নিচ থেকে পছন্দ মত কাজ করুন।',
      'findworks_nf':
          'আমরা এমন কোন কাজ খুঁজে পাইনি যা মেলে - একটি ভিন্ন ফিল্টার বা অনুসন্ধান শব্দ চেষ্টা করতে পারে?',
      'browse_tasks_empty_state_change_filters': 'ফিল্টার পরিবর্তন করুন',
      //  Filters
      'browse_tasks_chip_task_type_all': 'ব্যক্তিগতভাবে এবং দূরবর্তী',
      'browse_tasks_chip_task_location': '৫০০ কি.মি, ঢাকা',
      'browse_tasks_chip_task_price_any': 'যে কোন মূল্য',
      'browse_tasks_chip_task_state_open': 'উন্মুক্ত কাজগুলো',
      'browse_tasks_filter_screen_title': 'ফিল্টার',
      'apply': 'আবেদন',
      'browse_tasks_filter_task_type_task_with_location': 'ব্যক্তিগতভাবে',
      'browse_tasks_filter_task_type_online': 'অনলাইন',
      'browse_tasks_filter_task_type': 'যে কাজগুলো করতে হবে',
      'browse_tasks_map_location_label': 'অবস্থান',
      'distance': 'দুরত্ব',
      'price': 'মূল্য',
      'browse_tasks_filter_task_status_description':
          'শুধুমাত্র নিয়োগ পাওয়ার মত কাজগুলো',
      'browse_tasks_filter_task_status_sub_description':
          'নিয়োগপ্রাপ্ত কাজগুলো আড়াল করুন',
      'on': 'চালু',
      'off': 'বন্ধ',
      //  Private Messages
      'private_conversation_screen_title': 'ব্যক্তিগত ইনবক্স',
      'message_shohokari_support_team_contact':
          'সহকারী সাপোর্ট টিমের সাথে যোগাযোগ',
      'msg_nf':
          'আপনি এখনও কোন বার্তা পাননি - একটি কাজ বরাদ্দ করুন বা ব্যক্তিগতভাবে চ্যাট করার জন্য নিযুক্ত হন!',
      'refresh': 'রিফ্রেশ',
      //  More
      'more': 'আরও',
      'dashboard': 'ড্যাশবোর্ড',
      'profile': 'প্রোফাইল',
      'payment_history': 'পেমেন্ট ইতিহাস',
      'payment_methods': 'পেমেন্ট পদ্ধতি',
      'reviews': 'রিভিউ',
      'notifications': 'নোটিফিকেশন',
      'task_alerts_setting': 'কাজের এলার্ট সেটিংস',
      'settings': 'সেটিংস',
      'help': 'সাহায্য',
      'logout': 'লগ আউট',
      'log_out': 'প্রস্থান',
      'confirm_logout': 'আপনি লগ আউট করতে চান?',
      //  settings
      'edit_profile_screen_title': 'একাউন্ট সংশোধন করুন',
      'notifications_settings_screen_title': 'নোটিফিকেশন সেটিংস',
      //  dashboard
      'as_tasker': 'একজন টাস্কার হিসেবে',
      'as_poster': 'পোস্টার হিসেবে',
      'dashboard_column_bid_on_label': 'দরদাম চলছে',
      'dashboard_column_open_for_offers_label': 'অফারের জন্য চালু আছে',
      'dashboard_tab_overdue': 'বকেয়া',
      'dashboard_tab_awaiting_payments': 'পেমেন্ট এর জন্য অপেক্ষমান',
      'dialog_completion_rate_message_runner':
          'নিষ্পত্তির হার হলো সহকারী হিসেবে নিযুক্ত হওয়া কাজগুলোর মধ্যে সম্পন্ন হওয়া সফল কাজ এর হার',
      'dialog_completion_rate_message_sender':
          'নিষ্পত্তির হার হলো কাজদাতা হিসেবে নিযুক্ত করা কাজগুলোর মধ্যে সম্পন্ন হওয়া সফল কাজ এর হার',
      'noti_nf':
          'এখানে আমরা আপনাকে কাজ, মন্তব্য এবং অন্যান্য জিনিস সম্পর্কে জানাব। কিভাবে একটি টাস্ক পোস্ট করা বা ব্রাউজ টাস্কের মাধ্যমে করার জন্য একটি খুঁজে পেতে?',
      //  noti
      'commented_on': ' মন্তব্য প্রদান করেছে ',
      'completion_rate': 'কার্য সমাপ্তির হার',
      'completion_rate_none': 'কার্য সমাপ্তির হার নেই',
      'task_posted_matching_skills':
          'আপনার দক্ষতার সাথে মিলে একটি নতুন টাস্ক পোস্ট করা হয়েছে: ',
      'list_item_comment_popup_menu_report_label': 'রিপোর্ট',
      'task_details_reviews_someone_left_a_review_for_someone':
          ' জন্য একটি পর্যালোচনা রেখেছেন ',
      // noti settings
      'notifications_settings_test_header': 'এটা কি কাজ করছে?',
      'notifications_settings_test_send_button': 'টেস্ট করুন',
      'notifications_settings_test_description':
          'নিশ্চিত করুন- আপনি দরকারী সব পুশ-নোটিফিকেশন যাতে অবশ্যই পান।',
      'notifications_settings_section':
          'নিম্নোক্ত অপশনগুলো ব্যবহার করে আপনার নোটিফিকেশন যে কোন সময় আপডেট করা যাবে',
      //  noti settings array
      'notifications_settings_label_transactional': 'লেনদেন সংক্রান্ত',
      'notifications_settings_label_task_updates': 'কাজ এর আপডেটসমূহ',
      'notifications_settings_label_task_reminders': 'কাজ এর রিমাইন্ডার',
      'alerts': 'সতর্কতা',
      'notifications_settings_label_task_recommendations': 'কাজ এর সুপারিশগুলো',
      'notifications_settings_label_helpful_information': 'সহায়ক তথ্যাবলী',
      'notifications_settings_label_updates_newsletters':
          'আপডেটসমূহ এবং খবরাখবর',
      'notifications_settings_transactional_details':
          'যে কোন পেমেন্ট, বাতিলকরণ কিংবা আপনার একাউন্ট সংক্রান্ত সবধরনের দরকারী নোটিফকেশন আপনি সবসময় পাবেন।',
      'notifications_settings_task_updates_details':
          'যে কোন নতুন মন্তব্য, ব্যক্তিগত বার্তা, অফার বা রিভিউগুলোর আপডেট জেনে নিন',
      'notifications_settings_task_reminders_details':
          'ছোট্ট রিমাইন্ডার- এটি আপনার স্মরনে না থাকা \'অফার গ্রহণ\', \'পেমেন্ট\' ও \'রিভিউ দেয়া\'- ইত্যাদি বিষয় মনে করিয়ে দেবে',
      'notifications_settings_details_Shohokari_alerts':
          'আপনার সহকারী এলার্ট সার্ভিসটি চালু করলে, আপনার পছন্দের যে কোন কাজ পোস্ট হওয়া মাত্রই আপনার কাছে নোটিফিকেশন যাবে।',
      'notifications_settings_details_task_recommendations':
          'সুপারিশগুলো নিন এবং আপনার কাছাকাছি কাজগুলোর মাধ্যমে অনুপ্রাণিত হউন',
      'notifications_settings_updates_newsletters_details':
          'সহকারীর যে কোন নতুন ফিচার এর  আকর্ষণীয় আপডেট জেনে নিন সবার আগে।',
      'email_noti': 'ইমেল',
      'sms_noti': 'খুদেবার্তা',
      'push_noti': 'ধাক্কা',
      '': '',
      '': '',
      //  noti alert types
      'push_notification_task_alert':
          '[কাজের এলার্ট] #name# এই কাজটি পোস্ট করেছেন',
      'push_notification_make_offer':
          '[অফার দিন] #name# আপনার কাজে একটি অফার করেছেন',
      'push_notification_task_comment':
          '[কাজের কমেন্ট] #name# আপনার কাজে কমেন্ট করেছেন',
      'push_notification_private_message':
          '[ব্যক্তিগত মেসেজ] #name# আপনার কাজ সম্পর্কিত একটি মেসেজ পাঠিয়েছেন',
      'push_notification_offer_message':
          '[অফার মেসেজ] #name# আপনার কাজের অফার সম্পর্কিত একটি মেসেজ পাঠিয়েছেন',
      'push_notification_accept_offer':
          '[অফার গ্রহণ] #name# আপনার অফারটি গ্রহণ করেছেন',
      'push_notification_payment_confirmation':
          '[পেমেন্ট নিশ্চিতকরণ] #name# কাজের জন্য পেমেন্ট গ্রহণ করেছেন',
      'push_notification_user_review':
          '[ইউজার রিভিউ] #name# কাজের জন্য রেটিং ও রিভিউ দিয়েছেন',
      //  profile
      'myprofile': 'আমার প্রোফাইল',
      'task_alerts_edit_online': 'অনলাইন',
      'profile_last_online_format': 'সর্বশেষ অনলাইন ',
      'member_since': ' হতে সদস্য',
      'profile_button_request_a_quote': 'দাম জিজ্ঞাসা করুন',
      'profile_report_user_button_label': 'এই সদস্যকে রিপোর্ট করুন',
      'profile_no_reviews_runner':
          'দেখা যাচ্ছে আপনি এখনও পর্যন্ত কোন রিভিউ পাননি',
      'profile_user_no_reviews_runner': ' এখনও পর্যন্ত কোন রিভিউ পাননি',
      'profile_no_reviews_created':
          'মনে হচ্ছে আপনি এখনো কোনো রিভিউ তৈরি করেননি',
      'profile_user_no_reviews_created': ' এখনো কোনো রিভিউ তৈরি করেনি',
      'view_badges_badges_label': 'ব্যাজসমূহ',
      'profile_screen_portfolio_section_label': 'পোর্টফোলিও',
      'portfolio': 'পোর্টফোলিও',
      'full_screen_image_screen_title': 'সংযুক্তি',
      'view_user_no_badges_yet': 'আইডি ব্যাজগুলো কোথায়? এখনও কোনটি পাওয়া যায়নি',
      'task_details_missing_badge_learn_more': 'আরও জানুন',
      'skills_label': 'দক্ষতাসমূহ',
      'transportation_label': 'পরিবহন',
      'languages_label': 'ভাষাসমূহ',
      'education_label': 'শিক্ষা',
      'work_label': 'কাজ',
      'specialities_label': 'বিশেষত্বসমূহ',
      //  edit profile
      'profile_updated_alert': 'প্রোফাইল সফলভাবে আপডেট করা হয়েছে।',
      'pick_loc': 'আপনার অবস্থান বেছে নিন',
      'dialog_discard_changes_title': 'সেভ না করা পরিবর্তনসমূহ',
      'dialog_discard_changes_message': 'আপনি কি পরিবর্তনটি মুছে ফেলতে চাইছেন?',
      'edit_profile': 'প্রোফাইল সংশোধন করুন',
      'save': 'সেভ করুন',
      'deactive_acc': 'আমার অ্যাকাউন্ট নিষ্ক্রিয় করুন',
      'general_information_label': 'সাধারন তথ্যাবলী',
      'private_information_label': 'ব্যক্তিগত তথ্যাবলী',
      'additional_information_label': 'অতিরিক্ত তথ্যাবলী',
      'reason': 'কারণ',
      'deactivate': 'নিষ্ক্রিয় করুন',
      'headline': 'শিরোনাম',
      'edit_profile_about_me_label': 'আমার সম্পর্কে',
      'email_not_verified': 'ইমেল যাচাই করা হয়নি।\nআপনার ইমেইল যাচাই করুন।',
      'dob': 'জন্ম তারিখ',
      'current_mobile_number_label': 'বর্তমান মোবাইল নম্বর',
      'portfolio_item_delete': 'পোর্টফোলিও আইটেম ডিলিট',
      'portfolio_del_confirm':
          'আপনি কি নিশ্চিত, আপনি এই পোর্টফোলিও মুছে ফেলতে চান?',
      'means_transport': 'আপনার পরিবহনের উপায় নির্বাচন করুন?',
      'languages_versed': 'আপনি কোন ভাষায় পারদর্শী?',
      'education_input_hint': 'আপনি কোথায় পড়াশুনা করেছেন?',
      'work_input_hint': 'আপনি এর আগে কোথায় কাজ করেছেন?',
      'specialities_input_hint': 'আপনি কোন কাজে বেশি দক্ষ?',
      'enter_skills': 'দয়া করে কিছু দক্ষতা লিখুন',
      'add': 'যোগ করুন',
      //  Task details
      'task_details': 'কাজ এর বিস্তারিত',
      'active': 'সক্রিয়',
      'paid': 'পরিশোধিত',
      'cancelled': 'বাতিলকৃত',
      'posted_by': 'পোস্ট করেছেন',
      'task_details_view_map': 'ম্যাপ দেখুন',
      'task_loc': 'কাজ এর স্থান',
      'map_get_directions': 'দিকনির্দেশনা নিন',
      'reschedule': 'রি-শিডিউল',
      'approx': 'প্রায়',
      'hrs': 'ঘন্টা',
      'per_shohokari': 'সহকারী প্রতি',
      'task_price': 'দর',
      'task_price2': 'দর',
      'make_offer': 'একটি অফার দিন',
      'review_offer': 'অফারগুলো পর্যবেক্ষণ করুন',
      'task_details_prompt_info':
          'পেমেন্ট ছাড় নিশ্চিত করতে আপনাকে জিজ্ঞাসা করা হবে',
      'release_payment': 'পেমেন্ট ছাড়ুন',
      'receive_payment': 'পেমেন্ট গ্রহণ',
      'request_payment': 'পেমেন্ট অনুরোধ',
      'request': 'অনুরোধ',
      'funded': 'ফান্ডেড পেমেন্ট',
      'request_release_payment': 'পেমেন্ট রিলিজের অনুরোধ',
      'update_offer': 'অফার আপডেট করুন',
      'increase_price': 'মূল্যবৃদ্ধি',
      'task_details_open_private_messages_button': 'ব্যক্তিগত ইনবক্স চালু করুন',
      'task_summary': 'টাস্ক সারাংশ',
      'spots_left': 'কোটা খালি আছে',
      'offers': 'অফারসমূহ',
      'view_all_offers_button': 'সব অফারগুলো দেখুন',
      'task_details_awaiting_offers_label': 'অফারের জন্য অপেক্ষমান',
      'reply': 'উত্তর',
      'withdraw_offer': 'প্রত্যাহার',
      'are_you_sure_you_want_to_withdraw_your_offer':
          'আপনি কি নিশ্চিতভাবে অফারটি সরিয়ে ফেলতে চান?',
      'review': 'পুনঃমূল্যায়ন',
      'accept': 'গ্রহণ',
      'release': 'মুক্তি',
      'comments': 'কমেন্টসমূহ',
      'askq': '#name# কে একটি প্রশ্ন করুন',
      'donot_share_personal_info_msg':
          'Shohokari ব্যবহার করে যে কেউ এই মন্তব্য দেখতে পারেন। আপনার নিজের নিরাপত্তার জন্য, দয়া করে এখানে কোন ব্যক্তিগত তথ্য শেয়ার করবেন না।',
      'dialog_cancel_posted_task_message':
          'আপনি কি নিশ্চিত যে আপনার কাজটি বাতিল করবেন?',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      '': '',
      //  task alert keyword
      'kw_long': 'কীওয়ার্ড কমপক্ষে letters টি অক্ষরের হতে হবে',
      //  payment
      'payment_medthod_card': 'ব্যাংক/কার্ড',
      'payment_medthod_cash': 'নগদ',
      //  payment history
      'more_options_payment_history_label': 'পেমেন্ট ইতিহাস',
      'payment_history_tab_title_earned': ' অর্জিত ',
      'payment_history_tab_title_outgoing': 'ব্যয়',
      'payment_history_showing_label': 'প্রদর্শিত হচ্ছে:',
      'task_receipt_net_earning_label': 'নিট আয়',
      'task_receipt_net_paid_label': 'নিট পেমেন্ট',
      'file': 'ফাইল',
      'file_downloaded': 'ফাইল ডাউনলোড সফলভাবে।',
      'payment_history_nf': 'পেমেন্টের ইতিহাস পাওয়া যায়নি',
      'payment_history_download_button': 'ডাউনলোড',
      'payment_history_no_transactions_filtered':
          'আপনি এ যাবৎ কোন পেমেন্ট করেননি! আপনার ফিল্টার সেটিংস পরিবর্তন করুন ।',
      'payment_history_no_transactions_outgoing':
          'আপনি এ যাবৎ কোন কাজ এর পেমেন্ট করেননি',
      'task_receipt_funds_released_on_label': 'ফান্ড অবমুক্ত হয়েছে:',
      'task_receipt_funds_secured_on_label': 'ফান্ড সংরক্ষিত হয়েছে:',
      'task_receipt_payment_method_label': 'পেমেন্ট পদ্ধতি:',
      'All time': 'সব সময়',
      'Last financial year': 'বিগত অর্থবছর',
      'Current financial year': 'চলতি অর্থবছর',
      'Last quarter': 'বিগত কোয়ার্টার',
      'Current quarter': 'চলতি কোয়ার্টার',
      //  Payment settings
      'payment_settings': 'পেমেন্ট সেটিংস',
      'payment_settings_tab_title_make_payments': 'পেমেন্ট করুন',
      'payment_settings_tab_title_receive_payments': 'পেমেন্ট গ্রহণ করুন',
      'payment_setting_add_promotion_code': 'প্রমোশন কোড যোগ করুন',
      'add_billing_address': 'যে ঠিকানায় বিল পাঠানো হবে',
      'add_bank_account_screen_title_add': 'ব্যাংক হিসাব যোগ করুন',
      'mobile_payment_screen_title_bikash': 'বিকাশ একাউন্ট যোগ করুন',
      'mobile_payment_screen_title_rocket': 'রকেট একাউন্ট যোগ করুন',
      'add_Shohokari_card': 'প্রমোশন কোড যোগ করুন',
      'add_shohokari_card_code_hint': 'প্রোমো/ গিফ্‌ট কোড যোগ করুন',
      'code': 'কোড',
      'empty_promotion_code_error_message': 'প্রমোশন কোড খালি রাখা যাবে না',
      'promotion_code_added_message': 'প্রচার কোড সফলভাবে যোগ করা হয়েছে',
      'payment_setting_invalid_promotion_code': 'ভুল প্রোমোশন কোড',
      'payment_setting_invalid_promotion_code_messgae':
          'প্লিজ সঠিক প্রোমোশন কোড দিন',
      'billing_address_button_change': 'বর্তমান ঠিকানা পরিবর্তন করুন',
      'billing_address_au_address_line_1_hint': 'ঠিকানা লাইন ১',
      'billing_address_au_address_line_2_hint': 'ঠিকানা লাইন ২(বাধ্যতামূলক নয়)',
      'area': 'এলাকা',
      'city': 'বিভাগ',
      'address': 'ঠিকানা',
      'postcode': 'পোস্ট কোড',
      'country': 'দেশ',
      'billing_addr_added': 'বিলিং ঠিকানা সফলভাবে যোগ করা হয়েছে।',
      'billing_addr_updated': 'বিলিং ঠিকানা সফলভাবে আপডেট হয়েছে।',
      'add_bank_account': 'আপনার পেমেন্ট সম্পর্কিত তথ্য দিন',
      'account_name': 'একাউন্ট এর নাম',
      'bank_name': 'ব্যাংকের নাম',
      'account_number': 'একাউন্ট এর নম্বর',
      'add_bank_account_active_checkbox_label':
          'ডিফল্ট পেমেন্ট পদ্ধতি হিসেবে যোগ করুন',
      'add_bank_account_error_account_name':
          'দয়া করে ব্যাংক হিসাব নামটি সঠিকভাবে দিন',
      'add_bank_account_error_bank_name': 'একটি বৈধ ব্যাংকের নাম লিখুন',
      'add_bank_account_error_account_length_gb':
          'দয়া করে সঠিক ব্যাংক হিসাব নম্বর দিন ।',
      'bank_acc_updated': 'ব্যাংক অ্যাকাউন্ট সফলভাবে আপডেট করা হয়েছে।',
      'bank_acc_added': 'ব্যাঙ্ক অ্যাকাউন্ট সফলভাবে যোগ করা হয়েছে।',
      'mobile_payment_error_number': 'দয়া করে সঠিক নাম্বারটি দিন',
      'bikash_added_msg': 'বিকাশ অ্যাকাউন্ট সফলভাবে যোগ করা হয়েছে।',
      'bikash_updated_msg': 'বিকাশ অ্যাকাউন্ট সফলভাবে আপডেট হয়েছে।',
      'rocket_added_msg': 'রকেট অ্যাকাউন্ট সফলভাবে যোগ করা হয়েছে।',
      'rocket_updated_msg': 'রকেট অ্যাকাউন্ট সফলভাবে আপডেট হয়েছে।',
      //  Reviws
      'reviews_screen_empty_state': 'আপনি এখনও কোন রিভিউ পান নি...',
      //  Task Alerts
      'task_alerts': 'কাজ এর এলার্ট',
      'list_item_task_remote_label': 'অনলাইন',
      'within': 'মধ্যে ',
      'alerts_kw_nf_title':
          'যদি আপনি নীচের থেকে যে ধরনের চাকরির বিজ্ঞপ্তি পেতে চান তাতে কীওয়ার্ড যোগ করেন, তাহলে আপনি এই ধরনের কাজের পোস্ট করার সাথে সাথেই বিজ্ঞপ্তি পাবেন।',
      'alerts_kw_nf':
          'একটি টাস্ক যা আপনি আগ্রহী। কীওয়ার্ড যেমন মুভিং বা ক্লিন যোগ করুন।',
      'add_task_alert': 'কাজের সতর্কতা যোগ করুন',
      'edit_task_alert': 'টাস্ক সতর্কতা সম্পাদনা করুন',
      'dialog_task_alert_delete_title': 'সতর্কবার্তাটি মুছে ফেলুন',
      'dialog_task_alert_delete_message':
          'আপনি কি নিশ্চিত যে আপনি এই সতর্কবার্তাটি মুছে ফেলতে চান?',
      'add_alert': 'সতর্কতা যোগ করুন',
      'update_alert': 'আপডেট সতর্কতা',
      'kw_phrase':
          'কীওয়ার্ড বা ফ্রেজ (ড্রাইভার, ক্লিন, পার্ট টাইম, ওয়েব ইত্যাদি)',
      'enter_kw_phrase': 'কীওয়ার্ড বা ফ্রেজ লিখুন',
      //  Help
      'support_centre': 'সাপোর্ট সেন্টার',
      'terms_and_conditions': 'নীতিমালা ও শর্তাবলী',
      'privacy_screen_title': 'গোপনীয়তা',
      'guide_around_shohokari': 'শোহোকরির চারপাশে গাইড',
      'guide_become_tasker': 'একজন টাস্কার হওয়ার গাইড',
      'tasker_tutorial': 'সহকারী কর্মী টিউটোরিয়াল (ভিডিও)',
      'poster_tutorial': 'কাজ প্রদানকারী টিউটোরিয়াল (ভিডিও)',
      'support_activity_title': 'সহায়তা কেন্দ্র',
      'call': 'কল',
      'send_message': 'বার্তা পাঠান',
      'contact_us': 'যোগাযোগ করুন',
      'how_help_you': 'কিভাবে আমরা আপনাকে সাহায্য করতে পারে?',
      'choose_ticket_type': 'তালিকা থেকে টিকিটের ধরন বেছে নিন',
      'pls_enter_desc': 'বিবরণ লিখুন',
      'contact_from_app': 'অ্যাপ থেকে যোগাযোগ করুন',
      //  Contact us
      'select_sup_ticket_type': 'সমর্থন টিকিটের ধরন নির্বাচন করুন',
      //  Tutorial 1
      'tutorial_text_1':
          'প্রয়োজনীয় কাজের জন্য সহকারী খুঁজতে কাজ পোস্ট করুন  আইকন-এ ট্যাপ করুন',
      'tutorial_text_2':
          'আপনার সমস্ত কাজ একসাথে দেখতে আমার কাজ  আইকন- এ ট্যাপ করুন',
      'tutorial_text_3': 'সহকারী হিসেবে কাজ করতে কাজ খুঁজুন  আইকন এ ট্যাপ করুন',
      'tutorial_text_4':
          'আপনার মেসেজ বা বার্তাসমূহ দেখতে   ইনবক্স   আইকন এ ট্যাপ করুন',
      'tutorial_text_5':
          'আপনার প্রোফাইল, পেমেন্ট নোটিফিকেশন ও অন্যান্য সেটিংস এর জন্য  আরও   আইকন- এ ট্যাপ করুন',
      'tutorial_text_6':
          'সহকারী হিসেবে আপনার পছন্দের কাজের এলার্ট পেতে কাজের এলার্ট  আইকন-এ ট্যাপ করুন',
      'finish': 'শেষ করুন',
      'continue': 'চালিয়ে যান',
      //  Tutorial 2
      'hi': 'হাই',
      'lets_get_earning_title': 'চলুন আয় শুরু করা যাক!',
      'on_boarding_runner_one_message':
          'আপনার প্রোফাইলটিতে ছবি সংযুক্ত করুন এবং বিস্তারিত বর্ণনা দিন, যাতে কাজদাতাগণ আপনাকে ভালোভাবে জানতে পারে। একটু সময় প্রোফাইল তৈরি করুন যাতে তারা আপনাকে বাছাই করে।',
      'on_boarding_tasker_two_title': 'কাজ খুঁজুন',
      'on_boarding_tasker_two_message':
          'আপনি করতে পারবেন এমন কাজ খুঁজুন। দেখুন কি করতে হবে, কোথায় ও কখন করতে হবে? এরপর আপনার অফারটি করুন।',
      'on_boarding_tasker_three_title': 'একটি অফার করুন',
      'on_boarding_tasker_three_message':
          'যখন আপনি কোন কাজের জন্য অফার করবেন তখন আপনি সেই কাজের জন্য কেন যোগ্য তা উল্লেখ করুন। একটি গ্রহণযোগ্য দাম উল্লেখ করুন এবং কোন প্রশ্ন থাকলে উত্তর দিন',
      'on_boarding_tasker_four_title':
          'কাজটি ভালভাবে সম্পন্ন করে পেমেন্ট বুঝে নিন!',
      'on_boarding_tasker_four_message':
          'আপনার অফারটি গৃহীত হলে কাজ সম্পন্ন করে পেমেন্ট বুঝে নিন। অনলাইনে পরিশোধিত হলে আপনার পেমেন্ট সুরক্ষিত থাকবে। কাজটি শেষ করে \'পেমেন্ট রিলিজ\' এর জন্য কাজ পোস্টকারীকে অনুরোধ করুন!',
      'back': 'ফিরে যান',
      'previous': 'আগে',
      //  Chat
      'questions': 'প্রশ্ন সমূহ',
      'private_messages': 'ব্যক্তিগত ইনবক্স',
      'poster': 'কাজ পোস্টকারী',
      'private_conversation_messages_closed_message':
          'কাজ সম্পন্ন হয়েছে। গোপন বার্তা- যোগাযোগ বন্ধ করা হল।',
      'private_conversation_messages_cancel_message':
          'টাস্ক বাতিল। ব্যক্তিগত বার্তা বন্ধ',
      'hint_enter_a_message': 'একটি বার্তা লিখুন…',
      'offer_chat': 'অফার-আলাপ',
      //  Offer Comments
      'task_paid_offer_closed':
          'টাস্ক পেইড।. হয়েছে। অফার বার্তা- যোগাযোগ বন্ধ করা হল।',
      'task_cancel_offer_closed':
          'টাস্ক বাতিল. হয়েছে। অফার বার্তা- যোগাযোগ বন্ধ করা হল।',
      'reply_to': '#name# কে জবাব দিন',
      'send': 'পাঠান',
      'chat': 'আড্ডা',
      //  Receive Payment
      'review_additional_funds_payment_methods': 'পেমেন্ট পদ্ধতি',
      //  Resolution Page
      'flag_screen_title_task': 'কাজটি রিপোর্ট করুন',
      'flag_source_comment': 'মন্তব্য',
      'flag_screen_title_user': 'ব্যবহারকারীকে রিপোর্ট করুন',
      'flag_screen_send_report_button_label': 'রিপোর্ট পাঠান',
      'flag_screen_please_enter_a_comment_message': 'দয়া করে একটি মন্তব্য করুন',
      'select_report_type': 'রিপোর্ট টাইপ নির্বাচন করুন',
      'submitted_successfully': 'সফলভাবে সাবমিট  হল',
      'empty_description_error_message': 'বর্ণনা',
      'offer': 'অফার',
      'cash_payment': 'ক্যাশ পেমেন্ট',
      'cash_payment_message': 'প্লিজ সরাসরি #name# কে ক্যাশ পেমেন্ট করুন',
      'choose_payment_opt': 'তালিকা থেকে পেমেন্ট অপশন বেছে নিন',
      'release_payment_payment_secured': 'পেমেন্ট নিরাপদে সংরক্ষিত',
      'offer_accepted_confirmation_funds_secured_text':
          'কাজ এর মূল্য নিরাপদে সংরক্ষিত হবে। কাজ এর মূল্য অবমুক্ত করাটা আপনার নিয়ন্ত্রনেই থাকবে। একই কাজ এর জন্য দ্বিতীয়বার মূল্য দিতে হবে না।',
      'offer_accepted_cash_confirmation_funds_secured_text':
          'আপনার পেমেন্টটি সরাসরি #name# নিকট নগদ পরিশোধ করবেন এবং সহকারী সেটি প্রাপ্তিস্বীকার করবেন',
      'release_payment_terms':
          'দয়া করে মনে রাখবেন- পেমেন্ট ছেড়ে দিলেন মানে আপনি আমাদের পেমেন্ট নীতিমালা ও শর্তাবলী এর সাথে সম্মত হলেন',
      'funded_payment_methods': 'পেমেন্টের ধরন ঠিক করুন',
      'received': 'গৃহীত হয়েছে',
      'message_for_payment_request__title_label':
          'আপনি কি নিশ্চিত আপনি কাজটি সম্পন্ন করেছেন? (আপনি যদি কাজটি সম্পন্ন না করে থাকেন তবে হয়তো আপনার কাজদাতা বিরক্ত হতে পারেন!)',
      'message_for_payment_request_label': 'পেমেন্ট এর জন্য বার্তা',
      'requested_release_payment_message':
          ' আপনাকে অনুরধ করা হচ্ছে আপনার কাজের পারিশ্রমিক পরিশোধ করতে কারণ আপনার দেয়া কাজটি শেষ হয়েছে। : ',
      'requested_payment_message':
          ' আপনাকে অনুরধ করা হচ্ছে আপনার কাজের পারিশ্রমিক পরিশোধ করতে কারণ আমি আপনার কাজটি শেষ করেছি। : ',
      //  Review Page
      'leave_review': 'রিভিউ দিন',
      'leave_review_button_label': 'রিভিউ সাবমিট করুন',
      'leave_review_leave_review_for': 'রিভিউ দিন কাজ এর জন্য -',
      'leave_review_rating_bar_label':
          'এই রিভিউটির জন্য নির্দিষ্ট সংখ্যার স্টার চাপুন',
      'leave_review_review_hint': 'দয়া করে এখানে একটি রিভিউ দিন',
      //  Accept Offer
      'accept_offer': 'প্রস্তাব গ্রহণ',
      'by_accepting_offer_text':
          'এই প্রস্তাব গ্রহণ করে, আপনি সম্মত চাকরির জন্য টাস্কারকে অর্থ প্রদান করতে সম্মত হন। আপনি যদি টাস্কটি গ্রহণ করার পর তা বন্ধ করে দেন তাহলে আপনার কাছে ফি নেওয়া হতে পারে।',
      'no_offers_yet_message': 'এই কাজের জন্য আপনি এখনো কোন অফার পাননি',
      'update_make_offer': 'একটি অফার আপডেট করুন',
      'make_offer_your_offer_label': 'আপনার অফার:',
      'make_offer_why_you_the_best_label':
          'এই কাজের জন্য আপনি নিজেকে সবচাইতে যোগ্য লোক কেন ভাবছেন?',
      'make_offer_you_will_receive': 'আপনি পাবেন: #amt# টাকা।',
      'make_offer_service_fee': '(সার্ভিস ফি: #srv_fee#)',
      'offers_nf': 'আপনি এই কাজের জন্য এখনও কোন অফার পাননি',
      'others': 'অন্যান্য',
      'confirm_your_offer': 'আপনার অফারটি নিশ্চিত করুন',
      'pending_verification': 'ভেরিফিকেশন এর জন্য অপেক্ষাধীন আছে!',
      'no_yet_verified_user_message_message':
          'আপনার তথ্য ভেরিফিকেশন এর জন্য অপেক্ষাধীন আছে। আপনি কি আমাদের সাথে যোগাযোগ করতে চান?',
      //  User Role
      'user_role': 'ব্যবহারকারীর ভূমিকা',
      'user_role_header': 'আপনার ভূমিকা কি?',
      'employer': 'নিয়োগকর্তা',
      'user_role_employer_label': 'আমি কাজ দিতে চাই',
      'worker': 'কর্মী',
      'user_role_worker_label': 'আমি কাজ এবং অর্থ উপার্জন করতে চাই',
      //  Case Alert
      'case_alert': 'কেস সতর্কতা',
      'close': 'বন্ধ',
      'date': 'তারিখ',
      //  Confirm Offer Opt via Banner
      'confirm_your_offer_add_profile_picture': 'প্রোফাইল ছবি আপলোড করুন',
      'confirm_your_offer_add_bank_account': 'আপনার পেমেন্ট সম্পর্কিত তথ্য দিন',
      'confirm_your_offer_add_nid_number':
          'আপনার জাতীয় পরিচয়পত্রের ছবি আপলোড করুন',
      'success': 'সফলতা',
      'error': 'ত্রুটি',
      '': '',
      '': '',
      '': '',
      '': '',
    };
  }
}
