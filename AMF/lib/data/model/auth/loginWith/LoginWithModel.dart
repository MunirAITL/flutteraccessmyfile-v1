class LoginWithModel {
  String briefBio;
  String communityId;
  String deviceType;
  String email;
  String experienceCompany;
  String experienceFromDate;
  String experienceLocation;
  String experienceRemarks;
  String experienceTitle;
  String firstName;
  String headline;
  String lastName;
  bool persist;
  String referenceId;
  String referenceType;
  String version;

  LoginWithModel(
      {this.briefBio,
      this.communityId,
      this.deviceType,
      this.email,
      this.experienceCompany,
      this.experienceFromDate,
      this.experienceLocation,
      this.experienceRemarks,
      this.experienceTitle,
      this.firstName,
      this.headline,
      this.lastName,
      this.persist,
      this.referenceId,
      this.referenceType,
      this.version});

  LoginWithModel.fromJson(Map<String, dynamic> json) {
    briefBio = json['BriefBio'] ?? '';
    communityId = json['CommunityId'] ?? '';
    deviceType = json['DeviceType'] ?? '';
    email = json['Email'] ?? '';
    experienceCompany = json['ExperienceCompany'] ?? '';
    experienceFromDate = json['ExperienceFromDate'] ?? '';
    experienceLocation = json['ExperienceLocation'] ?? '';
    experienceRemarks = json['ExperienceRemarks'] ?? '';
    experienceTitle = json['ExperienceTitle'] ?? '';
    firstName = json['FirstName'] ?? '';
    headline = json['Headline'] ?? '';
    lastName = json['LastName'] ?? '';
    persist = json['Persist'] ?? false;
    referenceId = json['ReferenceId'] ?? '';
    referenceType = json['ReferenceType'] ?? '';
    version = json['Version'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['BriefBio'] = this.briefBio;
    data['CommunityId'] = this.communityId;
    data['DeviceType'] = this.deviceType;
    data['Email'] = this.email;
    data['ExperienceCompany'] = this.experienceCompany;
    data['ExperienceFromDate'] = this.experienceFromDate;
    data['ExperienceLocation'] = this.experienceLocation;
    data['ExperienceRemarks'] = this.experienceRemarks;
    data['ExperienceTitle'] = this.experienceTitle;
    data['FirstName'] = this.firstName;
    data['Headline'] = this.headline;
    data['LastName'] = this.lastName;
    data['Persist'] = this.persist;
    data['ReferenceId'] = this.referenceId;
    data['ReferenceType'] = this.referenceType;
    data['Version'] = this.version;
    return data;
  }
}
