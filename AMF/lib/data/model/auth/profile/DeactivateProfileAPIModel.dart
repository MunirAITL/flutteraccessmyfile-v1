class DeactivateProfileAPIModel {
  bool success;
  _ErrorMessages errorMessages;
  _Messages messages;
  dynamic responseData;

  DeactivateProfileAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory DeactivateProfileAPIModel.fromJson(Map<String, dynamic> j) {
    return DeactivateProfileAPIModel(
      success: j['Success'] as bool,
      errorMessages: _ErrorMessages.fromJson(j['ErrorMessages']) ?? [],
      messages: _Messages.fromJson(j['Messages']) ?? [],
      responseData: j['ResponseData'] ?? null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ErrorMessages {
  dynamic delete;
  _ErrorMessages({this.delete});
  factory _ErrorMessages.fromJson(Map<String, dynamic> j) {
    return _ErrorMessages(
      delete: j['delete'] ?? {},
    );
  }
  Map<String, dynamic> toMap() => {
        'delete': delete,
      };
}

class _Messages {
  dynamic delete;
  _Messages({this.delete});
  factory _Messages.fromJson(Map<String, dynamic> j) {
    return _Messages(
      delete: j['delete'] ?? {},
    );
  }
  Map<String, dynamic> toMap() => {
        'delete': delete,
      };
}
