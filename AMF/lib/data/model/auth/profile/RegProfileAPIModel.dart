import 'package:aitl/data/model/auth/UserModel.dart';

class RegProfileAPIModel {
  Messages messages;
  ErrorMessages errorMessages;
  bool success;
  ResponseData responseData;

  RegProfileAPIModel({this.messages, this.success, this.responseData});

  RegProfileAPIModel.fromJson(Map<String, dynamic> json) {
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        //'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class ErrorMessages {
  List<dynamic> postUser;
  ErrorMessages({this.postUser});
  ErrorMessages.fromJson(Map<String, dynamic> json) {
    postUser = json['post_user'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_user'] = this.postUser;
    return data;
  }
}

class Messages {
  List<dynamic> postUser;
  Messages({this.postUser});
  Messages.fromJson(Map<String, dynamic> json) {
    postUser = json['post_user'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_user'] = this.postUser;
    return data;
  }
}

class ResponseData {
  UserModel user;
  ResponseData({this.user});
  ResponseData.fromJson(Map<String, dynamic> json) {
    user = json['User'] != null ? new UserModel.fromJson(json['User']) : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    return data;
  }
}
