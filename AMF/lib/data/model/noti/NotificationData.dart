class NotificationData {
  var initiatorId;
  var description;
  var message;
  var entityName;
  var companyId;
  var initiatorName;
  var webUrl;
  var isRead;
  var entityId;
  var userId;
  var readDateTime;
  var id;
  var notificationEventId;
  var publishDateTime;

  NotificationData(
      {this.initiatorId,
        this.description,
        this.message,
        this.entityName,
        this.companyId,
        this.initiatorName,
        this.webUrl,
        this.isRead,
        this.entityId,
        this.userId,

        this.readDateTime,

        this.id,
        this.notificationEventId,
        this.publishDateTime});

  NotificationData.fromJson(Map<String, dynamic> json) {
    initiatorId = json['InitiatorId'];
    description = json['Description'];
    message = json['Message'];
    entityName = json['EntityName'];
    companyId = json['CompanyId'];
    initiatorName = json['InitiatorName'];
    webUrl = json['WebUrl'];
    isRead = json['IsRead'];
    entityId = json['EntityId'];
    userId = json['UserId'];
    readDateTime = json['ReadDateTime'];
    id = json['Id'];
    notificationEventId = json['NotificationEventId'];
    publishDateTime = json['PublishDateTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['InitiatorId'] = this.initiatorId;
    data['Description'] = this.description;
    data['Message'] = this.message;
    data['EntityName'] = this.entityName;
    data['CompanyId'] = this.companyId;
    data['InitiatorName'] = this.initiatorName;
    data['WebUrl'] = this.webUrl;
    data['IsRead'] = this.isRead;
    data['EntityId'] = this.entityId;
    data['UserId'] = this.userId;
    data['ReadDateTime'] = this.readDateTime;
    data['Id'] = this.id;
    data['NotificationEventId'] = this.notificationEventId;
    data['PublishDateTime'] = this.publishDateTime;
    return data;
  }
}
