class UserRatingSummaryDataModel {
  double avgRating;
  int ratingCount;
  int completedPosterTaskCount;
  int completedTaskerTaskCount;
  int id;
  bool isPoster;
  int posterAverageRating;
  int posterCompletionRate;
  int posterRatingCount;
  int taskerAverageRating;
  int taskerCompletionRate;
  int taskerRatingCount;
  int totalPosterFiveStar;
  int totalPosterFourStar;
  int totalPosterThreeStar;
  int totalPosterTwoStar;
  int totalTaskerFiveStar;
  int totalTaskerFourStar;
  int totalTaskerOneStar;
  int totalTaskerThreeStar;
  int totalTaskerTwoStar;
  int totalPosterOneStar;

  UserRatingSummaryDataModel({
    this.avgRating,
    this.ratingCount,
    this.completedPosterTaskCount,
    this.completedTaskerTaskCount,
    this.id,
    this.isPoster,
    this.posterAverageRating,
    this.posterCompletionRate,
    this.posterRatingCount,
    this.taskerAverageRating,
    this.taskerCompletionRate,
    this.taskerRatingCount,
    this.totalPosterFiveStar,
    this.totalPosterFourStar,
    this.totalPosterThreeStar,
    this.totalPosterTwoStar,
    this.totalTaskerFiveStar,
    this.totalTaskerFourStar,
    this.totalTaskerOneStar,
    this.totalTaskerThreeStar,
    this.totalTaskerTwoStar,
    this.totalPosterOneStar,
  });

  UserRatingSummaryDataModel.fromJson(Map<String, dynamic> json) {
    avgRating = json['AvgRating'] ?? 0.0;
    ratingCount = json['RatingCount'] ?? 0;
    completedPosterTaskCount = json['CompletedPosterTaskCount'] ?? 0;
    completedTaskerTaskCount = json['CompletedTaskerTaskCount'] ?? 0;
    id = json['Id'] ?? 0;
    isPoster = json['IsPoster'] ?? false;
    posterAverageRating = json['PosterAverageRating'] ?? 0;
    posterCompletionRate = json['PosterCompletionRate'] ?? 0;
    posterRatingCount = json['PosterRatingCount'] ?? 0;
    taskerAverageRating = json['TaskerAverageRating'] ?? 0;
    taskerCompletionRate = json['TaskerCompletionRate'] ?? 0;
    taskerRatingCount = json['TaskerRatingCount'] ?? 0;
    totalPosterFiveStar = json['TotalPosterFiveStar'] ?? 0;
    totalPosterFourStar = json['TotalPosterFourStar'] ?? 0;
    totalPosterThreeStar = json['TotalPosterThreeStar'] ?? 0;
    totalPosterTwoStar = json['TotalPosterTwoStar'] ?? 0;
    totalTaskerFiveStar = json['TotalTaskerFiveStar'] ?? 0;
    totalTaskerFourStar = json['TotalTaskerFourStar'] ?? 0;
    totalTaskerOneStar = json['TotalTaskerOneStar'] ?? 0;
    totalTaskerThreeStar = json['TotalTaskerThreeStar'] ?? 0;
    totalTaskerTwoStar = json['TotalTaskerTwoStar'] ?? 0;
    totalPosterOneStar = json['TotalPosterOneStar'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['AvgRating'] = this.avgRating;
    data['RatingCount'] = this.ratingCount;
    data['CompletedPosterTaskCount'] = this.completedPosterTaskCount;
    data['CompletedTaskerTaskCount'] = this.completedTaskerTaskCount;
    data['Id'] = this.id;
    data['IsPoster'] = this.isPoster;
    data['PosterAverageRating'] = this.posterAverageRating;
    data['PosterCompletionRate'] = this.posterCompletionRate;
    data['PosterRatingCount'] = this.posterRatingCount;
    data['TaskerAverageRating'] = this.taskerAverageRating;
    data['TaskerCompletionRate'] = this.taskerCompletionRate;
    data['TaskerRatingCount'] = this.taskerRatingCount;
    data['TotalPosterFiveStar'] = this.totalPosterFiveStar;
    data['TotalPosterFourStar'] = this.totalPosterFourStar;
    data['TotalPosterThreeStar'] = this.totalPosterThreeStar;
    data['TotalPosterTwoStar'] = this.totalPosterTwoStar;
    data['TotalTaskerFiveStar'] = this.totalTaskerFiveStar;
    data['TotalTaskerFourStar'] = this.totalTaskerFourStar;
    data['TotalTaskerOneStar'] = this.totalTaskerOneStar;
    data['TotalTaskerThreeStar'] = this.totalTaskerThreeStar;
    data['TotalTaskerTwoStar'] = this.totalTaskerTwoStar;
    data['TotalPosterOneStar'] = this.totalPosterOneStar;
    return data;
  }
}
