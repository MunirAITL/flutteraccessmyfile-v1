import 'UserBadgesModel.dart';

class UserBadgeAPIModel {
  bool success;
  ResponseData responseData;

  UserBadgeAPIModel({this.success, this.responseData});
  UserBadgeAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  List<UserBadgesModel> userBadges;
  ResponseData({this.userBadges});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['UserBadges'] != null) {
      userBadges = [];
      json['UserBadges'].forEach((v) {
        try {
          userBadges.add(new UserBadgesModel.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userBadges != null) {
      data['UserBadges'] = this.userBadges.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
