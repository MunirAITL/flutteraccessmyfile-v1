import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertsModel.dart';

class TaskAlertsAPIModel {
  bool success;
  ResponseData responseData;

  TaskAlertsAPIModel({this.success, this.responseData});

  TaskAlertsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  List<TaskAlertKeywordsModel> taskAlertKeywords;
  ResponseData({this.taskAlertKeywords});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['TaskAlertKeywords'] != null) {
      taskAlertKeywords = [];
      json['TaskAlertKeywords'].forEach((v) {
        try {
          taskAlertKeywords.add(new TaskAlertKeywordsModel.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taskAlertKeywords != null) {
      data['TaskAlertKeywords'] =
          this.taskAlertKeywords.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
