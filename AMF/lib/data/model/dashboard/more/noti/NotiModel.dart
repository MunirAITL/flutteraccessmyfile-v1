class NotiModel {
  bool isRead;
  String publishDateTime;
  String readDateTime;
  int entityId;
  String entityName;
  String entityTitle;
  String entityDescription;
  String eventName;
  int initiatorId;
  String initiatorDisplayName;
  String initiatorImageUrl;
  String message;
  String webUrl;
  String description;
  String receivedName;
  int userId;
  int notificationEventId;
  int id;

  NotiModel({
    this.isRead,
    this.publishDateTime,
    this.readDateTime,
    this.entityId,
    this.entityName,
    this.entityTitle,
    this.entityDescription,
    this.eventName,
    this.initiatorId,
    this.initiatorDisplayName,
    this.initiatorImageUrl,
    this.message,
    this.webUrl,
    this.description,
    this.receivedName,
    this.userId,
    this.notificationEventId,
    this.id,
  });

  factory NotiModel.fromJson(Map<String, dynamic> j) {
    return NotiModel(
      isRead: j['IsRead'] ?? false,
      publishDateTime: j['PublishDateTime'] ?? '',
      readDateTime: j['ReadDateTime'] ?? '',
      entityId: j['EntityId'] ?? 0,
      entityName: j['EntityName'] ?? '',
      entityTitle: j['EntityTitle'] ?? '',
      entityDescription: j['EntityDescription'] ?? '',
      eventName: j['EventName'] ?? '',
      initiatorId: j['InitiatorId'] ?? 0,
      initiatorDisplayName: j['InitiatorDisplayName'] ?? '',
      initiatorImageUrl: j['InitiatorImageUrl'] ?? '',
      message: j['Message'] ?? '',
      webUrl: j['WebUrl'] ?? '',
      description: j['Description'] ?? '',
      receivedName: j['ReceivedName'] ?? '',
      userId: j['UserId'] ?? 0,
      notificationEventId: j['NotificationEventId'] ?? 0,
      id: j['Id'] ?? 0,
    );
  }

  Map<String, dynamic> toJson() => {
        'IsRead': isRead,
        'PublishDateTime': publishDateTime,
        'ReadDateTime': readDateTime,
        'EntityId': entityId,
        'EntityName': entityName,
        'EntityTitle': entityTitle,
        'EntityDescription': entityDescription,
        'EventName': eventName,
        'InitiatorId': initiatorId,
        'InitiatorDisplayName': initiatorDisplayName,
        'InitiatorImageUrl': initiatorImageUrl,
        'Message': message,
        'WebUrl': webUrl,
        'Description': description,
        'ReceivedName': receivedName,
        'UserId': userId,
        'NotificationEventId': notificationEventId,
        'Id': id,
      };
}
