import 'package:aitl/data/model/dashboard/more/help/TermsPrivacyNoticeSetups.dart';

class PrivacyPolicyAPIModel {
  bool success;
  dynamic errorMessages;
  dynamic messages;
  ResponseData responseData;

  PrivacyPolicyAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory PrivacyPolicyAPIModel.fromJson(Map<String, dynamic> j) {
    return PrivacyPolicyAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class ResponseData {
  List<TermsPrivacyNoticeSetups> termsPrivacyNoticeSetupsList;

  ResponseData({this.termsPrivacyNoticeSetupsList});
  ResponseData.fromJson(Map<String, dynamic> json) {
    //catch document review
    if (json['TermsPrivacyNoticeSetups'] != null) {
      termsPrivacyNoticeSetupsList = [];
      json['TermsPrivacyNoticeSetups'].forEach((v) {
        try {
          termsPrivacyNoticeSetupsList
              .add(new TermsPrivacyNoticeSetups.fromJson(v));
        } catch (e) {
          print("Test task Jason getting not add because  = ${e.toString()}");
        }
      });
    } else {
      print(
          "Test task Jason getting null = ${json['TermsPrivacyNoticeSetups']}");
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.termsPrivacyNoticeSetupsList != null) {
      try {
        data['TermsPrivacyNoticeSetups'] =
            this.termsPrivacyNoticeSetupsList.map((v) => v.toJson()).toList();
      } catch (e) {}
    }
    return data;
  }
}
