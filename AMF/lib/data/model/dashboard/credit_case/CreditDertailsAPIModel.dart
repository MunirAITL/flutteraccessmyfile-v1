import 'CreditDashBoardReport.dart';

class CreditDetailsAPIModel {
  bool success;
  dynamic errorMessages;
  dynamic messages;
  CreditDashBoardReport responseData;

  CreditDetailsAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory CreditDetailsAPIModel.fromJson(Map<String, dynamic> j) {
    return CreditDetailsAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? CreditDashBoardReport.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}
