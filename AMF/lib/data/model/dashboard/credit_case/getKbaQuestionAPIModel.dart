import 'KbaQuestionResponse.dart';

class GetKbaQuestionAPIModel {
  bool success;
  dynamic errorMessages;
  dynamic messages;
  KbaQuestionResponseData responseData;

  GetKbaQuestionAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory GetKbaQuestionAPIModel.fromJson(Map<String, dynamic> j) {
    return GetKbaQuestionAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? KbaQuestionResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}
