import 'KbaQuestionResponse.dart';

class AnswerParams {
  List<dynamic> kbaAnswerItem;
  List<QuestionsAnswerList> questionsAnswerList;
  String validationIdentifier;
  int userCompanyId;
  int userId;

  AnswerParams(
      {this.kbaAnswerItem,
      this.questionsAnswerList,
      this.validationIdentifier,
      this.userCompanyId,
      this.userId});

  AnswerParams.fromJson(Map<String, dynamic> json) {
    if (json['KbaAnswerItem'] != null) {
      kbaAnswerItem = new List<Null>();
      json['KbaAnswerItem'].forEach((v) {
        kbaAnswerItem.add((v));
      });
    }
    if (json['QuestionsAnswerList'] != null) {
      questionsAnswerList = new List<QuestionsAnswerList>();
      json['QuestionsAnswerList'].forEach((v) {
        questionsAnswerList.add(new QuestionsAnswerList.fromJson(v));
      });
    }
    validationIdentifier = json['ValidationIdentifier'];
    userCompanyId = json['UserCompanyId'];
    userId = json['UserId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.kbaAnswerItem != null) {
      data['KbaAnswerItem'] =
          this.kbaAnswerItem.map((v) => v.toJson()).toList();
    }
    if (this.questionsAnswerList != null) {
      data['QuestionsAnswerList'] =
          this.questionsAnswerList.map((v) => v.toJson()).toList();
    }
    data['ValidationIdentifier'] = this.validationIdentifier;
    data['UserCompanyId'] = this.userCompanyId;
    data['UserId'] = this.userId;
    return data;
  }
}

class QuestionsAnswerList {
  AnswersItem answers;
  String id;
  String text;
  String selectedAnswer;

  QuestionsAnswerList({this.answers, this.id, this.text, this.selectedAnswer});

  QuestionsAnswerList.fromJson(Map<String, dynamic> json) {
    answers = json['Answers'] != null
        ? new AnswersItem.fromJson(json['Answers'])
        : null;
    id = json['Id'];
    text = json['Text'];
    selectedAnswer = json['SelectedAnswer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.answers != null) {
      data['Answers'] = this.answers.toJson();
    }
    data['Id'] = this.id;
    data['Text'] = this.text;
    data['SelectedAnswer'] = this.selectedAnswer;
    return data;
  }
}

class AnswersItem {
  List<KbaQuestionItem> kbaAnswerOption;

  AnswersItem({this.kbaAnswerOption});

  AnswersItem.fromJson(Map<String, dynamic> json) {
    if (json['KbaAnswerOption'] != null) {
      kbaAnswerOption = new List<KbaQuestionItem>();
      json['KbaAnswerOption'].forEach((v) {
        kbaAnswerOption.add(new KbaQuestionItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.kbaAnswerOption != null) {
      data['KbaAnswerOption'] =
          this.kbaAnswerOption.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

/*class KbaAnswerOptionAnswer {
  String id;
  String text;

  KbaAnswerOptionAnswer({this.id, this.text});

  KbaAnswerOptionAnswer.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    text = json['Text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['Text'] = this.text;
    return data;
  }
}*/
