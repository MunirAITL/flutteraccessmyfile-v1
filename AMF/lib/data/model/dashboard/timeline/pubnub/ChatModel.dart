import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/CommentAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/UserCommentPublicModelList.dart';
import 'package:aitl/data/model/dashboard/user/PublicUserModel.dart';

class ChatModel {
  TimelinePostModel timelinePostModel;
  UserCommentPublicModelList commentModel;
  User user;

  ChatModel({this.timelinePostModel, this.commentModel, this.user});

  ChatModel.fromJson(Map<String, dynamic> json) {
    timelinePostModel = json['data'] != null
        ? new TimelinePostModel.fromJson(json['data'])
        : null;
    commentModel = json['data'] != null
        ? new UserCommentPublicModelList.fromJson(json['data'])
        : null;
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.timelinePostModel != null) {
      data['data'] = this.timelinePostModel.toJson();
    }
    if (this.commentModel != null) {
      data['data'] = this.commentModel.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}
