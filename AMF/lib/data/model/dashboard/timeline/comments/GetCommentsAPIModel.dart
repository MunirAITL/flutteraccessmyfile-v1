import 'UserCommentPublicModelList.dart';

class GetCommentsAPIModel {
  bool success;
  List<UserCommentPublicModelList> comments;
  GetCommentsAPIModel({this.success, this.comments});
  GetCommentsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    if (json['Comments'] != null) {
      comments = [];
      json['Comments'].forEach((v) {
        try {
          comments.add(new UserCommentPublicModelList.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.comments != null) {
      data['Comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
