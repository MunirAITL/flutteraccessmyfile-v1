import 'TaskModel.dart';

class PostTask1APIModel {
  ErrorMessages errorMessages;
  ErrorMessages messages;
  bool success;
  ResponseData responseData;

  PostTask1APIModel(
      {this.errorMessages, this.messages, this.success, this.responseData});

  PostTask1APIModel.fromJson(Map<String, dynamic> json) {
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  TaskModel task;
  ResponseData({this.task});
  ResponseData.fromJson(Map<String, dynamic> json) {
    task = json['Task'] != null ? new TaskModel.fromJson(json['Task']) : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.task != null) {
      data['Task'] = this.task.toJson();
    }
    return data;
  }
}
