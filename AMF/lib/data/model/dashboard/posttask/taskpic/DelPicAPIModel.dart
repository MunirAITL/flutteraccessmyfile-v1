class DelPicAPIModel {
  bool success;
  int index; //  developer fixing for removing from list index

  dynamic responseData;
  DelPicAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'] ?? true;
    responseData = json['ResponseData'] ?? null;
  }
}
