import 'package:aitl/data/model/dashboard/mytasks/ratings/TaskUserRatingModel.dart';

class TaskUserRatingAPIModel {
  bool success;
  ResponseData responseData;

  TaskUserRatingAPIModel({this.success, this.responseData});

  TaskUserRatingAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  TaskUserRatingModel userRating;
  ResponseData({this.userRating});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userRating = json['UserRating'] != null
        ? new TaskUserRatingModel.fromJson(json['UserRating'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userRating != null) {
      data['UserRating'] = this.userRating.toJson();
    }
    return data;
  }
}
