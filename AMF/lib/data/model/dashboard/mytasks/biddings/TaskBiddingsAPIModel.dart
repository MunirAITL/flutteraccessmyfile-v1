import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';

class TaskBiddingsAPIModel {
  bool success;
  ResponseData responseData;

  TaskBiddingsAPIModel({this.success, this.responseData});

  TaskBiddingsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  List<TaskBiddingModel> taskBiddings;
  ResponseData({this.taskBiddings});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['TaskBiddings'] != null) {
      taskBiddings = [];
      json['TaskBiddings'].forEach((v) {
        try {
          taskBiddings.add(new TaskBiddingModel.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taskBiddings != null) {
      data['TaskBiddings'] = this.taskBiddings.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
