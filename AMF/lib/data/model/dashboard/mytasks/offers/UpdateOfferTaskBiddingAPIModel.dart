import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';

class UpdateOfferTaskBiddingAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  UpdateOfferTaskBiddingAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  UpdateOfferTaskBiddingAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  TaskBiddingModel taskBidding;
  ResponseData({this.taskBidding});
  ResponseData.fromJson(Map<String, dynamic> json) {
    taskBidding = json['TaskBidding'] != null
        ? new TaskBiddingModel.fromJson(json['TaskBidding'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taskBidding != null) {
      data['TaskBidding'] = this.taskBidding.toJson();
    }
    return data;
  }
}
