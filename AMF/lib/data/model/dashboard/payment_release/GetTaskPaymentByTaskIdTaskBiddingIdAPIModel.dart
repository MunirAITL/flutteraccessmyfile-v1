class GetTaskPaymentByTaskIdTaskBiddingIdAPIModel {
  bool success;
  dynamic responseData;
  Messages errorMessages;
  Messages messages;

  GetTaskPaymentByTaskIdTaskBiddingIdAPIModel.fromJson(
      Map<String, dynamic> json) {
    success = json['Success'] ?? true;
    responseData = json['ResponseData'] ?? null;
    errorMessages = json['ErrorMessages'] != null
        ? new Messages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
  }
}

class Messages {
  Messages();
  Messages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}
