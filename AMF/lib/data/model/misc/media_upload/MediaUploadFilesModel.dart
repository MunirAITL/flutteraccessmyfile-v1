class MediaUploadFilesModel {
  String url;
  String name;
  String thumbnailUrl;
  int mediaType;
  String mimeType;
  int id;
  String dateCreatedUtc;
  String dateCreatedLocal;
  String createdBy;
  int totalLikes;
  int totalComments;
  bool canComment;
  int likeStatus;
  int nextMediaId;
  int previousMediaId;
  bool fullyLoaded;

  MediaUploadFilesModel({
    this.url,
    this.name,
    this.thumbnailUrl,
    this.mediaType,
    this.mimeType,
    this.id,
    this.dateCreatedUtc,
    this.dateCreatedLocal,
    this.createdBy,
    this.totalLikes,
    this.totalComments,
    this.canComment,
    this.likeStatus,
    this.nextMediaId,
    this.previousMediaId,
    this.fullyLoaded,
  });

  factory MediaUploadFilesModel.fromJson(Map<String, dynamic> j) {
    print(j);
    return MediaUploadFilesModel(
      url: j['Url'] ?? '',
      name: j['Name'] ?? '',
      thumbnailUrl: j['ThumbnailUrl'] ?? '',
      mediaType: j['MediaType'] ?? 0,
      mimeType: j['MimeType'] ?? '',
      id: j['Id'] ?? 0,
      dateCreatedUtc: j['DateCreatedUtc'] ?? '',
      dateCreatedLocal: j['DateCreatedLocal'] ?? '',
      createdBy: j['CreatedBy'] ?? '',
      totalLikes: j['TotalLikes'] ?? 0,
      totalComments: j['TotalComments'] ?? 0,
      canComment: j['CanComment'] ?? false,
      likeStatus: j['LikeStatus'] ?? 0,
      nextMediaId: j['NextMediaId'] ?? 0,
      previousMediaId: j['PreviousMediaId'] ?? 0,
      fullyLoaded: j['FullyLoaded'] ?? false,
    );
  }

  Map<String, dynamic> toMap() => {
        'Url': url,
        'Name': name,
        'ThumbnailUrl': thumbnailUrl,
        'MediaType': mediaType,
        'MimeType': mimeType,
        'Id': id,
        'DateCreatedUtc': dateCreatedUtc,
        'DateCreatedLocal': dateCreatedLocal,
        'CreatedBy': createdBy,
        'TotalLikes': totalLikes,
        'TotalComments': totalComments,
        'CanComment': canComment,
        'LikeStatus': likeStatus,
        'NextMediaId': nextMediaId,
        'PreviousMediaId': previousMediaId,
        'FullyLoaded': fullyLoaded,
      };
}
