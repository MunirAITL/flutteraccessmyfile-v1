import 'dart:convert';
import 'dart:io';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/mixin.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBMgr with Mixin {
  DBMgr._();
  static final DBMgr shared = DBMgr._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    try {
      Directory documentsDirectory = await getApplicationDocumentsDirectory();
      String path = join(documentsDirectory.path, "shohokari.db");
      return await openDatabase(path, version: 1, onOpen: (db) {},
          onCreate: (Database db, int version) async {
        await db
            .execute("CREATE TABLE User (id INTEGER PRIMARY KEY, json TEXT)");
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  //  ********************  USER  start here***********
  //  USER: set/add
  Future<bool> setUserProfile({UserModel user}) async {
    try {
      String j = json.encode(user.toJson());
      //j = j.replaceAll('"', '\\"').replaceAll("'", "\\'");

      //final j =
      //'{"Address":"Rainham, UK","BankVerifiedStatus":907,"BriefBio":"Gemma\'s Brief Bio","CommunityId":"","CoverImageId":0,"CoverImageUrl":"https://herotasker.com:443/api//media/uploadpictures/ec0681a17a0140bc84d45fd4857c822e_0_0.jpg","DateCreatedLocal":"2021-06-18T15:18:43.833","DateofBirth":"10-Feb-2000","Email":"gemma@mortgage-magic.co.uk","FirstName":"Gemma","Headline":"Gemma\'s Headline","Id":115538,"IsEmailVerified":false,"IsFirstLogin":false,"IsMobileNumberVerified":false,"IsOnline":true,"IsProfileImageVerified":true,"LastLoginDate":"Online","LastName":"Bartlett","MobileNumber":"07965776666","Name":"Gemma B","NationalIDVerifiedStatus":907,"ProfileImageId":86665,"ProfileImageUrl":"https://herotasker.com:443/api//media/uploadpictures/89bacd45b7d947908939695d18b2af8b_128_128.jpg","Status":"","UnreadNotificationCount":42,"UserName":"gemma","UserProfileUrl":"Gemma-B-115538"}';

      final db = await database;
      await db.rawDelete("Delete FROM User");
      await db.rawInsert("INSERT INTO User (json) VALUES (?)", [j.toString()]);
      j = null;
      return true;
    } catch (e) {
      myLog(e.toString());
      return false;
    }
  }

  //  USER: get
  Future<UserModel> getUserProfile() async {
    try {
      final db = await database;
      List<Map> list = await db.rawQuery("SELECT json from User");
      //list.forEach((row) => print(row));  //for show all
      final map = list[0];
      return UserModel.fromJson(json.decode(map['json']));
    } catch (e) {
      myLog(e.toString());
      return null;
    }
  }

  //  ********************  USER  end here***********

  //  ********************  CRUD  ********************

  //  table: count row
  Future<int> getTotalRow(table) async {
    try {
      final db = await database;
      return Sqflite.firstIntValue(
          await db.rawQuery('SELECT COUNT(*) FROM ' + table));
    } catch (e) {
      myLog(e.toString());
      return 0;
    }
  }

  //  table: delete all rows
  delTable(table) async {
    try {
      final db = await database;
      await db.rawDelete("Delete FROM " + table);
    } catch (e) {
      myLog(e.toString());
    }
  }
}
