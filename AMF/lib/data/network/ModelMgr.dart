//  https://stackoverflow.com/questions/56271651/how-to-pass-a-generic-type-as-a-parameter-to-a-future-in-flutter
import 'package:aitl/data/model/auth/ChangePwdAPIModel.dart';
import 'package:aitl/data/model/auth/ForgotAPIModel.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/model/auth/loginWith/LoginWithModel.dart';
import 'package:aitl/data/model/auth/otp/sms1/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms1/SendOtpNotiAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/data/model/auth/profile/DeactivateProfileAPIModel.dart';
import 'package:aitl/data/model/auth/profile/RegProfileAPIModel.dart';
import 'package:aitl/data/model/dashboard/credit_case/CreditDertailsAPIModel.dart';
import 'package:aitl/data/model/dashboard/credit_case/CreditInfoPostAPIModel.dart';
import 'package:aitl/data/model/dashboard/credit_case/GetSimulatedScoreUseridAPIModel.dart';
import 'package:aitl/data/model/dashboard/credit_case/GetUserValidationOldUserApiModel.dart';
import 'package:aitl/data/model/dashboard/credit_case/PostKbaCrditQAAPIModel.dart';
import 'package:aitl/data/model/dashboard/credit_case/getKbaQuestionAPIModel.dart';
import 'package:aitl/data/model/dashboard/credit_case/getSummeryAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/help/PrivacyPolicyAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/help/WelcomeSmsAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/FcmTestNotiAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiSettingsAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiSettingsPostAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiTestAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/UserNotificationSettingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/history/TaskPaymentsHistoryAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/BillingAddrAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PaymentMethodAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PaymentMethodsAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PromoCodeAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/EntityPropertyAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/PublicProfileAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserBadgeAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortFolioAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/res/ResAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/res/ResolutionAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertsAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/GetTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskInfoSearchAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskSummaryDataAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingsAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/funds/PaymentConfirmationAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/offers/UpdateOfferTaskBiddingAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/ratings/TaskUserRatingAPIModel.dart';
import 'package:aitl/data/model/dashboard/payment_release/GetTaskPaymentByTaskIdTaskBiddingIdAPIModel.dart';
import 'package:aitl/data/model/dashboard/payment_release/PostSSLMobileAppValidationAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/DelTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/PostTask1APIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ChildAllCatAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ParentAllCatAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskform/PostTaskFormAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskform/SubmitTaskFormAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/DelPicAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/SavePicAPIModel.dart';
import 'package:aitl/data/model/dashboard/stripe/PaymentIntentAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/CommentAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/GetTimeLineByAppAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/GetCommentsAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/model/misc/device_info/FcmDeviceInfoAPIModel.dart';
import 'package:aitl/data/model/misc/device_info/UserDeviceAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/model/misc/rating/PendingReviewRatingByUserIdAPIModel.dart';

import '../model/auth/hreflogin/LoginAccExistsByEmailApiModel.dart';
import '../model/auth/hreflogin/PostEmailOtpAPIModel.dart';
import '../model/auth/hreflogin/SendUserEmailOtpAPIModel.dart';

enum APIType {
  login,
  loginWithG,
  loginWithFB,
  forgot,
  reg1,
  reg2,
  reg_fb_mobile,
  otp_post,
  otp_put,
  pending_review_rating_userid,
  task_rating_summary,
  user_rating_summary,
  user_rating,
  user_badge,
  about_skills,
  about_skills_post,
  edit_profile,
  public_profile,
  entity_profile_cover_image,
  entity_profile_image,
  media_profile_cover_image,
  media_profile_image,
  portfolio,
  portfolio_upload,
  taskinfo_search,
  media_upload_file,
  post_task1,
  post_task3,
  put_task1,
  put_task3,
  del_task,
  email_noti,
  get_noti,
  put_noti,
  user_noti_settings_get,
  user_noti_settings_put,
  parent_all_cat,
  child_all_cat,
  get_taskform,
  post_taskform,
  get_pic,
  save_pic,
  del_pic,
  get_timeline_by_app,
  post_timeline,
  post_comment,
  get_comments,
  task_biddings,
  task_bidding_put,
  task_bidding_post,
  task_bidding_del,
  res,
  resolution,
  put_payment_confirmation,
  post_review,
  payment_history_get,
  payment_methods_make_post_promo_card,
  payment_methods_make_get_promo_card,
  payment_methods_receive_post_billing_addr,
  payment_methods_receive_put_billing_addr,
  payment_methods_receive_get_billing_addr,
  payment_methods_receive_post_bank_acc,
  payment_methods_receive_put_bank_acc,
  payment_methods_receive_get_bank_acc,
  stripe_payment_intent_post,
  task_alert_get,
  task_alert_post,
  task_alert_put,
  task_alert_del,
  welcome_sms,
}

class APIState {
  APIType type;
  dynamic cls;
  dynamic data;
  APIState(type, cls, data) {
    this.type = type;
    this.cls = cls;
    this.data = data;
  }
}

class ModelMgr {
  /// If T is a List, K is the subtype of the list.
  ///
  /*List<T> fromJsonList<T>(List<dynamic> jsonList) {
    return jsonList
        ?.map<T>((dynamic json) => fromJson<T, void>(json))
        ?.toList();
  }*/

  Future<T> fromJson<T, K>(dynamic json) async {
    //return fromJsonList<K>(json) as T;

    if (identical(T, FcmDeviceInfoAPIModel)) {
      return FcmDeviceInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, UserDeviceAPIModel)) {
      return UserDeviceAPIModel.fromJson(json) as T;
    } else if (identical(T, MediaUploadFilesAPIModel)) {
      return MediaUploadFilesAPIModel.fromJson(json) as T;
    } else if (identical(T, GetPicAPIModel)) {
      return GetPicAPIModel.fromJson(json) as T;
    } else if (identical(T, SavePicAPIModel)) {
      return SavePicAPIModel.fromJson(json) as T;
    } else if (identical(T, DelPicAPIModel)) {
      return DelPicAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginAPIModel)) {
      return LoginAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPostAPIModel)) {
      return MobileUserOtpPostAPIModel.fromJson(json) as T;
    } else if (identical(T, SendOtpNotiAPIModel)) {
      return SendOtpNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiAPIModel)) {
      return NotiAPIModel.fromJson(json) as T;
    }

    //
    else if (identical(T, PrivacyPolicyAPIModel)) {
      return PrivacyPolicyAPIModel.fromJson(json) as T;
    }
    //
    else if (identical(T, NotiSettingsAPIModel)) {
      return NotiSettingsAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsPostAPIModel)) {
      return NotiSettingsPostAPIModel.fromJson(json) as T;
    } else if (identical(T, FcmTestNotiAPIModel)) {
      return FcmTestNotiAPIModel.fromJson(json) as T;
    }
    //

    else if (identical(T, MobileUserOtpPutAPIModel)) {
      return MobileUserOtpPutAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginRegOtpFBAPIModel)) {
      return LoginRegOtpFBAPIModel.fromJson(json) as T;
    } else if (identical(T, ForgotAPIModel)) {
      return ForgotAPIModel.fromJson(json) as T;
    } else if (identical(T, ChangePwdAPIModel)) {
      return ChangePwdAPIModel.fromJson(json) as T;
    } else if (identical(T, RegAPIModel)) {
      return RegAPIModel.fromJson(json) as T;
    } else if (identical(T, RegProfileAPIModel)) {
      return RegProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, DeactivateProfileAPIModel)) {
      return DeactivateProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, WelcomeSmsAPIModel)) {
      return WelcomeSmsAPIModel.fromJson(json) as T;
    } else if (identical(T, PendingReviewRatingByUserIdAPIModel)) {
      return PendingReviewRatingByUserIdAPIModel.fromJson(json) as T;
    } else if (identical(T, UserRatingSummaryAPIModel)) {
      return UserRatingSummaryAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskSummaryDataAPIModel)) {
      return TaskSummaryDataAPIModel.fromJson(json) as T;
    } else if (identical(T, UserRatingAPIModel)) {
      return UserRatingAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskUserRatingAPIModel)) {
      return TaskUserRatingAPIModel.fromJson(json) as T;
    } else if (identical(T, UserBadgeAPIModel)) {
      return UserBadgeAPIModel.fromJson(json) as T;
    } else if (identical(T, AboutAPIModel)) {
      return AboutAPIModel.fromJson(json) as T;
    } else if (identical(T, PublicProfileAPIModel)) {
      return PublicProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserPortFolioAPIModel)) {
      return UserPortFolioAPIModel.fromJson(json) as T;
    } else if (identical(T, EntityPropertyAPIModel)) {
      return EntityPropertyAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskInfoSearchAPIModel)) {
      return TaskInfoSearchAPIModel.fromJson(json) as T;
    } else if (identical(T, GetTaskAPIModel)) {
      return GetTaskAPIModel.fromJson(json) as T;
    } else if (identical(T, PostTask1APIModel)) {
      return PostTask1APIModel.fromJson(json) as T;
    } else if (identical(T, CommonAPIModel)) {
      return CommonAPIModel.fromJson(json) as T;
    } else if (identical(T, ParentAllCatAPIModel)) {
      return ParentAllCatAPIModel.fromJson(json) as T;
    } else if (identical(T, ChildAllCatAPIModel)) {
      return ChildAllCatAPIModel.fromJson(json) as T;
    } else if (identical(T, DelTaskAPIModel)) {
      return DelTaskAPIModel.fromJson(json) as T;
    } else if (identical(T, GetTimeLineByAppAPIModel)) {
      return GetTimeLineByAppAPIModel.fromJson(json) as T;
    } else if (identical(T, TimelinePostAPIModel)) {
      return TimelinePostAPIModel.fromJson(json) as T;
    } else if (identical(T, CommentAPIModel)) {
      return CommentAPIModel.fromJson(json) as T;
    } else if (identical(T, GetCommentsAPIModel)) {
      return GetCommentsAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskBiddingAPIModel)) {
      return TaskBiddingAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskBiddingsAPIModel)) {
      return TaskBiddingsAPIModel.fromJson(json) as T;
    } else if (identical(T, UpdateOfferTaskBiddingAPIModel)) {
      return UpdateOfferTaskBiddingAPIModel.fromJson(json) as T;
    } else if (identical(T, ResAPIModel)) {
      return ResAPIModel.fromJson(json) as T;
    } else if (identical(T, ResolutionAPIModel)) {
      return ResolutionAPIModel.fromJson(json) as T;
    } else if (identical(T, PaymentConfirmationAPIModel)) {
      return PaymentConfirmationAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskPaymentsHistoryAPIModel)) {
      return TaskPaymentsHistoryAPIModel.fromJson(json) as T;
    } else if (identical(T, PromoCodeAPIModel)) {
      return PromoCodeAPIModel.fromJson(json) as T;
    } else if (identical(T, BillingAddrAPIModel)) {
      return BillingAddrAPIModel.fromJson(json) as T;
    } else if (identical(T, PaymentMethodAPIModel)) {
      return PaymentMethodAPIModel.fromJson(json) as T;
    } else if (identical(T, PaymentMethodsAPIModel)) {
      return PaymentMethodsAPIModel.fromJson(json) as T;
    } else if (identical(T, PaymentIntentAPIModel)) {
      return PaymentIntentAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskAlertAPIModel)) {
      return TaskAlertAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskAlertsAPIModel)) {
      return TaskAlertsAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNotificationSettingAPIModel)) {
      return UserNotificationSettingAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiTestAPIModel)) {
      return NotiTestAPIModel.fromJson(json) as T;
    } else if (identical(T, PostTaskFormAPIModel)) {
      return PostTaskFormAPIModel.fromJson(json) as T;
    } else if (identical(T, SubmitTaskFormAPIModel)) {
      return SubmitTaskFormAPIModel.fromJson(json) as T;
    } else if (identical(T, GetTaskPaymentByTaskIdTaskBiddingIdAPIModel)) {
      return GetTaskPaymentByTaskIdTaskBiddingIdAPIModel.fromJson(json) as T;
    } else if (identical(T, PostSSLMobileAppValidationAPIModel)) {
      return PostSSLMobileAppValidationAPIModel.fromJson(json) as T;
    }

    //  Credit Report
    else if (identical(T, CreditDetailsAPIModel)) {
      return CreditDetailsAPIModel.fromJson(json) as T;
    } else if (identical(T, CreditInfoPostAPIModel)) {
      return CreditInfoPostAPIModel.fromJson(json) as T;
    } else if (identical(T, GetKbaQuestionAPIModel)) {
      return GetKbaQuestionAPIModel.fromJson(json) as T;
    } else if (identical(T, GetSimulatedScoreUseridAPIModel)) {
      return GetSimulatedScoreUseridAPIModel.fromJson(json) as T;
    } else if (identical(T, GetSummeryAPIModel)) {
      return GetSummeryAPIModel.fromJson(json) as T;
    } else if (identical(T, GetUserValidationOldUserApiModel)) {
      return GetUserValidationOldUserApiModel.fromJson(json) as T;
    } else if (identical(T, PostKbaCrditQAAPIModel)) {
      return PostKbaCrditQAAPIModel.fromJson(json) as T;
    }
    //

//  ******************************************* HREF LOGIN BY EMAIL
    else if (identical(T, LoginAccExistsByEmailApiModel)) {
      return LoginAccExistsByEmailApiModel.fromJson(json) as T;
    } else if (identical(T, PostEmailOtpAPIModel)) {
      return PostEmailOtpAPIModel.fromJson(json) as T;
    } else if (identical(T, SendUserEmailOtpAPIModel)) {
      return SendUserEmailOtpAPIModel.fromJson(json) as T;
    }

    /*if (json is Iterable) {
      return _fromJsonList<K>(json) as T;
    } else if (identical(T, LoginModel)) {
      return LoginModel.fromJson(json) as T;
    } else if (T == bool ||
        T == String ||
        T == int ||
        T == double ||
        T == Map) {
      // primitives
      return json;
    } else {
      throw Exception("Unknown class");
    }*/
  }

  /*List<K> _fromJsonList<K>(List<dynamic> jsonList) {
    return jsonList
        ?.map<K>((dynamic json) => fromJson<K, void>(json))
        ?.toList();
  }*/
}
